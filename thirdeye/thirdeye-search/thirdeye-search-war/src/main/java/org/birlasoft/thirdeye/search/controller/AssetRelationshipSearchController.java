package org.birlasoft.thirdeye.search.controller;

import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.AssetRelationshipSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller class for asset relationship elastic search.
 * @author samar.gupta
 *
 */
@RestController
public class AssetRelationshipSearchController extends BaseSearchController {
	
	@Autowired
	private AssetRelationshipSearchService assetRelationshipSearchService;
	
	/**
	 * Search asset relationship.
	 * @param searchConfig
	 * @return {@code IndexAssetParentChildBean}
	 */
	@RequestMapping( value = "/assetRelationship", method = RequestMethod.POST )
	public List<IndexAssetBean> searchAssetRelationship(@RequestBody SearchConfig searchConfig){		
		return assetRelationshipSearchService.getAssetsRelationship(searchConfig);
	}

}
