package org.birlasoft.thirdeye.search;

import org.birlasoft.thirdeye.search.bootstrap.BootstrapConfiguration;
import org.birlasoft.thirdeye.search.config.TenantConfiguration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Class for application initialization of root, servlet configuration and mappings and filters.
 */
public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer  {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class [] {BootstrapConfiguration.class, TenantConfiguration.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[0];
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{"/"};
	}
	
}
