package org.birlasoft.thirdeye.search.service;

import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.beans.SearchCostStructureBean;
import org.birlasoft.thirdeye.search.api.beans.SearchQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.TcoAssetBean;

public interface TcoReportSearchService {
	
	public List<SearchCostStructureBean> getCostStructureAggregateValue(SearchConfig searchConfig);

	public List<SearchQuestionBean> getCostElementAggregateValue(SearchConfig searchConfig);

	public List<IndexAssetBean> getListOfAssetbean(SearchConfig searchConfig);

	public List<TcoAssetBean> getTotalCostOfAsset(SearchConfig searchConfig);

}
