package org.birlasoft.thirdeye.search.api.beans;

import java.util.List;


public class IndexParameterBean {
	
	private Integer id;
	private String displayName;
	private String uniqueName;
	private String type;
	
	private IndexParameterValueTypeBean currentValue;
    private List<IndexParameterValueTypeBean> values;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public IndexParameterValueTypeBean getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(IndexParameterValueTypeBean currentValue) {
		this.currentValue = currentValue;
	}
	public List<IndexParameterValueTypeBean> getValues() {
		return values;
	}
	public void setValues(List<IndexParameterValueTypeBean> values) {
		this.values = values;
	}
	public String getUniqueName() {
		return uniqueName;
	}
	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	} 
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndexParameterBean other = (IndexParameterBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}
