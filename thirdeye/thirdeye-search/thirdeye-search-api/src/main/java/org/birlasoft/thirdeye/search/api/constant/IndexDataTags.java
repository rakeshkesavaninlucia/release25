package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum IndexDataTags {

	PROPERTIES("properties"),
	NESTED("nested"),
	FORMAT("format"),
	TYPE("type"),
	NOT_ANALYZED("not_analyzed"),
	ANALYZED("analyzed"),
	INDEX("index"),
	
	STRING("string"),
	LONG("long"),
	DOUBLE("double"),
	DATE("date"),
	DATE_FORMATE("yyyy-MM-dd"),
	DATE_FORMATE_SS("yyyy-MM-dd HH:mm:ss");
	
	
    String tagKey;
	
    IndexDataTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}	
	
}
