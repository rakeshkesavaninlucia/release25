package org.birlasoft.thirdeye.search.api.beans;

/**
 * @author dhruv.sood
 *
 */
public class IndexBCMAssetValueBean {
	
	private Integer assetId;	
	private String shortName;	
	private String evaluatedPercentage;

		
	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetid(Integer assetId) {
		this.assetId = assetId;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getEvaluatedPercentage() {
		return evaluatedPercentage;
	}

	public void setEvaluatedPercentage(String evaluatedPercentage) {
		this.evaluatedPercentage = evaluatedPercentage;
	}
	
}
