package org.birlasoft.thirdeye.search.api.beans;

/**
 * @author dhruv.sood
 *
 */
public class FilterBean{
	
	private String name;	
	private boolean checked;
	private int count;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}		
	
}