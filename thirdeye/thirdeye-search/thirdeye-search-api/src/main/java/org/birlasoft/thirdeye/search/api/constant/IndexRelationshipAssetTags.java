package org.birlasoft.thirdeye.search.api.constant;

/**
 * Enum for asset relationship index tag
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * @author samar.gupta
 *
 */
public enum IndexRelationshipAssetTags {

	ASSETID("assetId"),
	ASSETNAME("assetName"),
	ASSETSTYLE("assetStyle"),
	RELATIONSHIPASSETDATA("relationshipAssetData");
	
	String tagKey;
	
	IndexRelationshipAssetTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}
	
}
