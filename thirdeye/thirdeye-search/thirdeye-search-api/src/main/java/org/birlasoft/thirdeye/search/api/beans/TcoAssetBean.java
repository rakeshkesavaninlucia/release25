package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;

public class TcoAssetBean {
	
	private String assetName;
	private Integer assetId;
	private BigDecimal totalCost;
	
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public BigDecimal getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
}
