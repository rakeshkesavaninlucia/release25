package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author dhruv.sood
 *
 */
public enum IndexBCMColorTags {

	PARAMETERID("parameterId"),	
	DISPLAYNAME("displayName"),
	UNIQUENAME("uniqueName"),
	TYPE("type"),	
	COLOLR("color");
	

	String tagKey;

	IndexBCMColorTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}	
}
