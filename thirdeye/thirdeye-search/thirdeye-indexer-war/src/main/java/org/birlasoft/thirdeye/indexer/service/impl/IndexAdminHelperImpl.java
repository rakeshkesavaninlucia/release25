package org.birlasoft.thirdeye.indexer.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.indexer.service.AssetIndexService;
import org.birlasoft.thirdeye.indexer.service.BCMIndexService;
import org.birlasoft.thirdeye.indexer.service.IndexAdminHelper;
import org.birlasoft.thirdeye.indexer.service.IndexAdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class IndexAdminHelperImpl implements IndexAdminHelper{

	@Autowired
	private IndexAdministratorService adminService;

	@Autowired
	private AssetIndexService assetIndexService;
	
	@Autowired
	private BCMIndexService bcmIndexService;

	@Override
	public void buildIndexForTenantWorkspace(String tenantURL, List<Integer> workspaceIds) {
		tenantURL = tenantURL.toLowerCase();
	
			if( !adminService.tenantExists(tenantURL) )
				adminService.createTenant(tenantURL) ;
			
			List<Workspace> listOfWorkspace = new ArrayList<>();
			for (Integer oneID : workspaceIds){
				Workspace w = new Workspace();
				w.setId(oneID);
				listOfWorkspace.add(w);
			}
			//Start Asset Indexing
			assetIndexService.buildAssetIndex(listOfWorkspace, tenantURL);
							
			//Start BCM Indexing
			bcmIndexService.buildBCMIndex(listOfWorkspace, tenantURL);
		
	}

	@Override
	public void buildIncrementAssetIndex(List<Integer> assetIdlst, String tenantId) {
		
		assetIndexService.buildIncrementAssetIndex(assetIdlst, tenantId);
	}

	@Override
	public void buildAssetParameterIndex(Integer assetId,  String tenantId) {
		assetIndexService.updateAssetParameter(assetId, tenantId);	
	}

	@Override
	public void buildIncAssetAidDataIndex(Integer assetTemplateId, String tenantId) {
		
		assetIndexService.buildIncAssetAidDataIndex( assetTemplateId, tenantId) ;
	}

	@Override
	public void updateAssetTemplate(Integer assetTemplateId, String tenantId) {
		
		assetIndexService.updateAssetTemplate( assetTemplateId,  tenantId);
		
	}

	@Override
	public void deleteAsset(Integer assetId, String tenantId) {
		
		assetIndexService.deleteAsset(assetId, tenantId) ;
		
	}

	@Override
	public void addBcm(Integer bcmId, String tenantId) {
		
		bcmIndexService.addBcm(bcmId, tenantId);
	}
	
	
	@Override
	public void updateAssetCostStucture(List<Integer> assetIdlst, String tenantId) {
		
		assetIndexService.updateAssetCostStucture(assetIdlst, tenantId);
		
	}

	@Override
	public void updateAssetQuestionnaire(List<Integer> assetIdlst, String tenantId) {
		assetIndexService.updateAssetQuestionnaire(assetIdlst, tenantId);
		
	}

	@Override
	public void buildIncrementAssetRelationIndex(List<Integer> assetIdlst, String tenantId) {
		
		assetIndexService.buildIncrementAssetRelationIndex(assetIdlst, tenantId);
	}
	
}