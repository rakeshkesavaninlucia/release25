package org.birlasoft.thirdeye.indexer.service;

import org.elasticsearch.common.xcontent.XContentBuilder;

public interface IndexAdministratorService {

	public boolean createTenant(String tenant);
	
	/**
	 * Deletes the index for the particular tenant
	 * 
	 * @param tenant
	 */
	public boolean deleteTenant(String tenant);
	
	/**
	 * Returns whether the index for the tenant has been created or not
	 * 
	 * @param tenant
	 * @return
	 */
	public boolean tenantExists(String tenant);
	
	/**
	 * This method should be able to create 
	 * @param tenant
	 * @param mappingAndTypeObject
	 */
	public boolean createIndexAndMapping(String tenant, String type, XContentBuilder mappingAndTypeObject);
	
	
}
