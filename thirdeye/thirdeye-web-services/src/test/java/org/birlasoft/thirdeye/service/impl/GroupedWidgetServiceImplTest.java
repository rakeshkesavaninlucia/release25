package org.birlasoft.thirdeye.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.birlasoft.thirdeye.beans.widgets.GroupedDiscreteBarWrapper;
import org.birlasoft.thirdeye.beans.widgets.GroupedWidgetJSONConfig;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.AssetQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.AssetQuestionWrapper;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.GroupedWidgetService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.QuestionSearchService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GroupedWidgetServiceImplTest {
	
	@InjectMocks
	private GroupedWidgetService groupedWidgetService = new GroupedWidgetServiceImpl();
	@Mock
	private ParameterSearchService parameterSearchService;
	@Mock
	private QuestionSearchService questionSearchService;
	@Mock
	private DashboardService dashboardService;
	
	private AssetQuestionWrapper assetQuestionWrapper;
	private AssetParameterWrapper assetParameterWrapper;
	private Widget w;
	
	@Before
	public void setup() {
		assetParameterWrapper = new AssetParameterWrapper();
		assetParameterWrapper.setParameter("FQ");
		assetParameterWrapper.setParameterId(88);
		List<AssetParameterBean> parameterBeans = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			AssetParameterBean assetParameterBean = new AssetParameterBean();
			assetParameterBean.setAssetId(i);
			assetParameterBean.setDescription(i % 2 == 0 ? "Good" : "Bad");
			parameterBeans.add(assetParameterBean);
		}
		assetParameterWrapper.setValues(parameterBeans);
		
		assetQuestionWrapper = new AssetQuestionWrapper();
		assetQuestionWrapper.setQuestion("What is the technology stack");
		assetQuestionWrapper.setQuestionId(211);
		List<AssetQuestionBean> questionBeans = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			AssetQuestionBean assetQuestionBean = new AssetQuestionBean();
			assetQuestionBean.setAssetId(i);
			assetQuestionBean.setResponse(i % 3 == 0 ? "Java" : (i % 3 == 1 ? "Python" : ".Net"));
			questionBeans.add(assetQuestionBean);
		}
		assetQuestionWrapper.setResponse(questionBeans);
		
		w = new Widget();
		w.setId(101);
		w.setWidgetType("GROUPEDGRAPH");
		w.setWidgetConfig("{\"id\":101,\"width\":0,\"height\":0,\"title\":\"test\",\"questionnaireId\":21,\"fieldOne\":\"question\",\"fieldTwo\":\"parameter\",\"fieldOneId\":211,\"fieldTwoId\":88}");
		
	}

	private GroupedWidgetJSONConfig setWidgetConfig() {
		GroupedWidgetJSONConfig widgetJSONConfig = new GroupedWidgetJSONConfig();
		widgetJSONConfig.setQuestionnaireId(21);
		widgetJSONConfig.setFieldOne("question");
		widgetJSONConfig.setFieldOneId(211);
		widgetJSONConfig.setFieldTwo("parameter");
		widgetJSONConfig.setFieldTwoId(88);
		return widgetJSONConfig;
	}
	
	private void mockServiceClassMethods() {
		when(parameterSearchService.getParameterValueFromElasticsearch(88, 21, new HashSet<>(), null)).thenReturn(assetParameterWrapper);
		when(questionSearchService.getQuestionResponseFromElasticsearch(211, 21, new HashSet<>(), null)).thenReturn(assetQuestionWrapper);
	}
	
	@Test
	public void testGetDataForGroupedGraph() {
		GroupedWidgetJSONConfig widgetJSONConfig = setWidgetConfig();
		mockServiceClassMethods();
		
		List<GroupedDiscreteBarWrapper> discreteBarWrappers = groupedWidgetService.getDataForGroupedGraph(widgetJSONConfig);
		Assert.assertEquals(2, discreteBarWrappers.size());
	}
	
	@Test
	public void testGetDataForGroupedGraphForSizeOfNestedMap() {
		GroupedWidgetJSONConfig widgetJSONConfig = setWidgetConfig();
		mockServiceClassMethods();
		
		List<GroupedDiscreteBarWrapper> discreteBarWrappers = groupedWidgetService.getDataForGroupedGraph(widgetJSONConfig);
		Assert.assertEquals(new Integer(2), discreteBarWrappers.get(0).getMap().get("Java"));
		Assert.assertEquals(new Integer(2), discreteBarWrappers.get(1).getMap().get(".Net"));
	}
	
	@Test
	public void testGetDataForGroupedGraphWithFieldOneAsParameterAndTwoAsQuestion() {
		GroupedWidgetJSONConfig widgetJSONConfig = new GroupedWidgetJSONConfig();
		widgetJSONConfig.setQuestionnaireId(21);
		widgetJSONConfig.setFieldTwo("question");
		widgetJSONConfig.setFieldTwoId(211);
		widgetJSONConfig.setFieldOne("parameter");
		widgetJSONConfig.setFieldOneId(88);
		mockServiceClassMethods();
		
		List<GroupedDiscreteBarWrapper> discreteBarWrappers = groupedWidgetService.getDataForGroupedGraph(widgetJSONConfig);
		Assert.assertEquals(new Integer(1), discreteBarWrappers.get(0).getMap().get("Good"));
		Assert.assertEquals(3, discreteBarWrappers.size());
	}
	
	@Test
	public void testGetDataForGroupedGraphForEmptyFieldOne() {
		GroupedWidgetJSONConfig widgetJSONConfig = new GroupedWidgetJSONConfig();
		widgetJSONConfig.setQuestionnaireId(21);
		widgetJSONConfig.setFieldTwo("question");
		widgetJSONConfig.setFieldTwoId(211);
		mockServiceClassMethods();
		
		List<GroupedDiscreteBarWrapper> discreteBarWrappers = groupedWidgetService.getDataForGroupedGraph(widgetJSONConfig);
		Assert.assertTrue("Empty list is returned", discreteBarWrappers.isEmpty());
	}
	
	@Test
	public void testGetDataForGroupedGraphForBothFieldsEmpty() {
		GroupedWidgetJSONConfig widgetJSONConfig = new GroupedWidgetJSONConfig();
		widgetJSONConfig.setQuestionnaireId(21);
		mockServiceClassMethods();
		
		List<GroupedDiscreteBarWrapper> discreteBarWrappers = groupedWidgetService.getDataForGroupedGraph(widgetJSONConfig);
		Assert.assertTrue("Empty list is returned", discreteBarWrappers.isEmpty());
	}
	
	@Test
	public void testSaveGroupWidget() {
		GroupedWidgetJSONConfig widgetJSONConfig = setWidgetConfig();
		widgetJSONConfig.setId(101);

		when(dashboardService.findOneWidget(101, false)).thenReturn(w);
		
		groupedWidgetService.saveGroupWidget(widgetJSONConfig);
		verify(dashboardService).saveWidget(w);
	}
	
	@Test
	public void testSaveGroupWidgetWithNullId() {
		GroupedWidgetJSONConfig widgetJSONConfig = setWidgetConfig();

		when(dashboardService.findOneWidget(101, false)).thenReturn(w);
		
		groupedWidgetService.saveGroupWidget(widgetJSONConfig);
		verify(dashboardService, times(0)).saveWidget(w);
		verifyZeroInteractions(dashboardService);
	}


}
