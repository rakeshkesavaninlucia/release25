/*package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.repositories.BCMLevelRepository;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BCMLevelServiceImplTest {
	
	private List<BcmLevel> listOfBcmLevel;
	
	@InjectMocks
	private BCMLevelService bcmLevelService = new BCMLevelServiceImpl();
	@Mock
	private BCMLevelRepository bcmLevelRepository;
	
	@Before
	public void setup() {
		setupBcmLevelBean();
	}
	
	private void setupBcmLevelBean() {
		listOfBcmLevel = new ArrayList<>();
		
		Bcm bcm = new Bcm();
		bcm.setId(20);
		
		BcmLevel bcmLevel = new BcmLevel();
		bcmLevel.setBcmLevelName("L One");
		bcmLevel.setId(328);
		bcmLevel.setLevelNumber(1);
		bcmLevel.setSequenceNumber(1);
		bcmLevel.setCategory("");
		bcmLevel.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel);
		
		BcmLevel bcmLevel1 = new BcmLevel();
		bcmLevel1.setBcmLevelName("L Two 1");
		bcmLevel1.setId(329);
		bcmLevel1.setLevelNumber(2);
		bcmLevel1.setSequenceNumber(1);
		bcmLevel1.setCategory("C1");
		bcmLevel1.setBcmLevel(bcmLevel);
		bcmLevel1.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel1);
		
		BcmLevel bcmLevel2 = new BcmLevel();
		bcmLevel2.setBcmLevelName("L Three 1");
		bcmLevel2.setId(330);
		bcmLevel2.setLevelNumber(3);
		bcmLevel2.setSequenceNumber(1);
		bcmLevel2.setCategory("");
		bcmLevel2.setBcmLevel(bcmLevel1);
		bcmLevel2.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel2);
		
		BcmLevel bcmLevel3 = new BcmLevel();
		bcmLevel3.setBcmLevelName("L Four 1");
		bcmLevel3.setId(331);
		bcmLevel3.setLevelNumber(4);
		bcmLevel3.setSequenceNumber(1);
		bcmLevel3.setCategory("");
		bcmLevel3.setBcmLevel(bcmLevel2);
		bcmLevel3.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel3);
		
		BcmLevel bcmLevel4 = new BcmLevel();
		bcmLevel4.setBcmLevelName("L Four 2");
		bcmLevel4.setId(332);
		bcmLevel4.setLevelNumber(4);
		bcmLevel4.setSequenceNumber(2);
		bcmLevel4.setCategory("");
		bcmLevel4.setBcmLevel(bcmLevel2);
		bcmLevel4.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel4);
		
		BcmLevel bcmLevel5 = new BcmLevel();
		bcmLevel5.setBcmLevelName("L Four 3");
		bcmLevel5.setId(333);
		bcmLevel5.setLevelNumber(4);
		bcmLevel5.setSequenceNumber(3);
		bcmLevel5.setCategory("");
		bcmLevel5.setBcmLevel(bcmLevel2);
		bcmLevel5.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel5);
		
		BcmLevel bcmLevel6 = new BcmLevel();
		bcmLevel6.setBcmLevelName("L Three 2");
		bcmLevel6.setId(334);
		bcmLevel6.setLevelNumber(3);
		bcmLevel6.setSequenceNumber(2);
		bcmLevel6.setCategory("");
		bcmLevel6.setBcmLevel(bcmLevel1);
		bcmLevel6.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel6);
		
		BcmLevel bcmLevel7 = new BcmLevel();
		bcmLevel7.setBcmLevelName("L Four 4");
		bcmLevel7.setId(335);
		bcmLevel7.setLevelNumber(4);
		bcmLevel7.setSequenceNumber(1);
		bcmLevel7.setCategory("");
		bcmLevel7.setBcmLevel(bcmLevel6);
		bcmLevel7.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel7);
		
		BcmLevel bcmLevel8 = new BcmLevel();
		bcmLevel8.setBcmLevelName("L Four 5");
		bcmLevel8.setId(336);
		bcmLevel8.setLevelNumber(4);
		bcmLevel8.setSequenceNumber(2);
		bcmLevel8.setCategory("");
		bcmLevel8.setBcmLevel(bcmLevel6);
		bcmLevel8.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel8);
		
		BcmLevel bcmLevel9 = new BcmLevel();
		bcmLevel9.setBcmLevelName("L Four 6");
		bcmLevel9.setId(337);
		bcmLevel9.setLevelNumber(4);
		bcmLevel9.setSequenceNumber(3);
		bcmLevel9.setCategory("");
		bcmLevel9.setBcmLevel(bcmLevel6);
		bcmLevel9.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel9);
		
		BcmLevel bcmLevel10 = new BcmLevel();
		bcmLevel10.setBcmLevelName("L Four 7");
		bcmLevel10.setId(338);
		bcmLevel10.setLevelNumber(4);
		bcmLevel10.setSequenceNumber(4);
		bcmLevel10.setCategory("");
		bcmLevel10.setBcmLevel(bcmLevel6);
		bcmLevel10.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel10);
		
		BcmLevel bcmLevel11 = new BcmLevel();
		bcmLevel11.setBcmLevelName("L Four 8");
		bcmLevel11.setId(339);
		bcmLevel11.setLevelNumber(4);
		bcmLevel11.setSequenceNumber(5);
		bcmLevel11.setCategory("");
		bcmLevel11.setBcmLevel(bcmLevel6);
		bcmLevel11.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel11);
		
		BcmLevel bcmLevel12 = new BcmLevel();
		bcmLevel12.setBcmLevelName("L Three 3");
		bcmLevel12.setId(340);
		bcmLevel12.setLevelNumber(3);
		bcmLevel12.setSequenceNumber(3);
		bcmLevel12.setCategory("");
		bcmLevel12.setBcmLevel(bcmLevel1);
		bcmLevel12.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel12);
		
		BcmLevel bcmLevel13 = new BcmLevel();
		bcmLevel13.setBcmLevelName("L Four 9");
		bcmLevel13.setId(341);
		bcmLevel13.setLevelNumber(4);
		bcmLevel13.setSequenceNumber(1);
		bcmLevel13.setCategory("");
		bcmLevel13.setBcmLevel(bcmLevel12);
		bcmLevel13.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel13);
		
		BcmLevel bcmLevel14 = new BcmLevel();
		bcmLevel14.setBcmLevelName("L Four 10");
		bcmLevel14.setId(342);
		bcmLevel14.setLevelNumber(4);
		bcmLevel14.setSequenceNumber(2);
		bcmLevel14.setCategory("");
		bcmLevel14.setBcmLevel(bcmLevel12);
		bcmLevel14.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel14);
		
		BcmLevel bcmLevel15 = new BcmLevel();
		bcmLevel15.setBcmLevelName("L Three 4");
		bcmLevel15.setId(343);
		bcmLevel15.setLevelNumber(3);
		bcmLevel15.setSequenceNumber(4);
		bcmLevel15.setCategory("");
		bcmLevel15.setBcmLevel(bcmLevel1);
		bcmLevel15.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel15);
		
		BcmLevel bcmLevel16 = new BcmLevel();
		bcmLevel16.setBcmLevelName("L Four 11");
		bcmLevel16.setId(344);
		bcmLevel16.setLevelNumber(4);
		bcmLevel16.setSequenceNumber(1);
		bcmLevel16.setCategory("");
		bcmLevel16.setBcmLevel(bcmLevel15);
		bcmLevel16.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel16);
		
		BcmLevel bcmLevel17 = new BcmLevel();
		bcmLevel17.setBcmLevelName("L Four 12");
		bcmLevel17.setId(345);
		bcmLevel17.setLevelNumber(4);
		bcmLevel17.setSequenceNumber(2);
		bcmLevel17.setCategory("");
		bcmLevel17.setBcmLevel(bcmLevel15);
		bcmLevel17.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel17);
		
		BcmLevel bcmLevel18 = new BcmLevel();
		bcmLevel18.setBcmLevelName("L Three 13");
		bcmLevel18.setId(346);
		bcmLevel18.setLevelNumber(4);
		bcmLevel18.setSequenceNumber(3);
		bcmLevel18.setCategory("");
		bcmLevel18.setBcmLevel(bcmLevel15);
		bcmLevel18.setBcm(bcm);
		listOfBcmLevel.add(bcmLevel18);
	}
	
	private void mockServiceClassMethods() {
		Mockito.when(bcmLevelRepository.findByBcmLevel(listOfBcmLevel.get(6))).thenReturn(childrenOfLThree2());
		Mockito.when(bcmLevelRepository.findByBcmLevel(listOfBcmLevel.get(2))).thenReturn(childrenOfLThree1());
		Mockito.when(bcmLevelRepository.findByBcmLevel(listOfBcmLevel.get(1))).thenReturn(childrenOfLTwo1());
		Mockito.when(bcmLevelRepository.findByBcmLevel(listOfBcmLevel.get(0))).thenReturn(childrenOfLOne());
	}

	private List<BcmLevel> childrenOfLOne() {
		List<BcmLevel> listToReturn = new ArrayList<>();
		listToReturn.add(listOfBcmLevel.get(1));
		return listToReturn;
	}

	private List<BcmLevel> childrenOfLTwo1() {
		List<BcmLevel> listToReturn = new ArrayList<>();
		listToReturn.add(listOfBcmLevel.get(2));
		listToReturn.add(listOfBcmLevel.get(6));
		listToReturn.add(listOfBcmLevel.get(12));
		listToReturn.add(listOfBcmLevel.get(15));
		return listToReturn;
	}

	private List<BcmLevel> childrenOfLThree1() {
		List<BcmLevel> listToReturn = new ArrayList<>();
		listToReturn.add(listOfBcmLevel.get(3));
		listToReturn.add(listOfBcmLevel.get(4));
		listToReturn.add(listOfBcmLevel.get(5));
		return listToReturn;
	}

	private List<BcmLevel> childrenOfLThree2() {
		List<BcmLevel> listToReturn = new ArrayList<>();
		listToReturn.add(listOfBcmLevel.get(7));
		listToReturn.add(listOfBcmLevel.get(8));
		listToReturn.add(listOfBcmLevel.get(9));
		listToReturn.add(listOfBcmLevel.get(10));
		listToReturn.add(listOfBcmLevel.get(11));
		return listToReturn;
	}

	@Test
	public void testUpdateBcmSequence(){
		mockServiceClassMethods();
		BcmLevel bcmLevel = listOfBcmLevel.get(11);
		BcmLevel parentBcmLevel = listOfBcmLevel.get(2);
		bcmLevelService.updateBcmSequence(bcmLevel, parentBcmLevel, 3);
		
		Assert.assertEquals(parentBcmLevel.getId(),bcmLevel.getBcmLevel().getId());
	}
	
	@Test
	public void testUpdateBcmSequenceForNewPosition(){
		mockServiceClassMethods();
		BcmLevel bcmLevel = listOfBcmLevel.get(11);
		BcmLevel parentBcmLevel = listOfBcmLevel.get(2);
		bcmLevelService.updateBcmSequence(bcmLevel, parentBcmLevel, 3);
		
		Assert.assertEquals(3,bcmLevel.getSequenceNumber());
	}
	
	@Test
	public void testUpdateBcmSequenceForSequenceUpdateOfAllChildren(){
		mockServiceClassMethods();
		BcmLevel bcmLevel = listOfBcmLevel.get(11);
		BcmLevel parentBcmLevel = listOfBcmLevel.get(2);
		bcmLevelService.updateBcmSequence(bcmLevel, parentBcmLevel, 3);
		
		Assert.assertEquals(4,listOfBcmLevel.get(5).getSequenceNumber());
	}
	
	@Test
	public void testUpdateBcmSequenceForLevelChange(){
		mockServiceClassMethods();
		BcmLevel bcmLevel = listOfBcmLevel.get(11);
		BcmLevel parentBcmLevel = listOfBcmLevel.get(1);
		bcmLevelService.updateBcmSequence(bcmLevel, parentBcmLevel, 2);
		
		Assert.assertEquals(3, bcmLevel.getLevelNumber());
	}
	
	@Test
	public void testUpdateBcmSequenceWithinSameParent(){
		mockServiceClassMethods();
		BcmLevel bcmLevel = listOfBcmLevel.get(11);
		BcmLevel parentBcmLevel = listOfBcmLevel.get(6);
		bcmLevelService.updateBcmSequence(bcmLevel, parentBcmLevel, 3);
		
		Assert.assertEquals(5, listOfBcmLevel.get(10).getSequenceNumber());
	}
	
	@Test
	public void testUpdateBcmSequenceForNodeWithChildrenAndLevelChange(){
		mockServiceClassMethods();
		BcmLevel bcmLevel = listOfBcmLevel.get(6);
		BcmLevel parentBcmLevel = listOfBcmLevel.get(0);
		bcmLevelService.updateBcmSequence(bcmLevel, parentBcmLevel, 2);
		
		Assert.assertEquals(3, listOfBcmLevel.get(9).getLevelNumber());
	}
}
*/