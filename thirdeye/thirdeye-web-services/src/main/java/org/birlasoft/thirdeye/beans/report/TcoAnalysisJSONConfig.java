package org.birlasoft.thirdeye.beans.report;

import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.util.Utility;

/**
 * @author sunil1.gupta
 *
 */
public class TcoAnalysisJSONConfig {
	
	
	private Integer assetTypeId;
	private String  chartOfAccountYear;
	private String graphType;
	private Integer parameterId;
	private Integer questionnaireId;


	public TcoAnalysisJSONConfig() {
		
	}
	public TcoAnalysisJSONConfig(Report r) {
		TcoAnalysisJSONConfig tcoJSONConfig = Utility.convertJSONStringToObject(r.getReportConfig(), TcoAnalysisJSONConfig.class);
		if (tcoJSONConfig != null){
			this.assetTypeId = tcoJSONConfig.getAssetTypeId();
			this.chartOfAccountYear = tcoJSONConfig.getChartOfAccountYear();
			this.graphType = tcoJSONConfig.getGraphType();
			this.parameterId = tcoJSONConfig.getParameterId();
			this.questionnaireId = tcoJSONConfig.getQuestionnaireId();
		}	
	}

	public Integer getAssetTypeId() {
		return assetTypeId;
	}


	public void setAssetTypeId(Integer assetTypeId) {
		this.assetTypeId = assetTypeId;
	}


	public String getChartOfAccountYear() {
		return chartOfAccountYear;
	}


	public void setChartOfAccountYear(String chartOfAccountYear) {
		this.chartOfAccountYear = chartOfAccountYear;
	}


	public String getGraphType() {
		return graphType;
	}


	public void setGraphType(String graphType) {
		this.graphType = graphType;
	}


	public Integer getParameterId() {
		return parameterId;
	}


	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}


	public Integer getQuestionnaireId() {
		return questionnaireId;
	}


	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	
}
