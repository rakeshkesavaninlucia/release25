package org.birlasoft.thirdeye.beans.tco;

public class ChartOfAccountGridBean {
	
	
	private Integer questionnaireQuestionId; 
	private String response;
	
	public Integer getQuestionnaireQuestionId() {
		return questionnaireQuestionId;
	}
	public void setQuestionnaireQuestionId(Integer questionnaireQuestionId) {
		this.questionnaireQuestionId = questionnaireQuestionId;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}


}
