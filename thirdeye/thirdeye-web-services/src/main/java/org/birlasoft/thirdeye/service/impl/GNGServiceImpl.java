package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.gng.GNGValueWrapper;
import org.birlasoft.thirdeye.beans.gng.GNGWrapper;
import org.birlasoft.thirdeye.beans.php.PHPValueWrapper;
import org.birlasoft.thirdeye.beans.php.PHPWrapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.GNGService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class GNGServiceImpl implements GNGService {

	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Autowired
	private ResponseDataService responseDataService;
	@Autowired
	private AssetService assetService;
	@Autowired
	private ParameterSearchService parameterSearchService;
	@Autowired
	private QuestionnaireService questionnaireService;

	@Override
	public GNGWrapper getGNGWrappertoPlotReport(Set<Integer> idsOfParameters,
			Set<Integer> idsOfAssetTemplate, Integer idOfQuestionnaire,Map<String, List<String>> filterMap ) {

		GNGWrapper gngwrapper = new GNGWrapper();
		Set<AssetBean> assetBeans = assetService.getSetOfAssetBeansByIdsAndQeId(idsOfAssetTemplate, idOfQuestionnaire);

		Set<GNGValueWrapper> valueWrappersSet = new HashSet<>();
		List<AssetParameterWrapper> assetParameterWrappers = new ArrayList<>();
		for (Integer oneParam : idsOfParameters) {			
			AssetParameterWrapper assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(oneParam, idOfQuestionnaire, assetBeans, filterMap);
			assetParameterWrappers.add(assetParameterWrapper);
		}

		valueWrappersSet = prepareGNGValueWrapper(assetParameterWrappers,idOfQuestionnaire);
		gngwrapper.setAssetsValues(valueWrappersSet);
		return gngwrapper;
	}

	private Set<GNGValueWrapper> prepareGNGValueWrapper(
			List<AssetParameterWrapper> assetParameterWrappers,Integer idOfQuestionnaire ) {
		Set<GNGValueWrapper> valueWrappers = new HashSet<>();
		for(AssetParameterWrapper oneassetParam :assetParameterWrappers){
			if(!oneassetParam.getValues().isEmpty()) {        		
        		List<AssetParameterBean> listOfAssetBeans = oneassetParam.getValues();
        		for (AssetParameterBean assetBean : listOfAssetBeans) {
        			GNGValueWrapper gngValueWrapper = new GNGValueWrapper();
        			gngValueWrapper.setAssetId(assetBean.getAssetId());
        			gngValueWrapper.setAssetName(assetBean.getAssetName());
        			gngValueWrapper.setParameterId(oneassetParam.getParameterId());
        			gngValueWrapper.setParameterName(oneassetParam.getParameter());
        			gngValueWrapper.setParameterValue(assetBean.getParameterValue());
        			gngValueWrapper.setColor(assetBean.getColor());
        			gngValueWrapper.setDescription(assetBean.getDescription());
        			gngValueWrapper.setPercentage(getAssetDataCompleteness(assetBean,idOfQuestionnaire));
        			valueWrappers.add(gngValueWrapper);
        		}
        	}
        }			
		return valueWrappers;
	}

	private Integer getAssetDataCompleteness(AssetParameterBean assetBean, Integer idOfQuestionnaire ) {
		Integer percentage = 0;
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		Asset asset = assetService.findOne(assetBean.getAssetId());
		Set<QuestionnaireAsset> setofQuesAsset = questionnaireAssetService.findByQuestionnaireAndAsset(questionnaire, asset);
		Set<QuestionnaireQuestion> setOfQuesQuestion = questionnaireQuestionService.findByQuestionnaireAssetIn(setofQuesAsset);
		
		Integer noOfResponse = responseDataService.findByQuestionnaireQuestionIn(setOfQuesQuestion).size();		
		
		percentage = (noOfResponse * 100)/ (setOfQuesQuestion.size()) ;   
		
		return percentage;
	}
}
