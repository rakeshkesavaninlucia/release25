package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.Category;

/**
 * Service interface for category.
 * @author samar.gupta
 */
public interface CategoryService {
	/**
	 * List all category
	 * @return List{@code <Category>} Object
	 */
    public List<Category> findAll();
    /**
     * Only One Category  with given Id
     * @param id
     * @return {@code Category} Object
     */
    public Category findOne(Integer id);
    /**
     * save Category Object
     * @param category
     * @return {@code Category} Object
     */
    public Category save(Category category);
    
    /**
     * @param name
     * @return
     */
    public Category getCategoryFromName(String name);
}
