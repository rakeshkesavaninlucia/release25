package org.birlasoft.thirdeye.service.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.collections4.sequence.SequencesComparator;
import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.AssetLifeCycleStagesBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.service.AssetLifeCycleService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetLifeCycleServiceImpl implements AssetLifeCycleService {

	@Autowired
	private AssetTemplateService assetTemplateService;

	@Override
	public List<AssetTemplate> getListOfAssetTemplate() {
		List<AssetTemplate> listOfAssetTemplate = assetTemplateService.listTemplateForActiveWorkspace();
		Iterator<AssetTemplate> oneTemplate = listOfAssetTemplate.iterator();
		while (oneTemplate.hasNext()) {
			AssetTemplate oneAssetTemplate = oneTemplate.next();
			if(oneAssetTemplate.getAssets().isEmpty()) {
				oneTemplate.remove();
			}
		}
		return  listOfAssetTemplate;
	}

	@Override
	public Map<String,Map<String,List<String>>> getAssetlifeCycle(Integer assetTemplateId) {

		// find fully load asset template
		AssetTemplate oneAssettemplate = assetTemplateService.findFullyLoadedAssetTemplate(assetTemplateId);
		//get list of Asset Data
		List<AssetData> assetDatas = oneAssettemplate.getAssets().stream()
				.map(Asset :: getAssetDatas)
				.flatMap(Set::stream)
				.filter(assetData ->assetData.getAssetTemplateColumn().getDataType().equals(Constants.JSON_DATA_TYPE))
				//.map(ad -> getJsonMapper(ad.getData()))
				.collect(Collectors.toList());
		assetDatas.removeIf(assetdeleted->assetdeleted.getAsset().isDeleteStatus() == true);
		//get list of AssetLifeCycleStage Bean from list of assetData
		List<AssetLifeCycleStagesBean> listOfAssetlifeCycle = assetDatas.stream().map(ad -> getJsonMapper(ad.getData())).collect(Collectors.toList());
		Iterator<AssetLifeCycleStagesBean> oneTemplate = listOfAssetlifeCycle.iterator();
		while (oneTemplate.hasNext()) {
			AssetLifeCycleStagesBean oneAssetLife = oneTemplate.next();
			if(oneAssetLife.getLifeCycle_f().equals("") && oneAssetLife.getLifeCycle_s().equals("") && oneAssetLife.getLifeCycle_t().equals("")) {
				oneTemplate.remove();
			}
		}

		Map<String,List<AssetLifeCycleStagesBean>> mapOfAssetAndlifeCycle  = assetDatas.stream()
				.collect(Collectors.groupingBy(ad -> new AssetBean(ad.getAsset()).getShortName(), Collectors.mapping(ad ->getJsonMapper(ad.getData()), Collectors.toList())));
		//remove bean with life cycle stage value ""
		Iterator< List<AssetLifeCycleStagesBean>> it = mapOfAssetAndlifeCycle.values().iterator();
		while(it.hasNext()) {
			Iterator<AssetLifeCycleStagesBean> itasst = it.next().iterator();
			while( itasst.hasNext()) {
				AssetLifeCycleStagesBean temp = itasst.next();
				if(temp.getLifeCycle_f().equals("") && temp.getLifeCycle_s().equals("") && temp.getLifeCycle_t().equals("")) {
					itasst.remove();			
				}
				temp=null;
			}
		}
		//Get year Range
		List<String> yearRange = getListOfFromAndTo(listOfAssetlifeCycle);
		Map<String,Map<String,List<String>>> mapOfAssetLifeCycle =	getMap(mapOfAssetAndlifeCycle,yearRange);

		return mapOfAssetLifeCycle;
	}
	private Map<String,Map<String,List<String>>> getMap(Map<String,List<AssetLifeCycleStagesBean>> mapOfAssetAndlifeCycle,List<String> yearRange) {

		//	yearRange.stream().filter(e->e.length())
		Map<String,Map<String,List<String>>> mapofAssetlifeCycle = new LinkedHashMap<>();		
		for(String assetname:mapOfAssetAndlifeCycle.keySet()){
			Map<String,List<String>> mapOFyearAndStage = new LinkedHashMap<>();
			for(String year:yearRange) {
				List<String> stageName = new ArrayList<>();
				for(AssetLifeCycleStagesBean assetlifwSge : mapOfAssetAndlifeCycle.get(assetname)) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fromYear = LocalDate.parse("01/"+assetlifwSge.getLifeCycle_f(),formatter);
					LocalDate toDate =  LocalDate.parse("01/"+assetlifwSge.getLifeCycle_t(),formatter);
					String From = String.valueOf(fromYear.getYear());
					String To = String.valueOf(toDate.getYear());
					if( Integer.parseInt(year) >= Integer.parseInt(From) && Integer.parseInt(year) <= Integer.parseInt(To)){
						stageName.add(assetlifwSge.getLifeCycle_s());
					}
				}
				if(stageName.isEmpty()) 
					stageName.add("NA");
				//stageName.stream().sorted();
				mapOFyearAndStage.put(year, stageName);
			}
			mapofAssetlifeCycle.put(assetname, mapOFyearAndStage);
		}
		return mapofAssetlifeCycle;
	}

	private AssetLifeCycleStagesBean getJsonMapper(String assetData) {
		ObjectMapper objectMapper = new ObjectMapper();
		AssetLifeCycleStagesBean assetLifeCycleStagesBean =  new AssetLifeCycleStagesBean();
		try {
			assetLifeCycleStagesBean = objectMapper.readValue(assetData, AssetLifeCycleStagesBean.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return assetLifeCycleStagesBean;
	}

	private String getStage(String assetData) {
		ObjectMapper objectMapper = new ObjectMapper();
		String stage = ""  ;
		try {
			stage = objectMapper.readValue(assetData, AssetLifeCycleStagesBean.class).getLifeCycle_s();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stage;
	}
	private List<Integer> getYears(String assetData) {
		ObjectMapper objectMapper = new ObjectMapper();
		AssetLifeCycleStagesBean assetLifeCycleStagesBean =  new AssetLifeCycleStagesBean();
		try {
			assetLifeCycleStagesBean = objectMapper.readValue(assetData, AssetLifeCycleStagesBean.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

		LocalDate fromYear = LocalDate.parse("01/"+assetLifeCycleStagesBean.getLifeCycle_f(),formatter);
		LocalDate toDate =  LocalDate.parse("01/"+assetLifeCycleStagesBean.getLifeCycle_t(),formatter);
		long years = ChronoUnit.YEARS.between(fromYear,toDate);
		List<LocalDate> yearRanges= IntStream.iterate(0, i -> i + 1)
				.limit(years+1)
				.mapToObj(i -> fromYear.plusYears(i))
				.collect(Collectors.toList()); 

		return yearRanges.stream().map(ld -> ld.getYear()).collect(Collectors.toList());	
	}


	private List<String> getListOfFromAndTo(List<AssetLifeCycleStagesBean> listOfAssetlifeCycle) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");


		Iterator<AssetLifeCycleStagesBean> oneTemplate = listOfAssetlifeCycle.iterator();
		while (oneTemplate.hasNext()) {
			AssetLifeCycleStagesBean oneAssetLife = oneTemplate.next();
			if(oneAssetLife.getLifeCycle_f().equals("") && oneAssetLife.getLifeCycle_s().equals("") && oneAssetLife.getLifeCycle_t().equals("")) {
				oneTemplate.remove();
			}
		}
		LocalDate fromYear = listOfAssetlifeCycle.stream()
				.map(oneLifeCycle -> LocalDate.parse("01/"+oneLifeCycle.getLifeCycle_f(),formatter))
				.sorted((d1,d2)->d1.compareTo(d2)).findFirst().get();
		LocalDate toYear = listOfAssetlifeCycle.stream()
				.map(oneLifeCycle -> LocalDate.parse("01/"+oneLifeCycle.getLifeCycle_t(),formatter))
				.sorted((d1,d2)->d2.compareTo(d1)).findFirst().get();
		//		toYear.getYear()
		//long years = ChronoUnit.YEARS.between(fromYear.getYear(), toYear.getYear());
		List<String> yearRange = new ArrayList<>();
		for(int start =fromYear.getYear(); start<=toYear.getYear();start++) {
			yearRange.add(String.valueOf(start));
		}
		return yearRange;
	}
	//		List<LocalDate> yearRanges= IntStream.iterate(0, i -> i + 1)
	////			      .limit(years)
	//			      .mapToObj(i -> fromYear.plusYears(i))
	//			      .collect(Collectors.toList()); 
	//			}

	@Override
	public List<String> getYearRange(Integer assetTemplateId) {

		// find fully load asset template
		AssetTemplate oneAssettemplate = assetTemplateService.findFullyLoadedAssetTemplate(assetTemplateId);

		List<AssetData> assetDatas = oneAssettemplate.getAssets().stream()
				.map(Asset :: getAssetDatas)
				.flatMap(Set::stream)
				.filter(assetData ->assetData.getAssetTemplateColumn().getDataType().equals(Constants.JSON_DATA_TYPE))
				//.map(ad -> getJsonMapper(ad.getData()))
				.collect(Collectors.toList());
		List<AssetLifeCycleStagesBean> listOfAssetlifeCycle = assetDatas.stream().map(ad -> getJsonMapper(ad.getData())).collect(Collectors.toList());

		Map<String,List<AssetLifeCycleStagesBean>> mapOfAssetAndlifeCycle  = assetDatas.stream()
				.collect(Collectors.groupingBy(ad -> new AssetBean(ad.getAsset()).getShortName(), Collectors.mapping(ad ->getJsonMapper(ad.getData()), Collectors.toList())));

		//Get year Range

		List<String> yearRange = getListOfFromAndTo(listOfAssetlifeCycle);
		return yearRange;
	}

	@Override
	public HashMap<Integer,AssetLifeCycleStagesBean> getJSONMapper(Asset asset) {
		ObjectMapper objectMapper = new ObjectMapper();
		//List<AssetLifeCycleStagesBean> listOfAssetLifeCycleStagesBean = new ArrayList<>();

		HashMap<Integer,AssetLifeCycleStagesBean> LifeCycleMap = new HashMap<>();

		for(AssetData oneAsset:asset.getAssetDatas()) {
			AssetLifeCycleStagesBean assetLifeCycleStagesBean =  new AssetLifeCycleStagesBean();
			if(oneAsset.getAssetTemplateColumn().getDataType().equals(Constants.JSON_DATA_TYPE)) {
				try {
					assetLifeCycleStagesBean = objectMapper.readValue(oneAsset.getData(), AssetLifeCycleStagesBean.class);
				} catch (IOException e) {
					e.printStackTrace();
				}
				LifeCycleMap.put(oneAsset.getAssetTemplateColumn().getId(), assetLifeCycleStagesBean);
			}
		}

		return LifeCycleMap;
	}
}