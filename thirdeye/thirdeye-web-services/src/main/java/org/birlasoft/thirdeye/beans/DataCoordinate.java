package org.birlasoft.thirdeye.beans;

public class DataCoordinate {
	
	public static enum GraphCoordinate {
		X,
		Y,
		Z
	}
	
	private double x;
	private double y;
	private double  z = 12.0;
	
	private String label;
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getZ() {
		return z;
	}
	public void setZ(double z) {
		this.z = z;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public void setCoordinate(GraphCoordinate coordinateAxis, Double value){
		if (GraphCoordinate.X.equals(coordinateAxis)){
			this.x = value;
		} else if (GraphCoordinate.Y.equals(coordinateAxis)){
			this.y = value;
		} if (GraphCoordinate.Z.equals(coordinateAxis)){
			this.z = value;
		}
	}
	
}
