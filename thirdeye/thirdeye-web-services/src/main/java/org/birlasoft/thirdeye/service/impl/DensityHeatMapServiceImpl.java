package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.MultiMap;
import org.birlasoft.thirdeye.beans.DensityHeatMapWrapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.Country;
import org.birlasoft.thirdeye.repositories.AssetDataRepository;
import org.birlasoft.thirdeye.repositories.AssetRepository;
import org.birlasoft.thirdeye.repositories.AssetTemplateColumnRepository;
import org.birlasoft.thirdeye.repositories.AssetTemplateRepository;
import org.birlasoft.thirdeye.repositories.CountryRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DensityHeatMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class DensityHeatMapServiceImpl implements DensityHeatMapService  {

	@Autowired
	private AssetTemplateRepository assetTemplateRepository;
	@Autowired
	private AssetRepository assetRepository;
	@Autowired
	private AssetDataRepository assetDataRepository;
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private AssetTemplateColumnRepository assetTemplateColumnRepository;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public  List<DensityHeatMapWrapper> getAllAssetsFromAssetTemplatesForActiveWorkSpace() {
		Set<AssetData> setOfAssetData = new HashSet<>();
		List<DensityHeatMapWrapper> listWorldMapWrapper = new ArrayList<>();
		List<AssetTemplate> listOfAssetTemplate = assetTemplateRepository.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		List<AssetTemplateColumn> listOfTemplateCol  = new ArrayList<>();
		MultiMap mapOfnoOfColumns = new org.apache.commons.collections4.map.MultiValueMap<>();
		for(AssetTemplate oneAssetTemplate: listOfAssetTemplate) {
			listOfTemplateCol=  assetTemplateColumnRepository.findByAssetTemplate(oneAssetTemplate);
			Map<String,Integer> mpColId = new HashMap<>();
			for(AssetTemplateColumn oneCol: listOfTemplateCol) {
				if(oneCol.getAssetTemplateColName().equalsIgnoreCase("Country") || oneCol.getAssetTemplateColName().equalsIgnoreCase("No Of Users")){
					if(oneCol.getDataType().equalsIgnoreCase("TEXT")) {
						mpColId.put(oneCol.getAssetTemplateColName(), oneCol.getId());
					}
					else {
						mpColId.put(oneCol.getAssetTemplateColName(), oneCol.getId());
					}
				}
			}

			Set<Asset> listAsset =  assetRepository.findByAssetTemplate(oneAssetTemplate);
			for(Asset oneAsset : listAsset) {
				setOfAssetData = assetDataRepository.findByAsset(oneAsset);
				Map mapOfAssetTempCol= new HashMap();
				for(AssetData assetdata :  setOfAssetData)    	  
				{
					if( mpColId.containsValue(assetdata.getAssetTemplateColumn().getId()))
					{
						if(assetdata.getAssetTemplateColumn().getDataType().equals("TEXT"))
						{
							mapOfAssetTempCol.put("country", assetdata.getData());
						}
						else
							mapOfAssetTempCol.put("userscount", assetdata.getData());

						if(mapOfAssetTempCol.size()==2)
						{
							mapOfnoOfColumns.put(mapOfAssetTempCol.get("country"), mapOfAssetTempCol.get("userscount"));
							mapOfAssetTempCol= null ;
						}

					}	  
				}
			}
		}
		Set keySet = mapOfnoOfColumns.keySet();
		Iterator keyIterator = keySet.iterator();
		while (keyIterator.hasNext()) {
			Integer userCount = 0;
			String countryNamekey = (String) keyIterator.next();
			List listuserCounts = (List) mapOfnoOfColumns.get(countryNamekey);
			for(Object oneCount : listuserCounts) {
				userCount = userCount +  Integer.parseInt(oneCount.toString());
			}
			Country oneCountry = findByCountryName(countryNamekey);
			oneCountry.getCountryCode();
			listWorldMapWrapper.add(new DensityHeatMapWrapper(countryNamekey,userCount,listuserCounts.size(),oneCountry.getCountryCode()));
		}
		return listWorldMapWrapper;
	}

	@Override
	public Country findByCountryName(String CountryName) {
		return countryRepository.findBycountryName(CountryName);
	}

	@Override
	public List<Country> findListOfCountry() {
		return countryRepository.findAll();
	}
}
