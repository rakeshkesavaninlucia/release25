package org.birlasoft.thirdeye.beans;

import java.util.Map;

import org.birlasoft.thirdeye.entity.AssetTemplateColumn;

public class AssetLifeCycleExcelBean {
	
	private AssetTemplateColumn assetTemplateColumn;
	private Map<String,Integer> mapOfIdAndColumnIndex;
	
	public AssetTemplateColumn getAssetTemplateColumn() {
		return assetTemplateColumn;
	}
	public void setAssetTemplateColumn(AssetTemplateColumn assetTemplateColumn) {
		this.assetTemplateColumn = assetTemplateColumn;
	}
	public Map<String,Integer> getMapOfIdAndColumnIndex() {
		return mapOfIdAndColumnIndex;
	}
	public void setMapOfIdAndColumnIndex(Map<String,Integer> mapOfIdAndColumnIndex) {
		this.mapOfIdAndColumnIndex = mapOfIdAndColumnIndex;
	}
	

}
