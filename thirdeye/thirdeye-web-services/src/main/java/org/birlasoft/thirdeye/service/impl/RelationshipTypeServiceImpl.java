package org.birlasoft.thirdeye.service.impl;

import java.util.Date;
import java.util.List;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.RelationshipTypeRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.RelationshipTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("relationshipTypeService")
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class RelationshipTypeServiceImpl implements RelationshipTypeService{
	
	@Autowired
	private RelationshipTypeRepository relationshipTypeRepository;
	@Autowired
	CustomUserDetailsService customUserDetailsService;

	@Override
	public RelationshipType saveOrUpdate(RelationshipType incomingRelationshipType) {
		return relationshipTypeRepository.save(incomingRelationshipType);
	}

	@Override
	public RelationshipType findOne(Integer idOfRelationshipType) {
		return relationshipTypeRepository.findOne(idOfRelationshipType);
	}

	@Override
	public List<RelationshipType> findAll() {
		return relationshipTypeRepository.findAll();
	}

	@Override
	public List<RelationshipType> findByWorkspace(Workspace workspace) {
		return relationshipTypeRepository.findByWorkspace(workspace);
	}

	@Override
	public List<RelationshipType> findByWorkspaceIsNullOrWorkspace(Workspace workspace) {
		return relationshipTypeRepository.findByWorkspaceIsNullOrWorkspace(workspace);
	}

	@Override
	public RelationshipType createNewRelationshipType(RelationshipType incomingRelationshipType) {
		RelationshipType rtype = new RelationshipType();
		rtype.setParentDescriptor(incomingRelationshipType.getParentDescriptor().trim());
		rtype.setChildDescriptor(incomingRelationshipType.getChildDescriptor().trim());
		rtype.setDirection(incomingRelationshipType.getDirection());
		rtype.setWorkspace(incomingRelationshipType.getWorkspace());
		rtype.setDisplayName(incomingRelationshipType.getParentDescriptor() + " :: " + incomingRelationshipType.getChildDescriptor());
		rtype.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		rtype.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		rtype.setCreatedDate(new Date());
		rtype.setUpdatedDate(new Date());
		return rtype;
	}

	@Override
	public RelationshipType updateRelationshipType(RelationshipType incomingRelationshipType) {
		RelationshipType rtypeFromDB = findOne(incomingRelationshipType.getId());
		rtypeFromDB.setParentDescriptor(incomingRelationshipType.getParentDescriptor().trim());
		rtypeFromDB.setChildDescriptor(incomingRelationshipType.getChildDescriptor().trim());
		rtypeFromDB.setDirection(incomingRelationshipType.getDirection());
		rtypeFromDB.setWorkspace(incomingRelationshipType.getWorkspace());
		rtypeFromDB.setDisplayName(incomingRelationshipType.getParentDescriptor() + " :: " + incomingRelationshipType.getChildDescriptor());
		rtypeFromDB.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		rtypeFromDB.setUpdatedDate(new Date());
		return rtypeFromDB;
	}
}
