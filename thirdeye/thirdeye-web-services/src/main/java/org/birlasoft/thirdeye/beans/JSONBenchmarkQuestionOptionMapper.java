package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.comparator.Sequenced;

/**
 * This object will be used for JSON serialization
 * @author samar.gupta
 */
public class JSONBenchmarkQuestionOptionMapper extends JSONQuestionOptionMapper implements Sequenced {
	
	Integer benchmarkItemId;
	Integer benchmarkItemScoreId;
	
	/**
	 * default constructor 
	 */
	public JSONBenchmarkQuestionOptionMapper() {
		super();
	}

	public Integer getBenchmarkItemId() {
		return benchmarkItemId;
	}

	public void setBenchmarkItemId(Integer benchmarkItemId) {
		this.benchmarkItemId = benchmarkItemId;
	}

	public Integer getBenchmarkItemScoreId() {
		return benchmarkItemScoreId;
	}

	public void setBenchmarkItemScoreId(Integer benchmarkItemScoreId) {
		this.benchmarkItemScoreId = benchmarkItemScoreId;
	}
}
