package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;

public class AIDCentralAssetWrapperBean {

	private AssetBean centralAssetBean;
	List<JSONAIDBlockConfig> listOfBlocksInMainApp = new ArrayList<JSONAIDBlockConfig>();
	private Integer id;
	
	public void addBlock(JSONAIDBlockConfig oneBlock){
		if (oneBlock != null){
			listOfBlocksInMainApp.add(oneBlock);
		}
	}

	public List<JSONAIDBlockConfig> getListOfBlocksInMainApp() {
		return listOfBlocksInMainApp;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	
	public void prepareForDisplay(){
		Collections.sort(listOfBlocksInMainApp, new SequenceNumberComparator());
	}

	public AssetBean getCentralAssetBean() {
		return centralAssetBean;
	}

	public void setCentralAssetBean(AssetBean centralAssetBean) {
		this.centralAssetBean = centralAssetBean;
	}

	
}
