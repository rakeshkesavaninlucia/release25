package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.User;

/**
 * Service interface for cost structure.
 * @author samar.gupta
 */
public interface CostStructureService {
	/**
	 * Create new cost_structure/parameter object. 
	 * @param parameterBean
	 * @return {@code Parameter}
	 */
	public Parameter createNewCostStructure(ParameterBean parameterBean);
	/**
	 * Get maximum sequence number from list of 
	 * parameter function object sequence number.
	 * @param listOfPfs
	 * @return {@code int}
	 */
	public int getMaxSequenceNumber(List<ParameterFunction> listOfPfs);
	/**
	 * Extract cost elements from full list.
	 * @param listCostElement
	 * @param alreadyAddedCostElements
	 * @return {@code List<Question>}
	 */
	public List<Question> extractCostElementToDisplay(List<Question> listCostElement, List<ParameterFunction> alreadyAddedCostElements);
	/**
	 * Update cost structure.
	 * @param incomingCostStructure
	 * @param currentUser
	 * @return {@code Parameter}
	 */
	public Parameter updateCostStructure(ParameterBean incomingCostStructure, User currentUser);
	/**
	 * Create new parameter function object.
	 * @param incomingCostStructure
	 * @param parameter
	 * @return {@code ParameterFunction}
	 */
	public ParameterFunction createNewParameterFunction(ParameterBean incomingCostStructure, Parameter parameter);
	/**
	 * Delete parameter function 
	 * by parent parameter
	 * @param parameter
	 */
	public void deleteParamFunctionByParent(Parameter parameter);
	/**
	 * Delete parameter function 
	 * by child parameter
	 * @param parameter
	 */
	public void deleteParamFunctionByChild(Parameter parameter);
}
