package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.widgets.GroupedDiscreteBarWrapper;
import org.birlasoft.thirdeye.beans.widgets.GroupedWidgetJSONConfig;

public interface GroupedWidgetService {

	public List<GroupedDiscreteBarWrapper> getDataForGroupedGraph(GroupedWidgetJSONConfig widgetJSONConfig);

	public void saveGroupWidget(GroupedWidgetJSONConfig widgetJSONConfig);

}
