package org.birlasoft.thirdeye.beans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Bean for benchmark question for holding
 * request parameters and holding error in map
 * and constant keys in the map. 
 * @author samar.gupta
 *
 */
public class BenchmarkQuestionBean {
	
	private boolean benchmarkCheckboxMCQ;
	private Integer benchmarkId;
	private Set<Integer> benchmarkItem = new HashSet<>();
	private Map<String, String> mapOfBenchmarkParam = new HashMap<>();
	
	public static final String BENCHMARK_CHECKBOX_MCQ = "benchmarkCheckboxMCQ";
	public static final String BENCHMARK_ID = "benchmarkId";
	public static final String BENCHMARK_ITEM = "benchmarkItem";
	
	public boolean isBenchmarkCheckboxMCQ() {
		return benchmarkCheckboxMCQ;
	}
	public void setBenchmarkCheckboxMCQ(boolean benchmarkCheckboxMCQ) {
		this.benchmarkCheckboxMCQ = benchmarkCheckboxMCQ;
	}
	public Integer getBenchmarkId() {
		return benchmarkId;
	}
	public void setBenchmarkId(Integer benchmarkId) {
		this.benchmarkId = benchmarkId;
	}
	public Set<Integer> getBenchmarkItem() {
		return benchmarkItem;
	}
	public void setBenchmarkItem(Set<Integer> benchmarkItem) {
		this.benchmarkItem = benchmarkItem;
	}
	
	/**
	 * method for putting keys in the map.
	 */
	public void putKeysInMap(){
		mapOfBenchmarkParam.put(BENCHMARK_CHECKBOX_MCQ, null);
		mapOfBenchmarkParam.put(BENCHMARK_ID, null);
		mapOfBenchmarkParam.put(BENCHMARK_ITEM, null);
	}
	public Map<String, String> getMapOfBenchmarkParam() {
		return mapOfBenchmarkParam;
	}
	public void setMapOfBenchmarkParam(Map<String, String> mapOfBenchmarkParam) {
		this.mapOfBenchmarkParam = mapOfBenchmarkParam;
	}
	
	/**
	 * Put property as a key and 
	 * error as a value in the map.
	 * @param key
	 * @param value
	 */
	public void putInMap(String key, String value){
		mapOfBenchmarkParam.put(key, value);
	}
}
