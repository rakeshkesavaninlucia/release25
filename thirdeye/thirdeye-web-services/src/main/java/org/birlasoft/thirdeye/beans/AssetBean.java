package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.colorscheme.bean.CSSBean;

/**
 * Bean for creating the graph node from asset.
 */
public class AssetBean extends CSSBean{
	
	private Integer id;
	private String assetTemplateName;
	private String assetTypeName;
	private String shortName;
	private Integer relationshipAssetId;
	private String assetStyle;
	
	private List<TemplateColumnBean> assetDatas = new ArrayList<TemplateColumnBean>();
	private List<String> listOfAssetTemplateColName = new ArrayList<>(); 
	
	public AssetBean(Asset sourceAsset) {
		this.id = sourceAsset.getId();
		if(sourceAsset.getAssetTemplate() != null){
			this.assetTemplateName = sourceAsset.getAssetTemplate().getAssetTemplateName();
			this.assetTypeName = sourceAsset.getAssetTemplate().getAssetType().getAssetTypeName();
		}
		for (AssetData onePieceOfData : sourceAsset.getAssetDatas()){
			TemplateColumnBean templateColumnBean = new TemplateColumnBean();
			templateColumnBean.setName(onePieceOfData.getAssetTemplateColumn().getAssetTemplateColName());
			templateColumnBean.setData(onePieceOfData.getData());
			templateColumnBean.setSequenceNumber(onePieceOfData.getAssetTemplateColumn().getSequenceNumber());
			templateColumnBean.setDataType(onePieceOfData.getAssetTemplateColumn().getDataType());
			templateColumnBean.setAssetTemplateColId(onePieceOfData.getAssetTemplateColumn().getId());
			templateColumnBean.setMandatory(onePieceOfData.getAssetTemplateColumn().isMandatory());
			assetDatas.add(templateColumnBean);
	    	listOfAssetTemplateColName.add(onePieceOfData.getAssetTemplateColumn().getAssetTemplateColName());
			
		}
		//Condition added for template column, when a column is added and the existing asset doesn't have data for that
		//add "na" for that existing asset.

		if(sourceAsset.getAssetTemplate()!=null &&  sourceAsset.getAssetTemplate().getAssetTemplateColumns().size() > sourceAsset.getAssetDatas().size()){       
			for(AssetTemplateColumn oneAssetTemplateColumn : sourceAsset.getAssetTemplate().getAssetTemplateColumns()){
				if(!listOfAssetTemplateColName.contains(oneAssetTemplateColumn.getAssetTemplateColName())){
					TemplateColumnBean templateColBean = new TemplateColumnBean();
					templateColBean.setName(oneAssetTemplateColumn.getAssetTemplateColName());
					templateColBean.setData("N-A");
					templateColBean.setSequenceNumber(oneAssetTemplateColumn.getSequenceNumber());	
					assetDatas.add(templateColBean);
				}
			}			
		}
		
		if(assetDatas.size() > 0){
			Collections.sort(assetDatas, new SequenceNumberComparator());
		}
	}
	
	public AssetBean(){}

	public List<TemplateColumnBean> getAssetDatas() {
		return assetDatas;
	}

	public void setAssetDatas(List<TemplateColumnBean> assetDatas) {
		this.assetDatas = assetDatas;
	}

	public Integer getId() {
		return id;
	}

	public String getAssetTemplateName() {
		return assetTemplateName;
	}

	public String getAssetTypeName() {
		return assetTypeName;
	}
	
	public String getAssetDetails() {
		return "[Template name=" + assetTemplateName
				+ ", \n Asset Type=" + assetTypeName + ", \n assetDatas="
				+ assetDatas + "]";
	}
	
	public String getShortName(){
		if (shortName == null){
			return toString();
		}
		
		return shortName;
	}
	
	public String getShortDetail(){
		return "[Asset Type=" + assetTypeName + ", \n"
				+ assetDatas.get(0) + "]";
	}
	
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@Override
	public String toString() {	
		if(getAssetDatas() != null && getAssetDatas().size() > 0){
			Collections.sort(getAssetDatas(), new SequenceNumberComparator());
			TemplateColumnBean templateColumnBean = getAssetDatas().get(0);
			return templateColumnBean.getData();
		}
		return "";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssetBean other = (AssetBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getRelationshipAssetId() {
		return relationshipAssetId;
	}

	public void setRelationshipAssetId(Integer relationshipAssetId) {
		this.relationshipAssetId = relationshipAssetId;
	}

	public String getAssetStyle() {
		return assetStyle;
	}

	public void setAssetStyle(String assetStyle) {
		this.assetStyle = assetStyle;
	}
	
	
}
