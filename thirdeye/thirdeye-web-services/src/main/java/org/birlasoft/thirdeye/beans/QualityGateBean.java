package org.birlasoft.thirdeye.beans;

import java.util.List;

import org.birlasoft.thirdeye.entity.QualityGate;

public class QualityGateBean {
	
	private Integer id;
	private String name;
	private List<JSONQualityGateDescriptionValueMapper> decriptionMapper;
	
	public QualityGateBean(QualityGate qualityGate) {
		this.id = qualityGate.getId();
		this.name = qualityGate.getName();
	}
	
	public QualityGateBean () {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}	
    
	public List<JSONQualityGateDescriptionValueMapper> getDescription() {
		return decriptionMapper;
	}
	
	public void setDescription(List<JSONQualityGateDescriptionValueMapper> decriptionMapper) {
		this.decriptionMapper = decriptionMapper;
	}
}
