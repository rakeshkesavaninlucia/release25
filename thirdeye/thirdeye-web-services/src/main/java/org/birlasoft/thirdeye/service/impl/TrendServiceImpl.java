package org.birlasoft.thirdeye.service.impl;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.InputTrend;
import org.birlasoft.thirdeye.repositories.TrendRepository;
import org.birlasoft.thirdeye.service.TrendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class TrendServiceImpl implements TrendService{
	
	@Autowired
	private TrendRepository trendRepository;
	
	
	@Override
	public InputTrend save(InputTrend trend) {
		return trendRepository.save(trend);
	}

}
