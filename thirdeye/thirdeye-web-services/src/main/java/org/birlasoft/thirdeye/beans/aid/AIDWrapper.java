package org.birlasoft.thirdeye.beans.aid;

import java.util.List;
// One Aid to be displayed on the page
public class AIDWrapper {

	AIDCentralAssetWrapperBean centralAsset;
	List<RelatedAssetBean> listOfInboundAssets;
	List<RelatedAssetBean> listOfOutboundAssets;

	public AIDCentralAssetWrapperBean getCentralAsset() {
		return centralAsset;
	}
	public void setCentralAsset(AIDCentralAssetWrapperBean centralAsset) {
		this.centralAsset = centralAsset;
	}

	public List<RelatedAssetBean> getListOfInboundAssets() {
		return listOfInboundAssets;
	}
	public void setListOfInboundAssets(List<RelatedAssetBean> listOfInboundAssets) {
		this.listOfInboundAssets = listOfInboundAssets;
	}
	public List<RelatedAssetBean> getListOfOutboundAssets() {
		return listOfOutboundAssets;
	}
	public void setListOfOutboundAssets(List<RelatedAssetBean> listOfOutboundAssets) {
		this.listOfOutboundAssets = listOfOutboundAssets;
	}
}
