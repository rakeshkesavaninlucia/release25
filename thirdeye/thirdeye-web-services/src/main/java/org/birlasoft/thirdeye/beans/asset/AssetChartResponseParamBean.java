package org.birlasoft.thirdeye.beans.asset;

import java.math.BigDecimal;


public class AssetChartResponseParamBean {
	
	private String axis;
	private BigDecimal value;
	
	public String getAxis() {
		return axis;
	}
	public void setAxis(String axis) {
		this.axis = axis;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
}
