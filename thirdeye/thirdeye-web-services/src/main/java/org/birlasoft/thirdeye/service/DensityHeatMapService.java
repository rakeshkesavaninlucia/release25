package org.birlasoft.thirdeye.service;


import java.util.List;

import org.birlasoft.thirdeye.beans.DensityHeatMapWrapper;
import org.birlasoft.thirdeye.entity.Country;

/**
 * @author Shagun.sharma
 *
 */
public interface DensityHeatMapService {

	/**
	 * Method to get world map wrapper across active workspace
	 * @return{@code List Of World Map Wrapper}
	 */
	List<DensityHeatMapWrapper> getAllAssetsFromAssetTemplatesForActiveWorkSpace();
	
	/** Method to find the Country Data 
	 * @param CountryName
	 * @return{@code countrydata}
	 */
	Country findByCountryName(String CountryName); 
	
	/** Method to find the list of country data
	 * @return{@code list Of Country }
	 */
	List<Country> findListOfCountry();

}
