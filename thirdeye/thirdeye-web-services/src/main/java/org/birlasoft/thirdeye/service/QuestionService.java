package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.BenchmarkQuestionBean;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONNumberQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.entity.Category;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.search.api.beans.QuestionResponseSearchWrapper;

/** {@code Interface} for save {@code question} , {@code List of all questions} within a 
 *   {@code workspace} , {@code List of all questions} without {@code workspace}
 * @author sanjeev.mishra
 */
public interface QuestionService {
	/**
	 * {@code save} {@code question} Object
	 * @param question
	 * @return {@code Question}
	 */
	public Question save(Question question);
    /**
     * Find list of question by workspace in set of workspace.
     * @param workspaces
     * @return {@code List<Question>}
     */
    public List<Question> findByWorkspaceIn(Set<Workspace> workspaces);
    /**
     * Find list of question by workspace is null.
     * @return {@code List<Question>}
     */
    public List<Question> findQuestionsByWorkspaceNull();
	/**
	 * find one question object by id.
	 * @param id
	 * @return Question
	 */
    public Question findOne(Integer id);
    /**
     * Find list of question by workspace is null or workspace.
     * @param workspace
     * @return {@code List<Question>}
     */
    public List<Question> findByWorkspaceIsNullOrWorkspace(Workspace workspace);
   /**
	 * Update question object.
	 * @param question
	 * @param quantifiers 
	 * @param currentUser
	 * @param numberQuestionMapper 
	 * @param otherOption
	 * @param bqBean
	 * @return {@code Question}
	 */
   public Question updateQuestionObject(Question question, List<Double> quantifiers, User currentUser, boolean otherOption, JSONNumberQuestionMapper numberQuestionMapper, BenchmarkQuestionBean bqBean);
   /**
	 * Create new question object
	 * @param question
	 * @param quantifiers 
	 * @param currentUser
	 * @param otherOption 
	 * @param numberQuestionMapper
	 * @param bqBean
	 * @return {@code Question}
	 */
   public Question createQuestionObject(Question question, List <Double> quantifiers, User currentUser, boolean otherOption, JSONNumberQuestionMapper numberQuestionMapper, BenchmarkQuestionBean bqBean);
   /** find list of question by workspace is null or in set of workspaces.
	 * @param workspaces
	 * @return {@code List<Question>}
	 */
   public List<Question> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> workspaces);
	 /** get list of questions by user.
	 * @param user
	 * @return {@code List<Question>}
	 */
   public List<Question> getQuestionListByUser(User user);
   	/** Get options list of json question option mapper.
	 * @param question
	 * @return {@code List<JSONQuestionOptionMapper>}
	 */
   public List<JSONQuestionOptionMapper> getQuestionOptions(Question question);
   	 /** Get JSONMultiChoiceQuestionMapper object.
	 * @param questionType
	 * @return {@code JSONMultiChoiceQuestionMapper}
	 */
   public JSONMultiChoiceQuestionMapper getJSONMultiChoiceQuestionMapper(String questionType);

	 /** Create QQ Bean for view question.
	  * @param question
	  * @param shortName
	  * @param qqId
	  * @return {@code QuestionnaireQuestionBean}
	  */
   public QuestionnaireQuestionBean createQQBeanForViewQuestion(Question question, String shortName, Integer qqId);
	 /**
	 * Get extracted list of questions.
	 * @param questionnaire
	 * @param parentParameterId
	 * @return {@code List<Question>}
	 */
   public List<Question> getExtractedListOfQuestions(Questionnaire questionnaire, Integer parentParameterId);
	 /**
	 * Get set of question bean.
	 * @param parameter
	 * @param listOfFunctionsToBeSaved
	 * @return {@code Set<QuestionBean>}
	 */
   	public Set<QuestionBean> getSetOfQuestionBean(Parameter parameter, List<ParameterFunction> listOfFunctionsToBeSaved);
	
   	/**
   	 * Create Variant question
   	 * @param question
   	 * @return {@code Question}
   	 */
   	public Question createVariantQuestion(Question question);
   	/**
   	 * find list of Quetion by workspace and displayName
   	 * @param workspace
   	 * @param displayName
   	 * @return {@code List<Question>}
   	 */
	public List<Question> findByWorkspaceAndDisplayName(Workspace workspace, String displayName);

	/**
	 * Prepare Question With Error
	 * @param question
	 * @param quantifiers
	 * @param otherOption
	 * @param numberQuestionMapper
	 * @param bqBean
	 * @return
	 */
	public Question prepareQuestionWithError(Question question, List<Double> quantifiers, boolean otherOption, JSONNumberQuestionMapper numberQuestionMapper, BenchmarkQuestionBean bqBean);
	/**
	 * find Question by Ids
	 * @param setOfQuestionIds
	 * @return {@code List<Question> }
	 */
	public List<Question> findByIdIn(Set<Integer> setOfQuestionIds);
	
	/**
	 * find one question object by id.
	 * @param id
	 * @return Question
	 */
    public Question findFullyLoaded(Integer id);
    /**
     * find list of Question by WorkSpace
     * @param workspace
     * @return {@code List<Question>}
     */
    public List<Question> findByWorkspace(Workspace workspace);
    /**
     * find list of Question by WorkSpace
     * @param activeWorkSpaceForUser
     * @param string
     * @return {@code List<Question>}
     */
	public List<Question> findByWorkspaceAndQuestionType(Workspace activeWorkSpaceForUser, String string);
	/**
	 * Get Map Of Question By Category
	 * @param activeWorkSpaceForUser
	 * @return {@code Map<Category, List<Question>>}
	 */
	public Map<Category, List<Question>> getMapOfQuestionByCategory(Workspace activeWorkSpaceForUser);
	/**
	 * Create category if not exist
	 * @param categoryName
	 * @return {@code Category}
	 */
	
	public Category createCategoryIfNotExist(String categoryName);
	
	/**@author dhruv.sood
     * Find list of question as per question type and corresponding workspace
     * @param workspace
     * @param questionType
     * @return {@code List<Question>}
     */
	public List<Question> findQuestionListByWorkspaceAndQuestionType(Workspace workspace, String questionType);
	
	/**Find list of question as per category and corresponding workspace
     * @author dhruv.sood
     * @param setOfWorkspaces
     * @param category
     * @return {@code List<Question>}
     */
	public List<Question> findByWorkspaceIsNullOrWorkspaceInAndCategoryNot(Set<Workspace> setOfWorkspaces, Category category);
	/**
	 * Find by workspaceIn And Category.
	 * @param setOfWorkspaces
	 * @param category
	 * @return {@code List<Question>}
	 */
	public List<Question> findByWorkspaceInAndCategory(Set<Workspace> setOfWorkspaces, Category category);
	/**
	 * Convert benchmark MCQ into normal MCQ.
	 * @param benchmarkMultiChoiceQuestionMapper
	 * @return {@code JSONMultiChoiceQuestionMapper}
	 */
	public JSONMultiChoiceQuestionMapper convertBenchmarkMCQintoNormalMCQ(JSONBenchmarkMultiChoiceQuestionMapper benchmarkMultiChoiceQuestionMapper);
	
	public Map<Integer, QuestionResponseSearchWrapper> getQuestionValueFromElasticsearch(Integer questionnaireId,
			QuestionnaireParameter qp, Set<Integer> assetIds);
}
