package org.birlasoft.thirdeye.beans;


/**
 * This object will be used for JSON serialization
 * 
 * @author samar.gupta
 *
 */
public class JSONNumberQuestionMapper {

	private Integer min;
	private Integer max;
	private boolean limitTo;

	public JSONNumberQuestionMapper() {
		super();
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public boolean isLimitTo() {
		return limitTo;
	}

	public void setLimitTo(boolean limitTo) {
		this.limitTo = limitTo;
	}
}
