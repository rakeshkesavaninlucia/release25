package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.AssetLifeCycleStageJson;
import org.birlasoft.thirdeye.beans.TemplateColumnBean;
import org.birlasoft.thirdeye.beans.asset.AssetBarChartWrapper;
import org.birlasoft.thirdeye.beans.asset.AssetParameterHealthBean;
import org.birlasoft.thirdeye.beans.relationship.RelationshipAssetDataBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.DataType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.RelationshipAssetData;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.AssetRepository;
import org.birlasoft.thirdeye.repositories.AssetTemplateRepository;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.AssetAuditTrailService;
import org.birlasoft.thirdeye.service.AssetParameterHealthService;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateColumnService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.RelationshipTemplateService;
import org.birlasoft.thirdeye.util.Utility;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author samar.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetServiceImpl implements AssetService {
	
	private static Logger logger = LoggerFactory.getLogger(AssetServiceImpl.class);
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	
	@Autowired
	private AssetRepository assetRepository;
	@Autowired
	private AssetTemplateColumnService assetTemplateColumnService;
	@Autowired
	private QuestionnaireAssetService qaService; 
	@Autowired
	private QuestionnaireService qeService; 
	@Autowired
	private AssetTemplateService assetTemplateService;
	
	@Autowired
	private AssetTemplateRepository templateRepository;
	
	@Autowired
	private RelationshipTemplateService relationshipTemplateService;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
	@Autowired
	private AssetParameterHealthService assetParamHealthService;
	
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private AssetAuditTrailService assetAuditTrailService;
	
	@Override
	public List<Asset> findAll() {
		return assetRepository.findAll();
	}
	
	@Override
    public Asset save(Asset asset) {
        return assetRepository.save(asset);
    }

	@Override
	public Asset findOne(Integer id) {							
		return assetRepository.findOne(id);
	}

	@Override
	public Set<Asset> findByIdNotIn(Set<Integer> assetsIdsInGraph) {
		return assetRepository.findByIdNotIn(assetsIdsInGraph);
	}

	@Override
	public Set<Asset> findByAssetTemplateIn(Set<AssetTemplate> assetTemplates) {
		return assetRepository.findByAssetTemplateIn(assetTemplates);
	}

	@Override
	public List<AssetBean> getAssetBeansNotInGraph(List<AssetBean> assetBeansInGraph, Set<AssetTemplate> assetTemplates) {
		Set<Asset> allAssetsInWorkspace = new HashSet<>();
		if(!assetTemplates.isEmpty()){
			allAssetsInWorkspace = findByAssetTemplateIn(assetTemplates);
		}
		List<AssetBean> listAssetBeansInWorkspace = getAssetBeansFromAssets(allAssetsInWorkspace);
		
		if(!assetBeansInGraph.isEmpty()){
			listAssetBeansInWorkspace.removeAll(assetBeansInGraph);
		}
		
		return listAssetBeansInWorkspace;
	}

	@Override
	public Set<Asset> findByAssetTemplate(AssetTemplate assetTemplate) {

		Set<Asset> getAsset = assetRepository.findByAssetTemplate(assetTemplate);
		for(Asset oneAd : getAsset) {
			Iterator<AssetData> i  = oneAd.getAssetDatas().iterator();
			while(i.hasNext()) {
				AssetData oneAssetData = i.next();
				if(oneAssetData.getData().equalsIgnoreCase("")) {
					i.remove();
				}
			}
		}	
		return getAsset;
	}

	@Override
	public List<AssetBean> getAssetBeansFromAssets(Set<Asset> assets) {
		List<AssetBean> listOfAssetBeans = new ArrayList<>();
		for (Asset asset : assets) {
			listOfAssetBeans.add(new AssetBean(asset));
		}
		return listOfAssetBeans;
	}

	@Override
	public AssetData createAssetDataObject(String columnData, String reqParam, Asset asset) {
		AssetData assetData = new AssetData();
		
		int templateCol = 0;
		
		try{
			templateCol = Integer.parseInt(StringUtils.trim(reqParam));
			AssetTemplateColumn theColum =  assetTemplateColumnService.findOne(templateCol);
			if (theColum.getAssetTemplate().getId().equals(asset.getAssetTemplate().getId())){
				assetData.setAssetTemplateColumn(theColum);
				if(theColum.getDataType().equals(DataType.DATE.name())){
					setValidDateInAsset(columnData, assetData);
				}else{
					assetData.setData(columnData);
				}
				assetData.setAsset(asset);
				return assetData;
			}
		} catch (NumberFormatException nfe ){
			logger.error("Exception when parse parameter:" + reqParam, nfe);
		}

		return null;
	}

	/**
	 * @param columnData
	 * @param assetData
	 */
	private void setValidDateInAsset(String columnData, AssetData assetData) {
		if(Utility.isValidDate(columnData)){
			assetData.setData(columnData);
		}
	}

	@Override
	public List<AssetBean> getListOfAssetBean(Collection<Asset> assets) {
		List<AssetBean> assetBeans = new ArrayList<>();
		for (Asset asset : assets) {
			if (!asset.isDeleteStatus()) {
				AssetBean assetBean = new AssetBean(asset);
				Iterator<TemplateColumnBean> oneTemp = assetBean.getAssetDatas().iterator();
				 while(oneTemp.hasNext()) {
					 TemplateColumnBean oneAssetData = oneTemp.next();
					 if(oneAssetData.getSequenceNumber() ==2 || oneAssetData.getSequenceNumber() ==3)
						if(oneAssetData.getData().equalsIgnoreCase("N-A")) {
							oneTemp.remove();
						}
				}
				assetBeans.add(assetBean);
			}
		}
		return assetBeans;
	}
	
	
	@Override
	public List<AssetBean> findAllAssetsInWorkspace(Workspace activeWorkSpaceForUser) {
		Workspace [] listOfWorkspaces = {activeWorkSpaceForUser};
		List<AssetTemplate> listOfTemplates =  templateRepository.findByWorkspaceIn(Arrays.asList(listOfWorkspaces));
		
		List<AssetBean> assetsToReturn = new ArrayList<>();
		for(AssetTemplate oneAssetTemplate : listOfTemplates){
			oneAssetTemplate.getAssets().size();
			assetsToReturn.addAll(getListOfAssetBean(oneAssetTemplate.getAssets()));
		}
		
		return assetsToReturn;
	}

	@Override
	public Set<AssetBean> getSetOfAssetBeansByIdsAndQeId(Set<Integer> idsOfAssetTemplate, Integer qeId) {
		Set<AssetBean> assetBeans = new HashSet<>();
		
		Set<QuestionnaireAsset> questionnaireAssets = qaService.findByQuestionnaireLoadedAsset(qeService.findOne(qeId));
		
		Set<Integer> idsOfAsset = new HashSet<>();
		for (QuestionnaireAsset qa : questionnaireAssets) {
			idsOfAsset.add(qa.getAsset().getId());
		}
		
		List<AssetTemplate> assetTemplates = assetTemplateService.findByIdIn(idsOfAssetTemplate);
		Set<Asset> assets = findByIdInAndAssetTemplateIn(idsOfAsset, new HashSet<AssetTemplate>(assetTemplates));
		for (Asset asset : assets) {
			assetBeans.add(new AssetBean(asset));
		}
		
		return assetBeans;
	}

	@Override
	public Set<Asset> findByIdInAndAssetTemplateIn(Set<Integer> idsOfAsset,	Set<AssetTemplate> assetTemplates) {
		return assetRepository.findByIdInAndAssetTemplateIn(idsOfAsset, assetTemplates);
	}

	@Override
	public Set<Asset> findAssetsByWorkspaceIdAndAssetTypeId(Workspace workspaceId, AssetType assetTypeId) {
		return assetRepository.findAssetsByWorkspaceIdAndAssetTypeId(workspaceId, assetTypeId);
	}

	@Override
	public List<Asset> save(List<Asset> listOfAssets) {
		return assetRepository.save(listOfAssets);
	}
	@Override
	public Asset findByUid(String uid) {
		return assetRepository.findByUid(uid);
	}

	@Override
	public List<AssetBean> findAllAssetsInWorkspaceByAssetType(List<Workspace> activeWorkSpaceForUsers, AssetType assetType) {
		List<AssetTemplate> listOfTemplates =  templateRepository.findByAssetTypeAndWorkspaceIn(assetType, activeWorkSpaceForUsers);
		
		List<AssetBean> assetsToReturn = new ArrayList<>();
		for(AssetTemplate oneAssetTemplate : listOfTemplates){
			oneAssetTemplate.getAssets().size();
			assetsToReturn.addAll(getListOfAssetBean(oneAssetTemplate.getAssets().stream().filter(asset -> !asset.isDeleteStatus()).collect(Collectors.toList())));
		}
		
		return assetsToReturn;
	}

	@Override
	public Asset createNewAssetObject(AssetTemplate assetTemplate, User currentUser) {
		Asset asset = new Asset();
		asset.setUserByCreatedBy(currentUser);
		asset.setUserByUpdatedBy(currentUser);
		asset.setCreatedDate(new Date());
		asset.setUpdatedDate(new Date());
		asset.setAssetTemplate(assetTemplate);
		return asset;
	}

	@Override
	public String getAssetNameForUID(Asset asset) {
		String assetName = "";
		for (AssetData oneAssetdata :asset.getAssetDatas()){
			if(oneAssetdata.getAssetTemplateColumn().getAssetTemplateColName().equalsIgnoreCase(Constants.DEFAULT_COLUMN_NAME)){
				assetName = oneAssetdata.getData();
				break;
			}					
		}
		return assetName;
	}
	
	@Override
	public List<Asset> findAssetsInWorkspaceByAssetType(List<Workspace> activeWorkSpaceForUsers, AssetType assetType) {
		List<AssetTemplate> listOfTemplates =  templateRepository.findByAssetTypeAndWorkspaceIn(assetType, activeWorkSpaceForUsers);
		
		List<Asset> assetsToReturn = new ArrayList<>();
		for(AssetTemplate oneAssetTemplate : listOfTemplates){			
			assetsToReturn.addAll(oneAssetTemplate.getAssets());
		}
		
		return assetsToReturn;
	}

	@Override
	public List<Asset> saveAssetAndRelationship(List<Asset> listOfAssets,AssetTemplate assetTemplate) {
		List<Asset> assets = assetRepository.save(listOfAssets);
		if (!assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
		for(Asset oneAsset: assets) {
			assetAuditTrailService.createNewAssetAuditTrailObject(oneAsset,customUserDetailsService.getCurrentUser());
		}
		}
		if (assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			for(Asset oneAsset : assets){
				List<RelationshipAssetData> radListForParent = new ArrayList<>(oneAsset.getRelationshipAssetDatasForParentAssetId());
				List<RelationshipAssetData> radListForChild = new ArrayList<>(oneAsset.getRelationshipAssetDatasForChildAssetId());
			    relationshipTemplateService.createNewRelationshipAssetDataObject(oneAsset, radListForParent.get(0).getAssetByParentAssetId(),radListForChild.get(0).getAssetByChildAssetId());
			}
		}
		return assets;
	}

	@Override
	public Map<Integer, RelationshipAssetDataBean> getAssetsRelationship(AssetTemplate assetTemplate, Set<Asset> assets) {
		Map<Integer, RelationshipAssetDataBean> mapOfRelationshipAssetDataBean = new HashMap<>();
		if(assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			Set<RelationshipAssetData> listOfRelationshipAssetDatas = new HashSet<>();
			if(!assets.isEmpty())
				listOfRelationshipAssetDatas = relationshipTemplateService.findByAssetByAssetIdIn(assets);
			for (RelationshipAssetData relationshipAssetData : listOfRelationshipAssetDatas) {
			
				RelationshipAssetDataBean relationshipAssetDataBean = new RelationshipAssetDataBean(relationshipAssetData);
				if (!assetRepository.findOne(relationshipAssetDataBean.getChildAsset().getId()).isDeleteStatus()
						&& !assetRepository.findOne(relationshipAssetDataBean.getParentAsset().getId()).isDeleteStatus()){
					mapOfRelationshipAssetDataBean.put(relationshipAssetData.getAssetByAssetId().getId(),relationshipAssetDataBean);
				}							
			}
		}
		return mapOfRelationshipAssetDataBean;
	}
	

	@Override
	public List<Integer> getFilteredAssetIdFromElasticSearch(Map<String, List<String>> filterMap) {

		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/asset/id";

		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		List<Integer> assetIds = new ArrayList<>();
		List<Integer> list = new ArrayList<>();

		SearchConfig searchConfig = setSearchConfigForAssetId(filterMap);

		try {
			assetIds = (List<Integer>)restTemplate.postForObject(uri, searchConfig, List.class);
			list = mapper.convertValue(assetIds, new TypeReference<List<Integer>>() { });
		} catch (RestClientException e) {
			logger.error("Exception occured in AssetServiceImpl :: getFilteredAssetIdFromElasticSearch() :", e);
		}
		return list;
	}

	private SearchConfig setSearchConfigForAssetId(Map<String, List<String>> filterMap) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig = new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String> list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		searchConfig.setFilterMap(filterMap);

		return searchConfig;
	}

	@Override
	public List<Asset> findListOfAssetsByIds(List<Integer> id) {
		
		return assetRepository.findByIdIn(id);
	}

	@Override
	public Asset updateAsset(HttpServletRequest request, AssetTemplate assetTemplate, Integer assetid) {
		
		List<String> requestParameterNames = removeCsrfTokenAndId(request);
		Asset assets = updateAssetObject(request, assetTemplate, requestParameterNames, assetid);
		Asset savedAsset =assetRepository.save(assets);
		return savedAsset;
	}

	private List<String> removeCsrfTokenAndId(HttpServletRequest request) {
		List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());
		requestParameterNames.removeIf(value-> value.contains(Constants.REQUEST_PARAM_CSRF) || value.contains("id"));
		return requestParameterNames;
	}

	/**
	 * update new {@code Asset} Object.
	 * @param request
	 * @param templateId
	 * @param requestParameterNames
	 * @return {@code Asset}
	 */
	private Asset updateAssetObject(HttpServletRequest request, AssetTemplate assetTemplate, List<String> requestParameterNames,Integer assetid ) {
		
        Asset asset = assetRepository.findOne(assetid);
		asset.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		asset.setUpdatedDate(new Date());
		
		// get tenant id,asset type,asset template id,asset name for uid
		String assetType = assetTemplate.getAssetType().getAssetTypeName();
		Integer assetTemplateId = assetTemplate.getId();
		String tenantId = currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		asset.setAssetDatas(updateSetOfAssetData(request, requestParameterNames, asset,assetid));
		// Generate UId for a asset
		asset.setUid(Utility.generateUid(tenantId,assetType, assetTemplateId, getAssetNameForUID(asset)));
		
		
		return asset;
	}
	
	/**
	 * update set of asset data.
	 * @param request
	 * @param requestParameterNames
	 * @param asset
	 * @return {@code Set<AssetData>}
	 */
	private Set<AssetData> updateSetOfAssetData(HttpServletRequest request, List<String> requestParameterNames, Asset asset,Integer assetid) {
		Set<AssetData> assetDatas = new HashSet<>();
//		Iterator<String> nonLifeCycleRequestParam = requestParameterNames.iterator();
//		while (nonLifeCycleRequestParam.hasNext()) {
//		    String lifecycle = nonLifeCycleRequestParam.next();
//		    if (lifecycle.startsWith("lifeCycle")) {
//		    	nonLifeCycleRequestParam.remove();
//		    }
//		}
		List<AssetLifeCycleStageJson> assetLifeCycleStageJsons = listOfAssetLifeCycle(request);	
		List<AssetData> assetDatass = getAssetDatasForLifeCycle(assetLifeCycleStageJsons,asset);
		
		Map<String,Integer> mapOfColIdAndAssetDataId =	new HashMap<>();
    	asset.getAssetDatas().forEach(e->mapOfColIdAndAssetDataId.put(String.valueOf(e.getAssetTemplateColumn().getId()),e.getId()));
		for (String parameterName : requestParameterNames) {
			if (!parameterName.equalsIgnoreCase(Constants.TEMPLATE_ID)) {
				AssetData newData = updateAssetDataObject(request.getParameter(parameterName), parameterName, asset,mapOfColIdAndAssetDataId);
				if (newData != null){
					assetDatas.add(newData);
				}
			}
		}
		// update lifeCycle 
		for(AssetData oneLifeCycleData :assetDatass) {
			AssetData newDataForLifeCycle = updateAssetDataObject(oneLifeCycleData.getData(), oneLifeCycleData.getAssetTemplateColumn().getId().toString(), asset,mapOfColIdAndAssetDataId);
			if (newDataForLifeCycle != null){
				assetDatas.add(newDataForLifeCycle);
			}
		}
		return assetDatas;
	}

	private AssetData updateAssetDataObject(String columnData, String reqParam, Asset asset,Map<String,Integer> mapOfColIdAndAssetDataId) {
		AssetData assetData = new AssetData();
		int templateCol = 0;
		
		try{
			templateCol = Integer.parseInt(StringUtils.trim(reqParam));
			AssetTemplateColumn theColum =  assetTemplateColumnService.findOne(templateCol);
			if (theColum.getAssetTemplate().getId().equals(asset.getAssetTemplate().getId())){
				assetData.setId(mapOfColIdAndAssetDataId.get(reqParam));
				assetData.setAssetTemplateColumn(theColum);
				if(theColum.getDataType().equals(DataType.DATE.name())){
					setValidDateInAsset(columnData, assetData);
				}else{
					assetData.setData(columnData);
				}
				assetData.setAsset(asset);
				return assetData;
			}
		} catch (NumberFormatException nfe ){
			logger.error("Exception when parse parameter:" + reqParam, nfe);
		}

		return null;
	}

	@Override
	public List<AssetBarChartWrapper> getAssetBarChartWrapper(Integer idOfAsset) {
		
		Asset asset = findOne(idOfAsset);
		List<AssetParameterHealthBean> assetParamHealthBeanList = assetParamHealthService.getAssetViewHealthParameter(asset);
		List<AssetBarChartWrapper> wrapper = new ArrayList<>();
		AssetParameterHealthBean assetParmeterHealthBean = assetParamHealthBeanList.get(assetParamHealthBeanList.size()-1);
			wrapper.add(new AssetBarChartWrapper(assetParmeterHealthBean.getParameterBean().getDisplayName(),assetParmeterHealthBean.getParameterValue(),assetParmeterHealthBean.getAssetBean().getId(),assetParmeterHealthBean.getParameterBean().getId(),assetParmeterHealthBean.getQuestionnaire().getId(),assetParmeterHealthBean.getQuestionnaire().getName()));
		
		return wrapper;
	}

	@Override
	public void softDeleteAsset(Integer assetId) {

		Set<Workspace> listOfWorkspaces = new HashSet <> ();
		List<Questionnaire> listOfQuestionnaire = new ArrayList<>();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		if(!listOfWorkspaces.isEmpty()){
			listOfQuestionnaire =questionnaireService.findByWorkspaceInAndQuestionnaireType(listOfWorkspaces,QuestionnaireType.DEF.toString());
		}
		
		
		listOfQuestionnaire.stream().filter(questionnaire -> questionnaire.getQuestionnaireAssets().removeIf(q->q.getAsset().getId()==assetId));
		
		assetRepository.updateDeleteStatus(assetId);
	}
	@Override
	public Asset findByUidAndDeleteStatus(String uid , boolean deleteStatus) {
		return assetRepository.findByUidAndDeleteStatus(uid , deleteStatus);
	}

	@Override
	public Set<Asset> findByAssetTemplateAndDeleteStatus(AssetTemplate assetTemplate, boolean deleteStatus) {
		// TODO Auto-generated method stub
		return assetRepository.findByAssetTemplateAndDeleteStatus(assetTemplate, false);
	}
	@Override
	public List<AssetLifeCycleStageJson> listOfAssetLifeCycle(HttpServletRequest request) {
		List<AssetLifeCycleStageJson> listofAssetLifeCycleStages = new ArrayList<>();
		String cleansedParam=null ;
		String cleansedParam1=null ;
		Integer compareid=0 ; 
		Integer idOfLifeCycle ;
		Set<String> keyst = request.getParameterMap().keySet() ;
		List<String> keycompare = new ArrayList<>();
		for( String lifestring : keyst)
		{
			if( keycompare.contains(lifestring))
				continue;
			if( !lifestring.startsWith("lifeCycle_"))
				continue;			
			if (lifestring.startsWith("lifeCycle_")){
			cleansedParam = lifestring.replaceAll("lifeCycle_", "");
			if(cleansedParam.startsWith("t") || cleansedParam.startsWith("f")  ) {
				cleansedParam = cleansedParam.substring(1);
			}
			}		
			idOfLifeCycle = Integer.parseInt(cleansedParam);
			if(!compareid.equals(0) && idOfLifeCycle.equals(compareid))
				continue;
			AssetLifeCycleStageJson asststjson= null;
			for( String lifestrng : request.getParameterMap().keySet())
			{
				if( keycompare.contains(lifestrng))
					continue;
				if(asststjson == null)
					asststjson= new AssetLifeCycleStageJson();				
				if( !lifestrng.startsWith("lifeCycle_"))
					continue;	
				String keytouse = lifestrng ;
				if (lifestrng.startsWith("lifeCycle_")){
					cleansedParam1 = lifestrng.replaceAll("lifeCycle_", "");
					if(cleansedParam1.startsWith("t") || cleansedParam1.startsWith("f")  ) {
						cleansedParam1 = cleansedParam1.substring(1);
					}
							
					Integer idOfLifeCycle1 = Integer.parseInt(cleansedParam1);
					if( idOfLifeCycle1.equals(idOfLifeCycle) )
					{
						if(!(lifestrng.charAt(10)=='t') && !(lifestrng.charAt(10)=='f')  )
							lifestrng=lifestrng.substring(0, 10)+"s";
						else
							lifestrng=lifestrng.substring(0, 11);
						asststjson.setCycleValues(lifestrng, request.getParameterMap().get(keytouse)[0]);
						keycompare.add(keytouse);
					}
					if(asststjson.getLifecyc().get("id")==null) {
					asststjson.setCycleValues("id", Integer.toString(idOfLifeCycle1));
					}
					keytouse=null ;
					if(asststjson.getLifecyc().size()==4) {
					listofAssetLifeCycleStages.add(asststjson);
					asststjson=null;//
					break;
					} 
				}
			}			
			
			compareid= idOfLifeCycle;
			
		}
		return listofAssetLifeCycleStages;
	}

	@Override
	public List<AssetData> getAssetDatasForLifeCycle(List<AssetLifeCycleStageJson> assetLifeCycleStageJsons,Asset asset) {
		
		List<AssetData> assetDataLst = new ArrayList<>();		
		String json = null;
		for(AssetLifeCycleStageJson oneAssetLifeCycleJson:assetLifeCycleStageJsons) {
			
			AssetData lifeCycleStageData = new AssetData();
			try {
				json = new ObjectMapper().writeValueAsString(oneAssetLifeCycleJson.getLifecyc());
				lifeCycleStageData.setData(json);
				lifeCycleStageData.setAsset(asset);
				
				AssetTemplateColumn theColum =  assetTemplateColumnService.findOne(Integer.parseInt((String)oneAssetLifeCycleJson.getLifecyc().get("id")));
				lifeCycleStageData.setAssetTemplateColumn(theColum);
				
			}
			catch(Exception ex )
			{
				ex.printStackTrace();
			}
			
			assetDataLst.add(lifeCycleStageData);
			lifeCycleStageData=null;
		}	

		return assetDataLst;
	}
}
