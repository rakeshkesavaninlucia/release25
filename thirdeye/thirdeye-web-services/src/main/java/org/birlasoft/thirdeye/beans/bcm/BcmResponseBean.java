package org.birlasoft.thirdeye.beans.bcm;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.BcmLevelBean;

public class BcmResponseBean {

	private BcmLevelBean bcmLevelBean;
	private Map<AssetBean, BigDecimal> assetResponse = new HashMap<AssetBean, BigDecimal>();
	
	public void addAssetEvaluation (AssetBean ab, BigDecimal d){
		if (ab != null){
			assetResponse.put(ab, d);
		}
	}

	public Set<AssetBean> getAssets(){
		return assetResponse.keySet();
	}
	
	public BigDecimal fetchParamValueForAsset(AssetBean ab){
		return assetResponse.get(ab);
	}
	
	public BcmLevelBean getBcmLevelBean() {
		return bcmLevelBean;
	}

	public void setBcmLevelBean(BcmLevelBean bcmLevelBean) {
		this.bcmLevelBean = bcmLevelBean;
	}

	public Map<Integer, BigDecimal> fetchAssetMappedByAssetId(){
		 Map<Integer, BigDecimal> responseMap = new HashMap<Integer, BigDecimal>();
		 for (AssetBean oneBean : assetResponse.keySet() ){
			 responseMap.put(oneBean.getId(), assetResponse.get(oneBean));
		 }
		 
		 return responseMap;
	}
}
