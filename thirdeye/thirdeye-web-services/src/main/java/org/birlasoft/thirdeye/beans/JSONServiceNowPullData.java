package org.birlasoft.thirdeye.beans;

public class JSONServiceNowPullData {

	private String IncidentId;
	private Integer Priority;
	private String Category;
	private String IncidentDescription;
	public String getIncidentId() {
		return IncidentId;
	}
	public void setIncidentId(String incidentId) {
		IncidentId = incidentId;
	}
	public Integer getPriority() {
		return Priority;
	}
	public void setPriority(Integer priority) {
		Priority = priority;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getIncidentDescription() {
		return IncidentDescription;
	}
	public void setIncidentDescription(String incidentDescription) {
		IncidentDescription = incidentDescription;
	}
	
	
	
}
