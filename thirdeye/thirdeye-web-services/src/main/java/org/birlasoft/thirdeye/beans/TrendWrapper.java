package org.birlasoft.thirdeye.beans;

import java.util.List;


public class TrendWrapper {

	private String xAxisLabel;
	private String yAxisLabel;
	private List<TrendData> trendData ;
	
	public String getxAxisLabel() {
		return xAxisLabel;
	}
	public void setxAxisLabel(String xAxisLabel) {
		this.xAxisLabel = xAxisLabel;
	}
	public String getyAxisLabel() {
		return yAxisLabel;
	}
	public void setyAxisLabel(String yAxisLabel) {
		this.yAxisLabel = yAxisLabel;
	}
	public List<TrendData> getTrendData() {
		return trendData;
	}
	public void setTrendData(List<TrendData> trendData) {
		this.trendData = trendData;
	}
}
