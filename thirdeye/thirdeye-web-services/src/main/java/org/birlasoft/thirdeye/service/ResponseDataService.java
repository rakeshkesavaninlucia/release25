package org.birlasoft.thirdeye.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;

/**
 * Service interface for response data.
 * @author samar.gupta
 */
public interface ResponseDataService {
	/**
	 * save response data object.
	 * @param responseData
	 * @return {@code ResponseData}
	 */
	public ResponseData save(ResponseData responseData);
	/** Find list of response data by response.
	 * @param response
	 * @return {@code List<ResponseData>}
	 */
	public List<ResponseData> findByResponse(Response response);
	/**
	 * Find list of response data by questionnaire question.
	 * @param questionnaireQuestion
	 * @return {@code List<ResponseData>}
	 */
	public List<ResponseData> findByQuestionnaireQuestion(QuestionnaireQuestion questionnaireQuestion);
	/**
	 * Find one response data object by id.
	 * @param id
	 * @param loaded
	 * @return {@code ResponseData}
	 */
	public List<ResponseData> findByQuestionnaireQuestionIn(Collection<QuestionnaireQuestion> questionnaireQuestion);
	/** 
	 * Find response data for given set of questionnaireQuestion
	 * @param idj
	 * @return {@code ResponseData}
	 */
	public List<QuestionnaireQuestion> findDistinctQuestionnaireQuestionByQuestionnaireQuestionId(Set<QuestionnaireQuestion> questionnaireQuestion);
	/**
	 * Find Distinct QuestionnaireQuestion from responseData for given set of QuestionnaireQuestionId  
	 * @param id
	 * @param loaded
	 * @return {@code ResponseData}
	 */
	
	public ResponseData findOne(Integer id, boolean loaded);
	/**
	 * Find by questionnaire question and response.
	 * @param questionnaireQuestion
	 * @param response
	 * @return {@code ResponseData}
	 */
	public ResponseData findByQuestionnaireQuestionAndResponse(QuestionnaireQuestion questionnaireQuestion, Response response, boolean loaded );
	
	/**
	 * @param responseList
	 * @return
	 */
	public List<ResponseData> save(List<ResponseData> responseList);
}
