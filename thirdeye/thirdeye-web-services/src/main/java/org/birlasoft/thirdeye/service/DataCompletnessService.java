package org.birlasoft.thirdeye.service;

import java.math.BigDecimal;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;

/**
 * Get data completion view percentage
 * @author mehak.guglani
 * 
 */
public interface DataCompletnessService {

	/**
	 * To get completion percentage  
	 * @param numberOfQQ
	 * @param numberOfResponseData
	 * @return percentage in BigDecimal 
	 */
	public BigDecimal getDataCompletionViewForAsset(Integer numberOfQQ, Integer numberOfResponseData);

	/**
	 * Get {@code Set} of {@link QuestionnaireQuestion} for Asset.
	 * @param asset
	 * @return
	 */
	public Set<QuestionnaireQuestion> getQuestionsForAsset(Asset asset);

	/**
	 * Get number of questions which are responded
	 * @param setOfQuestionnaireQuestions
	 * @return
	 */
	public int getNumberOfQuestionsResponded(Set<QuestionnaireQuestion> setOfQuestionnaireQuestions);

}
