package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

public class JSONSNOWDataMapper {

	private List<JSONServiceNowPullData> results = new ArrayList<>();
	
	public JSONSNOWDataMapper(){
		
	}

	public List<JSONServiceNowPullData> getResults() {
		return results;
	}

	public void setResults(List<JSONServiceNowPullData> results) {
		this.results = results;
	}

	/**
	 * Add one {@link JSONServiceNowPullData} to {@link List}
	 * @param oneValue
	 */
	public void addOption (JSONServiceNowPullData oneValue){
		if (oneValue != null){
			results.add(oneValue);
		}
	}

}


