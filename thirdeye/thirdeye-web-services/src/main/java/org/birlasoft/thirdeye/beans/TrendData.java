package org.birlasoft.thirdeye.beans;

import java.util.Date;

public class TrendData {
	
	private String date;
	private Integer close;
	private Integer more;
	private String xParameterName;
	private String yParameterName;
	
	public TrendData(String date, Integer close, Integer more, String xParameterName, String yParameterName) {
		this.date = date;
		this.close = close;
		this.more = more;
		this.xParameterName = xParameterName;
		this.yParameterName = yParameterName;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getClose() {
		return close;
	}
	public void setClose(Integer close) {
		this.close = close;
	}
	public Integer getMore() {
		return more;
	}
	public void setMore(Integer more) {
		this.more = more;
	}
	public String getxParameterName() {
		return xParameterName;
	}
	public void setxParameterName(String xParameterName) {
		this.xParameterName = xParameterName;
	}
	public String getyParameterName() {
		return yParameterName;
	}
	public void setyParameterName(String yParameterName) {
		this.yParameterName = yParameterName;
	}
}
