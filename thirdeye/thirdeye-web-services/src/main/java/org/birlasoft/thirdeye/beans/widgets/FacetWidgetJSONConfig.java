package org.birlasoft.thirdeye.beans.widgets;

import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.util.Utility;


public class FacetWidgetJSONConfig extends BaseWidgetJSONConfig {

	private int questionnaireId;
	private int questionId;
	private int paramId;
	private String graphType;
	
	public FacetWidgetJSONConfig() {
		// TODO Auto-generated constructor stub
	}
	
	public FacetWidgetJSONConfig(Widget w) {
		super(w);
		
		FacetWidgetJSONConfig deserialized = Utility.convertJSONStringToObject(w.getWidgetConfig(), FacetWidgetJSONConfig.class);
		if (deserialized != null){
			this.questionId = deserialized.getQuestionId();
			this.questionnaireId = deserialized.getQuestionnaireId();
			this.graphType = deserialized.graphType;
			this.paramId = deserialized.paramId;
		}
				
	}
	public int getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getGraphType() {
		return graphType;
	}

	public void setGraphType(String graphType) {
		this.graphType = graphType;
	}

	public int getParamId() {
		return paramId;
	}

	public void setParamId(int paramId) {
		this.paramId = paramId;
	}
	
}
