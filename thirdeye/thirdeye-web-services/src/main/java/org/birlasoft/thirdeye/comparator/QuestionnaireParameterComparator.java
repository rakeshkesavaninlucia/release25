package org.birlasoft.thirdeye.comparator;

import java.util.Comparator;

import org.birlasoft.thirdeye.entity.QuestionnaireParameter;

/**
 * Comparator to compare ParameterBean   objects .
 * @author sunil1.gupta
 *
 */
public class QuestionnaireParameterComparator implements Comparator<QuestionnaireParameter> {	
	
	@Override
	public int compare(QuestionnaireParameter o1, QuestionnaireParameter o2) {
		if(o1.getQuestionnaire().getId() == o2.getQuestionnaire().getId() 
				&& o1.getParameterByParameterId().getId() ==  o2.getParameterByParameterId().getId() 
				&& o1.getParameterByParentParameterId() !=null && o1.getParameterByParentParameterId().getId() ==  o2.getParameterByParentParameterId().getId()
				&& o1.getParameterByRootParameterId().getId() == o2.getParameterByRootParameterId().getId()){
    		return 0;
    	}
    	return 1;
	}
}
