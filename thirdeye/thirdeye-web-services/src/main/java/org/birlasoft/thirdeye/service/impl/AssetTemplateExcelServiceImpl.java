package org.birlasoft.thirdeye.service.impl;

import java.math.BigInteger;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.AssetLifeCycleExcelBean;
import org.birlasoft.thirdeye.beans.AssetLifeCycleStagesBean;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.ApplicationLifeCycleStages;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.DataType;
import org.birlasoft.thirdeye.constant.ExportPurpose;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.Country;
import org.birlasoft.thirdeye.entity.ExportLog;
import org.birlasoft.thirdeye.entity.RelationshipAssetData;
import org.birlasoft.thirdeye.entity.RelationshipTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateColumnService;
import org.birlasoft.thirdeye.service.AssetTemplateExcelService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DensityHeatMapService;
import org.birlasoft.thirdeye.service.ExcelWriter;
import org.birlasoft.thirdeye.service.ExportLogService;
import org.birlasoft.thirdeye.service.RelationshipTemplateService;
import org.birlasoft.thirdeye.util.ExcelUtility;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.validators.CellDataLengthValidator;
import org.birlasoft.thirdeye.validators.CellTypeValidator;
import org.birlasoft.thirdeye.validators.ExcelCellValidator;
import org.birlasoft.thirdeye.validators.MandatoryCellValidator;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Service {@code class} for Asset Template Excel sheet generation and upload
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetTemplateExcelServiceImpl implements AssetTemplateExcelService {

	private static Logger logger = LoggerFactory.getLogger(AssetTemplateExcelServiceImpl.class);

	@Autowired
	private ExportLogService exportLogService;
	@Autowired
	private ExcelWriter excelWriter;
	@Autowired
	private AssetTemplateColumnService assetTemplateColumnService;
	@Autowired
	private CustomUserDetailsService userDetailsService;
	@Autowired
	private DensityHeatMapService worldMapService;
	@Autowired
	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
	@Autowired
	private AssetService assetService;
	@Autowired
	private RelationshipTemplateService relationshipTemplateService;
	@Autowired
	private AssetTypeService assetTypeService;
	
	@Override
	public void exportTemplate(HttpServletResponse response,AssetTemplate assetTemplate) {
		String fileName = assetTemplate.getAssetTemplateName().replaceAll("\\s+","_");
		String hash = Utility.getUniqueHash();
		XSSFWorkbook workbook = generateAssetUploadTemplateExcel(assetTemplate, hash);
		if(workbook != null){
			exportLogService.saveExportLog(assetTemplate.getId(), hash,ExportPurpose.INV.toString());
		}
		excelWriter.writeExcel(response, workbook, fileName);
	}

	private XSSFWorkbook generateAssetUploadTemplateExcel(AssetTemplate assetTemplate, String hash) {
		
		try {
			// create a new workbook
			XSSFWorkbook workbook = new XSSFWorkbook();
			
			createAssetTemplateSheet(assetTemplate, workbook);
			if (assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
				createParentChildAssetSheet(assetTemplate, workbook);
			}
			//prepare config sheet with security token
			ExcelUtility.setupConfigSheet(hash, workbook);
			
			return workbook;
		} catch (Exception e) {
			logger.error("Error in template excel", e);
		}
		return null;
	}

	private void createParentChildAssetSheet(AssetTemplate assetTemplate,XSSFWorkbook workbook) {
		
		List<RelationshipTemplate> listOfRelationshipTemplate = relationshipTemplateService.findByAssetTemplate(assetTemplate);
     	RelationshipTemplate relationshipTemplate = listOfRelationshipTemplate.get(0);
		// parent asset sheet creation.     	
     	createSheetForAsset(workbook,relationshipTemplate.getAssetTypeByParentAssetTypeId().getId(),"ParentAsset" ,0);
     	//child asset sheet creation.     
     	createSheetForAsset(workbook,relationshipTemplate.getAssetTypeByChildAssetTypeId().getId(),"ChildAsset",1);
 	}

	private void createSheetForAsset(XSSFWorkbook workbook,	Integer assetTypeId,String sheetName,int col) {
		
		int index = 0;		
		XSSFSheet sheet = workbook.createSheet(sheetName);
		XSSFRow rowColumnName = sheet.createRow(index);
		XSSFCellStyle style = getCellStyle(workbook);	
		
		
		Cell cell1 = rowColumnName.createCell(index);
		cell1.setCellValue(sheetName);	
		cell1.setCellStyle(style);
		
		List<Workspace> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(userDetailsService.getActiveWorkSpaceForUser());
		List<AssetBean> listOfAssets = assetService.findAllAssetsInWorkspaceByAssetType(listOfWorkspace, assetTypeService.findOne(assetTypeId));
		
		int rowNum = 1;
		for(AssetBean oneAssetbean : listOfAssets){
			XSSFRow oneRow = workbook.getSheet(sheetName).createRow(rowNum);
			Cell oneCell = oneRow.createCell(index);
			oneCell.setCellValue(oneAssetbean.getShortName());
			rowNum++;
		}
		
		sheet.autoSizeColumn(index);
		// protect the sheet with a password
		protectSheet(sheet);
		// add drop down on asset template sheet
		addDropDown(workbook, listOfAssets,sheetName, col);
	}

	private void protectSheet(XSSFSheet sheet) {
		Random random = new Random();
		String passwordToHash = new BigInteger(130, random).toString(32);
		sheet.protectSheet(passwordToHash);
	    // enable the locking features
		sheet.enableLocking();
	
		// fine tune the locking options (in this example we are blocking all
	    // operations on the cells: select, edit, etc.)
		 sheet.lockSelectLockedCells(true);
		 sheet.lockSelectUnlockedCells(true);
	}

	private void addDropDown(XSSFWorkbook workbook,	List<AssetBean> listOfAssets,String sheetName ,int col ) {
	 	 XSSFSheet realSheet = workbook.getSheet("AssetTemplate");
		 DataValidation dataValidation;
		 DataValidationConstraint constraint ;
		 DataValidationHelper validationHelper;
		 
		 validationHelper = new XSSFDataValidationHelper(realSheet);
		 CellRangeAddressList addressList = new  CellRangeAddressList(2,100,col,col);
		 constraint =validationHelper.createFormulaListConstraint(sheetName+"!$A$2:$A$" + listOfAssets.size()+1);
		 dataValidation = validationHelper.createValidation(constraint, addressList);
		 dataValidation.setSuppressDropDownArrow(true);
		 realSheet.addValidationData(dataValidation);
		 realSheet.autoSizeColumn(col);
	}
	
	
	
	private void addcountryDropdown(XSSFWorkbook workbook ,List<Integer> colIndexList,XSSFSheet realSheet){
        
		DataValidation dataValidation = null;
        DataValidationConstraint constraint = null;
        DataValidationHelper validationHelper = null;
        validationHelper=new XSSFDataValidationHelper(realSheet);
        List<Country> listOfCountries = worldMapService.findListOfCountry();
        
        for(int colIndex : colIndexList) {
        	CellRangeAddressList addressList = new  CellRangeAddressList(2,150,colIndex,colIndex);
        	constraint =validationHelper.createFormulaListConstraint("hidden!$A$8:$A$" + listOfCountries.size());
        	dataValidation = validationHelper.createValidation(constraint, addressList);
        	dataValidation.setSuppressDropDownArrow(true);
            realSheet.addValidationData(dataValidation);
        }
    }
	
	private void addDropDownforlifecycle(XSSFWorkbook workbook,List<Integer> colIndexList,XSSFSheet realSheet){
		 DataValidation dataValidation;
		 DataValidationConstraint constraint ;
		 DataValidationHelper validationHelper;
		 
        validationHelper=new XSSFDataValidationHelper(realSheet);
        for(int columnIndex : colIndexList) {
        	CellRangeAddressList addressList = new  CellRangeAddressList(2,150,columnIndex,columnIndex);
        	constraint =validationHelper.createFormulaListConstraint("hidden!$B$1:$B$6");
        	dataValidation = validationHelper.createValidation(constraint, addressList);
        	dataValidation.setSuppressDropDownArrow(true);
           	realSheet.addValidationData(dataValidation);
        }
       // realSheet.autoSizeColumn(col);
    }
	

	private void createAssetTemplateSheet(AssetTemplate assetTemplate, XSSFWorkbook workbook) {
		// add a new sheet to the workbook
		XSSFSheet assetSheet = workbook.createSheet("AssetTemplate");
		// create Row
		XSSFRow rowassetColID = assetSheet.createRow(0);
		XSSFRow rowtcName = assetSheet.createRow(1);
		
		// set Fonts
		XSSFCellStyle style = getCellStyle(workbook);
		// Sheet 1
		int col = 0;
		
		// Asset type relation ship then create parent child asset colum 
        if (assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
        	col = createParentChildAssetColumn(rowtcName, style, col);
		}
       
        HashMap<String,List<Integer>> dropdownColumns = createAssetTemplateColumn(assetTemplate, rowassetColID, rowtcName,style, col);
		
		rowassetColID.setZeroHeight(true);
		// Freeze just one row
		assetSheet.createFreezePane(0, 2, 0, 2);
		// Auto fit
		for (int i = 0; i < col; i++) {
			assetSheet.autoSizeColumn(i);
		}
		  if (!assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			  addDropDownValue(workbook,dropdownColumns);
			  
			  Set<String> columnIdSet = dropdownColumns.keySet();
			  Iterator<String> itr = columnIdSet.iterator();
			  while(itr.hasNext()) {
				  String columnType = itr.next();
				  if(columnType.equals("lifecycle") && dropdownColumns.get("lifecycle")!=null) {
					  addDropDownforlifecycle(workbook,dropdownColumns.get("lifecycle"),assetSheet);
				  }else if(columnType.equals("country") && dropdownColumns.get("country")!=null){
					  addcountryDropdown(workbook,dropdownColumns.get("country"),assetSheet);
				  }
				  else if(columnType.equals("date")){
				    	List<Integer> listOfdateColumnnindex= dropdownColumns.get("date");
				    	setDate(workbook,listOfdateColumnnindex);
				  }
			  }			  
		  }
		
	}
	private void setDate(XSSFWorkbook workbook,List<Integer> listOfdateColumnnindex) {

		XSSFSheet assetSheet =	workbook.getSheet("AssetTemplate");
		CellStyle cellStyle = workbook.createCellStyle();
		CreationHelper creationHelper = workbook.getCreationHelper();
		//	int rowNum = 2;
		for(int rowNum = 2;rowNum<2;rowNum++) {
			Row oneRow= assetSheet.createRow(rowNum);
			for(Integer cell:listOfdateColumnnindex) {
				Cell oneCell = oneRow.createCell(cell);
				cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("mm/yyyy"));
				oneCell.setCellStyle(cellStyle);
				}
		}
	}
	private void addDropDownValue(XSSFWorkbook workbook,HashMap<String,List<Integer>> dropDownColumns) {

		XSSFSheet hidden = workbook.createSheet("hidden");
		for(String dataTYpe:dropDownColumns.keySet()) {  
			if(dataTYpe.equals("country") && dropDownColumns.get(dataTYpe).size()>0) {
				List<Country> listOfCountries = worldMapService.findListOfCountry();
				List<String> countryList = new ArrayList<String>();
				for(Country oneCountry :listOfCountries ) {
					countryList.add(oneCountry.getCountryName());
				}
				String[] countryArray = countryList.stream().toArray(String[]::new);	        
				int rowNum = 7;
				int index = 0;
				for(String oneValue : countryArray){
					XSSFRow row = hidden.createRow(rowNum);
					XSSFCell oneCell = row.createCell(index);
					oneCell.setCellValue(oneValue);
					rowNum++;
				}
			}else if(dataTYpe.equals("lifecycle") && dropDownColumns.get(dataTYpe).size()>0) {
				List<ApplicationLifeCycleStages> lifestages = Arrays.asList(ApplicationLifeCycleStages.values());
				int rowNum = 0;
				int index = 1;

				for(ApplicationLifeCycleStages oneLifeCycle : lifestages){
					XSSFRow row = hidden.createRow(rowNum);
					XSSFCell oneCell = row.createCell(index);
					oneCell.setCellValue(oneLifeCycle.getStages());
					rowNum++;
				}
			}
		}
		workbook.setSheetHidden(1, true);		       
	}
	private int createParentChildAssetColumn(XSSFRow rowtcName,	XSSFCellStyle style, int col) {
		int column = col;
		Cell cellId1 = rowtcName.createCell(column);
		cellId1.setCellValue("Parent Asset");	
		cellId1.setCellStyle(style);
		
		Cell cellId2 = rowtcName.createCell(++column);
		cellId2.setCellValue("Child Asset");	
		cellId2.setCellStyle(style);
		return ++column;
	}

	private HashMap<String,List<Integer>> createAssetTemplateColumn(AssetTemplate assetTemplate,
			XSSFRow rowassetColID, XSSFRow rowtcName, XSSFCellStyle style, int r) {
		int ColumnIndex = r;
		HashMap<String,List<Integer>> dropDownColumnsMap = new HashMap<>(); 
		
		List<AssetTemplateColumn> assetTemplateColumns = assetTemplateColumnService.findByAssetTemplate(assetTemplate);	
		Iterator<AssetTemplateColumn> assetTempIter =  assetTemplateColumns.iterator();
		while (assetTempIter.hasNext()) {
			AssetTemplateColumn oneTemplateCol = assetTempIter.next();
			if(oneTemplateCol.getSequenceNumber() == 2 || oneTemplateCol.getSequenceNumber() == 3) {
				if(oneTemplateCol.isMandatory() == false) {
					assetTempIter.remove();
				}
			}
		}
		
		Collections.sort(assetTemplateColumns,new SequenceNumberComparator());
		List<Integer> lifeCycleColList = new ArrayList<>();
		List<Integer> countryColList = new ArrayList<>();
		List<Integer> fromAndTo = new ArrayList<>();
		
		for (AssetTemplateColumn atc : assetTemplateColumns) {
			
			if(atc.getDataType().equals(DataType.JSON.toString())) {
			Cell hideCellId = rowassetColID.createCell(ColumnIndex);
			hideCellId.setCellValue(atc.getId().toString());
			Cell cellId = rowtcName.createCell(ColumnIndex);
			cellId.setCellValue(atc.getAssetTemplateColName());
			cellId.setCellStyle(style);
			lifeCycleColList.add(ColumnIndex);			
			ColumnIndex++;
			
			
			hideCellId = rowassetColID.createCell(ColumnIndex);
			hideCellId.setCellValue(atc.getId()+"_from");
			cellId = rowtcName.createCell(ColumnIndex);
			cellId.setCellValue("From");
			cellId.setCellStyle(style);
			fromAndTo.add(ColumnIndex);
			ColumnIndex++;
			
			hideCellId = rowassetColID.createCell(ColumnIndex);
			hideCellId.setCellValue(atc.getId()+"_to");
			cellId = rowtcName.createCell(ColumnIndex);
			cellId.setCellValue("To");
			cellId.setCellStyle(style);		
			fromAndTo.add(ColumnIndex);
			}
			else {				
				Cell hideCellId = rowassetColID.createCell(ColumnIndex);
				hideCellId.setCellValue(atc.getId().toString());
				Cell cellId = rowtcName.createCell(ColumnIndex);
				cellId.setCellValue(atc.getAssetTemplateColName());	
				cellId.setCellStyle(style);
				if(atc.getAssetTemplateColName().equalsIgnoreCase("Country") && atc.isMandatory()==true)
					countryColList.add(ColumnIndex);
			}
			ColumnIndex++;
		//	break;
			}	
		dropDownColumnsMap.put("lifecycle",lifeCycleColList);
		dropDownColumnsMap.put("country",countryColList);
		dropDownColumnsMap.put("date",fromAndTo);
		return dropDownColumnsMap;
		}
	
	private XSSFCellStyle getCellStyle(XSSFWorkbook workbook) {
		
		XSSFFont font = workbook.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setFontName(ExcelUtility.EXCEL_FONT);
		font.setBold(true);
		// set style
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setLocked(true);
		return style;
	}

	@Override
	public void readExcel(AssetTemplate assetTemplate, Workbook workbook, List<Asset> listOfAssets, List<ExcelCellBean> listOfErrorCells) {
		List<AssetTemplateColumn> assetTemplateColumns = assetTemplateColumnService.findByAssetTemplate(assetTemplate);
		
		Iterator<AssetTemplateColumn> assetTempIter =  assetTemplateColumns.iterator();
		while (assetTempIter.hasNext()) {
			AssetTemplateColumn oneTemplateCol = assetTempIter.next();
			if(oneTemplateCol.getSequenceNumber() == 2 || oneTemplateCol.getSequenceNumber() == 3) {
				if(oneTemplateCol.isMandatory() == false) {
					assetTempIter.remove();
				}
			}
		}
		try {
			Sheet firstSheet = workbook.getSheetAt(0);

			Iterator<Row> firstSheetItr = firstSheet.iterator(); 

			Row firstRow = firstSheet.getRow(firstSheet.getFirstRowNum());
			Row secondRow = firstSheet.getRow(firstSheet.getFirstRowNum()+1);
			
			Map<Integer, AssetLifeCycleExcelBean> mapOfColumnIndexAndTemplateColumn = this.validateExcelStructure(firstRow, secondRow, assetTemplateColumns, listOfErrorCells);
     		
			Set<Object> names = new HashSet<>();

			if(listOfErrorCells.isEmpty()){
				while (firstSheetItr.hasNext()){
					Row nextRow = firstSheetItr.next();
					if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1){
						continue;
					}
					Asset asset = createNewAssetObject(assetTemplate);
					Set<AssetData> setOfAssetData = new HashSet<>();
					Set<AssetTemplateColumn> setOfTemplateColumn = new HashSet<>();	
					for(int i=0; i<nextRow.getLastCellNum(); i++) {
						Cell cell = nextRow.getCell(i, Row.CREATE_NULL_AS_BLANK);
						//To show error. 
						ExcelCellBean cellBean = new ExcelCellBean();
						cellBean.setRow(cell.getRowIndex()+1);
						cellBean.setColumn(cell.getColumnIndex()+1);
						List<ExcelCellValidator> cellValidators = new ArrayList<>();
						cellBean.setErrors(new ArrayList<String>());
						
						//validate and set asset relationship data.
						if (assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
						   if(cell.getColumnIndex() == 0){									
								validateAndSetParentAssetData(assetTemplate, asset, cell,cellBean, listOfErrorCells);
								continue;
							} 
							if(cell.getColumnIndex() == 1){
                                validateAndSetChildAssetData(assetTemplate, asset, cell, cellBean, listOfErrorCells);
								continue;								
							}
						}
						
						AssetLifeCycleExcelBean assetLifeCycleExcelBean = mapOfColumnIndexAndTemplateColumn.get(cell.getColumnIndex());
						if(assetLifeCycleExcelBean != null) {
						
						AssetTemplateColumn assetTemplateColumn = assetLifeCycleExcelBean.getAssetTemplateColumn();
						Map<String,Integer> mapOfIdAndColumn = assetLifeCycleExcelBean.getMapOfIdAndColumnIndex();
						
						//AssetTemplateColumn assetTemplateColumn = mapOfColumnIndexAndTemplateColumn.get(cell.getColumnIndex()).getAssetTemplateColumn();
						setOfTemplateColumn.add(assetTemplateColumn);
						//check for unique asset
						if (assetTemplateColumn.getAssetTemplateColName().equalsIgnoreCase(Constants.DEFAULT_COLUMN_NAME)
 								&& names.contains(cell.getStringCellValue())){
							cellBean.getErrors().add("Duplicate asset");
						}else{
							names.add(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ? cell.getNumericCellValue():cell.getStringCellValue());
						}
						this.addValidatorsToCell(assetTemplateColumn, cellValidators);

						cellBean.setCellValidators(cellValidators);

						this.validateCellData(cellBean, cell);

						if(cellBean.getErrors().isEmpty()){
							AssetData assetData = this.generateAssetData(asset, cell, assetTemplateColumn, cellBean,nextRow,mapOfIdAndColumn);
							if(assetTemplateColumn.getAssetTemplateColName().equalsIgnoreCase(Constants.DEFAULT_COLUMN_NAME)){
								updateAssetForUniqueId(assetTemplate, listOfErrorCells, asset, cellBean, assetData);
							}							
							setOfAssetData.add(assetData);
						} else {
							listOfErrorCells.add(cellBean);
						}

						asset.setAssetDatas(setOfAssetData);
					}
				}
					if(asset.getUid() == null && listOfErrorCells.isEmpty()){
						updateAssetForUniqueId(assetTemplate, listOfErrorCells, asset,new ExcelCellBean(),null);
					}
					
					Collection<AssetTemplateColumn> allTemplates = assetTemplateColumns;
					this.addDataForBlankCells(setOfTemplateColumn, allTemplates, asset);

					listOfAssets.add(asset);
				}
			}
            if(listOfAssets.isEmpty()){
            	ExcelCellBean cellBean = new ExcelCellBean();
            	List<String> errors = new ArrayList<>();
            	errors.add("No Data in excel for upload");
            	cellBean.setErrors(errors);
            	listOfErrorCells.add(cellBean);
            }
			workbook.close();

		} catch (Exception e) {
			logger.error("Error while reading excel. ", e);
			ExcelCellBean excelCellBean = new ExcelCellBean();
			List<String> errors = new ArrayList<>();
			errors.add("Something went wrong. Please try again or contact support.");
			excelCellBean.setErrors(errors);
			listOfErrorCells.add(excelCellBean);
		}

	}

	/**
	 * method to validate and set relation ship for child asset data
	 * @param assetTemplate
	 * @param listOfErrorCells
	 * @param asset
	 * @param cell
	 */
	private void validateAndSetChildAssetData(AssetTemplate assetTemplate, Asset asset, Cell cell, ExcelCellBean cellBean,
			List<ExcelCellBean> listOfErrorCells) {
		Set<RelationshipAssetData> setOfRelAssetData;
		this.validateRelationShipAssetData(cell,cellBean, listOfErrorCells);
		
		Asset pcAsset ;
		RelationshipAssetData relAssdata = new RelationshipAssetData();
		setOfRelAssetData = new HashSet<>(); 
		
		List<RelationshipTemplate> listOfRelationshipTemplate = relationshipTemplateService.findByAssetTemplate(assetTemplate);
		RelationshipTemplate relationshipTemplate = listOfRelationshipTemplate.get(0);
		Map<String, Asset> mapOfAsset =  getAssetMap(relationshipTemplate.getAssetTypeByChildAssetTypeId().getId());
		pcAsset = mapOfAsset.get(cell.getStringCellValue());
		
		relAssdata.setAssetByChildAssetId(pcAsset);								
		setOfRelAssetData.add(relAssdata);
		asset.setRelationshipAssetDatasForChildAssetId(setOfRelAssetData);
	}

	/**
	 *  method to validate and set relation ship for parent asset data
	 * @param assetTemplate
	 * @param listOfErrorCells
	 * @param asset
	 * @param cell
	 */
	private void validateAndSetParentAssetData(AssetTemplate assetTemplate, Asset asset, Cell cell,ExcelCellBean cellBean,
			List<ExcelCellBean> listOfErrorCells) {
		Set<RelationshipAssetData> setOfRelAssetData;
		this.validateRelationShipAssetData(cell,cellBean, listOfErrorCells);
		
		Asset pcAsset ;
		RelationshipAssetData relAssdata = new RelationshipAssetData();
		setOfRelAssetData = new HashSet<>(); 
		
		List<RelationshipTemplate> listOfRelationshipTemplate = relationshipTemplateService.findByAssetTemplate(assetTemplate);
		RelationshipTemplate relationshipTemplate = listOfRelationshipTemplate.get(0);
		Map<String, Asset> mapOfAsset =  getAssetMap(relationshipTemplate.getAssetTypeByParentAssetTypeId().getId());
		pcAsset = mapOfAsset.get(cell.getStringCellValue());
		
		relAssdata.setAssetByParentAssetId(pcAsset);								
		setOfRelAssetData.add(relAssdata);
		asset.setRelationshipAssetDatasForParentAssetId(setOfRelAssetData);
	}

	/**
	 * method to validate CellBean and get set of relationship asset data.
	 * @param assetTypeId
	 * @param cell
	 * @return {@code Set<RelationshipAssetData>}
	 */
	private void validateRelationShipAssetData(Cell cell,ExcelCellBean cellBean, List<ExcelCellBean> listOfErrorCells) {
		
		List<ExcelCellValidator> cellValidators = new ArrayList<>();
		cellValidators.add(new MandatoryCellValidator(true));
		cellBean.setCellValidators(cellValidators);
		this.validateCellData(cellBean, cell);
		if(!cellBean.getErrors().isEmpty()){
			listOfErrorCells.add(cellBean);
		}
	}

	/**
	 * method to get map of Asset type name ::ShortName   and Asset Object
	 * @param assetTemplate
	 * @return {@code Map<String, Asset>}
	 */
	private Map<String, Asset> getAssetMap(Integer assetTypeId) {
		Map<String, Asset> assetMap = new HashMap<>();		
		List<Workspace> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(userDetailsService.getActiveWorkSpaceForUser());
		List<Asset> listOfAssets = assetService.findAssetsInWorkspaceByAssetType(listOfWorkspace, assetTypeService.findOne(assetTypeId));
		for(Asset oneAsset : listOfAssets){
			AssetBean assetBean = new AssetBean(oneAsset);
			assetMap.put(assetBean.getShortName(), oneAsset);
		}
		
		return assetMap;
	}

	/**
	 * Generate asset unique id. And check if asset already exist or not
	 * @param assetTemplate
	 * @param listOfErrorCells
	 * @param asset
	 * @param cellBean
	 * @param assetData
	 */
	private void updateAssetForUniqueId(AssetTemplate assetTemplate, List<ExcelCellBean> listOfErrorCells, Asset asset,
			ExcelCellBean cellBean, AssetData assetData) {
		String assetType = assetTemplate.getAssetType().getAssetTypeName();
		Integer assetTemplateId = assetTemplate.getId();	
		String assetName;
		if(assetData != null){
		   assetName = assetData.getData();	
		}
		else{
		assetName = getAssetNameForRelationship(asset);
		}
		String tenantId = currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		String uniqueId  = Utility.generateUid(tenantId,assetType, assetTemplateId, assetName);
		if(isAssetUIDExist(uniqueId))
		{
			List<String> errors = new ArrayList<>();
    	    errors.add("Asset name already exists");
    	    cellBean.setErrors(errors);		
			listOfErrorCells.add(cellBean);
		}
		else{
			asset.setUid(uniqueId);
		}
	}

	/** method to get asset name in  relationship for uid ,when no name column is present  
	 * @param asset
	 * @return assetName
	 */
	private String getAssetNameForRelationship(Asset asset){
		
		String assetName;
		Set<RelationshipAssetData> setOfRelAssetDataForParent = asset.getRelationshipAssetDatasForParentAssetId();
		RelationshipAssetData relAssDataForParent= setOfRelAssetDataForParent.stream().findFirst().get();
		AssetBean parentAssetBean = new AssetBean(relAssDataForParent.getAssetByParentAssetId());
		
		Set<RelationshipAssetData> setOfRelAssetDataForChild = asset.getRelationshipAssetDatasForChildAssetId();
		RelationshipAssetData relAssdataForChild = setOfRelAssetDataForChild.stream().findFirst().get();
		AssetBean childAssetBean = new AssetBean(relAssdataForChild.getAssetByChildAssetId());

		assetName = parentAssetBean.getShortName() + "_"+ childAssetBean.getShortName();
		return assetName;
	}

	/**
	 * Validate excel structure. Gives error if the template column names are not correct
	 * @param firstRow
	 * @param secondRow
	 * @param assetTemplateColumns
	 * @param listOfErrorCells
	 * @return
	 */
	private Map<Integer, AssetLifeCycleExcelBean> validateExcelStructure(Row firstRow, Row secondRow, List<AssetTemplateColumn> assetTemplateColumns, List<ExcelCellBean> listOfErrorCells) {
		
		Map<String, String> mapOfExcelStructure = new HashMap<>();
		Map<Integer, AssetTemplateColumn> mapOfTemplateColumn = new HashMap<>();
		Map<String, Integer> mapOfCellValueAndColumnIndex = new HashMap<>();
			
		for (Cell oneCell : firstRow) {
			   System.out.println(oneCell.getCellType());
			   
			   switch (oneCell.getCellType()) {
			   	case Cell.CELL_TYPE_NUMERIC:
			   		mapOfExcelStructure.put(String.valueOf(oneCell.getNumericCellValue()), secondRow.getCell(oneCell.getColumnIndex()).getStringCellValue());
			   		mapOfCellValueAndColumnIndex.put(String.valueOf(oneCell.getNumericCellValue()),oneCell.getColumnIndex());
			   		break;
			   	case Cell.CELL_TYPE_STRING:
			   		mapOfExcelStructure.put(oneCell.getStringCellValue(), secondRow.getCell(oneCell.getColumnIndex()).getStringCellValue());
			   		mapOfCellValueAndColumnIndex.put(oneCell.getStringCellValue(),oneCell.getColumnIndex());
			   		break;
			   }
		}

//		Map<Integer,Set<String>> mapOfidandfrom = new HashMap<>();
//		Set<String> listOfFromAndTo = new HashSet<>();
//			for(Map.Entry<String, String> entry : mapOfExcelStructure.entrySet()) {
//				if(entry.getValue().equalsIgnoreCase("lifeCycle_f")|| entry.getValue().equalsIgnoreCase("lifeCycle_t")) {
//					Integer id = Integer.parseInt(entry.getKey().substring(0, 11));
//					listOfFromAndTo.add(entry.getValue());
//					mapOfidandfrom.put(id, listOfFromAndTo);
//				}
//		}
			
		
		for (AssetTemplateColumn assetTemplateColumn : assetTemplateColumns) {
			mapOfTemplateColumn.put(assetTemplateColumn.getId(), assetTemplateColumn);
			List<String> errors = new ArrayList<>();
			if(!assetTemplateColumn.getAssetTemplateColName().equalsIgnoreCase(mapOfExcelStructure.get(assetTemplateColumn.getId().toString()))){
			
				ExcelCellBean excelCellBean = new ExcelCellBean();
				errors.add("Column headers have been modified");
				excelCellBean.setErrors(errors);
				listOfErrorCells.add(excelCellBean);
			}
			
		}
			
		Map<Integer, AssetLifeCycleExcelBean> mapToReturn = new HashMap<>();
		AssetTemplateColumn assetTemplateColumn1 = null;
		HashMap<String,Integer> mapOfIdAndColumn = new HashMap<>();
		AssetLifeCycleExcelBean oneAssetLifeCycleExcelBean = null;
		
		for (Cell oneCell : firstRow) {
			
			 switch (oneCell.getCellType()) {
			   	case Cell.CELL_TYPE_NUMERIC:			   		
			   		assetTemplateColumn1 = mapOfTemplateColumn.get((int)oneCell.getNumericCellValue());		
			   		mapOfIdAndColumn = new HashMap<>();
			   		if(assetTemplateColumn1.getDataType().equals(DataType.JSON.toString())) {			   			
			   			mapOfIdAndColumn.put(assetTemplateColumn1.getId()+"_from", mapOfCellValueAndColumnIndex.get(assetTemplateColumn1.getId()+"_from"));
			   			mapOfIdAndColumn.put(assetTemplateColumn1.getId()+"_to", mapOfCellValueAndColumnIndex.get(assetTemplateColumn1.getId()+"_to"));			   			
			   		}
			   		
			   		oneAssetLifeCycleExcelBean = new AssetLifeCycleExcelBean();
			   		oneAssetLifeCycleExcelBean.setAssetTemplateColumn(assetTemplateColumn1);
			   		oneAssetLifeCycleExcelBean.setMapOfIdAndColumnIndex(mapOfIdAndColumn);
			   					   		
			   		mapToReturn.put(oneCell.getColumnIndex(),oneAssetLifeCycleExcelBean);
			   		//mapToReturn.put(oneCell.getColumnIndex(), mapOfTemplateColumn.get((int)oneCell.getNumericCellValue()));
			   		break;
			   	case Cell.CELL_TYPE_STRING:
			   		int cellValue=-1;
			   		try {
			   			cellValue = Integer.parseInt(oneCell.getStringCellValue());
			   		}catch(Exception e)
			   		{ 
			   			cellValue=-1; 
			   		}
			 
//			   		if(mapOfTemplateColumn.get(cellValue) == null){
//			   		AssetTemplateColumn lifeCycleExtraColumn = new AssetTemplateColumn();
//			  		lifeCycleExtraColumn.setAssetTemplateColName(oneCell.getStringCellValue());
//			  		lifeCycleExtraColumn.setDataType(DataType.JSON.toString());
//		   			mapToReturn.put(oneCell.getColumnIndex(), lifeCycleExtraColumn);
//			   	}
//			   	else 
			   		if(cellValue!=-1) {
			   			assetTemplateColumn1 = mapOfTemplateColumn.get(Integer.parseInt(oneCell.getStringCellValue()));		
			   		mapOfIdAndColumn = new HashMap<>();
			   		if(assetTemplateColumn1.getDataType().equals(DataType.JSON.toString())) {			   			
			   			mapOfIdAndColumn.put(assetTemplateColumn1.getId()+"_from", oneCell.getColumnIndex()+1);
			   			mapOfIdAndColumn.put(assetTemplateColumn1.getId()+"_to", oneCell.getColumnIndex()+2);			   			
			   		}
			   		
			   		oneAssetLifeCycleExcelBean = new AssetLifeCycleExcelBean();
			   		oneAssetLifeCycleExcelBean.setAssetTemplateColumn(assetTemplateColumn1);
			   		oneAssetLifeCycleExcelBean.setMapOfIdAndColumnIndex(mapOfIdAndColumn);
			   		
			   		//mapToReturn.put(oneCell.getColumnIndex(), mapOfTemplateColumn.get(cellValue));
			   		mapToReturn.put(oneCell.getColumnIndex(),oneAssetLifeCycleExcelBean);
			   		}
			   		break;
			   }
			//mapToReturn.put(oneCell.getColumnIndex(), mapOfTemplateColumn.get((int)oneCell.getNumericCellValue()));
		}
		return mapToReturn;
	}

	private Asset createNewAssetObject(AssetTemplate assetTemplate) {
		Asset asset = new Asset();
		User user = userDetailsService.getCurrentUser();
		asset.setUserByCreatedBy(user);
		asset.setUserByUpdatedBy(user);
		asset.setCreatedDate(new Date());
		asset.setUpdatedDate(new Date());
		asset.setAssetTemplate(assetTemplate);	

		return asset;
	}

	/**
	 * Add list of validators to be performed on cell
	 * @param assetTemplateColumn
	 * @param cellValidators
	 */
	private void addValidatorsToCell(AssetTemplateColumn assetTemplateColumn, 
			List<ExcelCellValidator> cellValidators) {
		cellValidators.add(new CellTypeValidator(assetTemplateColumn.getDataType()));
		cellValidators.add(new MandatoryCellValidator(assetTemplateColumn.isMandatory()));
		if (assetTemplateColumn.getDataType().equals(DataType.TEXT.toString())) {
			cellValidators.add(new CellDataLengthValidator(assetTemplateColumn.getLength()));
		}
		if (assetTemplateColumn.getDataType().equals(DataType.JSON.toString())) {
			cellValidators.add(new CellDataLengthValidator(assetTemplateColumn.getLength()));
		}
	}

	private void validateCellData(ExcelCellBean cellBean, Cell cell) {
		for (ExcelCellValidator oneValidator : cellBean.getCellValidators()) {
			oneValidator.validate(cell, cellBean.getErrors());
		}
	}

	/**
	 * Generate asset data for the asset
	 * @param asset
	 * @param cell
	 * @param assetTemplateColumn
	 * @param cellBean
	 * @return
	 */
	private AssetData generateAssetData(Asset asset, Cell cell, AssetTemplateColumn assetTemplateColumn, ExcelCellBean cellBean,Row row,Map<String,Integer> mapOfIdAndColumn) {
		AssetData assetData = new AssetData();
		assetData.setAssetTemplateColumn(assetTemplateColumn);
		assetData.setAsset(asset);
		List<AssetLifeCycleStagesBean> listOfAssetLifeCycleStagesBean = new ArrayList<>();
		AssetLifeCycleStagesBean oneAssetLifeCycle = new AssetLifeCycleStagesBean();
				
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			if(assetTemplateColumn.getDataType().equalsIgnoreCase("JSON")) {
				checkForJSON(cell, assetTemplateColumn, cellBean, row, mapOfIdAndColumn, assetData,	listOfAssetLifeCycleStagesBean, oneAssetLifeCycle);		
			}
			else {
				assetData.setData(cell.getStringCellValue());
				cellBean.setValue(cell.getStringCellValue());
			}	
			break;

		case Cell.CELL_TYPE_NUMERIC:
			handleNumericCellType(cell, cellBean, assetData);
			break;

		case Cell.CELL_TYPE_BLANK:
			assetData.setData(" ");
			cellBean.setValue(" ");
			break;

		default: break;

		}
		return assetData;
	}

	private void checkForJSON(Cell cell, AssetTemplateColumn assetTemplateColumn, ExcelCellBean cellBean, Row row,
			Map<String, Integer> mapOfIdAndColumn, AssetData assetData,
			List<AssetLifeCycleStagesBean> listOfAssetLifeCycleStagesBean, AssetLifeCycleStagesBean oneAssetLifeCycle) {
		String lfname = cell.getStringCellValue();			
		if(mapOfIdAndColumn!=null) {

			int fromColumnIndex = mapOfIdAndColumn.get(assetTemplateColumn.getId()+"_from");
			int toColumnIndex = mapOfIdAndColumn.get(assetTemplateColumn.getId()+"_to");

			String fromCellValue = row.getCell(fromColumnIndex).getDateCellValue().toString();
			String toCellValue = row.getCell(toColumnIndex).getDateCellValue().toString();

			String pattern1 = "EEE MMM dd yyyy";
			String  strDateFrom = null;
			String strDateTo = null;
			String formatDateFrom  = fromCellValue.replace(fromCellValue.substring(11,24),"");
			String formatDateTo  = toCellValue.replace(toCellValue.substring(11,24),"");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern1);
			try{
				Date date1 =  simpleDateFormat.parse(formatDateFrom);
				Date date2 = simpleDateFormat.parse(formatDateTo);
				SimpleDateFormat formatter1 = new SimpleDateFormat("MM/yyyy");
				strDateFrom = formatter1.format(date1); 
				strDateTo = formatter1.format(date2);
			}catch(Exception e){
				e.printStackTrace(); 
			}
			oneAssetLifeCycle.setId(assetTemplateColumn.getId().toString());
			oneAssetLifeCycle.setLifeCycle_f(strDateFrom);
			oneAssetLifeCycle.setLifeCycle_s(lfname);
			oneAssetLifeCycle.setLifeCycle_t(strDateTo);
			listOfAssetLifeCycleStagesBean.add(oneAssetLifeCycle);
			String json = null;
			try {
				json = new ObjectMapper().writeValueAsString(oneAssetLifeCycle);
				assetData.setData(json);
				cellBean.setValue(cell.getStringCellValue()+fromCellValue+toCellValue);
			}
			catch(Exception ex )
			{
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Handle numeric cell type. A numeric cell can hold Date or number
	 * @param cell
	 * @param cellBean
	 * @param assetData
	 */
	private void handleNumericCellType(Cell cell, ExcelCellBean cellBean, AssetData assetData) {
		if (DateUtil.isCellDateFormatted(cell)){
			String date = getFormatedDate(cell.getDateCellValue());
			assetData.setData(date);
			cellBean.setValue(date);
		} else {
			String value = getNumericValue(cell.getNumericCellValue());
			assetData.setData(value);
			cellBean.setValue(value);
		}
	}

	/**
	 * 
	 * @param numericCellValue
	 * @return
	 */
	private String getNumericValue(double numericCellValue) {
		String value = String.valueOf(numericCellValue);
		String[] splitValue = value.split("\\.");
		if (Integer.parseInt(splitValue[1]) == 0){
			value = splitValue[0];
		}
		return value;
	}

	private String getFormatedDate(Date date) {
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
	}

	// method for uid unique
	private boolean isAssetUIDExist(String uid)
	{
		Asset asset = assetService.findByUid(uid);
		return asset != null;
	}

	/**
	 * Generate asset data for blank cells
	 * @param setOfTemplateColumn
	 * @param allTemplates
	 * @param asset
	 */
	private void addDataForBlankCells(Set<AssetTemplateColumn> setOfTemplateColumn, Collection<AssetTemplateColumn> allTemplates, Asset asset) {
		Set<AssetTemplateColumn> copyOfSet = new HashSet<>();
		copyOfSet.addAll(allTemplates);
		copyOfSet.removeAll(setOfTemplateColumn);
		Set<AssetData> listOfAssetData = asset.getAssetDatas();
		if (!copyOfSet.isEmpty()){
			for (AssetTemplateColumn assetTemplateColumn : copyOfSet) {
				AssetData assetData = new AssetData();
				assetData.setAssetTemplateColumn(assetTemplateColumn);
				assetData.setAsset(asset);
				assetData.setData("");
				listOfAssetData.add(assetData);
			}
			asset.setAssetDatas(listOfAssetData);
		}

	}

	@Override
	public boolean validateSheetSecurity(Workbook workbook,AssetTemplate assetTemplate, List<ExcelCellBean> listOfErrorCells) {
		boolean isValidate;
		Sheet configSheet  = workbook.getSheetAt(1);		 
		Row firstRow = configSheet.getRow(configSheet.getFirstRowNum());
		Cell firstCell = firstRow.getCell(firstRow.getFirstCellNum());
		String hashFromUploadedWorkbook =firstCell.getStringCellValue();
		ExportLog exportLog = exportLogService.findByHash(hashFromUploadedWorkbook);
		if(exportLog !=null && (exportLog.getRefId() == assetTemplate.getId()) 
				&& (exportLog.getUser().getId() == userDetailsService.getCurrentUser().getId())
				&& !validateDate(exportLog.getTimeDown(),assetTemplate.getUpdatedDate())){

			isValidate =true;
		}else{			
			ExcelCellBean excelCellBean = new ExcelCellBean();
			List<String>errors = new ArrayList<>();		
			errors.add("Uploaded file is not valid");
			excelCellBean.setErrors(errors);
			listOfErrorCells.add(excelCellBean);
			isValidate =false;
		}
		return isValidate;
	}

	/**
	 * method will we used to validate export excel download time
	 * and asset template updated time
	 * if exportLogDate < assetUpdatedate then return error
	 * @param exportLogDate
	 * @param assetUpdatedate
	 * @return
	 */
	public boolean validateDate(Date exportLogDate,Date assetUpdatedate){
		boolean isDateValid = false;
		if(exportLogDate.compareTo(assetUpdatedate) < 0){
			isDateValid = true;
		}
		return isDateValid;
	}

}
