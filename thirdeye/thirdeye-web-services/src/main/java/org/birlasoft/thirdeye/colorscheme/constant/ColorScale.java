package org.birlasoft.thirdeye.colorscheme.constant;

/**
 * Enum for possible color scale which can be applied on a quality gate
 * @author shaishav.dixit
 *
 */
public enum ColorScale {

	CS3(3), CS4(4), CS5(5);
	
	private final int value;       

    private ColorScale(int value) {
        this.value = value;
    }

	public int getValue() {
		return value;
	}
}
