package org.birlasoft.thirdeye.service;

import java.util.Date;

import org.birlasoft.thirdeye.entity.BenchmarkItem;


/**
 * Service Interface for Benchmark.
 */

public interface BenchmarkItemService {
	
	 /**
	  * Get BenchmarkItem by BenchmarkItem id
	  * @param idOfBenchmarkItem
	  * @return {@code BenchmarkItem}
	  */
	public BenchmarkItem findById(Integer idOfBenchmarkItem);
	
    /**
     * Method to save or update {@code BenchmarkItem}
     * @param benchmarkItemBean
     * @return {@code BenchmarkItem}
     */
	public BenchmarkItem saveOrUpdate(BenchmarkItem benchmarkItemBean);

	/**
	 * method for generate SequenceNumber
	 * @param benchMarkId
	 * @return {@code int}
	 */
	int generateSequenceNumber(Integer benchMarkId);

	/**
	 * method to delete benchmark item
	 * @param idOfBenchmarkItem 
	 */
	public void deleteBenchmarkItem(Integer idOfBenchmarkItem);	
}
