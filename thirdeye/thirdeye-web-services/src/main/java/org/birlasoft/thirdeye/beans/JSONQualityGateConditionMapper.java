package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Quality gate color conditions are saved in the form of JSON in database. This class 
 * is a mapper to that JSON
 * @author shaishav.dixit
 *
 */
public class JSONQualityGateConditionMapper {
	
	private List<JSONQualityGateConditionValueMapper> values = new ArrayList<>();

	public List<JSONQualityGateConditionValueMapper> getValues() {
		return values;
	}

	public void setValues(List<JSONQualityGateConditionValueMapper> values) {
		this.values = values;
	}

	/**
	 * Add one {@link JSONQualityGateConditionValueMapper} to {@link List}
	 * @param oneValue
	 */
	public void addOption (JSONQualityGateConditionValueMapper oneValue){
		if (oneValue != null){
			values.add(oneValue);
		}
	}
}
