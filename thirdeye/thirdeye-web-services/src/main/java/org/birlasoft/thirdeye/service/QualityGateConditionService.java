package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.JSONQualityGateConditionValueMapper;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QualityGateConditionBean;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.QualityGateCondition;

/**
 * Service {@code interface} for {@link QualityGateCondition}
 * @author shaishav.dixit
 *
 */
public interface QualityGateConditionService {

	/**
	 * Save {@link QualityGateCondition}
	 * @param qualityGateCondition
	 * @return
	 */
	public QualityGateCondition save(QualityGateCondition qualityGateCondition);

	/**
	 * Create {@link QualityGateCondition} object from {@code list of values} and 
	 * {@code list of color}. {@code list of values} and {@code list of color} are 
	 * converted into JSON string.
	 * 
	 * @param listOfValues
	 * @param listOfColors
	 * @param qualityGateCondition
	 * @return
	 */
	public QualityGateCondition createConditionObject(List<Double> listOfValues, List<String> listOfColors, QualityGateConditionBean qualityGateCondition);

	/**
	 * Get {@code list} of {@link JSONQualityGateConditionValueMapper} from {@code JSON} 
	 * string.
	 * @param values
	 * @return
	 */
	public List<JSONQualityGateConditionValueMapper> getGateConditions(String values);

	/**
	 * Get {@code list} of {@link QualityGateConditionBean} by {@code quality gate id}
	 * @param idOfQualityGate
	 * @return
	 */
	public List<QualityGateConditionBean> getAllConditions(Integer idOfQualityGate);
	
	/**
	 * Convert {@link QualityGateCondition} to {@link QualityGateConditionBean}
	 * @param qualityGateCondition
	 * @return
	 */
	public QualityGateConditionBean createConditionBean(QualityGateCondition qualityGateCondition);

	/**
	 * Get a {@code map} of condition color scale used in a {@link QualityGate}
	 * @param qualityGate
	 * @return
	 */
	public Map<String, Integer> getConditionScale(QualityGate qualityGate);

	/**
	 * Get a {@code map} of {@link Parameter} and {@link QualityGateCondition} for a 
	 * {@link QualityGate}
	 * @param qualityGate
	 * @return
	 */
	public Map<ParameterBean, QualityGateCondition> getMapOfParameterAndCondition(QualityGate qualityGate);

	/**
	 * Get color hex code for a parameter based on the parameter value.
	 * @param conditionValues
	 * @param parameterValue
	 * @return
	 */
	public String getColorBasedOnParameterValue(String conditionValues, Double parameterValue);

	/**
	 * Calculate the color of asset from Quality gate conditions.
	 * @param mapOfSeries
	 * @param mapOfConditionAndParameterValue
	 * @return
	 */
	public String calculateAssetColor(Map<String, Integer> mapOfSeries, Map<QualityGateCondition, Double> mapOfConditionAndParameterValue);

	/**
	 * Find {@link QualityGateCondition} by id.
	 * @param idOfQualityGateCondition
	 * @return
	 */
	public QualityGateCondition findOne(Integer idOfQualityGateCondition);

	/**
	 * Get {@link QualityGateConditionBean} by condition id 
	 * @param idOfQualityGateCondition
	 * @return
	 */
	public QualityGateConditionBean getConditionBean(Integer idOfQualityGateCondition);

	/**
	 * Update quality gate condition.
	 * @param qualityGateConditionBean
	 * @param listOfValues
	 * @param listOfColors
	 * @return
	 */
	public QualityGateCondition updateConditionObject(QualityGateConditionBean qualityGateConditionBean, List<Double> listOfValues, List<String> listOfColors);

	/**
	 * Delete {@link QualityGateCondition} by id.
	 * @param idOfQualityGateCondition
	 */
	public void delete(Integer idOfQualityGateCondition);

	/**
	 * Delete {@code set} of {@link QualityGateCondition}
	 * @param qualityGateConditions
	 */
	public void deleteInBatch(Set<QualityGateCondition> qualityGateConditions);

	/**
	 * Converts list of color and list of values into object and sets it in 
	 * {@link QualityGateConditionBean}
	 * @param qualityGateConditionBean
	 * @param listOfValues
	 * @param listOfColors
	 * @return qualityGateConditionBean
	 */
	public QualityGateConditionBean insertValueMapperList(QualityGateConditionBean qualityGateConditionBean,
			List<Double> listOfValues, List<String> listOfColors);

	/**
	 * Creates a json string with default color and its values.
	 * Default values are:<br/>
	 * Red < 2 <br/>
	 * 2 < Amber < 5 <br/>
	 * Green > 5 <br/>
	 * 
	 * @param qualityGate
	 * @param parameter
	 * @return
	 */
	public QualityGateCondition getDefaultCondition(QualityGate qualityGate, Parameter parameter);

	/**
	 * Checks if the condition needs to be weighted or not.</br>
	 * Conditions for parameter quality gate are not weighted.
	 * @param qualityGate
	 * @return
	 */
	public boolean isWeightedCondition(Integer qualityGate);

	/**
	 * Update Quality gate condition bean
	 * @param qualityGateConditionBean
	 * @return
	 */
	public QualityGateConditionBean updateBean(QualityGateConditionBean qualityGateConditionBean);
	
	/**
	 * Get the {@code json string} with default color condition 
	 * @return
	 */
	public String getDefaultConditionValues();

	public Map<Integer, QualityGateCondition> getMapOfParameterIdAndCondition(QualityGate qualityGate);

}
