package org.birlasoft.thirdeye.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetAuditTrail;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.repositories.AssetAuditTrailRepository;

import org.birlasoft.thirdeye.service.AssetAuditTrailService;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;	
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("auditassettrail")
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetAuditTrailServiceImpl implements AssetAuditTrailService{
	@Autowired
	private AssetAuditTrailRepository auditAssetTrailRepository;
	@Autowired
	private AssetService assetService;
	@Autowired
	private AssetTemplateService assetTemplateService;
	@Autowired 
	private CustomUserDetailsService customUserDetailsService;
	@Override
	public AssetAuditTrail save(AssetAuditTrail oneAssetAuditTrail) {
		// TODO Auto-generated method stub
		return auditAssetTrailRepository.save(oneAssetAuditTrail);
	}

	public AssetAuditTrail findByAssetId(Integer assetId) {
		return auditAssetTrailRepository.findByassetId(assetId);
	}
	@Override
	public AssetAuditTrail createNewAssetAuditTrailObject(Asset asset,User currentUser) {
		AssetAuditTrail oneAssetAuditTrail = new AssetAuditTrail();
		String assetName ="";
		assetName = assetService.getAssetNameForUID(asset);
		oneAssetAuditTrail.setAssetId(asset.getId());
		oneAssetAuditTrail.setAssetName(assetName);
		oneAssetAuditTrail.setCreatedBy(currentUser.getFirstName() + " " + currentUser.getLastName());
		oneAssetAuditTrail.setCreatedDate(new Date());
		oneAssetAuditTrail.setEventDescription(assetName+" has been Created");
		oneAssetAuditTrail.setEventName("Create");
		oneAssetAuditTrail.setUpdatedBy(currentUser.getFirstName() + " " + currentUser.getLastName());
		oneAssetAuditTrail.setUpdatedDate(new Date());
		oneAssetAuditTrail.setWorkspaceId(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		save(oneAssetAuditTrail);
		return oneAssetAuditTrail ;
	}

	@Override
	public AssetAuditTrail updateAssetAuditTrailObject(Asset asset, User currentUser) {
		AssetAuditTrail oneAssetAuditTrail = new AssetAuditTrail();
		String assetName ="";
		assetName = assetService.getAssetNameForUID(asset);
		//List<AssetAuditTrail> assetAuditTrailFromDb = findByAssetId(asset.getId());
		oneAssetAuditTrail.setAssetId(asset.getId());
		oneAssetAuditTrail.setAssetName(assetName);
		oneAssetAuditTrail.setCreatedBy(currentUser.getFirstName() + " " + currentUser.getLastName());
		oneAssetAuditTrail.setCreatedDate(new Date());
		oneAssetAuditTrail.setEventDescription(assetName+" has been Modified");
		oneAssetAuditTrail.setEventName("Edit");
		oneAssetAuditTrail.setUpdatedBy(currentUser.getFirstName() + " " + currentUser.getLastName());
		oneAssetAuditTrail.setUpdatedDate(new Date());
		oneAssetAuditTrail.setWorkspaceId(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		save(oneAssetAuditTrail);
		return oneAssetAuditTrail ;
	}

	@Override
	public AssetAuditTrail deleteAssetAuditTrailObject(Asset asset, User currentUser) {
	
		AssetAuditTrail oneAssetAuditTrail = new AssetAuditTrail();
		String assetName ="";
		assetName = assetService.getAssetNameForUID(asset);
		//AssetAuditTrail assetAuditTrailFromDb = findByAssetId(asset.getId());
		oneAssetAuditTrail.setAssetId(asset.getId());
		oneAssetAuditTrail.setAssetName(assetName);
		oneAssetAuditTrail.setEventDescription(assetName+" has been Deleted");
		oneAssetAuditTrail.setEventName("Delete");
		oneAssetAuditTrail.setCreatedBy(currentUser.getFirstName() + " " + currentUser.getLastName());
		oneAssetAuditTrail.setCreatedDate(new Date());
		oneAssetAuditTrail.setUpdatedBy(currentUser.getFirstName() + " " + currentUser.getLastName());
		oneAssetAuditTrail.setUpdatedDate(new Date());
		oneAssetAuditTrail.setWorkspaceId(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		save(oneAssetAuditTrail);
		return oneAssetAuditTrail ;
	}

	@Override
	public List<AssetAuditTrail> findByActiveWorkspaceId(Integer workspaceId){
		// TODO Auto-generated method stub
		return auditAssetTrailRepository.findByWorkspaceId(workspaceId);
	}

	@Override
	public List<AssetAuditTrail> getAssetAudiTrailData(Timestamp startDate, Timestamp endDate, Set<String> userIds,
			List<String> eventName,Set<Integer> assetTemplateIds,Integer workspaceId) {
		List<AssetAuditTrail> listAssetauditTrail = new ArrayList<>(); 
		if(startDate != null && endDate!=null && userIds == null && eventName==null) {
			listAssetauditTrail = auditAssetTrailRepository.findByCreatedDateBetweenAndWorkspaceId(startDate, endDate,workspaceId);
		}
		else if (startDate != null && endDate!=null && userIds != null && eventName==null && assetTemplateIds==null) {
			listAssetauditTrail = auditAssetTrailRepository.findByCreatedDateBetweenAndCreatedByInAndWorkspaceId(startDate, endDate, userIds,workspaceId);	
		}
		if (userIds!=null && (!userIds.isEmpty()) && eventName == null && assetTemplateIds==null && startDate==null){
			listAssetauditTrail = auditAssetTrailRepository.findByCreatedByInAndWorkspaceId(userIds,workspaceId);
		}
		else if(userIds!=null  && eventName != null && assetTemplateIds==null && startDate==null) {
			listAssetauditTrail = auditAssetTrailRepository.findByCreatedByInAndEventNameInAndWorkspaceId(userIds, eventName,workspaceId);
		}
		if(eventName!=null && startDate == null  && endDate ==null && userIds==null && assetTemplateIds==null) {
			listAssetauditTrail = auditAssetTrailRepository.findByEventNameInAndWorkspaceId(eventName,workspaceId);
		}
		else if(eventName!=null && startDate != null  && endDate !=null && assetTemplateIds==null && userIds==null) {
			listAssetauditTrail = auditAssetTrailRepository.findByCreatedDateBetweenAndEventNameInAndWorkspaceId(startDate, endDate, eventName,workspaceId);
		}
		if(startDate!=null &&  endDate!=null && userIds!=null && eventName!=null && assetTemplateIds==null) {
			listAssetauditTrail= auditAssetTrailRepository.findByCreatedDateBetweenAndCreatedByInAndEventNameInAndWorkspaceId(startDate, endDate, userIds, eventName,workspaceId); 
		}
		if(assetTemplateIds!=null) {
			listAssetauditTrail = getAssetTemplateFilter(startDate, endDate, userIds,
					eventName,assetTemplateIds,workspaceId);
		}
		return listAssetauditTrail;
	}

	private List<AssetAuditTrail> getAssetTemplateFilter(Timestamp startDate, Timestamp endDate, Set<String> userIds,
			List<String> eventName,Set<Integer> assetTemplateIds,Integer workspaceId) {
		List<AssetAuditTrail> listAssetauditTrail = new ArrayList<>();
		Set<Integer> assetIds = new HashSet<>();
		if(assetTemplateIds!=null) {
			Set<Asset> setOfAsset = getAssetAuditTrailByAssetTemplateFilter(assetTemplateIds);
			for(Asset oneAsset: setOfAsset) {
				assetIds.add(oneAsset.getId());
			}
		}
		if(assetIds!=null &&startDate ==null && endDate ==null && eventName==null && userIds==null) {
			listAssetauditTrail = auditAssetTrailRepository.findByAssetIdInAndWorkspaceId(assetIds,workspaceId);
		}
		if(startDate !=null && endDate !=null && assetIds!=null && eventName==null && userIds==null) {
			listAssetauditTrail =auditAssetTrailRepository.findByCreatedDateBetweenAndAssetIdInAndWorkspaceId(startDate, endDate, assetIds,workspaceId);
		}

		if(userIds!=null && assetIds!=null && eventName==null && startDate==null) {
			listAssetauditTrail =auditAssetTrailRepository.findByCreatedByInAndAssetIdInAndWorkspaceId(userIds, assetIds,workspaceId);
		}

		if(eventName!=null && assetIds!=null && userIds ==null && startDate==null){
			listAssetauditTrail =auditAssetTrailRepository.findByEventNameInAndAssetIdInAndWorkspaceId(eventName, assetIds,workspaceId);
		}

		if(userIds!=null && eventName!=null && assetIds!=null && startDate==null ) {
			listAssetauditTrail =auditAssetTrailRepository.findByCreatedByAndAssetIdInAndEventNameInAndWorkspaceId(userIds, assetIds, eventName,workspaceId);

		}
		if(startDate !=null && endDate !=null && userIds!=null && assetIds!=null && eventName==null) {
			listAssetauditTrail = auditAssetTrailRepository.findByCreatedDateBetweenAndAssetIdInAndCreatedByInAndWorkspaceId(startDate,endDate,assetIds,userIds,workspaceId);
		}
		if(startDate !=null && endDate !=null && eventName!=null && assetIds!=null&& userIds==null) {
			listAssetauditTrail =auditAssetTrailRepository.findByCreatedDateBetweenAndAssetIdInAndEventNameInAndWorkspaceId(startDate,endDate,assetIds,eventName,workspaceId);
		}
		
		if(startDate !=null && endDate !=null && eventName!=null && assetIds!=null && userIds!=null) {
			listAssetauditTrail = auditAssetTrailRepository.findByCreatedDateBetweenAndAssetIdInAndEventNameInAndCreatedByInAndWorkspaceId(startDate, endDate, assetIds, eventName, userIds, workspaceId);
		}
		return listAssetauditTrail;
	}

	private Set<Asset> getAssetAuditTrailByAssetTemplateFilter(Set<Integer> assetTemplateIds){
		List<AssetTemplate> listAssetTemplate = assetTemplateService.findByIdIn(assetTemplateIds);
		Set<AssetTemplate> set = new HashSet<>();
		for(AssetTemplate oneTemp :listAssetTemplate) {
			set.add(oneTemp);
		}
		Set<Asset> listOfAsset = assetService.findByAssetTemplateIn(set);

		return listOfAsset;
	}

}
