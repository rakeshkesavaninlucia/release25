package org.birlasoft.thirdeye.constant;

public enum HomePageType {

DASHBOARD("DASHBOARD"),
BCM("BCM"), 
SAVEDREPORT("SAVEDREPORT");
	
	private final String description;       

    private HomePageType(String description) {
        this.description = description;
    }

	public String getHomePage() {
		return description;
	}
	
}
