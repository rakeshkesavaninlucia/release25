package org.birlasoft.thirdeye.colorscheme.constant;

public enum ParameterColorScale {
	CS2(2), CS3(3), CS4(4), CS5(5), CS6(6), CS7(7), CS8(8), CS9(9), CS10(10), CS11(11);
	
	private final int value;       

    private ParameterColorScale(int value) {
        this.value = value;
    }

	public int getValue() {
		return value;
	}
}
