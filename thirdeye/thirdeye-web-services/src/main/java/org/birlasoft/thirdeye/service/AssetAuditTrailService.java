package org.birlasoft.thirdeye.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetAuditTrail;
import org.birlasoft.thirdeye.entity.User;

/**
 * @author shagun.sharma
 *
 */
public interface AssetAuditTrailService {
	
	/** Method to find assetauditTrailbyassetId
	 * @param assetId
	 * @return{@code AssetAuditTrail}
	 */
	public AssetAuditTrail findByAssetId(Integer assetId);
	/**
	 * Method to find list of asset audit trail within active workspace
	 * @param workspaceId
	 * @return{@code List<AssetAuditTrail>}
	 */
	public List<AssetAuditTrail> findByActiveWorkspaceId(Integer workspaceId);
	/**
	 * @param oneAssetAuditTrail
	 * @return{@code AssetAuditTrail}
	 */
	public AssetAuditTrail save(AssetAuditTrail oneAssetAuditTrail);
	/**
	 * @param asset
	 * @param currentUser
	 *  @return{@code AssetAuditTrail}
	 */
	public AssetAuditTrail createNewAssetAuditTrailObject(Asset asset,User currentUser);
	/**
	 * @param asset
	 * @param currentUser
	 * @return{@code AssetAuditTrail}
	 */
	public AssetAuditTrail updateAssetAuditTrailObject(Asset asset,User currentUser);
	/**
	 * @param asset
	 * @param currentUser
	 * @return{@code AssetAuditTrail}
	 */
	public AssetAuditTrail deleteAssetAuditTrailObject(Asset asset,User currentUser);
	/**
	 * @param startDate
	 * @param endDate
	 * @param userIds
	 * @param eventName
	 * @param assetTemplateIds
	 * @param workspaceId
	 * @return {@code List<AssetAuditTrail>}
	 */
	public List<AssetAuditTrail> getAssetAudiTrailData(Timestamp startDate,Timestamp endDate,Set<String> userIds,List<String> eventName,Set<Integer> assetTemplateIds,Integer workspaceId);

}
