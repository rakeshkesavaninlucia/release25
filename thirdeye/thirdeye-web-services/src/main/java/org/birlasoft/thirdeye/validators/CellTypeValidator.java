/**
 * 
 */
package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.birlasoft.thirdeye.constant.DataType;

/**
 * @author shaishav.dixit
 *
 */
public class CellTypeValidator implements ExcelCellValidator {
	
	private String type;
	private boolean flag;
	private static final String CELL_TYPE_ERROR = "Not a valid data type";
	
	public CellTypeValidator(String type) {
		this.type = type;
	}

	@Override
	public boolean validate(Cell cell, List<String> errors) {
		flag = false;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			if(type.equalsIgnoreCase(DataType.TEXT.toString()) || type.equalsIgnoreCase(DataType.JSON.toString())) {
				flag = true;
			} else {
				errors.add(CELL_TYPE_ERROR);
			}
			break;
			
		case Cell.CELL_TYPE_NUMERIC:
			if(type.equals(DataType.DATE.toString()) && DateUtil.isCellDateFormatted(cell)){
				flag = true;
			} else if (type.equals(DataType.NUMBER.toString())) {
				flag = true;
			} else {
				errors.add(CELL_TYPE_ERROR);
			}
			break;

		default:
			errors.add(CELL_TYPE_ERROR);
			break;
		}
		return flag;
	}

}
