-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 10.193.146.222    Database: 3rdi_tanushree
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aid`
--

DROP TABLE IF EXISTS `aid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `aidTemplate` varchar(25) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_AID_Name_WorkspaceId` (`name`,`workspaceId`),
  KEY `FK_AID_WORKSPACEID_idx` (`workspaceId`),
  KEY `FK_AID_ASSETTEMPLATID_idx` (`assetTemplateId`),
  KEY `FK_CREATEDBY_USERID_idx` (`createdBy`),
  KEY `FK_UPDATEDBY_USERID_idx` (`updatedBy`),
  CONSTRAINT `FK_AID_ASSETTEMPLATID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AID_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CREATEDBY_USERID` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UPDATEDBY_USERID` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aid_block`
--

DROP TABLE IF EXISTS `aid_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aidId` int(11) NOT NULL,
  `aidBlockType` varchar(20) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `blockJSONConfig` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_AIDID_AID_idx` (`aidId`),
  CONSTRAINT `FK_AIDID_AID` FOREIGN KEY (`aidId`) REFERENCES `aid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL,
  `uid` varchar(256) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  KEY `ASSET_TEMPLATE_FK_idx` (`templateId`),
  KEY `ASSET_CREATED_BY_FK_idx` (`createdBy`),
  KEY `ASSET_UPDATED_BY_FK_idx` (`updatedBy`),
  CONSTRAINT `ASSET_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_TEMPLATE_FK` FOREIGN KEY (`templateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=553 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset_audit_trail`
--

DROP TABLE IF EXISTS `asset_audit_trail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) DEFAULT NULL,
  `assetName` varchar(45) DEFAULT NULL,
  `eventName` varchar(45) DEFAULT NULL,
  `eventDescription` varchar(45) DEFAULT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `createdDate` timestamp NULL DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset_data`
--

DROP TABLE IF EXISTS `asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text,
  `assetTemplateColId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assetTemplateColIdFk_idx` (`assetTemplateColId`),
  KEY `AssetData_Asset_FK_idx` (`assetId`),
  CONSTRAINT `AssetData_Asset_FK` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTemplateColIdFk` FOREIGN KEY (`assetTemplateColId`) REFERENCES `asset_template_column` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1838 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset_template`
--

DROP TABLE IF EXISTS `asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateName` varchar(45) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateName_createdBy_UNIQUE` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  UNIQUE KEY `UK_a35ol4p1y14cnqk9j9yqcoj13` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  KEY `id_idx` (`assetTypeId`),
  KEY `updatedByFk2_idx` (`updatedBy`),
  KEY `createdBy_UNIQUE` (`createdBy`),
  KEY `FK_ASSETTEMPLATE_WORKSPACEID_idx` (`workspaceId`),
  CONSTRAINT `FK_ASSETTEMPLATE_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeIdFk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk2` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk2` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset_template_column`
--

DROP TABLE IF EXISTS `asset_template_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateColName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `filterable` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateColName_assetTemplateId_UNIQUE` (`assetTemplateColName`,`assetTemplateId`),
  UNIQUE KEY `UK_i7n89jntp5xq390elbs233689` (`assetTemplateColName`,`assetTemplateId`),
  KEY `createdByFk3_idx` (`createdBy`),
  KEY `updatedByFk3_idx` (`updatedBy`),
  KEY `assettemplateIdFk_idx` (`assetTemplateId`),
  CONSTRAINT `assetTemplateIdFk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk3` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk3` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset_type`
--

DROP TABLE IF EXISTS `asset_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTypeName` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `imagePath` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeName_UNIQUE` (`assetTypeName`),
  UNIQUE KEY `UK_q1gprhj5b79ufwicjvkvef4fw` (`assetTypeName`),
  KEY `createdByFk1_idx` (`createdBy`),
  KEY `updatedByFk1_idx` (`updatedBy`),
  CONSTRAINT `createdByFk1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk1` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset_type_style`
--

DROP TABLE IF EXISTS `asset_type_style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(11) NOT NULL,
  `asset_colour` varchar(45) NOT NULL,
  `asset_image` blob,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeId_workspace_uq` (`asset_type_id`,`workspaceId`),
  KEY `assetTypeId_fk_idx` (`asset_type_id`),
  KEY `id_idx` (`asset_type_id`),
  KEY `assetTypeStyle_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `assetTypeId_fk` FOREIGN KEY (`asset_type_id`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeStyle_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bcm`
--

DROP TABLE IF EXISTS `bcm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bcmId` int(11) DEFAULT NULL,
  `bcmName` varchar(255) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_workspaceId_idx` (`workspaceId`),
  KEY `bcm_createdBy_idx` (`createdBy`),
  KEY `bcm_updatedBy_idx` (`updatedBy`),
  KEY `bcm_bcmId_idx` (`bcmId`),
  CONSTRAINT `bcm_bcmId` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bcm_level`
--

DROP TABLE IF EXISTS `bcm_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentBcmLevelId` int(11) DEFAULT NULL,
  `bcmId` int(11) NOT NULL,
  `levelNumber` int(2) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `bcmLevelName` varchar(60) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_level_parentBcmLevelId_fk_idx` (`parentBcmLevelId`),
  KEY `bcm_level_bcmId_fk_idx` (`bcmId`),
  KEY `bcm_level_createdBy_fk_idx` (`createdBy`),
  KEY `bcm_level_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `bcm_level_bcmId_fk` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_parentBcmLevelId_fk` FOREIGN KEY (`parentBcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=960 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `benchmark`
--

DROP TABLE IF EXISTS `benchmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benchmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `benchmark_name_wsid_uq` (`name`,`workspaceId`),
  KEY `benchmark_workspaceId_fk_idx` (`workspaceId`),
  KEY `benchmark_createdBy_fk_idx` (`createdBy`),
  KEY `benchmark_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `benchmark_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `benchmark_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `benchmark_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `benchmark_item`
--

DROP TABLE IF EXISTS `benchmark_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benchmark_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `baseName` varchar(45) NOT NULL,
  `version` varchar(45) DEFAULT NULL,
  `displayName` varchar(100) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `benchmarkId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `benchmark_item_displayName_benchmarkId_uq` (`displayName`,`benchmarkId`),
  KEY `benchmark_item_benchmarkId_fk_idx` (`benchmarkId`),
  CONSTRAINT `benchmark_item_benchmarkId_fk` FOREIGN KEY (`benchmarkId`) REFERENCES `benchmark` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `benchmark_item_score`
--

DROP TABLE IF EXISTS `benchmark_item_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benchmark_item_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `benchmarkItemId` int(11) NOT NULL,
  `startYear` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endYear` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `score` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bis_benchmarkItemId_fk_idx` (`benchmarkItemId`),
  CONSTRAINT `bis_benchmarkItemId_fk` FOREIGN KEY (`benchmarkItemId`) REFERENCES `benchmark_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `category_createdBy_fk_idx` (`createdBy`),
  KEY `category_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `category_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `category_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countryName` varchar(45) DEFAULT NULL,
  `countryCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dashboardName` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DASHBOARD_WORKSPACE_idx` (`workspaceId`),
  CONSTRAINT `FK_DASHBOARD_WORKSPACE` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `export_log`
--

DROP TABLE IF EXISTS `export_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `purpose` varchar(45) NOT NULL,
  `refId` int(11) NOT NULL,
  `timeDown` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeUp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `export_userId_fk_idx` (`userId`),
  KEY `export_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `export_userId_fk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `export_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `functional_map`
--

DROP TABLE IF EXISTS `functional_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `bcmId` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fm_bcm_idx` (`bcmId`),
  KEY `fm_createdBy_idx` (`createdBy`),
  KEY `fm_updatedBy_idx` (`updatedBy`),
  KEY `fm_workspaceId_idx` (`workspaceId`),
  KEY `fm_assetTemplate_idx` (`assetTemplateId`),
  KEY `fm_question_idx` (`questionId`),
  CONSTRAINT `fm_assetTemplate` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_bcm` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_question` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `functional_map_data`
--

DROP TABLE IF EXISTS `functional_map_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionalMapId` int(11) NOT NULL,
  `bcmLevelId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `data` text,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `questionnaireId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fmd_bmfId_idx` (`functionalMapId`),
  KEY `fmd_bcmLevel_idx` (`bcmLevelId`),
  KEY `fmd_asset_idx` (`assetId`),
  KEY `fmd_userId_fk_idx` (`updatedBy`),
  KEY `questionnaireId` (`questionnaireId`),
  CONSTRAINT `fmd_asset_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_bcmLevel_fk` FOREIGN KEY (`bcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_fmId_fk` FOREIGN KEY (`functionalMapId`) REFERENCES `functional_map` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_userId_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `functional_map_data_ibfk_2` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2061 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `graph`
--

DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphName` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `graphType` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHNAME_WORKSPACE` (`graphName`,`workspaceId`,`graphType`),
  KEY `Graph_Workspace_FK_idx` (`workspaceId`),
  KEY `Graph_User_FK_idx` (`createdBy`),
  KEY `Graph_Updated_By_FK_idx` (`updatedBy`),
  CONSTRAINT `Graph_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Workspace_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `graph_asset_template`
--

DROP TABLE IF EXISTS `graph_asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHID_ASSETTEMPLATEID` (`graphId`,`assetTemplateId`),
  KEY `FK_GRAPHID_GRAPHID_idx` (`graphId`),
  KEY `FK_ASSETTEMPLATEID_ASSETTEMPLATEID_idx` (`assetTemplateId`),
  CONSTRAINT `FK_ASSETTEMPLATEID_ASSETTEMPLATEID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GRAPHID_GRAPHID` FOREIGN KEY (`graphId`) REFERENCES `graph` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `input_trend`
--

DROP TABLE IF EXISTS `input_trend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `input_trend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `startDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `TREND_CREATED_BY_FK_idx` (`createdBy`),
  KEY `TREND_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `TREND_WORKSPACEID_BY_FK_idx` (`workspaceId`),
  KEY `TREND_PARAMETER_BY_FK_idx` (`parameterId`),
  CONSTRAINT `TREND_CREATED_BY_FK_idx` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `TREND_PARAMETER_BY_FK_idx` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `TREND_UPDATED_BY_FK_idx` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `TREND_WORKSPACEID_BY_FK_idx` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interface`
--

DROP TABLE IF EXISTS `interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interfaceAssetId` int(11) NOT NULL,
  `assetSourceId` int(11) NOT NULL,
  `assetDestId` int(11) NOT NULL,
  `interfaceDirection` varchar(45) NOT NULL,
  `dataType` varchar(20) DEFAULT NULL,
  `frequency` varchar(20) DEFAULT NULL,
  `communicationMethod` varchar(20) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_INTERFACE_SRC_DEST` (`interfaceAssetId`,`assetSourceId`,`assetDestId`),
  KEY `Interface_Asset_FK_idx` (`assetSourceId`),
  KEY `Interface_Asset_Dest_FK_idx` (`assetDestId`),
  KEY `Interface_Created_By_FK_idx` (`createdBy`),
  KEY `Interface_Updated_By_FK_idx` (`updatedBy`),
  KEY `FK_INTERFACEASSETID_ASSETID_idx` (`interfaceAssetId`),
  CONSTRAINT `FK_INTERFACEASSETID_ASSETID` FOREIGN KEY (`interfaceAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Dest_FK` FOREIGN KEY (`assetDestId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Source_FK` FOREIGN KEY (`assetSourceId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueName` varchar(45) NOT NULL,
  `displayName` varchar(250) NOT NULL,
  `description` text,
  `type` varchar(4) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameter_uniqueName_workspaceId_uq` (`uniqueName`,`workspaceId`),
  KEY `parameter_workspaceId_fk_idx` (`workspaceId`),
  KEY `parameter_createdBy_fk_idx` (`createdBy`),
  KEY `parameter_updatedBy_fk_idx` (`updatedBy`),
  KEY `parameter_parentParameterId_idx` (`parentParameterId`),
  CONSTRAINT `parameter_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_parentParameterId` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parameter_config`
--

DROP TABLE IF EXISTS `parameter_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `parameterConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pc_parameterId_idx` (`parameterId`),
  CONSTRAINT `pc_parameterId` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parameter_function`
--

DROP TABLE IF EXISTS `parameter_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentParameterId` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `constant` int(11) NOT NULL DEFAULT '1',
  `questionId` int(11) DEFAULT NULL,
  `mandatoryQuestion` tinyint(1) NOT NULL DEFAULT '0',
  `childparameterId` int(11) DEFAULT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pf_parentParam_que_childParam_uq` (`parentParameterId`,`questionId`,`childparameterId`),
  KEY `parameter_function_parentParameterId_fk_idx` (`parentParameterId`),
  KEY `parameter_function_childParameterId_fk_idx` (`childparameterId`),
  KEY `parameter_function_questionId_fk_idx` (`questionId`),
  CONSTRAINT `parameter_function_childParameterId_fk` FOREIGN KEY (`childparameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parameter_quality_gate`
--

DROP TABLE IF EXISTS `parameter_quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `qualityGateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `qualityGate_parameter_fk_idx` (`parameterId`),
  KEY `qgp_qualityGate_fk_idx` (`qualityGateId`),
  CONSTRAINT `qgp_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgp_qualityGate_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quality_gate`
--

DROP TABLE IF EXISTS `quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `colorDescription` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qg_assetType_parameter_workspace_uq` (`workspaceId`,`name`),
  KEY `qg_workspace_fk_idx` (`workspaceId`),
  KEY `qg_createdBy_fk_idx` (`createdBy`),
  KEY `qg_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qg_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_workspace_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quality_gate_condition`
--

DROP TABLE IF EXISTS `quality_gate_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityGateId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `weight` decimal(10,3) DEFAULT NULL,
  `gateCondition` varchar(45) NOT NULL,
  `conditionValues` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qgc_parameter_fk_idx` (`parameterId`),
  KEY `qgc_qg_fk_idx` (`qualityGateId`),
  KEY `qgc_createdBy_fk_idx` (`createdBy`),
  KEY `qgc_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qgc_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_qg_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentQuesId` int(11) DEFAULT NULL,
  `questionMode` varchar(20) NOT NULL,
  `title` text NOT NULL,
  `displayName` varchar(256) NOT NULL,
  `helpText` text,
  `workspaceId` int(11) DEFAULT NULL,
  `questionType` varchar(10) NOT NULL,
  `queTypeText` text,
  `level` smallint(6) NOT NULL,
  `questionCategoryId` int(11) DEFAULT NULL,
  `benchmarkId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ques_workspace_displayName_uq` (`workspaceId`,`displayName`),
  KEY `QUESTIONNAIRE_WORKSPACE_ID_FK_idx` (`workspaceId`),
  KEY `QUESTIONNAIRE_CREATED_BY_FK_idx` (`createdBy`),
  KEY `QUESTIONNAIRE_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `question_questioncategoryid_categoryid_idx` (`questionCategoryId`),
  KEY `ques_quesId_fk_idx` (`parentQuesId`),
  KEY `question_benchmarkId_fk_idx` (`benchmarkId`),
  CONSTRAINT `QUESTIONNAIRE_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_WORKSPACE_ID_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ques_quesId_fk` FOREIGN KEY (`parentQuesId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `question_benchmarkId_fk` FOREIGN KEY (`benchmarkId`) REFERENCES `benchmark` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `question_questioncategoryid_categoryid` FOREIGN KEY (`questionCategoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `questionnaireType` varchar(5) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `year` varchar(4) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_name_workspace_unique` (`name`,`workspaceId`),
  UNIQUE KEY `questionnaire_assetTypeId_year` (`assetTypeId`,`year`),
  KEY `questionnaire_workspaceId_fk_idx` (`workspaceId`),
  KEY `questionnaire_createdBy_fk_idx` (`createdBy`),
  KEY `questionnaire_updatedBy_fk_idx` (`updatedBy`),
  KEY `questionnaire_assetTypeId_fk_idx` (`assetTypeId`),
  CONSTRAINT `questionnaire_assetTypeId_fk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questionnaire_asset`
--

DROP TABLE IF EXISTS `questionnaire_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_asset_questionnaireAssetId_uq` (`questionnaireId`,`assetId`),
  KEY `questionnaire_asset_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_asset_assetId_fk_idx` (`assetId`),
  CONSTRAINT `questionnaire_asset_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_asset_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=665 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questionnaire_parameter`
--

DROP TABLE IF EXISTS `questionnaire_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `rootParameterId` int(11) NOT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qp_questionnaireId_parameterId_parentParameterId_uq` (`questionnaireId`,`parameterId`,`parentParameterId`,`rootParameterId`),
  KEY `qp_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `qp_parameterId_fk_idx` (`parameterId`),
  KEY `qp_rootParameterId_fk_idx` (`rootParameterId`),
  KEY `qp_parentParameterId_fk_idx` (`parentParameterId`),
  CONSTRAINT `qp_parameterId_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_rootParameterId_fk` FOREIGN KEY (`rootParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questionnaire_parameter_asset`
--

DROP TABLE IF EXISTS `questionnaire_parameter_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_parameter_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `boostPercentage` decimal(5,2) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Qe_P_A` (`questionnaireId`,`parameterId`,`assetId`),
  KEY `FK_ParameterId_idx` (`parameterId`),
  KEY `FK_AssetId_idx` (`assetId`),
  KEY `FK_CreatedBy_idx` (`createdBy`),
  KEY `FK_UpdatedBy_idx` (`updatedBy`),
  CONSTRAINT `FK_AssetId` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CreatedBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ParameterId` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_QuestionnaireId` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UpdatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questionnaire_question`
--

DROP TABLE IF EXISTS `questionnaire_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) DEFAULT NULL,
  `questionId` int(11) NOT NULL,
  `questionnaireAssetId` int(11) DEFAULT NULL,
  `questionnaireParameterId` int(11) NOT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qq_QEId_QId_QAId_QPId_uq` (`questionnaireId`,`questionId`,`questionnaireAssetId`,`questionnaireParameterId`),
  KEY `questionnaire_question_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_question_questionId_fk_idx` (`questionId`),
  KEY `questionnaire_question_assetId_fk_idx` (`questionnaireAssetId`),
  KEY `qq_questionnaireParameterId_fk_idx` (`questionnaireParameterId`),
  CONSTRAINT `qq_questionnaireParameterId_fk` FOREIGN KEY (`questionnaireParameterId`) REFERENCES `questionnaire_parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireAssetId_fk` FOREIGN KEY (`questionnaireAssetId`) REFERENCES `questionnaire_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13058 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relationship_asset_data`
--

DROP TABLE IF EXISTS `relationship_asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rad_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `rad_childAssetId_fk_idx` (`childAssetId`),
  KEY `rad_assetId_fk_idx` (`assetId`),
  CONSTRAINT `rad_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relationship_template`
--

DROP TABLE IF EXISTS `relationship_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `hasAttribute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_template_displayName_assetTemplateId_uq` (`displayName`,`assetTemplateId`),
  KEY `relationship_template_parentAssetTypeId_fk_idx` (`parentAssetTypeId`),
  KEY `relationship_template_childAssetTypeId_fk_idx` (`childAssetTypeId`),
  KEY `relationship_template_relationshipTypeId_fk_idx` (`relationshipTypeId`),
  KEY `relationship_template_assetTemplateId_fk_idx` (`assetTemplateId`),
  CONSTRAINT `relationship_template_assetTemplateId_fk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_childAssetTypeId_fk` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_parentAssetTypeId_fk` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_relationshipTypeId_fk` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relationship_type`
--

DROP TABLE IF EXISTS `relationship_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentDescriptor` varchar(50) NOT NULL,
  `childDescriptor` varchar(50) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `displayName` varchar(150) NOT NULL,
  `direction` varchar(4) NOT NULL DEFAULT 'UNI',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_type_displayName_workspaceId_uq` (`workspaceId`,`displayName`),
  KEY `relationship_type_createdBy_idx` (`createdBy`),
  KEY `relationship_type_updatedBy_idx` (`updatedBy`),
  KEY `relationship_type_workspaceId_idx` (`workspaceId`),
  CONSTRAINT `relationship_type_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportName` varchar(45) NOT NULL,
  `reportType` varchar(45) NOT NULL,
  `description` text,
  `reportConfig` varchar(500) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `deletedStatus` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `REPORT_CREATED_BY_FK_idx` (`createdBy`),
  KEY `REPORT_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `REPORT_WORKSPACEID_FK_idx_idx` (`workspaceId`),
  CONSTRAINT `REPORT_CREATED_BY_FK_idx` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `REPORT_UPDATED_BY_FK_idx` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `REPORT_WORKSPACEID_FK_idx` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `response_createdBy_fk_idx` (`createdBy`),
  KEY `response_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `response_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `response_data`
--

DROP TABLE IF EXISTS `response_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireQuestionId` int(11) NOT NULL,
  `responseId` int(11) NOT NULL,
  `responseData` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_response_data_questionnaireQuestionId_responseId` (`questionnaireQuestionId`,`responseId`),
  KEY `response_data_questionnaireId_fk_idx` (`questionnaireQuestionId`),
  KEY `response_data_responseId_fk_idx` (`responseId`),
  CONSTRAINT `response_data_questionnaireQuestionId_fk` FOREIGN KEY (`questionnaireQuestionId`) REFERENCES `questionnaire_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_data_responseId_fk` FOREIGN KEY (`responseId`) REFERENCES `response` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12432 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  `roleDescription` varchar(500) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `permissionname` varchar(45) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ROLE_PERMISSION` (`permissionname`,`roleid`),
  KEY `FK_ROLEPERM_ROLEID_idx` (`roleid`),
  CONSTRAINT `FK_ROLEPERM_ROLEID` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1451 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_master`
--

DROP TABLE IF EXISTS `style_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(45) DEFAULT NULL,
  `active_status` tinyint(1) DEFAULT '0',
  `bcmLevelId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmptable`
--

DROP TABLE IF EXISTS `tmptable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmptable` (
  `logmessage` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `organizationName` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `questionAnswer` text,
  `photo` blob,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `accountExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountLocked` tinyint(1) NOT NULL DEFAULT '0',
  `credentialExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `firstLogin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailAddress_UNIQUE` (`emailAddress`),
  UNIQUE KEY `UK_t40ack6c3x1y4m2nhaju018jg` (`emailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_report`
--

DROP TABLE IF EXISTS `user_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `REPORT_FK_idx_idx` (`reportId`),
  KEY `USER_FK_idx_idx` (`userId`),
  CONSTRAINT `REPORT_FK_idx` FOREIGN KEY (`reportId`) REFERENCES `report` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `USER_FK_idx` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_role_unique` (`userId`,`roleId`),
  KEY `roleIdFk_idx` (`roleId`),
  KEY `userIdFk_idx` (`userId`),
  CONSTRAINT `roleIdFk` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `userIdFk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_workspace`
--

DROP TABLE IF EXISTS `user_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `selectedHomePage` varchar(64) DEFAULT NULL,
  `homePageURL` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_USER_WORKSPACE` (`userId`,`workspaceId`),
  KEY `FK_WORKSPACEID_WORKSPACEID_JOIN_idx` (`workspaceId`),
  CONSTRAINT `FK_USERID_USERID_JOIN` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACEID_WORKSPACEID_JOIN` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widgetType` varchar(20) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `widgetConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_WIDGET_DASHBOARD_idx` (`dashboardId`),
  CONSTRAINT `FK_WIDGET_DASHBOARD` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workspace`
--

DROP TABLE IF EXISTS `workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceName` varchar(45) NOT NULL,
  `workspaceDescription` text,
  `deletedStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_WORKSPACE_USER_idx` (`createdBy`),
  KEY `FK_WORKSPACE_USER_UPDATE_idx` (`updatedBy`),
  CONSTRAINT `FK_WORKSPACE_USER_CREATE` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_USER_UPDATE` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workspace_dashboard`
--

DROP TABLE IF EXISTS `workspace_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceId` int(11) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_WORKSPACE_DASHBOARD` (`workspaceId`,`dashboardId`),
  KEY `FK_WORKSPACE_ID_idx` (`workspaceId`),
  KEY `FK_DASHBOARD_ID_idx` (`dashboardId`),
  CONSTRAINT `FK_DASHBOARD_ID` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_ID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database '3rdi_tanushree'
--
/*!50003 DROP FUNCTION IF EXISTS `deleteAidBlockAndAidForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteAidBlockAndAidForWorkspace`( vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
delete from aid_block  where aidid IN (select id from aid where workspaceId = vWorkspaceId);
delete from aid where workspaceId = vWorkspaceId;
RETURN vres;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteAssetForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteAssetForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE rad FROM relationship_asset_data rad join asset a on rad.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE rad FROM relationship_asset_data rad join asset a on rad.parentAssetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE rad FROM relationship_asset_data rad join asset a on rad.childAssetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE qa FROM questionnaire_asset qa join asset a on qa.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE qpa FROM questionnaire_parameter_asset qpa join asset a on qpa.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE fmd FROM functional_map_data fmd join asset a on fmd.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE ad FROM asset_data ad  join asset a on ad.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE rt FROM relationship_template rt join asset_template ast on rt.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE gat FROM graph_asset_template gat join asset_template ast on gat.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE fm FROM functional_map fm join asset_template ast on fm.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE a FROM asset a join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE atc FROM asset_template_column atc join asset_template ast on atc.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
                Delete a from aid a join asset_template at on a.assetTemplateId = at.id where at.workspaceId=vWorkspaceId;
DELETE at FROM asset_template at where at.workspaceId=vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteBenchmarkForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteBenchmarkForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
delete bis from benchmark_item_score bis join benchmark_item bi on bis.benchmarkItemId = bi.id join benchmark b where b.workspaceId = vWorkspaceId;
delete bi from benchmark_item bi join benchmark b on bi.benchmarkId = b.id where b.workspaceId =  vWorkspaceId;
delete b from benchmark b where b.workspaceId = vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteExportLogForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteExportLogForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete el from export_log el where el.workspaceId = vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteFmForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteFmForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete fmd from functional_map_data fmd join functional_map fm on fmd.functionalMapId = fm.id where fm.workspaceId = vWorkspaceId;
Delete fm from functional_map fm where fm.workspaceId = vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteGraphForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteGraphForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete gat from graph_asset_template gat join graph g on gat.graphId = g.id where g.workspaceId = vWorkspaceId;
Delete g from graph g where g.workspaceId= vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteParameterForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteParameterForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE pc FROM parameter_config pc join parameter p on pc.parameterId=p.id
                where p.workspaceId= vWorkspaceId;
 
DELETE pf FROM parameter_function pf join parameter p on pf.parentParameterId=p.id
                where p.workspaceId= vWorkspaceId;
DELETE pf FROM parameter_function pf join parameter p on pf.childparameterId=p.id
                where p.workspaceId= vWorkspaceId;
DELETE pqg FROM parameter_quality_gate pqg join parameter p on pqg.parameterId=p.id
                where p.workspaceId= vWorkspaceId;
Delete qp from questionnaire_parameter qp join parameter p on qp.parameterId = p.id where p.workspaceId =vWorkspaceId;
 
Delete qgc from quality_gate_condition qgc join parameter p on  qgc.parameterId = p.id where p.workspaceId =vWorkspaceId;
delete  from parameter where workspaceId = vWorkspaceId order by id desc;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteQualityGateForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteQualityGateForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE qgc FROM quality_gate_condition qgc join quality_gate qg on qgc.qualityGateId = qg.id  where qg.workspaceId = vWorkspaceId;
Delete pqg from parameter_quality_gate pqg join quality_gate qg on pqg.qualityGateId = qg.id where qg.workspaceId = vWorkspaceId;
DELETE qg from quality_gate qg where qg.workspaceId = vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteQuestionForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteQuestionForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE qq FROM questionnaire_question qq join question q on qq.questionId=q.id
                where q.workspaceId= vWorkspaceId;
DELETE pf FROM parameter_function pf join question q on pf.questionId=q.id
                where q.workspaceId= vWorkspaceId;
DELETE fm FROM functional_map fm join question q on fm.questionId=q.id
                where q.workspaceId= vWorkspaceId;
DELETE from question where workspaceId = vWorkspaceId order by id desc;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteQuestionnaireForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteQuestionnaireForWorkspace`(vWorkspaceId INT ) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE rd FROM response_data rd join questionnaire_question qq on rd.questionnaireQuestionId=qq.id
                                join questionnaire q on qq.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE r FROM response r join questionnaire q on r.questionnaireId=q.id
        where q.workspaceId=vWorkspaceId;
                DELETE qq FROM questionnaire_question qq join questionnaire q on qq.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE qa FROM questionnaire_asset qa join questionnaire q on qa.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE qp FROM questionnaire_parameter qp join questionnaire q on qp.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE qpa FROM questionnaire_parameter_asset qpa join questionnaire q on qpa.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE fmd FROM functional_map_data fmd join questionnaire q on fmd.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE q FROM questionnaire q where q.workspaceId=vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteRelationshipTypeForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteRelationshipTypeForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete rte from relationship_template rte join relationship_type rt on rte.relationshipTypeId = rt.id where rt.workspaceId = vWorkspaceId;
Delete rt from relationship_type rt where rt.workspaceId = vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteUserWorkspaceForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteUserWorkspaceForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete uw from user_workspace uw where uw.workspaceId = vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteWidgetAndDashboardForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteWidgetAndDashboardForWorkspace`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
delete w from widget w join dashboard d on w.dashboardId = d.id where d.workspaceId = vWorkspaceId;
delete from dashboard where workspaceId = vWorkspaceId;
RETURN vres;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `deleteWorkspaceDashboard` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` FUNCTION `deleteWorkspaceDashboard`(vWorkspaceId INT) RETURNS int(11)
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete wd from workspace_dashboard wd where wd.workspaceId = vWorkspaceId;
RETURN vres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GetParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GetParameterTree`(GivenID INT) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv,q,queue,queue_children VARCHAR(1024);
    DECLARE queue_length,front_id,pos INT;

    SET rv = '';
    SET queue = GivenID;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        
        IF queue_length = 1 THEN
            SET front_id = FORMAT(queue,0);
            SET queue = '';
        ELSE
			SET front_id = SUBSTR(queue,1,LOCATE(',',queue)-1);
            SET pos = LOCATE(',',queue) + 1;
            SET q = SUBSTR(queue,pos);
            SET queue = q;
        END IF;
        SET queue_length = queue_length - 1;

        SELECT IFNULL(qc,'') INTO queue_children
        FROM (SELECT GROUP_CONCAT(childparameterId) qc
        FROM parameter_function WHERE parentParameterId = front_id) A;

        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_children;
            ELSE
                SET rv = CONCAT(rv,',',queue_children);
            END IF;
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;

    RETURN rv;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deletebcmForWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` PROCEDURE `deletebcmForWorkspace`(vWorkspaceId INT)
begin
DECLARE done INT DEFAULT FALSE;
declare vres INT;
declare vbcm_id INT;
declare v_parentBcmLevelId INT;
 
DEClARE bcm_level_cursor CURSOR FOR
select parentBcmLevelId
from bcm_level b
where bcmid=vbcm_id and parentBcmLevelId is not null order by levelnumber desc;
 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
 
 
set vres = 0;
SELECT id
into vbcm_id
FROM bcm
where workspaceId=vWorkspaceId;
 
OPEN bcm_level_cursor;
 
 delete_parentBCMLevel: LOOP
 
 FETCH bcm_level_cursor INTO v_parentBcmLevelId;
 
 IF done THEN
      LEAVE delete_parentBCMLevel;
    END IF;

 
 delete from bcm_level where bcmid=vbcm_id and parentBcmLevelId=v_parentBcmLevelId;
 
 END LOOP delete_parentBCMLevel;
 
 CLOSE bcm_level_cursor;
 delete from bcm_level where bcmid=vbcm_id and parentBcmLevelId is null;
delete from bcm where workspaceId =vWorkspaceId;
delete from workspace where id = vWorkspaceId;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteWorkspace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`3rdi_app_tanushree`@`%` PROCEDURE `deleteWorkspace`( IN vWorkspaceId INT,OUT vres INT)
begin
DECLARE exit HANDLER FOR SQLEXCEPTION SET vres = 1;
set vres = 0;
set vres = deleteAidBlockAndAidForWorkspace(vWorkspaceId);
set vres = deleteWidgetAndDashboardForWorkspace(vWorkspaceId);
set vres = deleteQuestionnaireForWorkspace(vWorkspaceId);
set vres = deleteBenchmarkForWorkspace(vWorkspaceId);
set vres = deleteParameterForWorkspace(vWorkspaceId);
set vres = deleteQuestionForWorkspace(vWorkspaceId);
set vres =deleteAssetForWorkspace(vWorkspaceId);
set vres = deleteQualityGateForWorkspace(vWorkspaceId);
set vres = deleteFmForWorkspace(vWorkspaceId);
set vres =deleteExportLogForWorkspace(vWorkspaceId);
set vres =deleteGraphForWorkspace(vWorkspaceId);
set vres=deleteRelationshipTypeForWorkspace(vWorkspaceId);
set vres =deleteWorkspaceDashboard(vWorkspaceId);
set vres=deleteUserWorkspaceForWorkspace(vWorkspaceId);
call deletebcmForWorkspace(vWorkspaceId);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getParameterTree`(IN GivenID int, OUT rv VARCHAR(1024))
BEGIN

    set rv = getParameterTree(GivenID);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-02 14:12:15
