-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `relationship_type` 
ADD COLUMN `direction` VARCHAR(4) NOT NULL AFTER `displayName`;

ALTER TABLE `relationship_type` 
CHANGE COLUMN `direction` `direction` VARCHAR(4) NOT NULL DEFAULT 'UNI';

UPDATE `relationship_type` set direction = 'UNI';


ALTER TABLE `asset` ADD COLUMN  `uid` VARCHAR(256) AFTER `templateId`;

UPDATE asset a1 inner join asset a2 set a2.uid =  a2.id;

ALTER TABLE `asset` CHANGE COLUMN `uid` `uid` VARCHAR(256) NOT NULL ,
ADD UNIQUE INDEX `uid_UNIQUE` (`uid` ASC);


CREATE TABLE `relationship_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `hasAttribute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_template_displayName_assetTemplateId_uq` (`displayName`,`assetTemplateId`),
  KEY `relationship_template_parentAssetTypeId_fk_idx` (`parentAssetTypeId`),
  KEY `relationship_template_childAssetTypeId_fk_idx` (`childAssetTypeId`),
  KEY `relationship_template_relationshipTypeId_fk_idx` (`relationshipTypeId`),
  KEY `relationship_template_assetTemplateId_fk_idx` (`assetTemplateId`),
  CONSTRAINT `relationship_template_assetTemplateId_fk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_childAssetTypeId_fk` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_parentAssetTypeId_fk` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_relationshipTypeId_fk` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
