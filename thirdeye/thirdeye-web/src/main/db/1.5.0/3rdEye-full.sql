CREATE DATABASE  IF NOT EXISTS `3rdi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `3rdi`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: 3rdi
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aid`
--

DROP TABLE IF EXISTS `aid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `aidTemplate` varchar(25) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_AID_Name_WorkspaceId` (`name`,`workspaceId`),
  KEY `FK_AID_WORKSPACEID_idx` (`workspaceId`),
  KEY `FK_AID_ASSETTEMPLATID_idx` (`assetTemplateId`),
  KEY `FK_CREATEDBY_USERID_idx` (`createdBy`),
  KEY `FK_UPDATEDBY_USERID_idx` (`updatedBy`),
  CONSTRAINT `FK_AID_ASSETTEMPLATID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AID_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CREATEDBY_USERID` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UPDATEDBY_USERID` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid`
--

LOCK TABLES `aid` WRITE;
/*!40000 ALTER TABLE `aid` DISABLE KEYS */;
INSERT INTO `aid` VALUES (1,'Test AID','this is the test AID',1,'DEFAULT',1,2,'2016-04-26 06:03:25',2,'2016-04-26 06:03:25');
/*!40000 ALTER TABLE `aid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aid_block`
--

DROP TABLE IF EXISTS `aid_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aidId` int(11) NOT NULL,
  `aidBlockType` varchar(20) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `blockJSONConfig` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_AIDID_AID_idx` (`aidId`),
  CONSTRAINT `FK_AIDID_AID` FOREIGN KEY (`aidId`) REFERENCES `aid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid_block`
--

LOCK TABLES `aid_block` WRITE;
/*!40000 ALTER TABLE `aid_block` DISABLE KEYS */;
INSERT INTO `aid_block` VALUES (1,1,'BLOCK_4',1,'{\"blockTitle\":\"\",\"subBlock1\":{\"templateColumnId\":0,\"blockTitle\":\"Test Sub block 1\",\"paramQEId\":0,\"paramId\":0,\"questionId\":1,\"questionQEId\":1},\"subBlock2\":{\"templateColumnId\":3,\"blockTitle\":\"Test Sub block 2\",\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0}}'),(2,1,'BLOCK_1',2,'');
/*!40000 ALTER TABLE `aid_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL,
  `uid` varchar(256) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  KEY `ASSET_TEMPLATE_FK_idx` (`templateId`),
  KEY `ASSET_CREATED_BY_FK_idx` (`createdBy`),
  KEY `ASSET_UPDATED_BY_FK_idx` (`updatedBy`),
  CONSTRAINT `ASSET_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_TEMPLATE_FK` FOREIGN KEY (`templateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,1,'1',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(2,1,'2',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(3,1,'3',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(4,1,'4',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(5,2,'5',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(6,2,'6',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(7,3,'7',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(8,3,'8',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(9,3,'9',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(10,3,'10',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(11,3,'11',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(12,3,'12',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(13,3,'13',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(14,3,'14',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(15,1,'15',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(16,6,'16',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(17,6,'17',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(18,6,'18',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(19,6,'19',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(20,6,'20',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(21,6,'21',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(22,6,'22',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(23,6,'23',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(24,6,'24',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(25,6,'25',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(26,6,'26',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(27,6,'27',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(28,4,'28',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(29,6,'29',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(30,6,'30',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(31,6,'31',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(32,6,'32',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(33,6,'33',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(34,6,'34',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(35,1,'35',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(36,2,'36',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(37,6,'37',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(38,6,'38',0,2,'2016-07-07 11:04:42',2,'2016-07-07 11:04:42'),(39,6,'39',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(40,1,'40',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(41,4,'41',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(42,2,'42',0,2,'2016-07-18 11:11:18',2,'2016-07-18 11:11:18'),(43,12,'123::Asset::Relationship::12::',0,2,'2016-07-25 08:57:49',2,'2016-07-25 08:57:49'),(45,15,'123::Asset::Relationship::15::',0,2,'2016-07-25 09:08:34',2,'2016-07-25 09:08:34'),(48,14,'123::Asset::Relationship::14::',0,2,'2016-07-25 10:53:34',2,'2016-07-25 10:53:34'),(50,14,'123::Asset::Relationship::14::Nexia',0,2,'2016-07-25 12:34:54',2,'2016-07-25 12:34:54'),(51,2,'123::Asset::Server::2::Google',0,2,'2016-07-25 12:37:52',2,'2016-07-25 12:37:52'),(53,2,'123::Asset::Server::2::Google12',0,2,'2016-07-25 12:38:21',2,'2016-07-25 12:38:21'),(54,19,'123::Asset::Relationship::19::Google',0,2,'2016-07-25 12:38:49',2,'2016-07-25 12:38:49'),(57,19,'123::Asset::Relationship::19::Test App',0,2,'2016-07-25 13:36:58',2,'2016-07-25 13:36:58'),(59,19,'123::Asset::Relationship::19::EWDF',0,2,'2016-07-25 13:37:22',2,'2016-07-25 13:37:22'),(60,17,'123::Asset::Relationship::17::sdc',0,2,'2016-07-26 05:34:21',2,'2016-07-26 05:34:21'),(61,20,'123::Asset::Relationship::20::sfdffdf',0,2,'2016-07-26 12:15:10',2,'2016-07-26 12:15:10'),(62,20,'123::Asset::Relationship::20::Name2',0,2,'2016-07-26 12:41:59',2,'2016-07-26 12:41:59'),(64,14,'123::Asset::Relationship::14::ISSPL',0,2,'2016-07-26 12:56:05',2,'2016-07-26 12:56:05'),(65,14,'123::Asset::Relationship::14::Test App',0,2,'2016-07-26 12:56:30',2,'2016-07-26 12:56:30'),(66,14,'123::Asset::Relationship::14::stoleman',0,2,'2016-07-26 12:57:48',2,'2016-07-26 12:57:48'),(67,19,'123::Asset::Relationship::19::xcbg',0,2,'2016-07-26 13:09:42',2,'2016-07-26 13:09:42'),(68,19,'123::Asset::Relationship::19::wadrwe',0,2,'2016-07-27 07:08:53',2,'2016-07-27 07:08:53'),(69,2,'123::Asset::Server::2::Name 1',0,2,'2016-07-28 12:23:54',2,'2016-07-28 12:23:54'),(70,19,'123::Asset::Relationship::19::Name1',0,2,'2016-07-28 12:24:24',2,'2016-07-28 12:24:24'),(72,19,'123::Asset::Relationship::19::stoleman',0,2,'2016-07-28 12:33:30',2,'2016-07-28 12:33:30'),(73,19,'123::Asset::Relationship::19::xcvcx',0,2,'2016-07-28 12:33:54',2,'2016-07-28 12:33:54'),(75,19,'123::Asset::Relationship::19::HT',0,2,'2016-07-28 12:35:42',2,'2016-07-28 12:35:42'),(79,19,'123::Asset::Relationship::19::weferg_Test Application',0,2,'2016-07-29 12:09:49',2,'2016-07-29 12:09:49'),(80,19,'123::Asset::Relationship::19::Google_Google1',0,2,'2016-07-29 12:11:25',2,'2016-07-29 12:11:25'),(81,1,'123::Asset::Application::1::dfsdf',0,2,'2016-07-29 12:12:19',2,'2016-07-29 12:12:19'),(82,20,'123::Asset::Relationship::20::Deep',0,2,'2016-08-02 05:43:02',2,'2016-08-02 05:43:02'),(83,21,'123::Asset::Relationship::21::Google_Google',0,2,'2016-08-02 06:23:58',2,'2016-08-02 06:23:58'),(84,22,'123::Asset::Relationship::22::Google_sdc',0,2,'2016-08-02 06:38:37',2,'2016-08-02 06:38:37'),(85,17,'123::Asset::Relationship::17::Google_Google1',0,2,'2016-08-26 10:51:52',2,'2016-08-26 10:51:52');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_data`
--

DROP TABLE IF EXISTS `asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text,
  `assetTemplateColId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assetTemplateColIdFk_idx` (`assetTemplateColId`),
  KEY `AssetData_Asset_FK_idx` (`assetId`),
  CONSTRAINT `AssetData_Asset_FK` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTemplateColIdFk` FOREIGN KEY (`assetTemplateColId`) REFERENCES `asset_template_column` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_data`
--

LOCK TABLES `asset_data` WRITE;
/*!40000 ALTER TABLE `asset_data` DISABLE KEYS */;
INSERT INTO `asset_data` VALUES (1,'Google Inc',3,2),(2,'Google',1,1),(3,'',2,2),(4,'Google Inc1',3,1),(5,'',2,1),(6,'Google1',1,2),(7,'Naukri.com',3,3),(8,'Naukri',1,3),(9,'',2,3),(10,'Naukri1',1,4),(11,'',2,4),(12,'Naukri.com',3,4),(13,'Tomcat',4,5),(14,'Tomcat1',4,6),(15,'it is app server',5,5),(16,'it is app server1',5,6),(17,'3rdEye',6,7),(18,'FuelSignal',6,8),(19,'DVVNL',6,9),(20,'ISSPL',6,10),(21,'HT',6,11),(22,'Nexia',6,12),(23,'Edusoft',6,13),(24,'stoleman',6,14),(25,'dffff',9,15),(26,'dfdf',2,15),(27,'fdfdff',3,15),(28,'ferf',1,15),(29,NULL,10,16),(30,'sfdffdf',8,16),(31,'2016-08-07',10,17),(32,'gfhngh',8,17),(33,'2016-06-30',10,18),(34,'wadrwe',8,18),(35,'2016-04-29',10,19),(36,'tfghgf',8,19),(37,NULL,10,20),(38,'sdgffdg',8,20),(39,'2016-06-30',10,21),(40,'xcvcx',8,21),(41,'xcvcx',8,22),(42,'2016-06-30',10,22),(43,NULL,10,23),(44,'EWDF',8,23),(45,'EWDF',8,24),(46,NULL,10,24),(47,NULL,10,25),(48,'dfgdf',8,25),(49,'2016-06-22',10,26),(50,'xcbg',8,26),(51,'2016-06-22',10,27),(52,'sdfgdsffgdf',8,27),(53,'Test App',7,28),(54,'Test Application',8,29),(55,'2016-07-01',10,29),(56,'Test Application',8,30),(57,'2016-07-01',10,30),(58,'Name1',8,31),(59,'2016-07-02',10,31),(60,'2016-07-13',10,32),(61,'Name2',8,32),(62,'2016-07-02',10,33),(63,'Name2',8,33),(64,'weferg',8,34),(65,'2016-07-08',10,34),(66,'GIC Db',2,35),(67,'Google',1,35),(68,'dffff',9,35),(69,'5',3,35),(70,'dfdd',5,36),(71,'sdc',4,36),(72,'2016-07-04',10,37),(73,'EWDF',8,37),(74,'gfhngh',8,38),(75,'2016-07-15',10,38),(76,'2016-07-14',10,39),(77,'EWDF',8,39),(78,'Google',1,40),(79,'Samar',3,40),(80,'gdfdfg',2,40),(81,'fgh',9,40),(82,'Google',7,41),(83,'Google',4,42),(84,'test',5,42),(87,'Google',4,51),(88,'Google desc',5,51),(89,'Google desc',5,53),(90,'Google12',4,53),(91,'456',17,54),(92,'fgfd',16,54),(93,'sdg',16,57),(94,'4',17,57),(95,'4',17,59),(96,'sdg',16,59),(97,'Deep',18,61),(98,'Deep1',18,62),(99,'56',17,67),(100,'vgbngc',16,67),(101,'gh',16,68),(102,'56',17,68),(103,'Name 1',4,69),(104,'Desc ',5,69),(105,'Rela 2',16,70),(106,'3',17,70),(107,'4',17,72),(108,'Rela1324',16,72),(109,'655',17,73),(110,'trtrt',16,73),(111,'4',17,75),(112,'rtgrddf',16,75),(113,'Rela45455',16,79),(114,'5',17,79),(115,'4erwer',16,80),(116,'4',17,80),(117,'dfsdf',1,81),(118,'dfgdfg',3,81),(119,'dfgdfg',2,81),(120,'gdfgdfg',9,81),(121,'Deep',18,82);
/*!40000 ALTER TABLE `asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template`
--

DROP TABLE IF EXISTS `asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateName` varchar(45) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateName_createdBy_UNIQUE` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  UNIQUE KEY `UK_a35ol4p1y14cnqk9j9yqcoj13` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  KEY `id_idx` (`assetTypeId`),
  KEY `updatedByFk2_idx` (`updatedBy`),
  KEY `createdBy_UNIQUE` (`createdBy`),
  KEY `FK_ASSETTEMPLATE_WORKSPACEID_idx` (`workspaceId`),
  CONSTRAINT `FK_ASSETTEMPLATE_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeIdFk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk2` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk2` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template`
--

LOCK TABLES `asset_template` WRITE;
/*!40000 ALTER TABLE `asset_template` DISABLE KEYS */;
INSERT INTO `asset_template` VALUES (1,'template test',1,0,'',1,2,'2016-04-20 10:26:19',2,'2016-06-06 10:09:26'),(2,'Server Inventory',2,0,'Server Inventory desc',1,2,'2016-04-20 10:32:53',2,'2016-06-06 11:09:23'),(3,'App Inventory',1,0,'',1,2,'2016-05-27 06:44:17',2,'2016-06-06 11:09:04'),(4,'template tes',1,0,'fbgdfgfgggfgfg',1,2,'2016-06-06 09:29:28',2,'2016-06-06 11:01:23'),(6,'application Inventory',1,0,'',1,2,'2016-06-06 11:08:51',2,'2016-06-06 11:09:45'),(7,'template test rela',8,0,'sefs',1,2,'2016-07-06 08:21:44',2,'2016-08-02 06:35:45'),(9,'template test rela1',8,0,'sefs',1,2,'2016-07-06 08:28:29',2,'2016-07-06 08:28:29'),(10,'template test rela2',8,0,'sdc',1,2,'2016-07-06 08:31:48',2,'2016-07-06 08:31:48'),(11,'template test dsfd',8,0,'',1,2,'2016-07-06 11:31:40',2,'2016-07-06 11:31:40'),(12,'template test bfgghfgh',8,0,'',1,2,'2016-07-06 11:36:42',2,'2016-07-06 11:36:42'),(13,'erggerg',8,0,'dfdfgdf',1,2,'2016-07-06 13:28:41',2,'2016-07-06 13:28:41'),(14,'wdedryuyuuy',8,0,'',1,2,'2016-07-06 13:35:51',2,'2016-07-25 08:48:30'),(15,'template test',8,0,'',1,2,'2016-07-07 05:41:27',2,'2016-07-07 05:41:27'),(16,'sdcdsda',8,0,'',1,2,'2016-07-07 08:42:45',2,'2016-07-07 08:42:45'),(17,'template test98',8,0,'sa',1,2,'2016-07-07 08:48:00',2,'2016-07-19 10:05:29'),(18,'template test456',1,0,'retg',1,2,'2016-07-07 09:18:58',2,'2016-07-07 09:18:58'),(19,'Test Relationship Template',8,0,'Test Relationship Template',1,2,'2016-07-07 09:19:40',2,'2016-08-01 09:39:57'),(20,'Relat',8,0,'',1,2,'2016-07-26 12:13:37',2,'2016-07-26 12:13:37'),(21,'Test Relationship Template1',8,0,'',1,2,'2016-08-02 06:23:31',2,'2016-08-02 06:23:31'),(22,'Test Relationship Template2',8,0,'',1,2,'2016-08-02 06:38:24',2,'2016-08-02 06:38:24'),(23,'App Test',1,1,'',1,2,'2016-08-02 07:41:57',2,'2016-08-02 07:41:57'),(24,'gdrt',2,0,'drfgd',1,2,'2016-08-02 10:09:45',2,'2016-08-02 10:09:45'),(25,'sdf',8,0,'',1,2,'2016-08-02 10:17:37',2,'2016-08-02 10:17:37'),(26,'rftj',8,1,'rt',1,2,'2016-08-02 10:25:26',2,'2016-08-02 10:25:26'),(27,'Relationship template test',8,1,'WeWE',1,2,'2016-08-03 10:44:39',2,'2016-08-03 10:44:39'),(28,'template test relationship ',8,0,'',1,2,'2016-08-03 10:55:52',2,'2016-08-03 10:55:52'),(29,'test temp 112',1,1,'',1,2,'2016-08-03 10:58:33',2,'2016-08-03 10:58:33'),(30,'template test123',8,0,'gdrty',1,2,'2016-08-12 07:23:39',2,'2016-08-12 07:23:52'),(31,'Test server',8,0,'',1,2,'2016-08-12 10:37:09',2,'2016-08-12 10:37:35'),(32,'RLs_db&Inter',8,0,'',1,2,'2016-08-12 10:39:29',2,'2016-08-12 10:39:29'),(33,'samar 1',1,0,'',1,2,'2016-08-22 11:41:34',2,'2016-08-22 11:41:34');
/*!40000 ALTER TABLE `asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template_column`
--

DROP TABLE IF EXISTS `asset_template_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateColName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateColName_assetTemplateId_UNIQUE` (`assetTemplateColName`,`assetTemplateId`),
  UNIQUE KEY `UK_i7n89jntp5xq390elbs233689` (`assetTemplateColName`,`assetTemplateId`),
  KEY `createdByFk3_idx` (`createdBy`),
  KEY `updatedByFk3_idx` (`updatedBy`),
  KEY `assettemplateIdFk_idx` (`assetTemplateId`),
  CONSTRAINT `assetTemplateIdFk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk3` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk3` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template_column`
--

LOCK TABLES `asset_template_column` WRITE;
/*!40000 ALTER TABLE `asset_template_column` DISABLE KEYS */;
INSERT INTO `asset_template_column` VALUES (1,'Name','TEXT',100,1,0,1,1,2,'2016-04-20 10:26:19',2,'2016-04-20 10:26:19'),(2,'Description','TEXT',200,1,0,0,2,2,'2016-04-20 10:26:49',2,'2016-04-20 10:26:49'),(3,'Owner','TEXT',50,1,0,1,3,2,'2016-04-20 10:27:21',2,'2016-04-20 10:27:21'),(4,'Name','TEXT',100,2,0,1,1,2,'2016-04-20 10:32:53',2,'2016-04-20 10:32:53'),(5,'Desc','TEXT',200,2,0,0,2,2,'2016-04-20 10:33:03',2,'2016-04-20 10:33:03'),(6,'Name','TEXT',100,3,0,1,1,2,'2016-05-27 06:44:17',2,'2016-05-27 06:44:17'),(7,'Name','TEXT',100,4,0,1,1,2,'2016-06-06 09:29:28',2,'2016-06-06 09:29:28'),(8,'Name','TEXT',100,6,0,1,1,2,'2016-06-06 11:08:51',2,'2016-06-06 11:08:51'),(9,'Test','TEXT',10,1,0,0,4,2,'2016-06-08 10:30:52',2,'2016-06-08 10:30:52'),(10,'Date test','DATE',0,6,0,1,2,2,'2016-06-30 09:26:27',2,'2016-06-30 13:16:20'),(13,'Name','TEXT',100,10,0,1,1,2,'2016-07-06 08:31:48',2,'2016-07-06 08:31:48'),(14,'Name','TEXT',100,11,0,1,1,2,'2016-07-06 11:31:40',2,'2016-07-06 11:31:40'),(15,'Name','TEXT',100,18,0,1,1,2,'2016-07-07 09:18:58',2,'2016-07-07 09:18:58'),(16,'Relationship Name','TEXT',100,19,0,1,1,2,'2016-07-20 12:58:41',2,'2016-07-20 12:58:41'),(17,'Protocol','NUMBER',23,19,0,0,2,2,'2016-07-20 12:59:09',2,'2016-07-20 12:59:09'),(18,'Name','TEXT',10,20,0,0,1,2,'2016-07-26 12:14:03',2,'2016-07-26 12:14:03'),(19,'Name','TEXT',100,9,0,1,1,2,'2016-07-06 08:28:29',2,'2016-07-06 08:28:29'),(37,'Name','TEXT',100,24,0,1,1,2,'2016-08-02 10:09:45',2,'2016-08-02 10:09:45'),(38,'Desc','TEXT',200,20,0,0,2,2,'2016-08-03 10:26:46',2,'2016-08-03 10:26:46'),(39,'Protocoal','TEXT',20,20,0,1,4,2,'2016-08-03 10:27:09',2,'2016-08-03 10:27:09'),(40,'Title','TEXT',100,20,0,0,3,2,'2016-08-03 10:27:40',2,'2016-08-03 10:27:40'),(41,'w1','NUMBER',1,23,0,0,1,2,'2016-08-03 10:43:42',2,'2016-08-03 10:43:42'),(52,'Name','TEXT',100,27,0,1,1,2,'2016-08-03 10:54:40',2,'2016-08-03 10:54:40'),(53,'Desc','TEXT',200,27,0,0,2,2,'2016-08-03 10:55:04',2,'2016-08-03 10:55:04');
/*!40000 ALTER TABLE `asset_template_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type`
--

DROP TABLE IF EXISTS `asset_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTypeName` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeName_UNIQUE` (`assetTypeName`),
  UNIQUE KEY `UK_q1gprhj5b79ufwicjvkvef4fw` (`assetTypeName`),
  KEY `createdByFk1_idx` (`createdBy`),
  KEY `updatedByFk1_idx` (`updatedBy`),
  CONSTRAINT `createdByFk1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk1` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type`
--

LOCK TABLES `asset_type` WRITE;
/*!40000 ALTER TABLE `asset_type` DISABLE KEYS */;
INSERT INTO `asset_type` VALUES (1,'Application',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(2,'Server',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(3,'Database',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(4,'Person',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(5,'Business Services',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(6,'IT Services',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(7,'Interfaces',0,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(8,'Relationship',0,1,'2016-06-23 06:45:08',1,'2016-06-23 06:45:08');
/*!40000 ALTER TABLE `asset_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type_style`
--

DROP TABLE IF EXISTS `asset_type_style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(11) NOT NULL,
  `asset_colour` varchar(45) NOT NULL,
  `asset_image` blob,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeId_workspace_uq` (`asset_type_id`,`workspaceId`),
  KEY `assetTypeId_fk_idx` (`asset_type_id`),
  KEY `id_idx` (`asset_type_id`),
  KEY `assetTypeStyle_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `assetTypeId_fk` FOREIGN KEY (`asset_type_id`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeStyle_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type_style`
--

LOCK TABLES `asset_type_style` WRITE;
/*!40000 ALTER TABLE `asset_type_style` DISABLE KEYS */;
INSERT INTO `asset_type_style` VALUES (1,1,'#8AC007',NULL,NULL),(2,2,'#ADD8E6',NULL,NULL),(3,3,'#FFFF00',NULL,NULL),(4,4,'#FF8C00',NULL,NULL),(5,5,'#FF8C00',NULL,NULL),(6,6,'#FF8C00',NULL,NULL),(7,7,'#FF8C00',NULL,NULL);
/*!40000 ALTER TABLE `asset_type_style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcm`
--

DROP TABLE IF EXISTS `bcm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bcmId` int(11) DEFAULT NULL,
  `bcmName` varchar(255) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_workspaceId_idx` (`workspaceId`),
  KEY `bcm_createdBy_idx` (`createdBy`),
  KEY `bcm_updatedBy_idx` (`updatedBy`),
  KEY `bcm_bcmId_idx` (`bcmId`),
  CONSTRAINT `bcm_bcmId` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcm`
--

LOCK TABLES `bcm` WRITE;
/*!40000 ALTER TABLE `bcm` DISABLE KEYS */;
INSERT INTO `bcm` VALUES (1,NULL,'Test',NULL,2,'2016-04-20 12:35:57',2,'2016-04-20 12:35:57'),(2,1,'Test BCM1',1,2,'2016-04-20 12:37:37',2,'2016-05-23 10:37:48'),(4,NULL,'BCM Template',NULL,2,'2016-05-27 06:52:58',2,'2016-05-27 06:52:58'),(5,4,'Test BCM',1,2,'2016-06-03 08:54:42',2,'2016-06-03 08:54:42'),(6,4,'Test BCM',1,2,'2016-06-03 08:54:42',2,'2016-06-03 08:54:42'),(7,NULL,'Test BCM 6',NULL,2,'2016-06-06 10:00:59',2,'2016-06-06 10:00:59');
/*!40000 ALTER TABLE `bcm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcm_level`
--

DROP TABLE IF EXISTS `bcm_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentBcmLevelId` int(11) DEFAULT NULL,
  `bcmId` int(11) NOT NULL,
  `levelNumber` int(2) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `bcmLevelName` varchar(60) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_level_parentBcmLevelId_fk_idx` (`parentBcmLevelId`),
  KEY `bcm_level_bcmId_fk_idx` (`bcmId`),
  KEY `bcm_level_createdBy_fk_idx` (`createdBy`),
  KEY `bcm_level_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `bcm_level_bcmId_fk` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_parentBcmLevelId_fk` FOREIGN KEY (`parentBcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcm_level`
--

LOCK TABLES `bcm_level` WRITE;
/*!40000 ALTER TABLE `bcm_level` DISABLE KEYS */;
INSERT INTO `bcm_level` VALUES (1,NULL,1,1,1,'L1','',NULL,2,'2016-04-20 12:36:05',2,'2016-04-20 12:36:05'),(3,1,1,2,1,'L2','Strategy',NULL,2,'2016-04-20 12:36:23',2,'2016-04-20 12:36:23'),(4,1,1,2,2,'L21','Strategy',NULL,2,'2016-04-20 12:36:40',2,'2016-04-20 12:36:40'),(6,3,1,3,1,'L2','',NULL,2,'2016-04-20 12:36:58',2,'2016-04-20 12:36:58'),(8,NULL,2,1,1,'Legal','',NULL,2,'2016-04-20 12:37:37',2,'2016-05-27 06:55:13'),(9,8,2,2,1,'Legal Strategy','Strategy',NULL,2,'2016-04-20 12:37:38',2,'2016-05-27 06:55:29'),(11,9,2,3,1,'L3','',NULL,2,'2016-04-20 12:37:38',2,'2016-05-20 07:05:13'),(13,8,2,2,2,'Policy Formulation','Strategy',NULL,2,'2016-04-20 12:37:38',2,'2016-05-27 06:55:45'),(16,11,2,4,1,'L4','',NULL,2,'2016-05-20 07:05:37',2,'2016-05-20 07:05:37'),(17,NULL,2,1,2,'Finance','',NULL,2,'2016-05-20 07:06:12',2,'2016-05-27 06:56:25'),(18,17,2,2,1,'Financial Planning & Forecasting','Strategy',NULL,2,'2016-05-20 07:06:27',2,'2016-05-27 06:56:39'),(19,18,2,3,1,'L3 New','',NULL,2,'2016-05-20 07:06:37',2,'2016-05-20 07:06:37'),(20,19,2,4,1,'L4 New','',NULL,2,'2016-05-20 07:06:45',2,'2016-05-20 07:06:45'),(21,13,2,3,1,'L3 L21','',NULL,2,'2016-05-23 10:26:19',2,'2016-05-23 10:26:19'),(22,21,2,4,1,'L4 L21','',NULL,2,'2016-05-23 10:28:05',2,'2016-05-23 10:28:05'),(23,19,2,4,2,'L4 1 New','',NULL,2,'2016-05-23 10:28:34',2,'2016-05-23 10:28:34'),(24,18,2,3,2,'L3 1 New','',NULL,2,'2016-05-23 10:28:57',2,'2016-05-23 10:28:57'),(25,17,2,2,2,'Licensing / Targeting Acquisitions Management','Strategy',NULL,2,'2016-05-23 10:30:13',2,'2016-05-27 06:56:56'),(26,13,2,3,2,'L3 1 L21','',NULL,2,'2016-05-23 10:30:48',2,'2016-05-23 10:30:48'),(27,NULL,4,1,1,'Legal','',NULL,2,'2016-05-27 06:54:00',2,'2016-05-27 06:54:00'),(28,27,4,2,1,'Legal Strategy','Strategy',NULL,2,'2016-05-27 06:54:17',2,'2016-05-27 06:54:17'),(29,8,2,2,3,'Contract Framework','Strategy',NULL,2,'2016-05-27 06:56:07',2,'2016-05-27 06:56:07'),(30,NULL,2,1,3,'Business Development','',NULL,2,'2016-05-27 07:08:42',2,'2016-05-27 07:08:42'),(31,30,2,2,1,'Business Strategy','Strategy',NULL,2,'2016-05-27 07:09:03',2,'2016-05-27 07:09:03'),(32,30,2,2,2,'Product Lifecycle Planning','Strategy',NULL,2,'2016-05-27 07:09:49',2,'2016-05-27 07:09:49'),(33,31,2,3,1,'BS process 1','',NULL,2,'2016-05-27 07:10:19',2,'2016-05-27 07:10:19'),(34,31,2,3,2,'BS process 2','',NULL,2,'2016-05-27 07:10:35',2,'2016-05-27 07:10:35'),(35,33,2,4,1,'BS activity 1','',NULL,2,'2016-05-27 07:10:49',2,'2016-05-27 07:10:49'),(36,33,2,4,2,'BS activity 2','',NULL,2,'2016-05-27 07:11:05',2,'2016-05-27 07:11:05'),(37,32,2,3,1,'PLP process 1','',NULL,2,'2016-05-27 07:11:51',2,'2016-05-27 07:11:51'),(38,32,2,3,2,'PLP process 2','',NULL,2,'2016-05-27 07:12:03',2,'2016-05-27 07:12:03'),(39,37,2,4,1,'PLP activity 1','',NULL,2,'2016-05-27 07:12:15',2,'2016-05-27 07:12:15'),(40,34,2,4,1,'BS 2 activity 1','',NULL,2,'2016-05-27 07:13:00',2,'2016-05-27 07:13:00'),(41,38,2,4,1,'PLP 2 activity 1','',NULL,2,'2016-05-27 07:13:20',2,'2016-05-27 07:13:20'),(46,NULL,7,1,1,'L0','',NULL,2,'2016-06-06 10:01:06',2,'2016-06-06 10:01:06'),(47,NULL,2,1,4,'Sequence Number','',NULL,2,'2016-06-15 05:35:08',2,'2016-06-15 05:35:08'),(48,NULL,2,1,5,'Sequence Number','',NULL,2,'2016-06-15 05:35:09',2,'2016-06-15 05:35:09'),(49,47,2,2,1,'L1ew','Strategy',NULL,2,'2016-06-15 05:35:42',2,'2016-06-15 05:35:42'),(50,47,2,2,2,'L2','njk',NULL,2,'2016-06-15 05:36:09',2,'2016-06-15 05:36:09'),(51,47,2,2,3,'L1 New','Strategy',NULL,2,'2016-06-15 05:36:22',2,'2016-06-15 05:36:22'),(52,49,2,3,1,'L2','',NULL,2,'2016-06-15 05:37:15',2,'2016-06-15 05:37:15'),(53,52,2,4,1,'L3','',NULL,2,'2016-06-15 05:37:30',2,'2016-06-15 05:37:30'),(54,50,2,3,1,'L2','',NULL,2,'2016-06-15 05:37:46',2,'2016-06-15 05:37:46'),(55,47,2,2,4,'L2 New','Strategy',NULL,2,'2016-06-15 05:38:07',2,'2016-06-15 05:38:07');
/*!40000 ALTER TABLE `bcm_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `category_createdBy_fk_idx` (`createdBy`),
  KEY `category_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `category_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `category_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Functional Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(2,'Application Evolution',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(3,'Technology Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(4,'Application Management',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(5,'Interface Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(6,'Server Assessment',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(7,'Cloud Readiness',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(8,'Business Benefit',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(9,'Maintenance Quotient',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(10,'Criticality Quotient',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(11,'Sourcing Alignment Index',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(12,'Utilization Quotient for Servers',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(13,'Infra Health Quotient',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(14,'Governance Quality',NULL,1,'2016-04-18 10:34:44',1,'2016-04-18 10:34:44'),(16,'',NULL,2,'2016-05-27 06:36:18',2,'2016-05-27 06:36:18'),(17,'__SYS_TCO',NULL,1,'2016-09-09 12:47:10',1,'2016-09-09 12:47:10');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dashboardName` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DASHBOARD_WORKSPACE_idx` (`workspaceId`),
  CONSTRAINT `FK_DASHBOARD_WORKSPACE` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard`
--

LOCK TABLES `dashboard` WRITE;
/*!40000 ALTER TABLE `dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_log`
--

DROP TABLE IF EXISTS `export_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `purpose` varchar(45) NOT NULL,
  `refId` int(11) NOT NULL,
  `timeDown` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeUp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `export_userId_fk_idx` (`userId`),
  KEY `export_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `export_userId_fk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `export_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_log`
--

LOCK TABLES `export_log` WRITE;
/*!40000 ALTER TABLE `export_log` DISABLE KEYS */;
INSERT INTO `export_log` VALUES (2,'342d5ffbbf641bd47434bd1440b56386',1,2,'INV',1,'2016-04-20 10:27:45',NULL),(3,'07598197d906749f03e810bfb35795bb',1,2,'FM',2,'2016-04-20 12:39:24',NULL),(4,'a0b1b87b273daa490912d00225f32b8e',1,2,'FM',2,'2016-05-12 07:07:15',NULL),(5,'eb0418317fb6c67bdf1b5252754dccf7',1,2,'FM',2,'2016-05-12 07:27:02',NULL),(7,'b868a534ca3073af8a49764e15c10ac8',1,2,'FM',2,'2016-05-20 07:08:29',NULL),(9,'43717f975e6c1f601ac3aea77d242015',1,2,'FM',2,'2016-05-20 07:11:09','2016-05-20 07:20:14'),(11,'bea84aeeb65cb7caa9a8718c581041ea',1,2,'FM',2,'2016-05-23 10:31:16',NULL),(13,'f57997487dadcb17b114b81e2fa0c853',1,2,'FM',2,'2016-05-23 10:32:43','2016-05-23 10:34:08'),(14,'af5da77516f0778440e6750993185d18',1,2,'FM',2,'2016-05-27 07:16:45','2016-05-27 07:23:33'),(15,'7ce31f902675491ee47a4916cb724a09',1,2,'FM',2,'2016-05-27 09:44:13','2016-05-27 09:47:39'),(16,'ff9a1c8348b6b15ea78a5d7d305314a2',1,2,'QE',5,'2016-06-01 07:37:59',NULL),(17,'7fa3c45163f4ed0616276d5fb2b0153a',1,2,'INV',1,'2016-07-07 11:11:50','2016-07-07 11:13:34'),(18,'dfa1f8c1d33e3d6cfa91476bf5caa866',1,2,'INV',15,'2016-07-07 11:17:20',NULL),(19,'3ac8d5166e5e5a26b498ea4dba23b4c3',1,2,'INV',4,'2016-07-07 11:17:38','2016-07-07 11:18:05'),(20,'b54f2e4a18807775c01870d7ddd4f18a',1,2,'INV',2,'2016-07-07 11:23:21','2016-07-07 11:23:53'),(21,'4bb4537b60aaae4b743f0fbc100b7f7e',1,2,'INV',20,'2016-07-26 12:15:58',NULL),(22,'f6b77cc80c3091ccefb7bfd23937dea2',1,2,'INV',20,'2016-07-26 12:15:59',NULL),(23,'7518939446631226f89d9025b4cab653',1,2,'FM',1,'2016-08-10 07:49:55',NULL);
/*!40000 ALTER TABLE `export_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functional_map`
--

DROP TABLE IF EXISTS `functional_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `bcmId` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fm_bcm_idx` (`bcmId`),
  KEY `fm_createdBy_idx` (`createdBy`),
  KEY `fm_updatedBy_idx` (`updatedBy`),
  KEY `fm_workspaceId_idx` (`workspaceId`),
  KEY `fm_assetTemplate_idx` (`assetTemplateId`),
  KEY `fm_question_idx` (`questionId`),
  CONSTRAINT `fm_assetTemplate` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_bcm` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_question` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_map`
--

LOCK TABLES `functional_map` WRITE;
/*!40000 ALTER TABLE `functional_map` DISABLE KEYS */;
INSERT INTO `functional_map` VALUES (1,'Test FM',2,'L2',1,2,4,2,'2016-04-20 12:38:15',2,'2016-04-20 12:38:15'),(2,'Test FM1',2,'L4',1,3,4,2,'2016-04-20 12:38:15',2,'2016-05-27 07:13:46'),(3,'Test by Samar',2,'L1',1,3,5,2,'2016-06-08 06:10:15',2,'2016-06-08 06:10:34'),(4,'Test by Sunil',2,'L1',1,3,5,2,'2016-06-08 06:10:15',2,'2016-06-08 06:10:15');
/*!40000 ALTER TABLE `functional_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functional_map_data`
--

DROP TABLE IF EXISTS `functional_map_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionalMapId` int(11) NOT NULL,
  `bcmLevelId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `data` text,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `questionnaireId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fmd_bmfId_idx` (`functionalMapId`),
  KEY `fmd_bcmLevel_idx` (`bcmLevelId`),
  KEY `fmd_asset_idx` (`assetId`),
  KEY `fmd_userId_fk_idx` (`updatedBy`),
  KEY `questionnaireId` (`questionnaireId`),
  CONSTRAINT `fmd_asset_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_bcmLevel_fk` FOREIGN KEY (`bcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_fmId_fk` FOREIGN KEY (`functionalMapId`) REFERENCES `functional_map` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_userId_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `functional_map_data_ibfk_1` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_map_data`
--

LOCK TABLES `functional_map_data` WRITE;
/*!40000 ALTER TABLE `functional_map_data` DISABLE KEYS */;
INSERT INTO `functional_map_data` VALUES (93,2,35,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(94,2,35,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(95,2,35,13,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(96,2,35,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(97,2,35,11,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(98,2,35,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(99,2,35,12,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(100,2,35,14,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(101,2,20,7,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(102,2,20,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(103,2,20,13,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(104,2,20,8,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(105,2,20,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(106,2,20,10,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(107,2,20,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(108,2,20,14,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(109,2,36,7,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(110,2,36,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(111,2,36,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(112,2,36,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(113,2,36,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(114,2,36,10,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(115,2,36,12,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(116,2,36,14,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(117,2,22,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(118,2,22,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(119,2,22,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(120,2,22,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(121,2,22,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(122,2,22,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(123,2,22,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(124,2,22,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(125,2,23,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(126,2,23,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(127,2,23,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(128,2,23,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(129,2,23,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(130,2,23,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(131,2,23,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(132,2,23,14,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(133,2,39,7,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(134,2,39,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(135,2,39,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(136,2,39,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(137,2,39,11,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(138,2,39,10,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(139,2,39,12,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(140,2,39,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(141,2,40,7,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(142,2,40,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(143,2,40,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(144,2,40,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(145,2,40,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(146,2,40,10,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(147,2,40,12,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(148,2,40,14,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(149,2,41,7,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(150,2,41,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(151,2,41,13,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(152,2,41,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(153,2,41,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(154,2,41,10,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(155,2,41,12,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(156,2,41,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(157,2,16,7,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(158,2,16,9,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(159,2,16,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(160,2,16,8,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(161,2,16,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(162,2,16,10,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(163,2,16,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL),(164,2,16,14,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}',2,'2016-05-27 09:47:39',NULL);
/*!40000 ALTER TABLE `functional_map_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph`
--

DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphName` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `graphType` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHNAME_WORKSPACE` (`graphName`,`workspaceId`,`graphType`),
  KEY `Graph_Workspace_FK_idx` (`workspaceId`),
  KEY `Graph_User_FK_idx` (`createdBy`),
  KEY `Graph_Updated_By_FK_idx` (`updatedBy`),
  CONSTRAINT `Graph_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Workspace_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph`
--

LOCK TABLES `graph` WRITE;
/*!40000 ALTER TABLE `graph` DISABLE KEYS */;
/*!40000 ALTER TABLE `graph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_asset_template`
--

DROP TABLE IF EXISTS `graph_asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHID_ASSETTEMPLATEID` (`graphId`,`assetTemplateId`),
  KEY `FK_GRAPHID_GRAPHID_idx` (`graphId`),
  KEY `FK_ASSETTEMPLATEID_ASSETTEMPLATEID_idx` (`assetTemplateId`),
  CONSTRAINT `FK_ASSETTEMPLATEID_ASSETTEMPLATEID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GRAPHID_GRAPHID` FOREIGN KEY (`graphId`) REFERENCES `graph` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_asset_template`
--

LOCK TABLES `graph_asset_template` WRITE;
/*!40000 ALTER TABLE `graph_asset_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `graph_asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interface`
--

DROP TABLE IF EXISTS `interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interfaceAssetId` int(11) NOT NULL,
  `assetSourceId` int(11) NOT NULL,
  `assetDestId` int(11) NOT NULL,
  `interfaceDirection` varchar(45) NOT NULL,
  `dataType` varchar(20) DEFAULT NULL,
  `frequency` varchar(20) DEFAULT NULL,
  `communicationMethod` varchar(20) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_INTERFACE_SRC_DEST` (`interfaceAssetId`,`assetSourceId`,`assetDestId`),
  KEY `Interface_Asset_FK_idx` (`assetSourceId`),
  KEY `Interface_Asset_Dest_FK_idx` (`assetDestId`),
  KEY `Interface_Created_By_FK_idx` (`createdBy`),
  KEY `Interface_Updated_By_FK_idx` (`updatedBy`),
  KEY `FK_INTERFACEASSETID_ASSETID_idx` (`interfaceAssetId`),
  CONSTRAINT `FK_INTERFACEASSETID_ASSETID` FOREIGN KEY (`interfaceAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Dest_FK` FOREIGN KEY (`assetDestId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Source_FK` FOREIGN KEY (`assetSourceId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interface`
--

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;
/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueName` varchar(45) NOT NULL,
  `displayName` varchar(250) NOT NULL,
  `description` text,
  `type` varchar(4) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameter_uniqueName_workspaceId_uq` (`uniqueName`,`workspaceId`),
  KEY `parameter_workspaceId_fk_idx` (`workspaceId`),
  KEY `parameter_createdBy_fk_idx` (`createdBy`),
  KEY `parameter_updatedBy_fk_idx` (`updatedBy`),
  KEY `parameter_parentParameterId_idx` (`parentParameterId`),
  CONSTRAINT `parameter_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_parentParameterId` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (1,'Test Parameter-1','Test Param-1','This is the description of test que','LP',1,NULL,2,'2016-04-19 12:02:31',2,'2016-04-19 12:05:44'),(2,'Test Param-2','Display Test Parameter-2','desc of Test Param-2','LP',1,NULL,2,'2016-04-19 12:05:34',2,'2016-04-19 12:05:39'),(3,'Test Param-2 - 04-19 17:37:13','Display Test Parameter-2','desc of Test Param-2','LP',1,2,2,'2016-04-19 12:07:16',2,'2016-04-19 12:07:16'),(4,'Test Param-3','Display Test Param-3','Desc Test Param-3','LP',1,NULL,2,'2016-04-19 12:08:50',2,'2016-04-19 12:08:50'),(5,'IT Portfolio Survey - 1','dispaly test','rtret','AP',1,NULL,1,'2016-04-20 06:54:14',1,'2016-04-20 06:54:14'),(6,'Test Param-2 - 04-20 15:34:51 2nd','Display Test Parameter-2','desc of Test Param-2','LP',1,2,2,'2016-04-20 10:05:16',2,'2016-04-20 10:05:16'),(7,'IT Portfolio Survey - 1 - 04-20 15:36:35','dispaly test','rtret','AP',1,5,2,'2016-04-20 10:06:40',2,'2016-04-20 10:06:40'),(8,'IT Portfolio Survey - 1 - 04-20 15:36:351','dispaly test','rtret','AP',1,5,2,'2016-04-20 10:06:47',2,'2016-05-04 11:29:40'),(9,'Test T3 module createParam','Test T3 module createParam','Test T3 module createParam','LP',1,NULL,2,'2016-04-26 10:11:32',2,'2016-04-26 10:11:32'),(10,'Test for Shaishav','wef','df','AP',1,NULL,2,'2016-04-29 11:47:27',2,'2016-05-03 07:46:24'),(11,'tEst by samar','jgyj','ghhjghgj','AP',1,NULL,2,'2016-04-29 13:25:43',2,'2016-04-29 13:25:43'),(12,'tEst by samarerfdef','jgyj','ghhjghgj','AP',1,NULL,2,'2016-04-29 13:25:56',2,'2016-05-04 09:27:16'),(13,'Test to initiate','Test to initiate dispaly','Test to initiate desc','AP',1,NULL,2,'2016-05-02 06:32:40',2,'2016-05-02 06:53:39'),(14,'Test to initiate1','Test to initiate dispaly','Test to initiate desc','AP',1,NULL,2,'2016-05-02 06:36:24',2,'2016-05-02 06:36:24'),(23,'First Test world param','First Test world param display name','First Test world param description','LP',1,NULL,2,'2016-05-04 09:53:52',2,'2016-05-04 09:54:44'),(25,'First Test world param - 05-045','First Test world param display name','First Test world param description','LP',1,23,2,'2016-05-04 10:08:32',2,'2016-05-05 12:37:09'),(26,'First Test world param - 05-04 15:39:21','First Test world param display name','First Test world param description','LP',1,23,2,'2016-05-04 10:09:28',2,'2016-05-05 12:37:33'),(28,'IT Portfolio Survey - gdfgdfgdfg','fgfgdfg','gdfgfdg','AP',1,NULL,2,'2016-05-04 11:20:09',2,'2016-05-04 11:20:09'),(32,'dfgdfgfdggdfghrtyy567','dfddfg','ghfghfghfg','AP',1,NULL,2,'2016-05-04 11:29:15',2,'2016-05-05 12:37:42'),(35,'First for lp1','display lp test','eferg','LP',1,NULL,2,'2016-05-04 11:58:13',2,'2016-05-19 06:09:13'),(36,'Test for AP','ap diplau1','12','AP',1,NULL,2,'2016-05-04 12:00:05',2,'2016-05-04 12:02:46'),(37,'Test for AP - 05-04 17:30:51','ap diplau1','12','AP',1,36,2,'2016-05-04 12:01:09',2,'2016-08-05 07:11:26'),(39,'Level type','disp','desc','FC',1,NULL,2,'2016-05-05 11:40:37',2,'2016-05-05 12:49:15'),(40,'dsfsdffg','dfgfdfgdf','gdfgfdgdfg','FC',1,NULL,2,'2016-05-05 12:18:51',2,'2016-05-05 12:48:54'),(42,'Test by Samardeep','ghjghj1','ghjghj1','FC',1,NULL,2,'2016-05-05 12:22:08',2,'2016-05-06 10:46:05'),(43,'test23','dfdfgfh','','FC',1,NULL,2,'2016-05-05 12:23:03',2,'2016-05-05 12:39:23'),(44,'First Test world param - 05-04 15:39:21 - 05-','First Test world param display name','First Test world param description','LP',1,26,2,'2016-05-05 12:37:50',2,'2016-05-05 12:37:50'),(45,'dfdf','rgtdfgdfg','','FC',1,NULL,2,'2016-05-05 12:39:06',2,'2016-05-05 12:49:08'),(46,'dfgdfgbfg','hbfghfgh','hfghgfhgfh','FC',1,NULL,2,'2016-05-05 12:49:28',2,'2016-08-09 11:18:23'),(47,'Test by Ananta','Test by Ananta','Test by Ananta','LP',1,NULL,2,'2016-05-06 07:03:05',2,'2016-05-06 07:03:05'),(48,'test 3','dfv','cvcv','LP',1,NULL,2,'2016-05-06 07:04:43',2,'2016-05-06 07:04:43'),(49,'test 31','dfv','cvcv','LP',1,NULL,2,'2016-05-06 07:04:46',2,'2016-05-06 07:04:46'),(50,'test by ananta sethi','test by ananta sethi','test by ananta sethi','LP',1,NULL,2,'2016-05-06 07:12:44',2,'2016-05-06 07:12:44'),(51,'Test FC','Display Test FC','','FC',1,NULL,2,'2016-05-06 10:34:19',2,'2016-05-06 10:34:19'),(52,'Test for FC evaluation','Test for FC evaluation','Test for FC evaluation','FC',1,NULL,2,'2016-05-25 07:47:56',2,'2016-05-25 07:47:56'),(53,'Add FC in AP','Add FC in AP','Add FC in AP','AP',1,NULL,2,'2016-05-25 07:49:56',2,'2016-05-25 07:49:56'),(54,'IT tryrtytyrtSurvey - 1','rtrty','tyty','AP',1,NULL,2,'2016-05-27 12:05:00',2,'2016-05-27 12:05:21'),(55,'Test by Sunil','First FC param','sax','FC',1,NULL,2,'2016-05-30 06:39:22',2,'2016-05-30 06:39:39'),(56,'AP','AP','AP','AP',1,NULL,2,'2016-06-01 07:37:07',2,'2016-08-09 11:18:18'),(57,'IT Portfolio Survey - 1ghgh','dispaly test','gdfgfg','FC',1,NULL,2,'2016-06-08 06:13:37',2,'2016-06-08 06:13:37'),(58,'Parameter Tree View','Parameter Tree View','Parameter Tree View','LP',1,NULL,2,'2016-06-15 05:04:15',2,'2016-06-15 05:04:15'),(59,'Parameter Tree View - 06-15 10:37:09','Parameter Tree View','Parameter Tree View','LP',1,58,2,'2016-06-15 05:07:13',2,'2016-06-15 05:07:13'),(61,'Parameter Tree View - 06-15 10:37:17','Parameter Tree View','Parameter Tree View','LP',1,58,2,'2016-06-15 05:07:19',2,'2016-06-15 05:07:19'),(62,'Test by Samar1','Test by Samar display','','AP',1,NULL,2,'2016-08-05 07:12:17',2,'2016-08-05 07:12:46'),(63,'dfgdfgfdggdfghrtyy567 - 08-05 12:44:44','dfddfg','ghfghfghfg','AP',1,32,2,'2016-08-05 07:14:47',2,'2016-08-09 11:18:35'),(64,'First for lp1 - 08-05 12:44:52','display lp test','eferg','LP',1,35,2,'2016-08-05 07:15:11',2,'2016-08-05 07:15:11'),(65,'LP parameter to check weight add','LP parameter to check weight addition display','LP parameter to check weight addition discr','LP',1,NULL,2,'2016-08-10 06:22:00',2,'2016-08-10 06:22:01'),(66,'LP to test1','dispaly test434','','LP',1,NULL,2,'2016-08-10 06:47:40',2,'2016-08-10 07:11:36'),(67,'45yt45y','yrtyrty','','LP',1,NULL,2,'2016-08-10 07:15:10',2,'2016-08-10 07:15:10'),(68,'test1-lp1','ttestt','','LP',1,NULL,2,'2016-08-10 07:44:47',2,'2016-08-10 07:44:47'),(69,'Test-LP2','test -lp2','','LP',1,NULL,2,'2016-08-10 07:46:07',2,'2016-08-10 07:46:07'),(70,'FC test by Samar','FC test by Samar','','FC',1,NULL,2,'2016-08-10 07:47:59',2,'2016-08-10 07:47:59'),(71,'TCO-A','TCO-A','TCO-A','TCOA',1,NULL,2,'2016-09-01 10:16:32',2,'2016-09-01 10:16:32'),(72,'Test TCO_A','Test TCO_A','Test TCO_A','TCOA',1,NULL,2,'2016-09-01 10:18:20',2,'2016-09-01 10:18:20'),(73,'Test TCO_A1','Test TCO_A1','Test TCO_A1','TCOA',1,NULL,2,'2016-09-01 10:19:45',2,'2016-09-01 10:19:45'),(74,'Test TCO_A3','Test TCO_A3','Test TCO_A3','TCOA',1,NULL,2,'2016-09-01 12:30:57',2,'2016-09-01 12:30:57'),(75,'Test TCO_A4','Test TCO_A4','Test TCO_A4','TCOA',1,NULL,2,'2016-09-01 12:35:15',2,'2016-09-01 12:35:15'),(76,'Test TCO_A5','Test TCO_A5','Test TCO_A5','TCOA',1,NULL,2,'2016-09-01 12:36:48',2,'2016-09-01 12:36:48'),(79,'Test TCO_A6','Test TCO_A5','Test TCO_A5','TCOA',1,NULL,2,'2016-09-01 12:40:52',2,'2016-09-01 12:40:52'),(80,'Test TCO_A7','Test TCO_A7','Test TCO_A7','TCOA',1,NULL,2,'2016-09-01 12:50:15',2,'2016-09-01 12:50:15'),(81,'Test TCO_A8','Test TCO_A8','Test TCO_A8','TCOA',1,NULL,2,'2016-09-01 12:51:25',2,'2016-09-01 12:51:25'),(82,'45yt45y - 09-01 18:43:39','yrtyrty','','LP',1,67,2,'2016-09-01 13:13:55',2,'2016-09-01 13:13:55'),(83,'Test TCO_A11','Test TCO_A11','Test TCO_A11','TCOA',1,NULL,2,'2016-09-01 13:33:28',2,'2016-09-01 13:33:28'),(84,'Test TCO_A12','Test TCO_A12','Test TCO_A12','TCOA',1,NULL,2,'2016-09-02 05:24:41',2,'2016-09-02 05:24:41'),(85,'Test TCO_A13','Test TCO_A13','Test TCO_A13','TCOA',1,NULL,2,'2016-09-02 05:25:44',2,'2016-09-02 05:25:44'),(86,'Test TCO_A14','Test TCO_A14','Test TCO_A14','TCOA',1,NULL,2,'2016-09-02 05:26:55',2,'2016-09-02 05:26:55'),(87,'Hello JSTree','Hello JSTree1','Hello JSTree','TCOA',1,NULL,2,'2016-09-02 10:26:30',2,'2016-11-07 11:39:23'),(88,'Test TCO_A134','Test TCO_A134','Test TCO_A134','TCOA',1,NULL,2,'2016-09-05 11:01:57',2,'2016-09-05 11:01:57'),(89,'Test TCO_A4556','fdfd','f','TCOA',1,NULL,2,'2016-09-05 11:27:06',2,'2016-09-05 11:27:06'),(90,'fgh','ghg','gh','TCOA',1,NULL,2,'2016-09-05 11:38:23',2,'2016-09-05 11:38:23'),(91,'fhyy6','trygr','fgdfg','TCOA',1,NULL,2,'2016-09-05 11:46:52',2,'2016-09-05 11:46:52'),(92,'gty5656','ghgh','thfgh','TCOA',1,NULL,2,'2016-09-05 11:49:51',2,'2016-09-05 11:49:51'),(93,'wqdsgdfg','dtgdfgdfg','','TCOA',1,NULL,2,'2016-09-05 11:54:06',2,'2016-09-05 11:54:06'),(94,'uyjkuyi76u','jtuj','jjk','TCOA',1,NULL,2,'2016-09-05 11:54:44',2,'2016-09-05 11:54:44'),(95,'ABC','ABC','ABC','TCOA',1,NULL,2,'2016-09-05 11:58:46',2,'2016-09-05 11:58:46'),(96,'Test TCO_A123','Test TCO_A123','Test TCO_A123','TCOL',1,NULL,2,'2016-09-05 12:09:47',2,'2016-09-05 12:09:47'),(97,'Test 96','Test 96','Test 96','TCOA',1,NULL,2,'2016-09-06 07:46:01',2,'2016-09-06 07:46:01'),(98,'Test 97','Test 97','Test 97','TCOA',1,NULL,2,'2016-09-06 09:49:54',2,'2016-09-06 09:49:54'),(99,'Test TCO_A1233','weffwewf','sad','TCOL',1,NULL,2,'2016-09-06 09:57:50',2,'2016-09-06 09:57:50'),(100,'Test TCA','Test TCA','Test TCA','TCOA',1,NULL,2,'2016-09-07 07:33:33',2,'2016-09-07 07:33:33'),(101,'Test TCO_A54663tt','565fg','fgfgg','TCOA',1,NULL,2,'2016-09-07 10:51:31',2,'2016-09-07 10:51:31'),(102,'Sama','Samar','Samar','TCOA',1,NULL,2,'2016-09-07 12:37:38',2,'2016-11-04 12:48:21'),(103,'edrwerttrytryrt','yrtyrty','rtyrty','TCOA',1,NULL,2,'2016-09-07 12:46:35',2,'2016-09-07 12:46:35'),(105,'Test AP 1','Test AP 1','Test AP 1','TCOA',1,NULL,2,'2016-09-07 13:14:55',2,'2016-09-07 13:14:55'),(106,'Test AP 1 LP1','Test AP 1 LP1','Test AP 1 LP1','TCOA',1,NULL,2,'2016-09-07 13:15:21',2,'2016-09-07 13:15:21'),(107,'Test AP 1 LP1 LP1','Test AP 1 LP1 LP1','Test AP 1 LP1 LP1','TCOA',1,NULL,2,'2016-09-07 13:15:54',2,'2016-09-07 13:15:54'),(108,'Test AP 1 LP2','Test AP 1 LP2','Test AP 1 LP2','TCOA',1,NULL,2,'2016-09-07 13:16:14',2,'2016-09-07 13:16:14'),(109,'Test AP 1 LP3','Test AP 1 LP3','Test AP 1 LP3','TCOL',1,NULL,2,'2016-09-07 13:16:33',2,'2016-09-07 13:16:33'),(110,'Test 123','Test 123','','TCOA',1,NULL,2,'2016-09-08 05:51:26',2,'2016-09-08 05:51:26'),(111,'Test for li ui','Test for li ui','Test for li ui','TCOA',1,NULL,2,'2016-09-08 13:36:12',2,'2016-09-08 13:36:12'),(112,'dsfergdfg','dfddfgdfg','dfgdfg','TCOA',1,NULL,2,'2016-09-08 13:36:36',2,'2016-09-08 13:36:36'),(114,'sadweferg','serferrfer','ferfsdfff','TCOL',1,NULL,2,'2016-09-09 05:02:43',2,'2016-09-09 05:02:43'),(115,'dcfwerfterytr','hyrthyrtyrt','yrty','TCOA',1,NULL,2,'2016-09-09 05:03:11',2,'2016-09-09 05:03:11'),(116,'weqwde','dfsdsfdsff','dfdsdfdsf','TCOA',1,NULL,2,'2016-09-09 05:04:45',2,'2016-09-09 05:04:45'),(117,'Test TCO cost structure','Test TCO cost structure','Test TCO cost structure','TCOA',1,NULL,2,'2016-09-09 05:06:27',2,'2016-09-09 05:06:27'),(118,'FQ','ds','sdf','TCOA',1,NULL,2,'2016-09-09 07:54:19',2,'2016-09-09 07:54:19'),(119,'vfdvxffv','vxdfvxcv','xcvxcvxcv','TCOL',1,NULL,2,'2016-09-09 07:54:32',2,'2016-09-09 07:54:32'),(120,'Test for li ui1','ds','','TCOA',1,NULL,2,'2016-09-09 11:26:32',2,'2016-09-09 11:26:32'),(121,'Test for li ui11','saaxda','','TCOA',1,NULL,2,'2016-09-09 11:27:26',2,'2016-09-09 11:27:26'),(122,'Test for li ui111','er','','TCOA',1,NULL,2,'2016-09-09 11:30:41',2,'2016-11-24 12:42:56'),(125,'Test for li ui123','hdf','','TCOA',1,NULL,2,'2016-09-09 11:49:31',2,'2016-11-04 12:29:47'),(126,'Test for li ui456','rtr','','TCOA',1,NULL,2,'2016-09-09 11:55:54',2,'2016-09-09 11:55:54'),(127,'Test for li ui1245','fgfd','','TCOA',1,NULL,2,'2016-09-09 11:56:44',2,'2016-09-09 11:56:44'),(128,'Test for li ui5345654','dsfdrgdf','fgdfg','TCOA',1,NULL,2,'2016-09-09 12:25:55',2,'2016-09-09 12:25:55'),(129,'Test for li ui6567567','ABC','','TCOA',1,NULL,2,'2016-09-09 12:29:27',2,'2016-09-09 12:29:27'),(130,'GTRT','DFGDFG','DFGDFG','TCOA',1,NULL,2,'2016-09-09 12:41:33',2,'2016-09-09 12:41:33'),(131,'Hello Samar','Hello Samar','Hello Samar','TCOL',1,NULL,2,'2016-09-19 05:18:02',2,'2016-09-19 05:18:02'),(132,'Test CS for response eval','Test CS for response eval','Test CS for response eval','TCOA',1,NULL,2,'2016-09-21 08:00:46',2,'2016-09-21 08:00:46'),(133,'Test 1','Test 1','Test 1','TCOL',1,NULL,2,'2016-09-21 08:01:16',2,'2016-09-21 08:01:16'),(134,'Test for li uier','dfdfhyj','dfsdf','TCOL',1,NULL,2,'2016-09-21 11:20:15',2,'2016-09-21 11:20:15'),(135,'efef','dfdsf','dfsdf','TCOL',1,NULL,2,'2016-09-21 11:20:22',2,'2016-09-21 11:20:22'),(136,'Test TCA new','Test TCA new','Test TCA new','TCOA',1,NULL,2,'2016-09-22 09:32:39',2,'2016-09-22 09:32:39'),(137,'Test TCOL new','Test TCOL new','Test TCOL new','TCOL',1,NULL,2,'2016-09-22 09:32:56',2,'2016-09-22 09:32:56'),(138,'Test for li ui45','dfdsf','sdfsdf','TCOL',1,NULL,2,'2016-09-22 12:52:34',2,'2016-09-22 12:52:34'),(139,'Test Again','Test Again final','Test Again','TCOA',1,NULL,2,'2016-09-23 08:55:37',2,'2016-09-23 08:55:37'),(140,'1st test is good','1st test is good','1st test is good','TCOA',1,NULL,2,'2016-09-23 08:56:03',2,'2016-09-23 08:56:03'),(141,'2nd is good','2nd is good','2nd is good','TCOL',1,NULL,2,'2016-09-23 08:56:29',2,'2016-09-23 08:56:29'),(142,'3rd is very good','3rd is very good','v','TCOL',1,NULL,2,'2016-09-23 08:57:04',2,'2016-09-23 08:57:04'),(143,'Add 2nd child','Add 2nd child','','TCOL',1,NULL,2,'2016-10-17 09:57:25',2,'2016-10-17 09:57:25'),(144,'Add 3rd child','Add 3rd child','','TCOA',1,NULL,2,'2016-10-17 09:58:08',2,'2016-10-17 09:58:08'),(145,'Add 4th in 3rd','Add 4th in 3rd','','TCOL',1,NULL,2,'2016-10-17 09:58:23',2,'2016-10-17 09:58:23'),(146,'hwelo','hwelo','','TCOA',1,NULL,2,'2016-10-21 09:43:04',2,'2016-10-21 09:43:04'),(147,'Hello1','Hello1','Hello1','TCOA',1,NULL,2,'2016-10-21 11:08:55',2,'2016-10-21 11:08:55'),(148,'Test for doc','Test for doc','Test for doc','TCOA',1,NULL,2,'2016-10-25 07:22:05',2,'2016-10-25 07:22:05'),(149,'(;','Samar','','TCOA',1,NULL,2,'2016-10-25 07:22:54',2,'2016-11-07 11:27:43'),(151,'drfsdfsdf','sdfsdfd','fsdfsdsf','TCOL',1,NULL,2,'2016-11-03 08:29:42',2,'2016-11-03 08:29:42'),(162,'bcvb','cbcvb','','TCOL',1,NULL,2,'2016-11-07 12:00:50',2,'2016-11-07 12:00:50'),(163,'a','a','a','TCOL',1,NULL,2,'2016-11-21 11:12:07',2,'2016-11-21 11:12:07'),(164,'b','b','b','TCOL',1,NULL,2,'2016-11-21 11:12:16',2,'2016-11-21 11:12:16'),(165,'c','c','c','TCOL',1,NULL,2,'2016-11-21 11:12:33',2,'2016-11-21 11:12:33');
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_config`
--

DROP TABLE IF EXISTS `parameter_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `parameterConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pc_parameterId_idx` (`parameterId`),
  CONSTRAINT `pc_parameterId` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_config`
--

LOCK TABLES `parameter_config` WRITE;
/*!40000 ALTER TABLE `parameter_config` DISABLE KEYS */;
INSERT INTO `parameter_config` VALUES (9,39,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(10,40,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L2\"}'),(11,42,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(12,43,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L2\"}'),(13,45,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(14,46,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L2\"}'),(15,51,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L1\"}'),(16,52,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L2\"}'),(17,55,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L1\"}'),(18,70,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L2\"}');
/*!40000 ALTER TABLE `parameter_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_function`
--

DROP TABLE IF EXISTS `parameter_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentParameterId` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `constant` int(11) NOT NULL DEFAULT '1',
  `questionId` int(11) DEFAULT NULL,
  `mandatoryQuestion` tinyint(1) NOT NULL DEFAULT '0',
  `childparameterId` int(11) DEFAULT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pf_parentParam_que_childParam_uq` (`parentParameterId`,`questionId`,`childparameterId`),
  KEY `parameter_function_parentParameterId_fk_idx` (`parentParameterId`),
  KEY `parameter_function_childParameterId_fk_idx` (`childparameterId`),
  KEY `parameter_function_questionId_fk_idx` (`questionId`),
  CONSTRAINT `parameter_function_childParameterId_fk` FOREIGN KEY (`childparameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_function`
--

LOCK TABLES `parameter_function` WRITE;
/*!40000 ALTER TABLE `parameter_function` DISABLE KEYS */;
INSERT INTO `parameter_function` VALUES (4,2,0.200,0,1,1,NULL,NULL),(5,2,0.300,0,2,0,NULL,NULL),(6,1,0.100,0,1,0,NULL,NULL),(7,3,0.200,0,1,1,NULL,NULL),(8,3,0.300,0,2,0,NULL,NULL),(9,4,0.200,0,2,1,NULL,NULL),(10,5,0.100,0,NULL,0,1,NULL),(11,5,0.200,0,NULL,0,2,NULL),(12,6,0.300,0,1,1,NULL,NULL),(13,6,0.500,0,2,0,NULL,NULL),(14,7,0.100,0,NULL,0,1,NULL),(15,7,0.200,0,NULL,0,2,NULL),(16,8,0.400,0,NULL,0,1,NULL),(17,8,0.600,0,NULL,0,2,NULL),(18,9,0.200,0,1,1,NULL,NULL),(21,11,0.200,0,NULL,0,2,NULL),(22,11,0.300,0,NULL,0,3,NULL),(25,14,0.200,0,NULL,0,1,NULL),(26,14,0.300,0,NULL,0,2,NULL),(27,13,0.200,0,NULL,0,3,NULL),(28,13,0.300,0,NULL,0,4,NULL),(29,10,0.200,0,NULL,0,1,NULL),(30,10,0.300,0,NULL,0,2,NULL),(31,12,0.200,0,NULL,0,2,NULL),(32,12,0.300,0,NULL,0,3,NULL),(33,23,0.700,0,1,0,NULL,NULL),(34,23,0.200,0,2,1,NULL,NULL),(37,25,0.900,0,1,0,NULL,NULL),(38,25,0.100,0,2,1,NULL,NULL),(39,26,0.600,0,1,0,NULL,NULL),(40,26,0.400,0,2,1,NULL,NULL),(43,28,0.200,0,NULL,0,2,NULL),(44,28,0.200,0,NULL,0,3,NULL),(45,32,0.500,0,NULL,0,3,NULL),(46,32,0.500,0,NULL,0,7,NULL),(49,35,0.200,0,1,0,NULL,NULL),(50,35,0.800,0,2,1,NULL,NULL),(51,36,0.200,0,NULL,0,1,NULL),(52,36,0.300,0,NULL,0,2,NULL),(53,36,0.200,0,NULL,0,3,NULL),(54,36,0.300,0,NULL,0,6,NULL),(55,37,0.600,0,NULL,0,1,NULL),(56,37,0.100,0,NULL,0,2,NULL),(57,37,0.100,0,NULL,0,3,NULL),(58,37,0.200,0,NULL,0,6,NULL),(61,44,0.600,0,1,0,NULL,NULL),(62,44,0.400,0,2,1,NULL,NULL),(63,47,1.000,0,2,1,NULL,NULL),(64,48,1.000,0,4,0,NULL,NULL),(65,49,1.000,0,4,0,NULL,NULL),(66,50,1.000,0,5,1,NULL,NULL),(67,53,0.200,0,NULL,0,35,NULL),(68,53,0.300,0,NULL,0,36,NULL),(69,53,0.500,0,NULL,0,52,NULL),(70,54,0.600,0,NULL,0,3,NULL),(71,54,0.400,0,NULL,0,4,NULL),(72,56,1.000,0,NULL,0,51,NULL),(73,58,0.500,0,1,1,NULL,NULL),(74,58,0.500,0,3,0,NULL,NULL),(75,59,0.500,0,1,1,NULL,NULL),(76,59,0.500,0,3,0,NULL,NULL),(77,62,0.500,0,NULL,0,1,NULL),(78,62,0.500,0,NULL,0,2,NULL),(79,63,0.600,0,NULL,0,3,NULL),(80,63,0.400,0,NULL,0,7,NULL),(81,64,0.400,0,1,0,NULL,NULL),(82,64,0.600,0,2,1,NULL,NULL),(83,65,0.600,0,1,1,NULL,NULL),(84,65,0.100,0,2,0,NULL,NULL),(85,65,0.300,0,3,0,NULL,NULL),(86,66,0.600,0,1,0,NULL,NULL),(87,66,0.100,0,2,0,NULL,NULL),(88,66,0.300,0,3,0,NULL,NULL),(89,67,0.300,0,1,0,NULL,NULL),(90,67,0.600,0,2,0,NULL,NULL),(91,67,0.100,0,3,0,NULL,NULL),(92,68,0.600,0,2,0,NULL,NULL),(93,68,0.300,0,3,0,NULL,NULL),(94,68,0.100,0,4,0,NULL,NULL),(95,69,0.300,0,2,0,NULL,NULL),(96,69,0.300,0,3,0,NULL,NULL),(97,69,0.300,0,4,0,NULL,NULL),(98,69,0.100,0,5,0,NULL,NULL),(99,82,0.300,0,1,0,NULL,NULL),(100,82,0.600,0,2,0,NULL,NULL),(101,82,0.100,0,3,0,NULL,NULL),(102,97,1.000,0,NULL,0,98,1),(103,97,1.000,0,NULL,0,99,2),(104,99,1.000,0,6,0,NULL,1),(105,99,1.000,0,6,0,NULL,2),(106,99,1.000,0,6,0,NULL,3),(107,99,1.000,0,6,0,NULL,4),(108,99,1.000,0,6,0,NULL,5),(109,96,1.000,0,6,0,NULL,1),(110,96,1.000,0,7,0,NULL,2),(111,96,1.000,0,8,0,NULL,3),(112,97,1.000,0,NULL,0,101,3),(113,99,1.000,0,NULL,0,102,6),(114,102,1.000,0,NULL,0,103,1),(115,102,1.000,0,NULL,0,NULL,2),(116,105,1.000,0,NULL,0,106,1),(117,106,1.000,0,NULL,0,107,1),(118,105,1.000,0,NULL,0,108,2),(119,105,1.000,0,NULL,0,109,3),(120,109,1.000,0,6,0,NULL,1),(121,103,1.000,0,NULL,0,110,1),(124,102,1.000,0,NULL,0,111,3),(125,111,1.000,0,NULL,0,112,1),(130,102,1.000,0,NULL,0,114,4),(131,102,1.000,0,NULL,0,115,5),(132,114,1.000,0,6,0,NULL,1),(133,114,1.000,0,7,0,NULL,2),(134,114,1.000,0,8,0,NULL,3),(135,114,1.000,0,7,0,NULL,4),(136,114,1.000,0,8,0,NULL,5),(137,115,1.000,0,NULL,0,116,1),(138,117,1.000,0,NULL,0,118,1),(139,118,1.000,0,NULL,0,119,1),(140,111,1.000,0,NULL,0,120,2),(141,116,1.000,0,NULL,0,121,1),(144,102,1.000,0,NULL,0,125,6),(145,102,1.000,0,NULL,0,126,7),(146,102,1.000,0,NULL,0,127,8),(147,102,1.000,0,NULL,0,128,9),(148,102,1.000,0,NULL,0,129,10),(149,102,1.000,0,NULL,0,130,11),(150,119,1.000,0,8,0,NULL,1),(151,119,1.000,0,9,0,NULL,2),(152,102,1.000,0,NULL,0,131,12),(153,131,1.000,0,8,0,NULL,1),(154,131,1.000,0,9,0,NULL,2),(155,114,1.000,0,9,0,NULL,6),(156,114,1.000,0,7,0,NULL,7),(159,132,1.000,0,NULL,0,133,1),(160,133,1.000,0,7,0,NULL,1),(161,133,1.000,0,8,0,NULL,2),(162,133,1.000,0,9,0,NULL,3),(163,132,1.000,0,NULL,0,134,2),(164,134,1.000,0,NULL,0,135,1),(165,135,1.000,0,7,0,NULL,1),(166,134,1.000,0,7,0,NULL,2),(167,136,1.000,0,NULL,0,137,1),(168,137,1.000,0,7,0,NULL,1),(169,137,1.000,0,8,0,NULL,2),(170,120,1.000,0,NULL,0,138,1),(171,138,1.000,0,7,0,NULL,1),(172,139,1.000,0,NULL,0,140,1),(173,140,1.000,0,NULL,0,141,1),(174,141,1.000,0,7,0,NULL,1),(175,139,1.000,0,NULL,0,142,2),(176,141,1.000,0,8,0,NULL,2),(177,142,1.000,0,8,0,NULL,1),(178,136,1.000,0,NULL,0,143,2),(179,143,1.000,0,7,0,NULL,1),(180,143,1.000,0,8,0,NULL,2),(181,143,1.000,0,9,0,NULL,3),(182,136,1.000,0,NULL,0,144,3),(183,144,1.000,0,NULL,0,145,1),(184,145,1.000,0,7,0,NULL,1),(185,145,1.000,0,8,0,NULL,2),(186,145,1.000,0,9,0,NULL,3),(187,102,1.000,0,NULL,0,146,13),(188,139,1.000,0,NULL,0,147,3),(189,136,1.000,0,NULL,0,148,4),(190,149,1.000,0,NULL,0,NULL,1),(193,102,1.000,0,NULL,0,151,14),(194,151,1.000,0,7,0,NULL,1),(195,151,1.000,0,8,0,NULL,2),(196,151,1.000,0,9,0,NULL,3),(198,149,1.000,0,NULL,0,NULL,2),(201,149,1.000,0,NULL,0,NULL,3),(205,149,1.000,0,NULL,0,NULL,4),(211,149,1.000,0,NULL,0,NULL,5),(216,149,1.000,0,NULL,0,NULL,6),(219,87,1.000,0,NULL,0,NULL,1),(223,87,1.000,0,NULL,0,162,2),(224,162,1.000,0,7,0,NULL,1),(225,162,1.000,0,8,0,NULL,2),(226,87,1.000,0,NULL,0,NULL,3),(227,149,1.000,0,NULL,0,163,7),(228,149,1.000,0,NULL,0,164,8),(229,163,1.000,0,7,0,NULL,1),(230,164,1.000,0,8,0,NULL,1),(231,149,1.000,0,NULL,0,165,9),(232,165,1.000,0,9,0,NULL,1);
/*!40000 ALTER TABLE `parameter_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_quality_gate`
--

DROP TABLE IF EXISTS `parameter_quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `qualityGateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `qualityGate_parameter_fk_idx` (`parameterId`),
  KEY `qgp_qualityGate_fk_idx` (`qualityGateId`),
  CONSTRAINT `qgp_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgp_qualityGate_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_quality_gate`
--

LOCK TABLES `parameter_quality_gate` WRITE;
/*!40000 ALTER TABLE `parameter_quality_gate` DISABLE KEYS */;
INSERT INTO `parameter_quality_gate` VALUES (1,57,13),(2,58,14),(3,59,15),(4,61,16),(5,62,17),(6,63,18),(7,64,19),(8,65,20),(9,66,21),(10,67,22),(11,68,23),(12,69,24),(13,70,25),(14,82,26);
/*!40000 ALTER TABLE `parameter_quality_gate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quality_gate`
--

DROP TABLE IF EXISTS `quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qg_assetType_parameter_workspace_uq` (`workspaceId`,`name`),
  KEY `qg_workspace_fk_idx` (`workspaceId`),
  KEY `qg_createdBy_fk_idx` (`createdBy`),
  KEY `qg_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qg_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_workspace_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quality_gate`
--

LOCK TABLES `quality_gate` WRITE;
/*!40000 ALTER TABLE `quality_gate` DISABLE KEYS */;
INSERT INTO `quality_gate` VALUES (10,'Test QG',1,2,'2016-05-09 04:35:01',2,'2016-05-09 04:35:01'),(11,'tej test',1,2,'2016-05-26 09:32:50',2,'2016-05-26 09:32:50'),(12,'lknjkl',1,2,'2016-05-27 07:37:47',2,'2016-05-27 07:37:47'),(13,'QG_IT Portfolio Survey - 1ghgh',1,2,'2016-06-08 06:13:37',2,'2016-06-08 06:13:37'),(14,'QG_Parameter Tree View',1,2,'2016-06-15 05:04:15',2,'2016-06-15 05:04:15'),(15,'QG_Parameter Tree View - 06-15 10:37:09',1,2,'2016-06-15 05:07:13',2,'2016-06-15 05:07:13'),(16,'QG_Parameter Tree View - 06-15 10:37:17',1,2,'2016-06-15 05:07:19',2,'2016-06-15 05:07:19'),(17,'QG_Test by Samar1',1,2,'2016-08-05 07:12:17',2,'2016-08-05 07:12:17'),(18,'QG_dfgdfgfdggdfghrtyy567 - 08-05 12:44:44',1,2,'2016-08-05 07:14:47',2,'2016-08-05 07:14:47'),(19,'QG_First for lp1 - 08-05 12:44:52',1,2,'2016-08-05 07:15:11',2,'2016-08-05 07:15:11'),(20,'QG_LP parameter to check weight add',1,2,'2016-08-10 06:22:01',2,'2016-08-10 06:22:01'),(21,'QG_LP to test1',1,2,'2016-08-10 06:47:41',2,'2016-08-10 06:47:41'),(22,'QG_45yt45y',1,2,'2016-08-10 07:15:10',2,'2016-08-10 07:15:10'),(23,'QG_test1-lp1',1,2,'2016-08-10 07:44:47',2,'2016-08-10 07:44:47'),(24,'QG_Test-LP2',1,2,'2016-08-10 07:46:07',2,'2016-08-10 07:46:07'),(25,'QG_FC test by Samar',1,2,'2016-08-10 07:47:59',2,'2016-08-10 07:47:59'),(26,'QG_45yt45y - 09-01 18:43:39',1,2,'2016-09-01 13:13:55',2,'2016-09-01 13:13:55');
/*!40000 ALTER TABLE `quality_gate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quality_gate_condition`
--

DROP TABLE IF EXISTS `quality_gate_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityGateId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `weight` decimal(10,3) DEFAULT NULL,
  `gateCondition` varchar(45) NOT NULL,
  `conditionValues` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qgc_parameter_fk_idx` (`parameterId`),
  KEY `qgc_qg_fk_idx` (`qualityGateId`),
  KEY `qgc_createdBy_fk_idx` (`createdBy`),
  KEY `qgc_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qgc_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_qg_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quality_gate_condition`
--

LOCK TABLES `quality_gate_condition` WRITE;
/*!40000 ALTER TABLE `quality_gate_condition` DISABLE KEYS */;
INSERT INTO `quality_gate_condition` VALUES (1,10,2,0.400,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-09 05:12:58',2,'2016-05-09 05:12:58'),(2,10,2,0.300,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-09 05:13:44',2,'2016-05-09 05:13:44'),(3,11,53,1.000,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":3.0},{\"color\":\"#ffffbf\",\"value\":6.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-26 09:34:37',2,'2016-05-26 09:34:37'),(4,11,35,3.000,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-26 09:38:32',2,'2016-05-26 09:38:32'),(5,12,6,1.000,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":1.0},{\"color\":\"#ffffbf\",\"value\":3.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-05-27 07:38:15',2,'2016-05-27 07:38:15'),(6,13,57,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-08 06:13:37',2,'2016-06-08 06:13:37'),(7,14,58,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-15 05:04:15',2,'2016-06-15 05:04:15'),(8,15,59,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-15 05:07:13',2,'2016-06-15 05:07:13'),(9,16,61,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-06-15 05:07:19',2,'2016-06-15 05:07:19'),(10,17,62,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-05 07:12:17',2,'2016-08-05 07:12:17'),(11,18,63,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-05 07:14:47',2,'2016-08-05 07:14:47'),(12,19,64,NULL,'LESS','{\"values\":[{\"color\":\"#d7191c\",\"value\":1.0},{\"color\":\"#fdae61\",\"value\":1.0},{\"color\":\"#a6d96a\",\"value\":3.0},{\"color\":\"#1a9641\",\"value\":null}]}',2,'2016-08-05 07:15:11',2,'2016-08-05 07:35:05'),(13,20,65,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 06:22:01',2,'2016-08-10 06:22:01'),(14,21,66,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 06:47:41',2,'2016-08-10 06:47:41'),(15,22,67,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:15:10',2,'2016-08-10 07:15:10'),(16,23,68,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:44:47',2,'2016-08-10 07:44:47'),(17,24,69,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:46:07',2,'2016-08-10 07:46:07'),(18,25,70,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-08-10 07:47:59',2,'2016-08-10 07:47:59'),(19,26,82,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',2,'2016-09-01 13:13:55',2,'2016-09-01 13:13:55');
/*!40000 ALTER TABLE `quality_gate_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentQuesId` int(11) DEFAULT NULL,
  `questionMode` varchar(20) NOT NULL,
  `title` text NOT NULL,
  `displayName` varchar(256) NOT NULL,
  `helpText` text,
  `workspaceId` int(11) DEFAULT NULL,
  `questionType` varchar(10) NOT NULL,
  `queTypeText` text,
  `level` smallint(6) NOT NULL,
  `questionCategoryId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ques_workspace_displayName_uq` (`workspaceId`,`displayName`),
  KEY `QUESTIONNAIRE_WORKSPACE_ID_FK_idx` (`workspaceId`),
  KEY `QUESTIONNAIRE_CREATED_BY_FK_idx` (`createdBy`),
  KEY `QUESTIONNAIRE_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `question_questioncategoryid_categoryid_idx` (`questionCategoryId`),
  KEY `ques_quesId_fk_idx` (`parentQuesId`),
  CONSTRAINT `QUESTIONNAIRE_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_WORKSPACE_ID_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ques_quesId_fk` FOREIGN KEY (`parentQuesId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `question_questioncategoryid_categoryid` FOREIGN KEY (`questionCategoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,NULL,'FIXED','This is title of test que','This is the test question?','Testing Que',1,'TEXT',NULL,0,1,2,'2016-04-19 12:00:37',2,'2016-04-19 12:00:37'),(2,NULL,'FIXED','This is the test question-1','Test Question - 1','',1,'PARATEXT',NULL,0,2,2,'2016-04-19 12:04:26',2,'2016-04-19 12:04:26'),(3,NULL,'FIXED','Another Test Widget','dispaly test','This is Test',1,'TEXT',NULL,0,2,1,'2016-04-20 06:53:24',1,'2016-04-20 06:53:24'),(4,NULL,'MODIFIABLE','This is test que 1','This is test que','This is test que 2',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"No\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,2,2,'2016-05-27 09:43:18',2,'2016-06-14 11:28:49'),(5,NULL,'MODIFIABLE','test by ananta','test by ananta','test by ananta',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,1,2,'2016-05-06 07:12:00',2,'2016-05-12 09:21:11'),(6,NULL,'FIXED','Marketing','Marketing','Marketing',1,'CURRENCY','{\"min\":null,\"max\":null,\"limitTo\":false}',0,NULL,2,'2016-09-21 10:01:15',2,'2016-09-21 10:01:15'),(7,NULL,'FIXED','Strategy','Strategy','Strategy',1,'CURRENCY','{\"min\":0,\"max\":null,\"limitTo\":false}',0,17,2,'2016-09-21 11:07:59',2,'2016-09-21 11:07:59'),(8,NULL,'FIXED','Operation','Operation','Operation',1,'CURRENCY','{\"min\":0,\"max\":null,\"limitTo\":false}',0,17,2,'2016-09-21 11:07:59',2,'2016-09-21 11:07:59'),(9,NULL,'FIXED','Test Widget','Test Widget','SDAFSD',1,'CURRENCY','{\"min\":0,\"max\":null,\"limitTo\":false}',0,17,2,'2016-09-21 11:07:59',2,'2016-09-21 11:07:59');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `questionnaireType` varchar(5) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_name_workspace_unique` (`name`,`workspaceId`),
  KEY `questionnaire_workspaceId_fk_idx` (`workspaceId`),
  KEY `questionnaire_createdBy_fk_idx` (`createdBy`),
  KEY `questionnaire_updatedBy_fk_idx` (`updatedBy`),
  KEY `questionnaire_assetTypeId_fk_idx` (`assetTypeId`),
  CONSTRAINT `questionnaire_assetTypeId_fk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` VALUES (1,'Test by Sunil','this is the test',1,'EDITABLE',1,'DEF',2,'2016-09-06 12:54:57',2,'2016-09-09 04:58:06'),(2,'Test for Dhruv','',1,'EDITABLE',1,'DEF',2,'2016-09-06 12:54:57',2,'2016-09-06 12:54:57'),(5,'Tej 1447','',1,'CLOSED',1,'DEF',2,'2016-09-06 12:54:57',2,'2016-11-03 10:37:07'),(6,'Test for FC Evaluation','',1,'EDITABLE',1,'DEF',2,'2016-09-06 12:54:57',2,'2016-09-06 12:54:57'),(7,'Twest saf','',1,'CLOSED',1,'DEF',2,'2016-09-06 12:54:57',2,'2016-09-20 13:02:06'),(8,'Test by Sunil1','',1,'PUBLISHED',1,'TCO',2,'2016-09-08 10:00:45',2,'2016-10-17 10:00:46'),(9,'Test for response','Test for response then evaluation',1,'PUBLISHED',1,'TCO',2,'2016-09-21 07:59:10',2,'2016-09-22 09:30:48'),(10,'Chart Of Account new','Chart Of Account new',1,'PUBLISHED',1,'TCO',2,'2016-09-22 09:33:33',2,'2016-11-21 11:12:47'),(11,'Test for Sequence Number','Test for Sequence Number',1,'EDITABLE',1,'DEF',2,'2016-09-29 06:33:58',2,'2016-09-29 06:33:58'),(12,'test','',1,'EDITABLE',1,'DEF',2,'2016-11-03 10:11:57',2,'2016-11-03 10:11:57');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_asset`
--

DROP TABLE IF EXISTS `questionnaire_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_asset_questionnaireAssetId_uq` (`questionnaireId`,`assetId`),
  KEY `questionnaire_asset_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_asset_assetId_fk_idx` (`assetId`),
  CONSTRAINT `questionnaire_asset_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_asset_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_asset`
--

LOCK TABLES `questionnaire_asset` WRITE;
/*!40000 ALTER TABLE `questionnaire_asset` DISABLE KEYS */;
INSERT INTO `questionnaire_asset` VALUES (1,1,2),(3,1,3),(2,2,1),(17,5,9),(111,5,33),(8,6,7),(9,6,8),(15,6,9),(10,6,10),(11,6,11),(12,6,12),(13,6,13),(14,6,14),(28,7,1),(29,7,2),(30,7,3),(31,7,4),(32,7,7),(33,7,8),(34,7,9),(35,7,10),(36,7,11),(37,7,12),(38,7,13),(39,7,14),(40,7,15),(41,7,16),(42,7,17),(44,7,18),(45,7,19),(46,7,20),(47,7,21),(48,7,22),(49,7,23),(50,7,24),(51,7,25),(52,7,26),(53,7,27),(54,7,28),(55,7,29),(56,7,30),(57,7,31),(58,7,32),(20,7,33),(21,7,34),(22,7,35),(23,7,37),(24,7,38),(25,7,39),(26,7,40),(27,7,41),(43,7,81),(110,8,7),(68,8,8),(69,8,9),(70,8,10),(71,8,11),(99,9,8),(100,9,9),(101,9,14),(94,9,22),(97,9,31),(93,9,35),(98,9,38),(103,10,8),(104,10,9),(102,10,17),(105,11,1),(106,11,7),(107,11,9),(108,11,10),(109,11,12),(112,12,7),(113,12,8);
/*!40000 ALTER TABLE `questionnaire_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_parameter`
--

DROP TABLE IF EXISTS `questionnaire_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `rootParameterId` int(11) NOT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qp_questionnaireId_parameterId_parentParameterId_uq` (`questionnaireId`,`parameterId`,`parentParameterId`,`rootParameterId`),
  KEY `qp_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `qp_parameterId_fk_idx` (`parameterId`),
  KEY `qp_rootParameterId_fk_idx` (`rootParameterId`),
  KEY `qp_parentParameterId_fk_idx` (`parentParameterId`),
  CONSTRAINT `qp_parameterId_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_rootParameterId_fk` FOREIGN KEY (`rootParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_parameter`
--

LOCK TABLES `questionnaire_parameter` WRITE;
/*!40000 ALTER TABLE `questionnaire_parameter` DISABLE KEYS */;
INSERT INTO `questionnaire_parameter` VALUES (41,1,1,36,36),(31,1,1,53,36),(43,1,2,36,36),(29,1,2,53,36),(42,1,3,36,36),(32,1,3,53,36),(40,1,6,36,36),(30,1,6,53,36),(38,1,35,35,NULL),(35,1,35,53,53),(39,1,36,36,NULL),(33,1,36,53,53),(34,1,52,53,53),(28,1,53,53,NULL),(13,2,6,6,NULL),(15,2,6,6,NULL),(16,2,23,23,NULL),(58,5,2,2,NULL),(59,5,3,3,NULL),(60,5,9,9,NULL),(57,5,50,50,NULL),(47,6,1,53,36),(48,6,2,53,36),(46,6,3,53,36),(45,6,6,53,36),(52,6,35,35,NULL),(51,6,35,53,53),(49,6,36,53,53),(50,6,52,53,53),(44,6,53,53,NULL),(77,8,136,136,NULL),(81,8,137,136,136),(82,8,140,140,NULL),(83,8,141,140,140),(78,8,143,136,136),(80,8,144,136,136),(79,8,145,136,144),(67,9,132,132,NULL),(68,9,133,132,132),(84,10,149,149,NULL),(85,10,163,149,149),(86,10,164,149,149),(87,10,165,149,149),(74,11,1,8,8),(73,11,2,8,8),(71,11,6,6,NULL),(72,11,8,8,NULL);
/*!40000 ALTER TABLE `questionnaire_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_question`
--

DROP TABLE IF EXISTS `questionnaire_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) DEFAULT NULL,
  `questionId` int(11) NOT NULL,
  `questionnaireAssetId` int(11) DEFAULT NULL,
  `questionnaireParameterId` int(11) NOT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qq_QEId_QId_QAId_QPId_uq` (`questionnaireId`,`questionId`,`questionnaireAssetId`,`questionnaireParameterId`),
  KEY `questionnaire_question_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_question_questionId_fk_idx` (`questionId`),
  KEY `questionnaire_question_assetId_fk_idx` (`questionnaireAssetId`),
  KEY `qq_questionnaireParameterId_fk_idx` (`questionnaireParameterId`),
  CONSTRAINT `qq_questionnaireParameterId_fk` FOREIGN KEY (`questionnaireParameterId`) REFERENCES `questionnaire_parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireAssetId_fk` FOREIGN KEY (`questionnaireAssetId`) REFERENCES `questionnaire_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=457 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_question`
--

LOCK TABLES `questionnaire_question` WRITE;
/*!40000 ALTER TABLE `questionnaire_question` DISABLE KEYS */;
INSERT INTO `questionnaire_question` VALUES (19,2,1,2,13,1),(20,2,2,2,13,2),(21,2,1,2,15,3),(22,2,2,2,15,4),(23,2,1,2,16,5),(24,2,2,2,16,6),(35,1,1,1,35,1),(37,1,2,1,35,2),(38,1,1,1,29,3),(39,1,2,1,29,4),(40,1,1,1,30,5),(41,1,2,1,30,6),(42,1,1,1,31,7),(43,1,1,1,32,8),(44,1,2,1,32,9),(45,1,1,3,35,10),(46,1,2,3,35,11),(47,1,1,3,29,12),(48,1,2,3,29,13),(49,1,1,3,30,14),(50,1,2,3,30,15),(51,1,1,3,31,16),(52,1,1,3,32,17),(53,1,2,3,32,18),(63,1,1,1,38,28),(64,1,2,1,38,29),(65,1,1,1,40,30),(66,1,2,1,40,31),(67,1,1,1,41,32),(68,1,1,1,42,33),(69,1,2,1,42,34),(70,1,1,1,43,35),(71,1,2,1,43,36),(72,1,1,3,38,37),(73,1,2,3,38,38),(74,1,1,3,40,39),(75,1,2,3,40,40),(76,1,1,3,41,41),(77,1,1,3,42,42),(78,1,2,3,42,43),(79,1,1,3,43,44),(80,1,2,3,43,45),(104,6,1,8,51,1),(105,6,2,8,51,2),(106,6,1,8,52,3),(107,6,2,8,52,4),(108,6,1,8,45,5),(109,6,2,8,45,6),(110,6,1,8,46,7),(111,6,2,8,46,8),(112,6,1,8,47,9),(113,6,1,8,48,10),(114,6,2,8,48,11),(115,6,1,9,51,12),(116,6,2,9,51,13),(117,6,1,9,52,14),(118,6,2,9,52,15),(119,6,1,9,45,16),(120,6,2,9,45,17),(121,6,1,9,46,18),(122,6,2,9,46,19),(123,6,1,9,47,20),(124,6,1,9,48,21),(125,6,2,9,48,22),(126,6,1,10,51,23),(127,6,2,10,51,24),(128,6,1,10,52,25),(129,6,2,10,52,26),(130,6,1,10,45,27),(131,6,2,10,45,28),(132,6,1,10,46,29),(133,6,2,10,46,30),(134,6,1,10,47,31),(135,6,1,10,48,32),(136,6,2,10,48,33),(137,6,1,11,51,34),(138,6,2,11,51,35),(139,6,1,11,52,36),(140,6,2,11,52,37),(141,6,1,11,45,38),(142,6,2,11,45,39),(143,6,1,11,46,40),(144,6,2,11,46,41),(145,6,1,11,47,42),(146,6,1,11,48,43),(147,6,2,11,48,44),(148,6,1,12,51,45),(149,6,2,12,51,46),(150,6,1,12,52,47),(151,6,2,12,52,48),(152,6,1,12,45,49),(153,6,2,12,45,50),(154,6,1,12,46,51),(155,6,2,12,46,52),(156,6,1,12,47,53),(157,6,1,12,48,54),(158,6,2,12,48,55),(159,6,1,13,51,56),(160,6,2,13,51,57),(161,6,1,13,52,58),(162,6,2,13,52,59),(163,6,1,13,45,60),(164,6,2,13,45,61),(165,6,1,13,46,62),(166,6,2,13,46,63),(167,6,1,13,47,64),(168,6,1,13,48,65),(169,6,2,13,48,66),(170,6,1,14,51,67),(171,6,2,14,51,68),(172,6,1,14,52,69),(173,6,2,14,52,70),(174,6,1,14,45,71),(175,6,2,14,45,72),(176,6,1,14,46,73),(177,6,2,14,46,74),(178,6,1,14,47,75),(179,6,1,14,48,76),(180,6,2,14,48,77),(181,6,1,15,51,78),(182,6,2,15,51,79),(183,6,1,15,52,80),(184,6,2,15,52,81),(185,6,1,15,45,82),(186,6,2,15,45,83),(187,6,1,15,46,84),(188,6,2,15,46,85),(189,6,1,15,47,86),(190,6,1,15,48,87),(191,6,2,15,48,88),(192,5,5,17,57,1),(334,9,7,97,68,1),(335,9,8,97,68,2),(336,9,9,97,68,3),(337,9,7,98,68,4),(338,9,8,98,68,5),(339,9,9,98,68,6),(340,9,7,99,68,7),(341,9,8,99,68,8),(342,9,9,99,68,9),(343,9,7,100,68,10),(344,9,8,100,68,11),(345,9,9,100,68,12),(346,9,7,101,68,13),(347,9,8,101,68,14),(348,9,9,101,68,15),(349,9,7,93,68,16),(350,9,8,93,68,17),(351,9,9,93,68,18),(352,9,7,94,68,19),(353,9,8,94,68,20),(354,9,9,94,68,21),(361,11,1,105,71,1),(362,11,2,105,71,2),(363,11,1,105,73,3),(364,11,2,105,73,4),(365,11,1,105,74,5),(366,11,1,106,71,6),(367,11,2,106,71,7),(368,11,1,106,73,8),(369,11,2,106,73,9),(370,11,1,106,74,10),(371,11,1,107,71,11),(372,11,2,107,71,12),(373,11,1,107,73,13),(374,11,2,107,73,14),(375,11,1,107,74,15),(376,11,1,108,71,16),(377,11,2,108,71,17),(378,11,1,108,73,18),(379,11,2,108,73,19),(380,11,1,108,74,20),(381,11,1,109,71,21),(382,11,2,109,71,22),(383,11,1,109,73,23),(384,11,2,109,73,24),(385,11,1,109,74,25),(396,8,7,68,81,1),(397,8,8,68,81,2),(398,8,7,68,78,3),(399,8,8,68,78,4),(400,8,9,68,78,5),(401,8,7,68,79,6),(402,8,8,68,79,7),(403,8,9,68,79,8),(404,8,7,69,81,9),(405,8,8,69,81,10),(406,8,7,69,78,11),(407,8,8,69,78,12),(408,8,9,69,78,13),(409,8,7,69,79,14),(410,8,8,69,79,15),(411,8,9,69,79,16),(412,8,7,70,81,17),(413,8,8,70,81,18),(414,8,7,70,78,19),(415,8,8,70,78,20),(416,8,9,70,78,21),(417,8,7,70,79,22),(418,8,8,70,79,23),(419,8,9,70,79,24),(420,8,7,71,81,25),(421,8,8,71,81,26),(422,8,7,71,78,27),(423,8,8,71,78,28),(424,8,9,71,78,29),(425,8,7,71,79,30),(426,8,8,71,79,31),(427,8,9,71,79,32),(428,8,7,110,81,33),(429,8,8,110,81,34),(430,8,7,110,78,35),(431,8,8,110,78,36),(432,8,9,110,78,37),(433,8,7,110,79,38),(434,8,8,110,79,39),(435,8,9,110,79,40),(436,8,7,68,83,41),(437,8,8,68,83,42),(438,8,7,69,83,43),(439,8,8,69,83,44),(440,8,7,70,83,45),(441,8,8,70,83,46),(442,8,7,71,83,47),(443,8,8,71,83,48),(444,8,7,110,83,49),(445,8,8,110,83,50),(446,5,5,111,57,2),(447,5,1,111,58,3),(448,5,2,111,58,4),(449,5,1,111,59,5),(450,5,2,111,59,6),(451,5,1,111,60,7),(452,5,1,17,58,8),(453,5,2,17,58,9),(454,5,1,17,59,10),(455,5,2,17,59,11),(456,5,1,17,60,12);
/*!40000 ALTER TABLE `questionnaire_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_asset`
--

DROP TABLE IF EXISTS `relationship_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionId` int(11) NOT NULL,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `relationship_asset_relationshipSuggestionId_fk_idx` (`relationshipSuggestionId`),
  KEY `relationship_asset_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `relationship_asset_childAssetId_fk_idx` (`childAssetId`),
  KEY `relationship_asset_createdBy_fk_idx` (`createdBy`),
  KEY `relationship_asset_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_asset`
--

LOCK TABLES `relationship_asset` WRITE;
/*!40000 ALTER TABLE `relationship_asset` DISABLE KEYS */;
INSERT INTO `relationship_asset` VALUES (1,1,2,5,2,'2016-06-14 11:00:32',2,'2016-06-14 11:00:32'),(2,1,2,5,2,'2016-06-14 11:00:32',2,'2016-06-14 11:00:32');
/*!40000 ALTER TABLE `relationship_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_asset_data`
--

DROP TABLE IF EXISTS `relationship_asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rad_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `rad_childAssetId_fk_idx` (`childAssetId`),
  KEY `rad_assetId_fk_idx` (`assetId`),
  CONSTRAINT `rad_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_asset_data`
--

LOCK TABLES `relationship_asset_data` WRITE;
/*!40000 ALTER TABLE `relationship_asset_data` DISABLE KEYS */;
INSERT INTO `relationship_asset_data` VALUES (1,1,3,48),(2,12,9,50),(3,1,2,54),(4,28,4,57),(5,37,4,59),(6,36,12,60),(7,16,6,61),(8,32,42,62),(9,10,40,64),(10,28,13,65),(11,14,31,66),(12,26,14,67),(13,18,14,68),(14,31,33,70),(15,14,9,72),(16,21,14,73),(17,11,12,75),(18,34,30,79),(19,1,2,80),(20,33,51,82),(21,1,42,83),(22,1,36,84),(23,51,2,85);
/*!40000 ALTER TABLE `relationship_asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_attribute_data`
--

DROP TABLE IF EXISTS `relationship_attribute_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_attribute_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionAttributeId` int(11) NOT NULL,
  `relationshipAssetId` int(11) NOT NULL,
  `attributeData` text,
  PRIMARY KEY (`id`),
  KEY `rad_relationshipSuggestionAttributeId_fk_idx` (`relationshipSuggestionAttributeId`),
  KEY `rad_relationshipAssetId_fk_idx` (`relationshipAssetId`),
  CONSTRAINT `rad_relationshipAssetId_fk` FOREIGN KEY (`relationshipAssetId`) REFERENCES `relationship_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_relationshipSuggestionAttributeId_fk` FOREIGN KEY (`relationshipSuggestionAttributeId`) REFERENCES `relationship_suggestion_attribute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_attribute_data`
--

LOCK TABLES `relationship_attribute_data` WRITE;
/*!40000 ALTER TABLE `relationship_attribute_data` DISABLE KEYS */;
INSERT INTO `relationship_attribute_data` VALUES (1,2,1,'GIC Db'),(2,1,1,'Google'),(3,2,2,'GIC Db'),(4,1,2,'Google');
/*!40000 ALTER TABLE `relationship_attribute_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_suggestion`
--

DROP TABLE IF EXISTS `relationship_suggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rs_displayName_workspaceId_uq` (`workspaceId`,`displayName`),
  KEY `rs_parentAssetTypeId_idx` (`parentAssetTypeId`),
  KEY `rs_childAssetTypeId_idx` (`childAssetTypeId`),
  KEY `rs_relationshipTypeId_idx` (`relationshipTypeId`),
  KEY `rs_workspaceId_idx` (`workspaceId`),
  KEY `rs_createdBy_idx` (`createdBy`),
  KEY `rs_updatedBy_idx` (`updatedBy`),
  CONSTRAINT `rs_childAssetTypeId` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_parentAssetTypeId` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_relationshipTypeId` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_suggestion`
--

LOCK TABLES `relationship_suggestion` WRITE;
/*!40000 ALTER TABLE `relationship_suggestion` DISABLE KEYS */;
INSERT INTO `relationship_suggestion` VALUES (1,1,1,2,'Application Runs :: Runs on Server',1,0,2,'2016-04-20 10:31:12',2,'2016-04-20 10:31:12'),(2,1,1,1,'Application Runs :: Runs on Application',1,0,2,'2016-06-13 13:44:00',2,'2016-06-13 13:44:00'),(3,1,1,4,'Application Runs :: Runs on Person',1,0,2,'2016-06-14 10:55:52',2,'2016-06-14 10:55:52'),(4,1,1,5,'Application Runs :: Runs on Business Services',1,0,2,'2016-06-15 05:06:36',2,'2016-06-15 05:06:36');
/*!40000 ALTER TABLE `relationship_suggestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_suggestion_attribute`
--

DROP TABLE IF EXISTS `relationship_suggestion_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_suggestion_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attributeName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(5) DEFAULT NULL,
  `relationshipSuggestionId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ra_uq_attributeName_rSuggestionId1` (`relationshipSuggestionId`,`attributeName`),
  KEY `ra_relationshipSuggestionId_idx` (`relationshipSuggestionId`),
  KEY `ra_createdBy_fk_idx` (`createdBy`),
  KEY `ra_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_suggestion_attribute`
--

LOCK TABLES `relationship_suggestion_attribute` WRITE;
/*!40000 ALTER TABLE `relationship_suggestion_attribute` DISABLE KEYS */;
INSERT INTO `relationship_suggestion_attribute` VALUES (1,'Name','TEXT',100,1,0,1,1,2,'2016-04-20 10:31:12',2,'2016-04-20 10:31:12'),(2,'Desc','TEXT',200,1,0,0,2,2,'2016-04-20 10:31:37',2,'2016-04-20 10:31:37'),(3,'Name','TEXT',100,2,0,1,1,2,'2016-06-13 13:44:00',2,'2016-06-13 13:44:00'),(4,'Name','TEXT',100,3,0,1,1,2,'2016-06-14 10:55:52',2,'2016-06-14 10:55:52'),(5,'Name','TEXT',100,4,0,1,1,2,'2016-06-15 05:06:36',2,'2016-06-15 05:06:36');
/*!40000 ALTER TABLE `relationship_suggestion_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_template`
--

DROP TABLE IF EXISTS `relationship_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `hasAttribute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_template_displayName_assetTemplateId_uq` (`displayName`,`assetTemplateId`),
  UNIQUE KEY `assetTemplateId_UNIQUE` (`assetTemplateId`),
  KEY `relationship_template_parentAssetTypeId_fk_idx` (`parentAssetTypeId`),
  KEY `relationship_template_childAssetTypeId_fk_idx` (`childAssetTypeId`),
  KEY `relationship_template_relationshipTypeId_fk_idx` (`relationshipTypeId`),
  KEY `relationship_template_assetTemplateId_fk_idx` (`assetTemplateId`),
  CONSTRAINT `relationship_template_assetTemplateId_fk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_childAssetTypeId_fk` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_parentAssetTypeId_fk` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_relationshipTypeId_fk` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_template`
--

LOCK TABLES `relationship_template` WRITE;
/*!40000 ALTER TABLE `relationship_template` DISABLE KEYS */;
INSERT INTO `relationship_template` VALUES (2,1,1,2,'Application Runs :: Runs on Server',10,1),(3,2,1,5,'Server Runs :: Runs on Business Services',11,0),(4,4,1,5,'Person Runs :: Runs on Business Services',12,1),(5,6,1,8,'IT Services Runs :: Runs on Relationship',13,1),(6,1,1,1,'Application Runs :: Runs on Application',14,0),(7,5,1,4,'Business Services Runs :: Runs on Person',15,0),(8,1,1,3,'Application Runs :: Runs on Database',16,1),(9,2,1,1,'Server Runs :: Runs on Application',17,0),(10,1,1,2,'Application Runs :: Runs on Server',19,1),(11,1,1,2,'Application Runs :: Runs on Server',20,1),(12,1,1,2,'Application Runs :: Runs on Server',21,1),(13,1,1,2,'Application Runs :: Runs on Server',22,0),(16,1,1,2,'Application Runs :: Runs on Server',30,1),(17,1,2,4,'Application Allocated from :: Allocated to Person',31,1),(18,3,5,7,'Database Use :: Used by Interfaces',32,0);
/*!40000 ALTER TABLE `relationship_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_type`
--

DROP TABLE IF EXISTS `relationship_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentDescriptor` varchar(50) NOT NULL,
  `childDescriptor` varchar(50) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `displayName` varchar(150) NOT NULL,
  `direction` varchar(4) NOT NULL DEFAULT 'UNI',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_type_displayName_workspaceId_uq` (`workspaceId`,`displayName`),
  KEY `relationship_type_createdBy_idx` (`createdBy`),
  KEY `relationship_type_updatedBy_idx` (`updatedBy`),
  KEY `relationship_type_workspaceId_idx` (`workspaceId`),
  CONSTRAINT `relationship_type_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_type`
--

LOCK TABLES `relationship_type` WRITE;
/*!40000 ALTER TABLE `relationship_type` DISABLE KEYS */;
INSERT INTO `relationship_type` VALUES (1,'Runs','Runs on',1,0,'Runs :: Runs on','UNI',2,'2016-07-18 11:11:05',2,'2016-07-18 11:11:05'),(2,'Allocated from','Allocated to',1,0,'Allocated from :: Allocated to','UNI',2,'2016-07-07 07:14:59',2,'2016-07-07 07:14:59'),(3,'Allocated to','Runs on',1,0,'Allocated to :: Runs on','UNI',2,'2016-07-18 11:11:05',2,'2016-07-18 11:11:05'),(4,'Runs','Runs on',NULL,0,'Runs :: Runs on','UNI',2,'2016-08-12 07:05:21',2,'2016-08-12 07:05:21'),(5,'Use','Used by',NULL,0,'Use :: Used by','BIDI',2,'2016-08-12 10:38:56',2,'2016-08-12 10:38:56');
/*!40000 ALTER TABLE `relationship_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `response_createdBy_fk_idx` (`createdBy`),
  KEY `response_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `response_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
INSERT INTO `response` VALUES (1,1,2,'2016-04-26 05:12:59',2,'2016-06-17 07:01:30'),(2,2,2,'2016-05-06 07:00:52',2,'2016-06-14 11:15:41'),(3,6,2,'2016-05-27 07:14:37',2,'2016-05-27 07:47:55'),(4,5,2,'2016-06-01 07:37:58',2,'2016-06-14 11:28:04'),(5,8,2,'2016-09-21 07:56:38',2,'2016-10-17 10:02:55'),(6,9,2,'2016-09-21 08:02:02',2,'2016-09-21 11:18:37'),(7,10,2,'2016-09-22 09:34:06',2,'2016-09-22 09:34:23');
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response_data`
--

DROP TABLE IF EXISTS `response_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireQuestionId` int(11) NOT NULL,
  `responseId` int(11) NOT NULL,
  `responseData` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_response_data_questionnaireQuestionId_responseId` (`questionnaireQuestionId`,`responseId`),
  KEY `response_data_questionnaireId_fk_idx` (`questionnaireQuestionId`),
  KEY `response_data_responseId_fk_idx` (`responseId`),
  CONSTRAINT `response_data_questionnaireQuestionId_fk` FOREIGN KEY (`questionnaireQuestionId`) REFERENCES `questionnaire_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_data_responseId_fk` FOREIGN KEY (`responseId`) REFERENCES `response` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response_data`
--

LOCK TABLES `response_data` WRITE;
/*!40000 ALTER TABLE `response_data` DISABLE KEYS */;
INSERT INTO `response_data` VALUES (230,434,5,'{\"quantifier\":1.0,\"responseNumber\":1.0}'),(231,433,5,'{\"quantifier\":2.0,\"responseNumber\":2.0}'),(232,435,5,'{\"quantifier\":3.0,\"responseNumber\":3.0}'),(233,430,5,'{\"quantifier\":4.0,\"responseNumber\":4.0}'),(234,431,5,'{\"quantifier\":5.0,\"responseNumber\":5.0}'),(235,432,5,'{\"quantifier\":6.0,\"responseNumber\":6.0}'),(236,429,5,'{\"quantifier\":7.0,\"responseNumber\":7.0}'),(237,428,5,'{\"quantifier\":8.0,\"responseNumber\":8.0}'),(238,444,5,'{\"quantifier\":9.0,\"responseNumber\":9.0}'),(239,445,5,'{\"quantifier\":10.0,\"responseNumber\":10.0}'),(240,402,5,'{\"quantifier\":11.0,\"responseNumber\":11.0}'),(241,401,5,'{\"quantifier\":12.0,\"responseNumber\":12.0}'),(242,403,5,'{\"quantifier\":13.0,\"responseNumber\":13.0}'),(243,398,5,'{\"quantifier\":14.0,\"responseNumber\":14.0}'),(244,399,5,'{\"quantifier\":15.0,\"responseNumber\":15.0}'),(245,400,5,'{\"quantifier\":16.0,\"responseNumber\":16.0}'),(246,397,5,'{\"quantifier\":17.0,\"responseNumber\":17.0}'),(247,396,5,'{\"quantifier\":18.0,\"responseNumber\":18.0}'),(248,436,5,'{\"quantifier\":19.0,\"responseNumber\":19.0}'),(249,437,5,'{\"quantifier\":20.0,\"responseNumber\":20.0}'),(250,410,5,'{\"quantifier\":21.0,\"responseNumber\":21.0}'),(251,409,5,'{\"quantifier\":22.0,\"responseNumber\":22.0}'),(252,411,5,'{\"quantifier\":23.0,\"responseNumber\":23.0}'),(253,406,5,'{\"quantifier\":24.0,\"responseNumber\":24.0}'),(254,407,5,'{\"quantifier\":25.0,\"responseNumber\":25.0}'),(255,408,5,'{\"quantifier\":26.0,\"responseNumber\":26.0}'),(256,405,5,'{\"quantifier\":27.0,\"responseNumber\":27.0}'),(257,404,5,'{\"quantifier\":28.0,\"responseNumber\":28.0}'),(258,438,5,'{\"quantifier\":29.0,\"responseNumber\":29.0}'),(259,439,5,'{\"quantifier\":30.0,\"responseNumber\":30.0}'),(260,418,5,'{\"quantifier\":31.0,\"responseNumber\":31.0}'),(261,417,5,'{\"quantifier\":32.0,\"responseNumber\":32.0}'),(262,419,5,'{\"quantifier\":33.0,\"responseNumber\":33.0}'),(263,414,5,'{\"quantifier\":34.0,\"responseNumber\":34.0}'),(264,415,5,'{\"quantifier\":35.0,\"responseNumber\":35.0}'),(265,416,5,'{\"quantifier\":36.0,\"responseNumber\":36.0}'),(266,413,5,'{\"quantifier\":37.0,\"responseNumber\":37.0}'),(267,412,5,'{\"quantifier\":38.0,\"responseNumber\":38.0}'),(268,440,5,'{\"quantifier\":39.0,\"responseNumber\":39.0}'),(269,441,5,'{\"quantifier\":40.0,\"responseNumber\":40.0}'),(270,426,5,'{\"quantifier\":41.0,\"responseNumber\":41.0}'),(271,425,5,'{\"quantifier\":42.0,\"responseNumber\":42.0}'),(272,427,5,'{\"quantifier\":43.0,\"responseNumber\":43.0}'),(273,422,5,'{\"quantifier\":44.0,\"responseNumber\":44.0}'),(274,423,5,'{\"quantifier\":45.0,\"responseNumber\":45.0}'),(275,424,5,'{\"quantifier\":46.0,\"responseNumber\":46.0}'),(276,421,5,'{\"quantifier\":47.0,\"responseNumber\":47.0}'),(277,420,5,'{\"quantifier\":48.0,\"responseNumber\":48.0}'),(278,442,5,'{\"quantifier\":49.0,\"responseNumber\":49.0}'),(279,443,5,'{\"quantifier\":50.0,\"responseNumber\":50.0}');
/*!40000 ALTER TABLE `response_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  `roleDescription` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_APP_ADMIN',NULL),(2,'ROLE_APP_USER',NULL),(3,'ROLE_CONSULTANT',NULL),(4,'ROLE_CUSTOMER',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `permissionname` varchar(45) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ROLE_PERMISSION` (`permissionname`,`roleid`),
  KEY `FK_ROLEPERM_ROLEID_idx` (`roleid`),
  CONSTRAINT `FK_ROLEPERM_ROLEID` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (231,'AID_MODIFY',1),(207,'AID_VIEW',1),(230,'APP_LOGIN',1),(218,'ASSET_FILLDETAILS',1),(221,'BCM_MODIFY',1),(210,'BCM_VIEW',1),(234,'COLOR_SCHEME_MODIFY',1),(226,'COLOR_SCHEME_VIEW',1),(238,'FUNCTIONAL_MAP_MODIFY',1),(239,'FUNCTIONAL_MAP_VIEW',1),(229,'INDEX_CREATION',1),(227,'MODIFY_TCO',1),(228,'PARAMETER_MODIFY',1),(222,'PARAMETER_VIEW',1),(232,'QUESTIONNAIRE_MODIFY',1),(237,'QUESTIONNAIRE_RESPOND',1),(225,'QUESTIONNAIRE_VIEW',1),(209,'QUESTION_MODIFY',1),(214,'QUESTION_VIEW',1),(215,'RELATIONSHIP_MODIFY',1),(217,'RELATIONSHIP_VIEW',1),(219,'REPORT_ASSET_GRAPH',1),(206,'REPORT_FACET_GRAPH',1),(240,'REPORT_FUNCTIONAL_REDUNDANCY',1),(236,'REPORT_PHP',1),(216,'REPORT_SCATTER_GRAPH',1),(224,'TEMPLATE_MODIFY',1),(223,'TEMPLATE_VIEW',1),(241,'USER_MODIFY',1),(211,'USER_ROLE_MODIFY',1),(233,'USER_ROLE_VIEW',1),(205,'USER_VIEW',1),(220,'VIEW_TCO',1),(208,'WORKSPACES_MODIFY',1),(235,'WORKSPACES_MODIFY_DASHBOARD',1),(213,'WORKSPACES_VIEW',1),(212,'WORKSPACES_VIEW_DASHBOARD',1);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `accountExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountLocked` tinyint(1) NOT NULL DEFAULT '0',
  `credentialExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailAddress_UNIQUE` (`emailAddress`),
  UNIQUE KEY `UK_t40ack6c3x1y4m2nhaju018jg` (`emailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Admin','Admin','thirdeye-admin@birlasoft.com','$2a$10$2E3ud9YmM9U4EAcJd16lmusYswSzmx5DGhQ3tk6L9g4A/5eyvxCpO',0,1,1,0,0,-1,'2016-04-18 10:34:44',-1,'2016-04-18 10:34:44'),(2,'Samar','Gupta','samar.gupta@birlasoft.com','$2a$10$VKZyaHTwz2nYfx0PkUU.CuXqxeuy.10hkNG.aiDEoBfKZ0/0TEFDm',0,0,0,0,0,1,'2016-04-19 11:57:28',1,'2016-04-19 11:57:28');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_role_unique` (`userId`,`roleId`),
  KEY `roleIdFk_idx` (`roleId`),
  KEY `userIdFk_idx` (`userId`),
  CONSTRAINT `roleIdFk` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `userIdFk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_workspace`
--

DROP TABLE IF EXISTS `user_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_USER_WORKSPACE` (`userId`,`workspaceId`),
  KEY `FK_WORKSPACEID_WORKSPACEID_JOIN_idx` (`workspaceId`),
  CONSTRAINT `FK_USERID_USERID_JOIN` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACEID_WORKSPACEID_JOIN` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_workspace`
--

LOCK TABLES `user_workspace` WRITE;
/*!40000 ALTER TABLE `user_workspace` DISABLE KEYS */;
INSERT INTO `user_workspace` VALUES (1,1,1),(3,2,1);
/*!40000 ALTER TABLE `user_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widgetType` varchar(20) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `widgetConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_WIDGET_DASHBOARD_idx` (`dashboardId`),
  CONSTRAINT `FK_WIDGET_DASHBOARD` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget`
--

LOCK TABLES `widget` WRITE;
/*!40000 ALTER TABLE `widget` DISABLE KEYS */;
/*!40000 ALTER TABLE `widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace`
--

DROP TABLE IF EXISTS `workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceName` varchar(45) NOT NULL,
  `workspaceDescription` text,
  `deletedStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_WORKSPACE_USER_idx` (`createdBy`),
  KEY `FK_WORKSPACE_USER_UPDATE_idx` (`updatedBy`),
  CONSTRAINT `FK_WORKSPACE_USER_CREATE` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_USER_UPDATE` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace`
--

LOCK TABLES `workspace` WRITE;
/*!40000 ALTER TABLE `workspace` DISABLE KEYS */;
INSERT INTO `workspace` VALUES (1,'MyCust','Workspace for MyCust',0,1,'2016-04-18 10:34:45',1,'2016-04-18 10:34:45');
/*!40000 ALTER TABLE `workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace_dashboard`
--

DROP TABLE IF EXISTS `workspace_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceId` int(11) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_WORKSPACE_DASHBOARD` (`workspaceId`,`dashboardId`),
  KEY `FK_WORKSPACE_ID_idx` (`workspaceId`),
  KEY `FK_DASHBOARD_ID_idx` (`dashboardId`),
  CONSTRAINT `FK_DASHBOARD_ID` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_ID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace_dashboard`
--

LOCK TABLES `workspace_dashboard` WRITE;
/*!40000 ALTER TABLE `workspace_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `workspace_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database '3rdi'
--
/*!50003 DROP FUNCTION IF EXISTS `GetParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GetParameterTree`(GivenID INT) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv,q,queue,queue_children VARCHAR(1024);
    DECLARE queue_length,front_id,pos INT;

    SET rv = '';
    SET queue = GivenID;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        
        IF queue_length = 1 THEN
            SET front_id = FORMAT(queue,0);
            SET queue = '';
        ELSE
			SET front_id = SUBSTR(queue,1,LOCATE(',',queue)-1);
            SET pos = LOCATE(',',queue) + 1;
            SET q = SUBSTR(queue,pos);
            SET queue = q;
        END IF;
        SET queue_length = queue_length - 1;

        SELECT IFNULL(qc,'') INTO queue_children
        FROM (SELECT GROUP_CONCAT(childparameterId) qc
        FROM parameter_function WHERE parentParameterId = front_id) A;

        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_children;
            ELSE
                SET rv = CONCAT(rv,',',queue_children);
            END IF;
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;

    RETURN rv;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getParameterTree`(IN GivenID int, OUT rv VARCHAR(1024))
BEGIN

    set rv = getParameterTree(GivenID);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-09 15:07:00
