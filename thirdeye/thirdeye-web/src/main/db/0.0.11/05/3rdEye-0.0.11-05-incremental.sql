-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE `3rdi`.`parameter_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parameterId` INT(11) NOT NULL,
  `parameterConfig` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `pc_parameterId_idx` (`parameterId` ASC),
  CONSTRAINT `pc_parameterId`
    FOREIGN KEY (`parameterId`)
    REFERENCES `3rdi`.`parameter` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
CREATE TABLE `3rdi`.`quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qg_assetType_parameter_workspace_uq` (`assetTypeId`,`workspaceId`,`name`),
  KEY `qg_assetType_fk_idx` (`assetTypeId`),
  KEY `qg_workspace_fk_idx` (`workspaceId`),
  KEY `qg_createdBy_fk_idx` (`createdBy`),
  KEY `qg_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qg_assetType_fk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_workspace_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;


CREATE TABLE `3rdi`.`quality_gate_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityGateId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `gateCondition` varchar(45) NOT NULL,
  `conditionValues` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qgc_parameter_fk_idx` (`parameterId`),
  KEY `qgc_qg_fk_idx` (`qualityGateId`),
  KEY `qgc_createdBy_fk_idx` (`createdBy`),
  KEY `qgc_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qgc_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_qg_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
