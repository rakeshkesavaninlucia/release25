-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `3rdi`.`quality_gate` 
DROP FOREIGN KEY `qg_assetType_fk`;
ALTER TABLE `3rdi`.`quality_gate` 
DROP COLUMN `assetTypeId`,
DROP INDEX `qg_assetType_parameter_workspace_uq` ,
ADD UNIQUE INDEX `qg_assetType_parameter_workspace_uq` (`workspaceId` ASC, `name` ASC),
DROP INDEX `qg_assetType_fk_idx` ;


CREATE TABLE `3rdi`.`parameter_quality_gate` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parameterId` INT(11) NOT NULL,
  `qualityGateId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `qualityGate_parameter_fk_idx` (`parameterId` ASC),
  INDEX `qgp_qualityGate_fk_idx` (`qualityGateId` ASC),
  CONSTRAINT `qgp_parameter_fk`
    FOREIGN KEY (`parameterId`)
    REFERENCES `3rdi`.`parameter` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `qgp_qualityGate_fk`
    FOREIGN KEY (`qualityGateId`)
    REFERENCES `3rdi`.`quality_gate` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	

ALTER TABLE `3rdi`.`quality_gate_condition` 
CHANGE COLUMN `weight` `weight` DECIMAL(10,3) NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
