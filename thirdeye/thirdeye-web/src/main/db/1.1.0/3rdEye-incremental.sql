-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE functional_map_data
ADD questionnaireId INT(11);

ALTER TABLE functional_map_data
ADD FOREIGN KEY (questionnaireId)
REFERENCES questionnaire(Id);

--changes related to questionnaire status from INT to varchar--
ALTER TABLE questionnaire 
CHANGE COLUMN `status` `status` VARCHAR(10) NOT NULL ;

UPDATE questionnaire SET status='EDITABLE';

ALTER TABLE `questionnaire` 
ADD COLUMN `assetTypeId` INT(11) NOT NULL AFTER `status`,
ADD INDEX `questionnaire_assetTypeId_fk_idx` (`assetTypeId` ASC);

UPDATE `questionnaire` SET `assetTypeId`='1';

ALTER TABLE `questionnaire`
ADD CONSTRAINT `questionnaire_assetTypeId_fk`
  FOREIGN KEY (`assetTypeId`)
  REFERENCES `asset_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


