package org.birlasoft.thirdeye.controller.worldmap;

import java.util.List;

import org.birlasoft.thirdeye.beans.DensityHeatMapWrapper;
import org.birlasoft.thirdeye.service.DensityHeatMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/densityHeatMap")
public class DensityHeatMapController {

	private DensityHeatMapService densityHeatMapService;
	
	@Autowired
	public DensityHeatMapController(DensityHeatMapService densityHeatMapService) {
		this.densityHeatMapService = densityHeatMapService;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String viewDensityHeatMap() {
		return "densityHeatMap/densityHeatMap" ;
	}

	@RequestMapping(value = "/getDensityHeatMapWrapper", method = RequestMethod.GET)
	@ResponseBody
	public List<DensityHeatMapWrapper> worldWrapper(Model model) {
		List<DensityHeatMapWrapper> densityHeatMapWrapper = densityHeatMapService.getAllAssetsFromAssetTemplatesForActiveWorkSpace();
		return densityHeatMapWrapper ;
	}

}
