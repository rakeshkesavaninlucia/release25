package org.birlasoft.thirdeye.controller.parameter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QualityGateBean;
import org.birlasoft.thirdeye.beans.QualityGateConditionBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateConditionService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.ParameterBeanValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller for create {@code question} , save {@code question} , list of all {@code question} ,
 * edit {@code question} , view {@code question} .
 *
 */
@Controller
@RequestMapping(value="/parameter")
public class ParameterController {
	
	private static final String FC_PARAM_CONFIG_BEAN = "_fcParamConfigBean";
	private static final String SELECTED_PARAMETERS = "_selectedParameters";
	private static final String SELECTED_MANDATORY_QUESTIONS = "_selectedMandatoryQuestions";
	private static final String SELECTED_QUESTIONS = "_selectedQuestions";
	private static final String MAP_OF_WEIGHT = "mapOfWeight";
	private static final String LIST_OF_VARIANTS = "listOfVariants";
	private static final String PARAMETER_BEAN_FORM = "parameterBeanForm";
	private static final String ACTIVE_WORKSPACE = "activeWorkspace";
	private static final String PARAM_CREATE_PARAMETER = "param/createParameter";
	
	private CustomUserDetailsService customUserDetailsService; 
	private ParameterService parameterService;
	private ParameterFunctionService parameterFunctionService;
	private ParameterBeanValidator parameterBeanValidator;
	private WorkspaceSecurityService workspaceSecurityService;
	private FunctionalMapService functionalMapService;
	
	@Autowired
	private QualityGateService qualityGateService;
	@Autowired
	private QualityGateConditionService qualityGateConditionService;
	
	/**
	 * Constructor to {@code Autowire} dependencies
	 * @param customUserDetailsService
	 * @param parameterService
	 * @param parameterFunctionService
	 * @param parameterBeanValidator
	 * @param workspaceSecurityService
	 * @param functionalMapService
	 */
	@Autowired
	public ParameterController(CustomUserDetailsService customUserDetailsService,
			ParameterService parameterService,
			ParameterFunctionService parameterFunctionService,
			ParameterBeanValidator parameterBeanValidator,
			WorkspaceSecurityService workspaceSecurityService,
			FunctionalMapService functionalMapService) {
		this.customUserDetailsService = customUserDetailsService;
		this.parameterService = parameterService;
		this.parameterFunctionService = parameterFunctionService;
		this.parameterBeanValidator = parameterBeanValidator;
		this.workspaceSecurityService = workspaceSecurityService;
		this.functionalMapService = functionalMapService;
	}

	/**
	 * To {@code create} the {@code question} form .
	 * @param model
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String createParameter(Model model, HttpSession session) {
		model.addAttribute(ACTIVE_WORKSPACE, customUserDetailsService.getActiveWorkSpaceForUser());
		model.addAttribute(PARAMETER_BEAN_FORM, new ParameterBean());
		model.addAttribute(LIST_OF_VARIANTS, new HashSet<Parameter>());
		session.setAttribute(SELECTED_QUESTIONS, new HashSet<Question>());
		session.setAttribute(SELECTED_MANDATORY_QUESTIONS, new HashSet<Integer>());
		session.setAttribute(SELECTED_PARAMETERS, new HashSet<Parameter>());
		session.setAttribute(FC_PARAM_CONFIG_BEAN, new FunctionalCoverageParameterConfigBean());
		return PARAM_CREATE_PARAMETER;
	}

	
	/**
	 * Save {@code Parameter}
	 * @param parameterBean
	 * @param result
	 * @param model
	 * @param request
	 * @param session
	 * @param functionalMapLevelType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String saveParameter(@ModelAttribute(PARAMETER_BEAN_FORM) ParameterBean parameterBean,  BindingResult result,
			Model model, HttpServletRequest request, HttpSession session, @RequestParam(value = "functionalMapLevelType", required = false) String functionalMapLevelType) {

		Map<Integer, BigDecimal> mapOfChildEntityIdAndWeight = createMapOfChildEntityAndWeight(request);
		
		parameterBean.setName(parameterBean.getName().substring(0, Math.min(parameterBean.getName().length(), 45)).replaceAll(" +", " "));
		parameterBeanValidator.validate(parameterBean, result, mapOfChildEntityIdAndWeight);
		if(result.hasErrors()){
			Set<Parameter> listOfVariants = setVariantsAndChildEntitiesInModel(parameterBean, model);
			
			setFunctionalMapInModel(model, session);
			
			model.addAttribute(ACTIVE_WORKSPACE, customUserDetailsService.getActiveWorkSpaceForUser());
			model.addAttribute(PARAMETER_BEAN_FORM, parameterBean);
			model.addAttribute(MAP_OF_WEIGHT, mapOfChildEntityIdAndWeight);
			model.addAttribute(LIST_OF_VARIANTS, listOfVariants);
			addAttributesInModelFromSession(model, session);
			return PARAM_CREATE_PARAMETER;
		}
		
		Parameter savedParameter = null;
		//Create a new Parameter
		if(parameterBean.getId() == null){
			savedParameter = saveNewParameter(parameterBean, session, functionalMapLevelType);
		}else if(parameterBean.getId() != null){
			Parameter paramFromDB = parameterService.updateParameterObject(parameterBean);
			savedParameter = parameterService.save(paramFromDB);
		}
		
		Set<Integer> mandatoryQuestionIds = (HashSet<Integer>)session.getAttribute(SELECTED_MANDATORY_QUESTIONS);
		
		parameterFunctionService.saveOrUpdateParameterFuntion(parameterBean, mapOfChildEntityIdAndWeight, savedParameter, mandatoryQuestionIds);
		
		return "redirect:/parameter/list";
	}

	/**
	 * Sets parameter child entities in model and extracts the list of variant parameters
	 * @param parameterBean
	 * @param model
	 * @return
	 */
	private Set<Parameter> setVariantsAndChildEntitiesInModel(ParameterBean parameterBean, Model model) {
		Set<Parameter> listOfVariants = new HashSet<>();
		if(parameterBean.getId() != null && parameterBean.getBaseParameterId() == null){
			listOfVariants = parameterService.findByParameter(parameterService.findOne(parameterBean.getId()));
			Map<Integer, Map<Integer, BigDecimal>> mapToShow = parameterService.createVariantAndChildEntitiesMapping(listOfVariants);
			model.addAttribute("mapToShow", mapToShow);
			model.addAttribute("listOfChildEntities", !(listOfVariants.isEmpty()) ?  new ArrayList<Parameter>(listOfVariants).get(0).getParameterFunctionsForParentParameterId() : new ArrayList<Parameter>());
		}
		return listOfVariants;
	}

	/**
	 * Sets functional map in model
	 * @param model
	 * @param session
	 */
	private void setFunctionalMapInModel(Model model, HttpSession session) {
		FunctionalCoverageParameterConfigBean fcConfigBean = (FunctionalCoverageParameterConfigBean) session.getAttribute(FC_PARAM_CONFIG_BEAN);
		if(fcConfigBean != null && fcConfigBean.getFunctionalMapId() != null){
			FunctionalMap functionalMap = functionalMapService.findOne(fcConfigBean.getFunctionalMapId(), false);
			Map<Integer, String> mapOfFunctionalMapType = functionalMapService.getMapOfLevelTypes(functionalMap);
			model.addAttribute("mapOfFunctionalMapType", mapOfFunctionalMapType);
		}
	}

	/**
	 * Create and save new parameter
	 * @param parameterBean
	 * @param session
	 * @param functionalMapLevelType
	 * @return
	 */
	private Parameter saveNewParameter(ParameterBean parameterBean, HttpSession session, String functionalMapLevelType) {
		Parameter savedParameter;
		Parameter parameter = parameterService.createNewParameter(parameterBean);
		savedParameter = parameterService.save(parameter);
		qualityGateService.createDefaultQualityGateForParameter(savedParameter);
		if(savedParameter.getType().equals(ParameterType.FC.name())){
			FunctionalCoverageParameterConfigBean fcBean = (FunctionalCoverageParameterConfigBean) session.getAttribute(FC_PARAM_CONFIG_BEAN);
			if(fcBean != null && fcBean.getFunctionalMapId() != null){
				ParameterConfig pc = parameterService.createNewParameterConfigObject(functionalMapLevelType, savedParameter, fcBean);
				parameterService.saveParameterConfig(pc);
			}
		}
		return savedParameter;
	}

	/**
	 * Add attributes in model, get from session
	 * @param model
	 * @param session
	 */
	private void addAttributesInModelFromSession(Model model, HttpSession session) {
		model.addAttribute("selectedQuestions", session.getAttribute(SELECTED_QUESTIONS));
		model.addAttribute("selectedParameters", session.getAttribute(SELECTED_PARAMETERS));
		model.addAttribute("selectedFunctionalMap", session.getAttribute(FC_PARAM_CONFIG_BEAN));
	}

	/**
	 * Extract request to create map of child entity as key and weight as value. 
	 * @param request
	 * @return {@code Map<Integer, Double>}
	 */
	private Map<Integer, BigDecimal> createMapOfChildEntityAndWeight(HttpServletRequest request) {
		List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());
		
		//Remove csrf from request parameters
		Iterator<String> iter = requestParameterNames.iterator();
		while(iter.hasNext()){
		    if(Constants.REQUEST_PARAM_CSRF.equalsIgnoreCase(iter.next()))
		        iter.remove();
		}
		
		Map<Integer, BigDecimal> map = new HashMap<>(); 
		for (String paramName : requestParameterNames){
			if(isParamChildEntity(paramName) && isParamDetails(paramName)){
				map.put(Integer.parseInt(paramName), BigDecimal.valueOf(Double.parseDouble(request.getParameter(paramName))));
			}
		}
		return map;
	}

	/**
	 * Returns true if request param names are Parameter details.
	 * @param paramName
	 * @return
	 */
	private boolean isParamDetails(String paramName) {
		return !"displayName".equalsIgnoreCase(paramName) 
				&& !"description".equalsIgnoreCase(paramName) 
				&& !"id".equalsIgnoreCase(paramName) 
				&& !"name".equalsIgnoreCase(paramName);
	}

	/**
	 * Returns false if request param names is {@code baseParameterId, workspaceId, paramType, functionalMapLevelType}
	 * @param paramName
	 * @return
	 */
	private boolean isParamChildEntity(String paramName) {
		return !"baseParameterId".equalsIgnoreCase(paramName) 
				&& !"workspaceId".equalsIgnoreCase(paramName) 
				&& !"paramType".equalsIgnoreCase(paramName) 
				&& !"functionalMapLevelType".equalsIgnoreCase(paramName);
	}

	/**
	 * List of {@code Questions} that the user has access to (based on the workspaces they are linked to).
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_VIEW'})")
	public String listOfParameter(Model model) {
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		// fetch parameters of type AP, LP and FC only.
		Set<Parameter> listOfParameter = parameterService.findByWorkspaceIsNullOrWorkspaceInAndTypeIn(listOfWorkspaces, createListOfAvailableParamsType());
		
		// Parameters which are on Questionnaire
		Map<Integer, Integer> mapOfParams = parameterService.getParamsOnQuestionnaire();
		
		//Parameter with Questionnaire Status as closed 
		String enumStatus4 = QuestionnaireStatusType.CLOSED.toString();
		Set<Parameter> setOfClosedParam = parameterService.findParameterIdByClosedStatus(enumStatus4);
		
		// Set of extended base parameter to hide delete icon on list view
		Set<Integer> setOfExtendedBaseParams = parameterService.createExtendedParameters(listOfParameter);
		
		model.addAttribute("setOfExtendedBaseParams", setOfExtendedBaseParams);
		model.addAttribute("mapOfParams", mapOfParams);
	    model.addAttribute("listOfParameter", listOfParameter);
	    model.addAttribute("setOfClosedParam", setOfClosedParam);
	    return "param/listParameter";
	}

	private List<String> createListOfAvailableParamsType() {
		List<String> types = new ArrayList<>();
		types.add(ParameterType.AP.toString());
		types.add(ParameterType.LP.toString());
		types.add(ParameterType.FC.toString());
		return types;
	}

	/**
	 * Remove questions from {@code HttpSession}
	 * @param session
	 */
	@RequestMapping(value = "/initialize", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void removeAllQuestionsFromSession(HttpSession session){
		session.setAttribute(SELECTED_QUESTIONS, new HashSet<Question>());
		session.setAttribute(SELECTED_MANDATORY_QUESTIONS, new HashSet<Integer>());
		session.setAttribute(SELECTED_PARAMETERS, new HashSet<Parameter>());
		session.setAttribute(FC_PARAM_CONFIG_BEAN, new FunctionalCoverageParameterConfigBean());
	}
	
	/**
	 * Edit {@code parameter}
	 * @param idOfParameter
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String editParameter(@PathVariable(value = "id") Integer idOfParameter, Model model, HttpSession session){
		
		Parameter parameter = parameterService.findOne(idOfParameter);
		// Check if the user can access this parameter.
		workspaceSecurityService.authorizeParameterAccess(parameter);
		
		Map<Integer, Integer> mapOfParams = parameterService.getParamsOnQuestionnaire();
		
		// Check Parameter Questionnaire Status if Closed
		Boolean paramStatus = parameterService.getStatusOfParam(parameter);
		
	    //Check if parameterId is in Questionnaire Parameter
        List<QuestionnaireParameter> listOfQuestParam = parameterService.getQuestionnaireByParam(parameter);
		
        if(paramStatus.equals(true)){
        throw new AccessDeniedException("User is not allowed to edit this parameter");
        }
        
		model.addAttribute("mapOfParams", mapOfParams);
		model.addAttribute(ACTIVE_WORKSPACE, customUserDetailsService.getActiveWorkSpaceForUser());
		model.addAttribute(PARAMETER_BEAN_FORM, new ParameterBean(parameter));
		Map<Integer, Double> map = getChildEntitiesAndWeightMapping(model, session, parameter);
		model.addAttribute(MAP_OF_WEIGHT, map);
		if(parameter.getParameter() == null){
			Set<Parameter> listOfVariants = parameterService.findByParameter(parameter);
			Map<Integer, Map<Integer, BigDecimal>> mapToShow = parameterService.createVariantAndChildEntitiesMapping(listOfVariants);
			model.addAttribute("mapToShow", mapToShow);
			model.addAttribute("listOfChildEntities", !(listOfVariants.isEmpty()) ?  new ArrayList<Parameter>(listOfVariants).get(0).getParameterFunctionsForParentParameterId() : new ArrayList<Parameter>());
			model.addAttribute(LIST_OF_VARIANTS, listOfVariants);
			model.addAttribute("paramStatus", paramStatus);
			model.addAttribute("listOfQuestParam", listOfQuestParam);
		}
		return PARAM_CREATE_PARAMETER;
	}

	/**
	 * Create a new variant of {@code parameter}
	 * @param idOfParameter
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/{id}/createVariant", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String createVariantParameter(@PathVariable(value = "id") Integer idOfParameter, Model model, HttpSession session){
		Parameter parameter = parameterService.findOne(idOfParameter);
		// Check if the user can access this parameter.
		workspaceSecurityService.authorizeParameterAccess(parameter);
		
		Parameter variantParameter = parameterService.createVariantParameter(parameter);
		model.addAttribute(PARAMETER_BEAN_FORM, new ParameterBean(variantParameter));
		model.addAttribute(ACTIVE_WORKSPACE, customUserDetailsService.getActiveWorkSpaceForUser());
		Map<Integer, Double> map = getChildEntitiesAndWeightMapping(model, session, parameter);
		model.addAttribute(MAP_OF_WEIGHT, map);
		return PARAM_CREATE_PARAMETER;
	}

	private Map<Integer, Double> getChildEntitiesAndWeightMapping(Model model, HttpSession session, Parameter parameter) {
		List<ParameterFunction> parameterFunctions = parameterFunctionService.findByParameterByParentParameterId(parameter);
		Map<Integer, Double> map = new HashMap<>();
		if(parameter.getType().equals(ParameterType.AP.name())){
			getChildEntitiesForAggregateParameter(model, session, parameterFunctions, map);
		} else if(parameter.getType().equals(ParameterType.LP.name())){
			getChildEntitiesForLeafParameter(model, session, parameterFunctions, map);
		} else if(parameter.getType().equals(ParameterType.FC.name())){
			getChildEntitiesForFunctionalCoverage(model, session, parameter);
		}
		return map;
	}

	/**
	 * Get Functional coverage config and functional map for functional coverage parameter
	 * @param model
	 * @param session
	 * @param parameter
	 */
	private void getChildEntitiesForFunctionalCoverage(Model model, HttpSession session, Parameter parameter) {
		List<ParameterConfig> parameterConfigs = parameterService.findParameterConfigListByParameter(parameter);
		if(parameterConfigs.size() == 1){
			FunctionalCoverageParameterConfigBean fcParamConfigBean = parameterService.getFcConfigBeanFromParameteConfig(parameterConfigs);
			session.setAttribute(FC_PARAM_CONFIG_BEAN, fcParamConfigBean);
			model.addAttribute("selectedFunctionalMap", fcParamConfigBean);
		}
	}

	/**
	 * Get child questions for Leaf parameter
	 * @param model
	 * @param session
	 * @param parameterFunctions
	 * @param map
	 */
	private void getChildEntitiesForLeafParameter(Model model, HttpSession session, List<ParameterFunction> parameterFunctions,
			Map<Integer, Double> map) {
		HashSet<Question> questions = new HashSet<>();
		HashSet<Integer> mandatoryQuestions = new HashSet<>();
		parameterService.extractSetOfQuestionsAndMandatoryQuestions(parameterFunctions, map, questions, mandatoryQuestions);
		session.setAttribute(SELECTED_QUESTIONS, questions);
		session.setAttribute(SELECTED_MANDATORY_QUESTIONS, mandatoryQuestions);
		model.addAttribute("selectedQuestions", questions);
	}


	/**
	 * Get child entities for Aggregate parameter
	 * @param model
	 * @param session
	 * @param parameterFunctions
	 * @param map
	 */
	private void getChildEntitiesForAggregateParameter(Model model, HttpSession session, List<ParameterFunction> parameterFunctions,
			Map<Integer, Double> map) {
		HashSet<Parameter> parameters = new HashSet<>();
		for (ParameterFunction pf : parameterFunctions) {
			map.put(pf.getParameterByChildparameterId().getId(), pf.getWeight().doubleValue());
			parameters.add(pf.getParameterByChildparameterId());
		}
		session.setAttribute(SELECTED_PARAMETERS, parameters);
		model.addAttribute("selectedParameters", parameters);
		
		
	}

	/**
	 * Generate a tree view of {@code parameter} to show its child {@code parameters} 
	 * or {@code questions}
	 * @param idOfParameter
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/treeview", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_VIEW'})")
	public String parameterTreeView(@RequestParam(value = "parameterId") Integer idOfParameter, Model model){
		Parameter parameter = parameterService.findFullyLoaded(idOfParameter);
		// Check if the user can access this parameter.
		workspaceSecurityService.authorizeParameterAccess(parameter);
		ParameterBean parameterBean = parameterService.generateParameterTree(parameter);
		model.addAttribute("parameterBean", parameterBean);
		model.addAttribute("listOfParameters", parameterBean.getChildParameters());
		model.addAttribute("listOfQuestions", parameterBean.getChildQuestions());
		return "param/parameterTreeView :: parameterTreeView";
	}
	
	/**
	 * Delete a {@code parameter}
	 * @param idOfParameter
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteParameter(@RequestParam(value = "parameterId") Integer idOfParameter){
		Parameter parameter = parameterService.findOne(idOfParameter);
		// Check if the user can access this parameter.
		workspaceSecurityService.authorizeParameterAccess(parameter);
		
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		Set<Parameter> listOfParameter = parameterService.findByWorkspaceIsNullOrWorkspaceIn(listOfWorkspaces);
		
		// Parameters which are on Questionnaire
		Map<Integer, Integer> mapOfParams = parameterService.getParamsOnQuestionnaire();
		
		// Set of extended base parameter to hide delete icon on list view
		Set<Integer> setOfExtendedBaseParams = parameterService.createExtendedParameters(listOfParameter);
		
		if(mapOfParams.containsKey(idOfParameter) || setOfExtendedBaseParams.contains(idOfParameter)){
			throw new AccessDeniedException("User is not allowed to delete this parameter");
		}
		if(parameter.getType().equals(ParameterType.AP.name()) || parameter.getType().equals(ParameterType.LP.name())){
			List<ParameterFunction> parameterFunctions = parameterFunctionService.findByParameterByParentParameterId(parameter);
			parameterFunctionService.deleteInBatch(parameterFunctions);
		}else if(parameter.getType().equals(ParameterType.FC.name())){
			parameterService.deleteParameterConfigs(parameterService.findParameterConfigListByParameter(parameter));
		}
		//delete parameter quality gate
		parameterService.deleteParameterQualityGate(parameter);
		parameterService.deleteParameter(idOfParameter);
	}
	
	/**
	 * Create a {@code Quality Gate} to define coloring conditions for a {@code parameter}
	 * @param model
	 * @param idOfParam
	 * @return
	 */
	@RequestMapping(value = "/color/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_VIEW'})")
	public String parameterColoring(Model model, @PathVariable(value="id") Integer idOfParam){
		QualityGate qualityGate = parameterService.getQualityGate(idOfParam);
		QualityGateBean qualityGateBean = new QualityGateBean();
		List<QualityGateConditionBean> listOfConditionBean = new ArrayList<>(); 
		if(null != qualityGate){
			qualityGateBean = qualityGateService.createQGBean(qualityGate);
			listOfConditionBean = qualityGateConditionService.getAllConditions(qualityGate.getId());
			model.addAttribute("qualityGateId", qualityGate.getId());
		}
		int colorScale = 0;
		if (!listOfConditionBean.isEmpty()){
			colorScale = listOfConditionBean.get(0).getValueMapper().size();
		}
		model.addAttribute("colorScaleCount", colorScale);
		model.addAttribute("qualityGateDescription",qualityGateBean.getDescription());
		model.addAttribute("listOfConditions", listOfConditionBean);
		model.addAttribute("paramId", idOfParam);
		return "qualityGates/qualityGateFragments :: parameterColor";		
	}
}
