/**
 * 
 */
package org.birlasoft.thirdeye.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.birlasoft.thirdeye.config.packages.BeansConfig;
import org.birlasoft.thirdeye.config.packages.ControllerConfig;
import org.birlasoft.thirdeye.config.packages.EntityConfig;
import org.birlasoft.thirdeye.config.packages.FormatterConfig;
import org.birlasoft.thirdeye.config.packages.RepositoryConfig;
import org.birlasoft.thirdeye.config.packages.ServicesConfig;
import org.birlasoft.thirdeye.config.packages.ValidatorsConfig;
import org.hibernate.MultiTenancyStrategy;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author shaishav.dixit
 *
 */
@Configuration
@EnableJpaRepositories(
		entityManagerFactoryRef = EntityManagers.TENANTENTITYMANAGER,
        transactionManagerRef = TransactionManagers.TENANTTRANSACTIONMANAGER,
        basePackages = {"org.birlasoft.thirdeye.repositories"})
@Import({EntityConfig.class, ControllerConfig.class, FormatterConfig.class, ServicesConfig.class, BeansConfig.class, RepositoryConfig.class, ValidatorsConfig.class, WebSecurityConfig.class})
@EnableTransactionManagement
public class TenantConfiguration {
	
	@Autowired
	private Environment env;

	@Bean
    public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//		vendorAdapter.setGenerateDdl(Boolean.TRUE);
		vendorAdapter.setShowSql(Boolean.valueOf(env.getProperty("hibernate.show_sql")));
		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5Dialect");
		return vendorAdapter;
    }


    @Bean(name = EntityManagers.TENANTENTITYMANAGER)
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                           MultiTenantConnectionProvider connectionProvider,
                                                           CurrentTenantIdentifierResolver tenantResolver) {
        LocalContainerEntityManagerFactoryBean emfBean = new LocalContainerEntityManagerFactoryBean();
        emfBean.setDataSource(dataSource);
        emfBean.setPackagesToScan("org.birlasoft.thirdeye.entity");
        emfBean.setJpaVendorAdapter(jpaVendorAdapter());

        Map<String, Object> properties = new HashMap<>();
        properties.put(org.hibernate.cfg.Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);
        properties.put(org.hibernate.cfg.Environment.MULTI_TENANT_CONNECTION_PROVIDER, connectionProvider);
        properties.put(org.hibernate.cfg.Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, tenantResolver);

		
        //properties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");

        emfBean.setJpaPropertyMap(properties);
        return emfBean;
    }

    @Bean(name = TransactionManagers.TENANTTRANSACTIONMANAGER)
    public JpaTransactionManager transactionManager(EntityManagerFactory tenantEntityManager){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(tenantEntityManager);
        return transactionManager;
    }
}
