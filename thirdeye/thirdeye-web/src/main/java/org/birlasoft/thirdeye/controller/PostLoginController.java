package org.birlasoft.thirdeye.controller;

import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *Controller for {@code login}.
 *
 */
@Controller
public class PostLoginController {
	
	/**
	 * Maps the login URL with the login.jsp page 
	 * @return
	 */
    	
	@RequestMapping( value = "/tenantRedirect", method = RequestMethod.GET )
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public String tenantRedirect(Model model) {
		
		return "forward:/home";
	}	
	
	
}
