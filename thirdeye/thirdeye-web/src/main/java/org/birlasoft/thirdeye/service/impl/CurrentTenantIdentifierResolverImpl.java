/**
 * 
 */
package org.birlasoft.thirdeye.service.impl;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.master.entity.Tenant;
import org.birlasoft.thirdeye.master.repository.TenantRepository;
import org.birlasoft.thirdeye.security.CustomDetailsFilter;
import org.birlasoft.thirdeye.security.UserAuthenticationDetailsBean;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * The tenant identifier resolver is called by <put link>.
 * 
 * This will happen only in those classes which are marked with the {@link TransactionManagers#TENANTTRANSACTIONMANAGER}.
 * 
 * The purpose of this class is to identify the tenant database which the repository classes will hit.
 * 
 * This is done in two ways:
 * <ol>
 * 	<li>Extract it from the authentication details that are stored in the {@link SecurityContextHolder#getContext()} as a part of the user session.</li>
 * 	<li> From the request attribute - this is only done in the case of the login process where the request attribute is set in
*   {@link CustomDetailsFilter} which is only during the login process and is before spring security fires up</li>
 * </ol>
 * 
 * 
 * @author shaishav.dixit
 *
 */
@Component
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {
	@Autowired
	private TenantRepository tenantRepository;
	/**
	 * If this method returns {@link DataSourceBasedMultiTenantConnectionProviderImpl#DEFAULT_TENANT_ID} you will get
	 * a sure shot Null pointer. 
	 */
	@Override
	public String resolveCurrentTenantIdentifier() {
		
		// First try and extract it from the session details
		if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null){
			// Okay so spring security has a nasty habit of letting through Anonymous users. So we make sure
			// that the authentication token is what we are looking for
			if (SecurityContextHolder.getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken){
				UserAuthenticationDetailsBean userDetailsInSession = (UserAuthenticationDetailsBean)SecurityContextHolder.getContext().getAuthentication().getDetails();
				if (userDetailsInSession.getTenantURL() != null){
					return userDetailsInSession.getTenantURL();
				}
			}
		} else if (RequestContextHolder.getRequestAttributes() != null){
			// This is required at the time of login ONLY because rest of the time we can use the session.
			// TODO can we somehow check if we ar ein the login sequence ?
			RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
			
			  if (requestAttributes != null) {
			   String identifier = (String) requestAttributes.getAttribute("CURRENT_TENANT_IDENTIFIER",RequestAttributes.SCOPE_REQUEST);
			   if (identifier != null) {
			    return identifier;
			   }
			}
		}
		
		return DataSourceBasedMultiTenantConnectionProviderImpl.DEFAULT_TENANT_ID;
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}
	public Tenant getTenantForUser() {
	    //	Tenant tenant = null;
	    	if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null){
				// Okay so spring security has a nasty habit of letting through Anonymous users. So we make sure
				// that the authentication token is what we are looking for
				if (SecurityContextHolder.getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken){
					UserAuthenticationDetailsBean userDetailsInSession = (UserAuthenticationDetailsBean)SecurityContextHolder.getContext().getAuthentication().getDetails();
					if (userDetailsInSession.getTenantURL() != null){
						Tenant tenant = tenantRepository.findByTenantAccountId(userDetailsInSession.getAccountId());
						return tenant;
					}
				}
			}
	    	return null;
	    }

}
