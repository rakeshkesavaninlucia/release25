package org.birlasoft.thirdeye.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

/**
 * {@code Formatter} , which allows you to define custom parse and print capabilities for just about any type
 */
public class QuestionFormatter implements Formatter<Question> {
	
	@Autowired
	QuestionService questionService;

	@Override
	public String print(Question question, Locale locale) {
		return question.getId().toString();
	}

	@Override
	public Question parse(String id, Locale locale) throws ParseException {
		return questionService.findOne(Integer.parseInt(id));
	}

}
