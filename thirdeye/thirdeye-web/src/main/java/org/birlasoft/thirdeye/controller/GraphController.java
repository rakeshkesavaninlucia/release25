package org.birlasoft.thirdeye.controller;

import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.service.GraphService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for view {@code graph}.
 */
@Controller
@RequestMapping(value="/graph")
public class GraphController {
	
	private GraphService graphService;
	private final WorkspaceSecurityService workspaceSecurityService;
	private static final String GRAPH_ID = "graphId";
	
	/**
	 * Constructor to Initialize services
	 * @param graphService
	 * @param workspaceSecurityService
	 */
	@Autowired
	public GraphController(GraphService graphService,
						   WorkspaceSecurityService workspaceSecurityService) {
		this.graphService = graphService;
		this.workspaceSecurityService = workspaceSecurityService;
	}
	
	/**
	 * View {@code graph} by Id.
	 * @param model
	 * @param idOfGraph
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String viewGraphByID(Model model, @PathVariable(value = "id") Integer idOfGraph) {
		// TODO Authorization necessary. The user should be linked to the workspace in which the graph is there.
		Graph graph = graphService.findOne(idOfGraph,true);
		// Check if the user can access this graph.
		workspaceSecurityService.authorizeGraphAccess(graph);
		model.addAttribute(GRAPH_ID, idOfGraph);
		return "add/viewGraph";
	}
}
