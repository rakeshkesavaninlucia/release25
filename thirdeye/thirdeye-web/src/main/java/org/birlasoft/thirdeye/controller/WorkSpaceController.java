package org.birlasoft.thirdeye.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.validators.WorkspaceValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for create {@code workspace} , {@code List} of all
 * {@code workspaces}, save {@code workspace}, {@code List} of all {@code Users}
 * belongs to same {@code workspace} and delete also.
 */
@Controller
@RequestMapping(value = "/workspace")
public class WorkSpaceController {

	private WorkspaceService workSpaceService;
	private WorkspaceValidator createWorkspaceValidator;
	private UserWorkspaceService userWorkspaceService;
	private CustomUserDetailsService customUserDetailsService;
	private UserService userService;
	private static final String CREATE_WORKSPACE = "workspace/createWorkSpace";
	
	/**
	 * Parameterized Constructor
	 * 
	 * @param workSpaceService
	 * @param userService
	 * @param createWorkspaceValidator
	 * @param userWorkspaceService
	 * @param customUserDetailsService
	 * @param userService
	 */
	@Autowired
	public WorkSpaceController(WorkspaceService workSpaceService, WorkspaceValidator createWorkspaceValidator,
			UserWorkspaceService userWorkspaceService, CustomUserDetailsService customUserDetailsService,
			UserService userService) {
		this.workSpaceService = workSpaceService;
		this.createWorkspaceValidator = createWorkspaceValidator;
		this.userWorkspaceService = userWorkspaceService;
		this.customUserDetailsService = customUserDetailsService;
		this.userService = userService;
	}

	/**
	 * This Method Create a {@code new} {@code workspace}
	 * 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public String createWorkSpace(Model model) {
		model.addAttribute("workspace", new Workspace());
		return CREATE_WORKSPACE;
	}

	/**
	 * {@code save} and {@code validate} {@code workspace}, if found
	 * {@code error} then show {@code error message}
	 * 
	 * @param workSpace
	 * @param model
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public String editWorkspace(Model model,@RequestParam(value="wsid",required=false) Integer workspaceId) {
		if(workspaceId!=null){
			Workspace oneWorkspace = workSpaceService.findOne(workspaceId);
			model.addAttribute("workspace", oneWorkspace);
		}
		return CREATE_WORKSPACE;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public String saveWorkSpace(@ModelAttribute("workspace") Workspace workSpace, BindingResult result, Model model) {
		createWorkspaceValidator.validate(workSpace, result);
		if (result.hasErrors()) {
			return CREATE_WORKSPACE;
		}
		Workspace workspacesave = workSpaceService
				.save(workSpaceService.createWorkspaceObject(workSpace, customUserDetailsService.getCurrentUser()));
		model.addAttribute("workspacesave", workspacesave);
		return "redirect:/workspace/view/" + workspacesave.getId();
	}

	/**
	 * @param workSpace
	 * @param result
	 * @param model
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateworkspacemodal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public Object updateworkspace(@ModelAttribute("workspace") Workspace workSpace, BindingResult result, Model model,HttpServletResponse response) {
		createWorkspaceValidator.validate(workSpace, result);
		if (result.hasErrors()) {
			return CREATE_WORKSPACE;
		}
		Workspace workspaceSave = workSpaceService
				.save(workSpaceService.updateWorkspaceObject(workSpace, customUserDetailsService.getCurrentUser()));
		model.addAttribute("workspacesave", workspaceSave);
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect", "workspace/list");
		return JSONView.render(jsonToReturn, response);
	}

	/**
	 * List of all {@code workspace}
	 * 
	 * @param model
	 *            to add {@code workspace} Object and pass to jsp for
	 *            manipulation
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_VIEW'})")
	public String listAllWorkspaces(Model model) {
		model.addAttribute("allworkspaces", workSpaceService.findAll());
		return "workspace/listAllWorkspaces";
	}

	/**
	 * View {@code workspace} with give {@code Id} and {@code manipulate}
	 * {@code User} for this {@code workspace}
	 * 
	 * @param model
	 * @param idOfworkspace
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_VIEW'})")
	public String viewWorkspace(Model model, @PathVariable(value = "id") Integer idOfworkspace) {
		Workspace oneWorkspace = workSpaceService.findByIdLoadedUserWorspaces(idOfworkspace);
		List<User> userList = workSpaceService.getUserListByWorkspace(oneWorkspace);
		model.addAttribute("workspaceId", idOfworkspace);
		model.addAttribute("userListInWorkspace", userList);
		model.addAttribute("viewWorkspace", oneWorkspace);
		return "workspace/viewWorkspaceWithId";
	}

	/**
	 * Method to Delete a user from workspace
	 * 
	 * @param idOfuser
	 * @param idOfWorkspace
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'WORKSPACES_MODIFY'})")
	public String deleteUserFromWorkspace(@RequestParam(value = "userId") Integer idOfuser,
			@RequestParam(value = "workspaceId") Integer idOfWorkspace) {
		Workspace activeWorkspace = customUserDetailsService.getActiveWorkSpaceForUser();
		User currentUser = customUserDetailsService.getCurrentUser();
		User userToDelete = userService.findUserById(idOfuser);
		if(activeWorkspace.getId().equals(idOfWorkspace) && currentUser.getId().equals(idOfuser)) {
			return "redirect:/logout";
		}
		if(userWorkspaceService.findByUser(userToDelete).size() == 1) {
			return "redirect:/logout";
		}
		Workspace workspace = workSpaceService.findOneLoaded(idOfWorkspace,true);
		userWorkspaceService.deleteUserFromWorkspace(workspace, idOfuser);
		return "redirect:/workspace/view/" + workspace.getId();
	}
	
	/**
	 * @param model
	 * @param idOfworkspace
	 * @return {@code String}
	 */
	@RequestMapping(value = "/viewdeleteworkspace/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'DELETE_WORKSPACE'})")
	public String viewdeleteworkspace(Model model,@PathVariable(value = "id") Integer idOfworkspace) {
		Integer error =1;
		Workspace oneWorkspace = workSpaceService.findByIdLoadedUserWorspaces(idOfworkspace);
		List<User> userList = workSpaceService.getUserListByWorkspace(oneWorkspace);
		if(userList.isEmpty()){
			   workSpaceService.deleteWorkspace(idOfworkspace);	
			   return "redirect:/workspace/list";
		}
		else{
		Map<User, List<String>> mapOfUserAndWorkspace = getMapOfUserAndAssignedWorkspace(idOfworkspace, userList);
		    List<Workspace> workspaceList = new ArrayList<>();
			User currentUser = customUserDetailsService.getCurrentUser();
			currentUser.getUserWorkspaces().forEach(adminWorkspace-> workspaceList.add(adminWorkspace.getWorkspace()));
			error = isAssignedWorkspaceMorethanOne(error, mapOfUserAndWorkspace);
		    model.addAttribute("adminListWorkspace", workspaceList);
            model.addAttribute("mapOfUserAndWorkspace", mapOfUserAndWorkspace);
		    model.addAttribute("viewWorkspace", oneWorkspace);
		    model.addAttribute("error",error);
		return "workspace/deleteWorkspace";
		}
	}

	/**
	 * @param error
	 * @param mapOfUserAndWorkspace
	 * @return error
	 */
	private Integer isAssignedWorkspaceMorethanOne(Integer error, Map<User, List<String>> mapOfUserAndWorkspace) {
		for ( Map.Entry<User, List<String>> entry : mapOfUserAndWorkspace.entrySet()) {
			if(entry.getValue().size()>1 || entry.getValue().size() ==1){
				error = 1;
			}else{
				error = 0;
				break;
			}
			
		}
		return error;
	}

	/**
	 * @param idOfworkspace
	 * @param userList
	 * @return
	 */
	private Map<User, List<String>> getMapOfUserAndAssignedWorkspace(Integer idOfworkspace, List<User> userList) {
		List<String>listOfWorkSpaceName  = new ArrayList<>();
        Map<User,List<String>> mapOfUserAndWorkspace = new HashMap<>();
		  for(User oneUser : userList){
		   listOfWorkSpaceName =  getUserWorkSpace(oneUser,idOfworkspace);
			mapOfUserAndWorkspace.put(oneUser, listOfWorkSpaceName);
		    }
		return mapOfUserAndWorkspace;
	}

	private List<String> getUserWorkSpace(User oneUser,Integer idOfworkspace) {
		List<UserWorkspace> listOfUserWorkSpace  = userWorkspaceService.findByUser(oneUser);
		listOfUserWorkSpace.removeIf(workspace -> workspace.getWorkspace().getId()==idOfworkspace);
		List<String>listOfWorkSpaceName = new ArrayList<>();
		for(UserWorkspace oneUserWorkspace :listOfUserWorkSpace){
			listOfWorkSpaceName.add(oneUserWorkspace.getWorkspace().getWorkspaceName());
		}
		return listOfWorkSpaceName;
	}

	
	/**
	 * @param model
	 * @param userId
	 * @param adminWorkspaceId
	 * @param workspaceId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'DELETE_WORKSPACE'})")
	public String addWorkspace(Model model,@RequestParam(value="userids[]",required=false) Integer[] userId,@RequestParam(value="adminWsid",required=false) Integer adminWorkspaceId,@RequestParam(value="workspaceId",required=false) Integer workspaceId) {
		List<User> user = userService.findUserByIds(userId);
		Workspace workspace = workSpaceService.findOne(adminWorkspaceId);
		userWorkspaceService.save(userWorkspaceService.updateUserWorkspaceObject(user, workspace));
		return "redirect:/workspace/viewdeleteworkspace/" + workspaceId;
	}

	/**
	 * @param model
	 * @param idOfworkspace
	 * @return {@code String}
	 */
	@RequestMapping(value = "/deleteWorkspace", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'DELETE_WORKSPACE'})")
	public String deleteworkspace (Model model,@RequestParam(value = "workspaceId") Integer idOfWorkspace) {
	      workSpaceService.deleteWorkspace(idOfWorkspace);	
	    if(customUserDetailsService.getActiveWorkSpaceForUser().getId()==idOfWorkspace){
	    	 return "redirect:/logout";
	    } else{
	    	 return "redirect:/workspace/list";
	    }
	  
     	 
}
}