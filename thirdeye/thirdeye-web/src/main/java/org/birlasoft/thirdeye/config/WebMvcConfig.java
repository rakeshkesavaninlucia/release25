package org.birlasoft.thirdeye.config;

import org.birlasoft.thirdeye.filters.DefaultWorkspaceSessionInterceptor;
import org.birlasoft.thirdeye.formatter.AssetFormatter;
import org.birlasoft.thirdeye.formatter.CategoryFormatter;
import org.birlasoft.thirdeye.formatter.GraphFormatter;
import org.birlasoft.thirdeye.formatter.WorkspaceFormatter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.format.FormatterRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * This boots the MVC part of the application by loading the master tenant 
 * and other resourcehandlers
 * 
 * @author shaishav.dixit
 *
 */
@EnableWebMvc
@Configuration
@EnableScheduling	
@Import({ThymeLeafConfig.class, MasterPersistenceConfig.class, PropertyLoader.class})
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	@Override
	public void configureDefaultServletHandling( DefaultServletHandlerConfigurer configurer ){
		configurer.enable();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/META-INF/resources/webjars/").setCachePeriod(31556926);
		registry.addResourceHandler("/css/**").addResourceLocations("static/css/").setCachePeriod(31556926);
		registry.addResourceHandler("/img/**").addResourceLocations("static/img/").setCachePeriod(31556926);
		registry.addResourceHandler("/js/**").addResourceLocations("static/js/").setCachePeriod(31556926);
	}

	@Override 
	public void addViewControllers(ViewControllerRegistry registry) { 
		super.addViewControllers(registry); 
		registry.addViewController("/login").setViewName("login");
		registry.addViewController("/forgot").setViewName("forgot");
		registry.addViewController("/questionAnswer").setViewName("questionAnswer");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	} 
	
	@Override
    public void addFormatters(FormatterRegistry formatterRegistry) {
        formatterRegistry.addFormatter(new WorkspaceFormatter());
        formatterRegistry.addFormatter(new AssetFormatter());
        formatterRegistry.addFormatter(new GraphFormatter());
        formatterRegistry.addFormatter(new CategoryFormatter());
//        formatterRegistry.addFormatter(new QuestionFormatter());
    }

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(getDefaultWorkspaceInterceptor());
	}
	
	@Bean
	public HandlerInterceptorAdapter getDefaultWorkspaceInterceptor(){
		return new DefaultWorkspaceSessionInterceptor();
	}
	
}
