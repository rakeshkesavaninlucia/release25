package org.birlasoft.thirdeye.controller.gng;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.gng.GNGWrapper;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.GNGService;
import org.birlasoft.thirdeye.service.PHPService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for GNG Report
 * @author mehak.guglani
 *
 */

@Controller
@RequestMapping(value = "/gng")
public class GNGController {

	@Autowired
	private ParameterService parameterService;
	@Autowired
	private QuestionnaireService questionnaireService;	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private GNGService gngService;
	@Autowired
	private PHPService phpService;

	@RequestMapping(value = "/view")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'GNG_VIEW'})")
	public String viewGNG(Model model) {
		final Workspace activeWS = customUserDetailsService.getActiveWorkSpaceForUser();

		Set<Workspace> activeWorkspace = new HashSet<>(Arrays.asList( activeWS ));
		List<Questionnaire> questionnairesInWorkspaces = questionnaireService.findByWorkspaceInAndQuestionnaireType(activeWorkspace,QuestionnaireType.DEF.toString());

		model.addAttribute("listOfQuestionnaire", questionnairesInWorkspaces);
		return "gng/viewGNG";			
	}

	/**
	 * Get {@code list} of root {@link parameter} for Questionnaire .
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value="/fetchRootParameters/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'GNG_VIEW'})")
	public String getRootParameter(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);		
		// Get root parameters of Questionnaire
		List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(questionnaire);		

		model.addAttribute("rootParamList", listOfRootParameters);			
		return "gng/viewGNG :: optionListOfParameters(listOfParameters=${rootParamList}) ";
	}

	/**
	 * Get {@code set} of {@link AssetTemplate} by idOfQuestionnaire
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */

	@RequestMapping(value="/fetchAssetTemplates/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'GNG_VIEW'})")
	public String getAssetTemplates(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		// First find the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);

		Set<AssetTemplate> assetTemplates = phpService.getAssetTemplatesFromQuestionnaire(questionnaire);	

		model.addAttribute("assetTemplates", assetTemplates);

		return "gng/viewGNG :: optionListOfAssetTemplates(listOfAssetTemplates=${assetTemplates})";
	}
	
	@RequestMapping(value="/plot", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'GNG_VIEW'})")
	//@ResponseBody
	public String plotReport(Model model, @RequestParam(value = "parameterIds") Set<Integer> idsOfParameters, 
            @RequestParam(value = "assetTemplateIds") Set<Integer> idsOfAssetTemplate, @RequestParam(value = "questionnaireId") Integer idOfQuestionnaire) {
		
		//to do- Add filter Map from Request
		Map<String,List<String>> filterMap = new HashMap<>();
		
		GNGWrapper gngWrapper= gngService.getGNGWrappertoPlotReport(idsOfParameters, idsOfAssetTemplate, idOfQuestionnaire, filterMap);
		
		model.addAttribute("gngWrapper", gngWrapper);
		return "gng/gngFragment :: gngReportPlot(assetWrapper=${gngWrapper})";
	}
	
}
