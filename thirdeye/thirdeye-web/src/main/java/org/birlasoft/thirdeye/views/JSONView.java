package org.birlasoft.thirdeye.views;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.servlet.ModelAndView;

public class JSONView {

	private static Logger logger = LoggerFactory.getLogger(JSONView.class);
	
	public static ModelAndView render(Object model, HttpServletResponse response){
		MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
		
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		try {
			jsonConverter.write(model, jsonMimeType, new ServletServerHttpResponse(response));
		} catch (HttpMessageNotWritableException e) {
			logger.error(" Unable to writable message " + e);
		} catch (IOException e) {
			logger.error(" Input Output Exception " + e);
		}

		return null;
	}
}
