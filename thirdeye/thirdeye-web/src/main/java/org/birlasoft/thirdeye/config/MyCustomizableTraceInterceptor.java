package org.birlasoft.thirdeye.config;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.interceptor.CustomizableTraceInterceptor;

public class MyCustomizableTraceInterceptor extends CustomizableTraceInterceptor {
	
	private static final long serialVersionUID = -6254701812205139648L;

	protected void writeToLog(Log logger, String message, Throwable ex) {
	    if (ex != null) {
	     logger.debug(message, ex);
	    } else {
	     logger.debug(message);
	    }
	 }
	
	@Override
	protected String replacePlaceholders(String message,
			MethodInvocation methodInvocation, Object returnValue,
			Throwable throwable, long invocationTime) {
		String placeHolders = null;;
		try{			
			placeHolders = super.replacePlaceholders(message, methodInvocation, returnValue,
					throwable, invocationTime);
		} catch (Exception ex){
			placeHolders = ex.getMessage();
		}
		return placeHolders;
	}
	
	protected Log getLoggerForInvocation(MethodInvocation invocation) {
		return LogFactory.getLog(MyCustomizableTraceInterceptor.class);
	}
	
	protected boolean isInterceptorEnabled(MethodInvocation invocation, Log logger) {
		return true;
	}
}
