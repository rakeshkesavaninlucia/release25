package org.birlasoft.thirdeye.controller.benchmark;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.BenchmarkService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.BenchmarkValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sunil1.gupta 
 */
@Controller
@RequestMapping(value="/benchmark")
public class BenchmarkController {
	
	
	private final WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private BenchmarkService benchmarkService;
	@Autowired
	private BenchmarkValidator benchmarkValidator;
	@Autowired
	private CustomUserDetailsService customUserDetailsService; 

		
	/**
	 * Constructor to Initialize services
	 * @param workspaceSecurityService
	 */
	@Autowired
	public BenchmarkController(	WorkspaceSecurityService workspaceSecurityService) {		
		this.workspaceSecurityService = workspaceSecurityService;
	}

	/**
	 * method to create benchmark.
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String createBenchmark(Model model) {	
		model.addAttribute("activeWorkspace", customUserDetailsService.getActiveWorkSpaceForUser());
		model.addAttribute("benchmarkForm", new Benchmark());
		return "benchmark/createBenchmark";
	}
	
	/** Method to edit  benchmark
	 * @param id
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String editBenchmark(@PathVariable(value = "id") Integer idOfBenchMark, Model model){
		Benchmark benchmark = benchmarkService.findfullyLoadedBenchmark(idOfBenchMark);
		model.addAttribute("activeWorkspace", customUserDetailsService.getActiveWorkSpaceForUser());
		model.addAttribute("benchmarkForm", benchmark);
		return "benchmark/createBenchmark";
	}
	
	/**
	 * Method to get list of benchmark
	 * @param model
	 * @return {@code String}
	 */
	
	@RequestMapping(value = "/listBenchmark", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_VIEW'})")
	public String listOfBenchmark(Model model) {
		// Fetch the user's active workspaces
		 Set<Workspace> listOfWorkspaces = new HashSet<> ();
		 listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		 List<Benchmark> listOfBenchmark = benchmarkService.findByWorkspaceIsNullOrWorkspaceIn(listOfWorkspaces);
	     model.addAttribute("listOfBenchmark", listOfBenchmark);
	    return "benchmark/listBenchmarks";
	}


	/**
	 * method to save benchmark
	 * @param benchmark
	 * @param result
	 * @param model
	 * @return {@code}
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")	
	public String saveUpdateBenchmark(@ModelAttribute("benchmarkForm") Benchmark benchmark , BindingResult result,Model model) {
	    benchmarkValidator.validate(benchmark, result);	   
		if(!result.hasErrors())
			benchmark  = benchmarkService.saveUpdateBenchmark(benchmark);	
		model.addAttribute("activeWorkspace", customUserDetailsService.getActiveWorkSpaceForUser());
		model.addAttribute("benchmarkForm", benchmark);
		return "benchmark/benchmarkFragment :: createBenchmark";
	}
	
	/**
	 * Delete benchmark 
	 * @param idOfBenchmark	
	 */
	@RequestMapping(value = "/benchmark/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteBenchmark(@RequestParam(value="id") Integer idOfBenchmark) {
		benchmarkService.deleteBenchmark(idOfBenchmark);
	}
	
	
}
