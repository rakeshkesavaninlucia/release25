package org.birlasoft.thirdeye.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.birlasoft.thirdeye.entity.Graph;
import org.springframework.format.Formatter;

/**
 * {@code Formatter} , which allows you to define custom parse and print capabilities for just about any type
 */
public class GraphFormatter implements Formatter<Graph> {

	@Override
	public String print(Graph graph, Locale locale) {
		return graph.getId().toString();
	}

	@Override
	public Graph parse(String id, Locale locale) throws ParseException {
		Graph graph=new Graph();
		graph.setId(Integer.parseInt(id));
		return graph;
	}
}
