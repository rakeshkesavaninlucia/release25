package org.birlasoft.thirdeye.controller.tco;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountGridWrapper;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.service.IncrementIndexService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *Controller for {@code pre}{@code publish} {@code chart of account} and {@code publish} {@code chart of account}.
 */
@Controller
@RequestMapping("/tco")
public class ChartOfAccountPublishController {

	private QuestionnaireService questionnaireService;
	private QuestionnaireQuestionService questionnaireQuestionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	private ResponseService responseService;
	@Autowired
	private TcoResponseService tcoResponseService;
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private IncrementIndexService incrementIndexService ;
	
	@Autowired
	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
	
	/**
	 * Constructor to initialize services 
	 * @param questionnaireService
	 * @param questionnaireQuestionService
	 * @param workspaceSecurityService
	 * @param parameterFunctionService
	 */
	@Autowired
	public ChartOfAccountPublishController(
			QuestionnaireService questionnaireService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService,ResponseService responseService) {
		this.questionnaireService = questionnaireService;
		this.responseService = responseService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.workspaceSecurityService = workspaceSecurityService;

	}

	/**
	 * Create a {@code pre publish} {@code chart of account}
	 * @param chartOfAccountId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/prepublish", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String prePublishChartOfAccount(@PathVariable(value = "id") Integer chartOfAccountId, Model model,HttpSession httpSession) {

		Questionnaire coa = questionnaireService.findOneFullyLoaded(chartOfAccountId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		questionnaireQuestionService.refreshQuestionnaireQuestions(coa);

		if(coa.getQuestionnaireParameters().isEmpty()){
			List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(coa,true) ;
			// Get root parameters of QE
			List<ParameterBean> listOfRootParameters = parameterService.getParametersOfQuestionnaire(parameterList);
			model.addAttribute("error", 1);
			model.addAttribute("questionnaireId", chartOfAccountId);
			model.addAttribute("rootParameterList", listOfRootParameters);
			model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Cost Structure Selection for Chart of Account");
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
			return "questionnaire/questionnaireParameters";
		}	


		// Is there already any response for this qe. Currently only one response per qe.
		// we can add the ability to add more later.
		List<Response> listOfResponseAvailable = responseService.findByQuestionnaire(coa,false);
		Response newResponse = new Response();
		if (listOfResponseAvailable == null || listOfResponseAvailable.isEmpty()){
			newResponse = null;
		} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() == 1){
			newResponse = listOfResponseAvailable.get(0);
		} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() > 1 ){
			// this is an error scenario in the given application
			return null;
		}

		List<QuestionnaireQuestion> questionnaireQuestions = questionnaireQuestionService.findByQuestionnaire(coa, true);
		//get sorted list of asset name
		List<String> sortedList= tcoResponseService.getAssetName(questionnaireQuestions);
		//get the wrapper
		ChartOfAccountGridWrapper chartOfAccountGridWraper = tcoResponseService.getCostStructureName(coa,newResponse,questionnaireQuestions);

		model.addAttribute("questionnaireId", chartOfAccountId);
		model.addAttribute("questionnaire", coa);
		model.addAttribute("chartOfAccountGridWraper",chartOfAccountGridWraper);
		model.addAttribute("status", "false");
		model.addAttribute("AssetName", sortedList);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
		model .addAttribute("hide", "savebutton");
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Publish Chart of Account");

		return "questionnaire/questionnairePrePublish";
	}

	/**
	 * Create a {@code publish} {@code chartOfAccount}
	 * @param chartOfAccountId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/publish", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String publishChartOfAccount(@PathVariable(value = "id") Integer chartOfAccountId) {
		
		Questionnaire coa = questionnaireService.findOne(chartOfAccountId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		
		coa.setStatus(QuestionnaireStatusType.PUBLISHED.name());
		
		questionnaireService.save(coa);
		
		//code to reflect changes in indexfor edit in COA
		
				Iterator<QuestionnaireAsset> itrquassets = coa.getQuestionnaireAssets().iterator() ;
				List<Integer> assetIdLst = new ArrayList<Integer>() ;
				while(itrquassets.hasNext() ){
					assetIdLst.add(itrquassets.next().getAsset().getId()) ;
				}
				String tenantId = currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
				incrementIndexService.updateAssetCostStucture(assetIdLst, tenantId);
				
		// index logic ends...
		
		return "redirect:/tco/list";
	}
}
