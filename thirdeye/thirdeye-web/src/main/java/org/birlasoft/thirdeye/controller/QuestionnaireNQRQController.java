package org.birlasoft.thirdeye.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *Controller for NQRQ (non-quantifiable response question). 
 */
@Controller
@RequestMapping(value="/questionnaire")
public class QuestionnaireNQRQController {
	
	private QuestionnaireService questionnaireService;
	private ResponseService responseService;
	private final WorkspaceSecurityService workspaceSecurityService;
	
	/**
	 * Constructor to initialize services
	 * @param questionnaireService
	 * @param responseService
	 * @param questionnaireQuestionService
	 * @param workspaceSecurityService
	 * @param responseDataService
	 */
	@Autowired
	public QuestionnaireNQRQController(QuestionnaireService questionnaireService,
			ResponseService responseService,
			WorkspaceSecurityService workspaceSecurityService) {
		this.questionnaireService = questionnaireService;
		this.responseService = responseService;
		this.workspaceSecurityService = workspaceSecurityService;
	}

	/**
	 * method to show List of questionnaire NQRQ response list.
	 * @param model
	 * @param questionnaireId
	 * @param httpSession
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/nqrq/response/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_VIEW'})")
	public String questionnaireNQRQResponseList(Model model, @PathVariable(value = "id") Integer questionnaireId, HttpSession httpSession) {
		
		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		List<Response> listOfResponseAvailable = responseService.findByQuestionnaire(qe, false);
		
		if (isOnlyOneResponseAllowed()){
			if (listOfResponseAvailable != null && listOfResponseAvailable.size() == 1){
				// put the response id into the session because only one response is allowed
				// per questionnaire.
				httpSession.setAttribute("activeResponseId", listOfResponseAvailable.get(0).getId());
				
			} else if (listOfResponseAvailable != null && listOfResponseAvailable.size() > 1 ){
				// this is an error scenario in the given application
				return null;
			}
		} else {
			// We can put in a new response id into the database
			// and then put it in the session
		}
		List<QuestionnaireQuestionBean> qqbeanListForGUI = responseService.prepareQQsListForNQRQ(qe);
		if(qqbeanListForGUI.isEmpty()) {
			model.addAttribute("emptyQQBean", "No DATA");
		}
		model.addAttribute("listOfQuestions", qqbeanListForGUI);
		model.addAttribute("questionnaireId", questionnaireId);
		model.addAttribute("questionnaire", qe);
		return "questionnaire/listNQRQResponse";
	}
	
	/** 
	 * Check only one response allowed.
	 * @param qe
	 * @return {@code boolean}
	 */
	private boolean isOnlyOneResponseAllowed () {
		return true;
	}

	/**
	 * Save questionnaire question response
	 * @param request
	 * @param session
	 * @param questionnaireId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/nqrq/response/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_RESPOND'})")
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public String saveOneQQResponse(HttpServletRequest request, HttpSession session, @PathVariable(value = "id") Integer questionnaireId) {	
	
		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		Integer responseId = (Integer) session.getAttribute("activeResponseId");		
		return questionnaireService.saveQQResponseData(request,responseId);	
		
	}
}

