/**
 * @author: dhruv.sood
 */
package org.birlasoft.thirdeye.controller;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for handling all the Auto-complete requests. */

@Controller
@RequestMapping(value = "/autocomplete")
public class AutocompleteController {

	private WorkspaceService workSpaceService;
	private UserService userService;

	/**
	 * Parameterized constructor
	 * @param workSpaceService
	 * @param userService
	 */
	@Autowired
	public AutocompleteController(WorkspaceService workSpaceService,
			UserService userService) {
		this.workSpaceService = workSpaceService;
		this.userService = userService;
	}

	/**
	 * View User {@code workspace} with give {@code Id} and {@code manipulate} {@code User}
	 * for this {@code workspace}
	 * @param search
	 * @param idOfworkspace
	 * @return {@code Map}
	 */
	@RequestMapping(value = "/usersnotinworkspace/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'USER_VIEW'})")
	@ResponseBody
	public Map<Integer, String> viewUsersNotInWorkspace(@PathVariable(value = "id") Integer idOfworkspace, @RequestParam(value="q") String search) {
		Workspace oneWorkspace = workSpaceService.findByIdLoadedUserWorspaces(idOfworkspace);
		List<User> userList = workSpaceService.getUserListByWorkspace(oneWorkspace);
		List<User> userNotIn= workSpaceService.getUserListNotInWorkspace(userList, userService.findByDeleteStatusAndAccountExpiredAndAccountLocked(Constants.DELETE_STATUS_FALSE, Constants.ACCOUNT_EXPIRED_FALSE, Constants.ACCOUNT_LOCKED_FALSE));
		List<User> usersContainingSearch= userService.findByFirstNameContainingOrLastNameContaining(search,search);
		
		return workSpaceService.getUserMapMatchingActiveUsers(usersContainingSearch, userNotIn);
	}

}

