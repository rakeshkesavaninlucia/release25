package org.birlasoft.thirdeye.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.BenchmarkQuestionBean;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONBenchmarkQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONNumberQuestionMapper;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionMode;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.Category;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.BenchmarkService;
import org.birlasoft.thirdeye.service.CategoryService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.validators.QuestionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for create {@code question} , save {@code question} , list of all {@code question} ,
 * edit {@code question} , view {@code question} .
 *
 */
@Controller
@RequestMapping(value="/question")
public class QuestionController {
	
	private QuestionService questionService;  
	private CustomUserDetailsService customUserDetailsService; 
	private QuestionValidator questionValidator;
	private CategoryService categoryService;
	private final WorkspaceSecurityService workspaceSecurityService;
	private static final String QUESTION_TEMPLATE_URL = "question/questionTemplate";	
	@Autowired
	private BenchmarkService benchmarkService;
	
	/**
	 * constructor to initialize  the services
	 * @param questionService
	 * @param customUserDetailsService
	 * @param questionValidator
	 * @param categoryService
	 * @param workspaceSecurityService
	 */
	@Autowired
	public QuestionController(QuestionService questionService,
			CustomUserDetailsService customUserDetailsService,
			QuestionValidator questionValidator,
			CategoryService categoryService,
			WorkspaceSecurityService workspaceSecurityService) {
		this.questionService = questionService;
		this.customUserDetailsService = customUserDetailsService;
		this.questionValidator = questionValidator;
		this.categoryService = categoryService;
		this.workspaceSecurityService = workspaceSecurityService;		
	}

	/**
	 * To {@code create} the {@code question} form .
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY'})")
	public String createQuestion(Model model) {
		setAttributesInModel(model);
		model.addAttribute("questionForm", new Question());
		model.addAttribute("benchmarkQuestionBean", new BenchmarkQuestionBean());
		return QUESTION_TEMPLATE_URL;
	}

	/**
	 * To save the {@code question}	
	 * @param question
	 * @param result
	 * @param quantifiers
	 * @param model
	 * @param otherOption
	 * @param limitTo
	 * @param min
	 * @param max
	 * @param queTextHidden
	 * @param parentQuesId
	 * @param categoryName
	 * @param benchmarkCheckboxMCQ
	 * @param benchmarkId
	 * @param benchmarkItems
	 * @return {@code String}
	 */
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY'})")
	public String saveQuestion(@ModelAttribute("questionForm") Question question,  BindingResult result,
			@RequestParam(value = "quantifiable", required = false) List <Double> quantifiers, Model model,
			@RequestParam(value = "otherOption", required = false) boolean otherOption,
			@RequestParam(value = "limitTo", required = false) boolean limitTo,
			@RequestParam(value = "min", required = false) Integer min,
			@RequestParam(value = "max", required = false) Integer max,
			@RequestParam(value = "queTypeTextHidden", required = false) String queTextHidden,
			@RequestParam(value = "parentQuestionId", required = false) Integer parentQuesId,
			@RequestParam(value = "questionCategory", required = false) String categoryName,
			@RequestParam(value = "benchmarkMCQ", required = false) boolean benchmarkCheckboxMCQ,
			@RequestParam(value = "benchmarkId", required = false) Integer benchmarkId,
			@RequestParam(value = "benchmarkItem", required = false) Set<Integer> benchmarkItems) {
		
		if(parentQuesId != null){
			Question parentQuestion = questionService.findOne(parentQuesId);
			question.setQuestion(parentQuestion);
		}		
		//Create new category ,if category does'nt exist.		 
		Category category = questionService.createCategoryIfNotExist(categoryName);		
		question.setCategory(category);
		
		JSONNumberQuestionMapper numberQuestionMapper = null;
		if(question.getQuestionType().equals(QuestionType.NUMBER.toString())){
			numberQuestionMapper = new JSONNumberQuestionMapper();
			numberQuestionMapper.setLimitTo(limitTo);
			if(limitTo){
				questionValidator.validateMinMax(min, max, result);
				numberQuestionMapper.setMin(min);
				numberQuestionMapper.setMax(max);
			}
		}
		if (queTextHidden != null && !queTextHidden.isEmpty()){
				question.setQueTypeText(queTextHidden);
				questionValidator.validateQueTypeText(queTextHidden, result);
		}
		questionValidator.validate(question, result);
		if(quantifiers !=null)
		   questionValidator.validateQuantifiers(quantifiers, result);		
		
		BenchmarkQuestionBean bqBean = new BenchmarkQuestionBean();
		if(question.getQuestionType().equals(QuestionType.MULTCHOICE.toString())){	  
			if(!benchmarkCheckboxMCQ){
		    	String qTHidden = queTextHidden +",N/A";
		    	question.setQueTypeText(qTHidden);
		    	if(quantifiers !=null)
		    	 quantifiers.add(new Double(-1));
			}else {
				bqBean.setBenchmarkCheckboxMCQ(benchmarkCheckboxMCQ);
				bqBean.setBenchmarkId(benchmarkId);
				if(benchmarkItems != null)
					bqBean.setBenchmarkItem(benchmarkItems);
			}
		}
		
		boolean hasErrors = false;
		if(question.getQuestionType().equals(QuestionType.MULTCHOICE.toString()) && benchmarkCheckboxMCQ){
			hasErrors = questionValidator.validateBenchmark(bqBean);
		}
		
	    if(result.hasErrors() || hasErrors){
	    	Question quest = questionService.prepareQuestionWithError(question, quantifiers, otherOption, numberQuestionMapper, bqBean);
	    	prepareQuestionModel(quest, model, bqBean);
	    	setAttributesInModel(model);
	    	List<Benchmark> listOfBenchmarks;
			if(question.getWorkspace() == null){
				listOfBenchmarks = benchmarkService.findByWorkspaceIsNull();
			} else {
				Set<Workspace> setOfWorkspace = new HashSet<>();
				setOfWorkspace.add(question.getWorkspace());
				listOfBenchmarks = benchmarkService.findByWorkspaceIn(setOfWorkspace);
			}
			model.addAttribute("listOfBenchmark", listOfBenchmarks);
	    	return QUESTION_TEMPLATE_URL;	
	    }	    
	    if(question.getId() == null){
	    	questionService.save(questionService.createQuestionObject(question, quantifiers, customUserDetailsService.getCurrentUser(), otherOption, numberQuestionMapper, bqBean));
	    }else{	    	
	    	questionService.save(questionService.updateQuestionObject(question, quantifiers, customUserDetailsService.getCurrentUser(), otherOption, numberQuestionMapper, bqBean));
	    }
		return "redirect:/question/list";
	}

	/**
	 * List of {@code Questions} that the user has access to (based on the workspaces they are linked to).
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_VIEW'})")
	public String listOfQuestion(Model model) {
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		//Get the TCO category
		Category tcoCategory = categoryService.getCategoryFromName(Constants.__SYS_TCO);		
		
		List<Question> listOfQuestion = questionService.findByWorkspaceIsNullOrWorkspaceInAndCategoryNot(listOfWorkspaces, tcoCategory);
		
	    model.addAttribute("listOfQuestion", listOfQuestion);
	    return "question/listQuestion";
	}
	
	/**
	 * Edit question by Id.
	 * @param idOfQuetion
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY'})")
	public String editQuestion(@PathVariable(value = "id") Integer idOfQuetion, Model model){
		Question question = questionService.findFullyLoaded(idOfQuetion);
		if(question != null && question.getQuestionMode().equalsIgnoreCase(QuestionMode.FIXED.toString())){
			throw new AccessDeniedException("User is not allowed to edit this question");
		}
		// Check if the user can access this question.
		workspaceSecurityService.authorizeQuestionAccess(question);		
		BenchmarkQuestionBean benchmarkQuestionBean = new BenchmarkQuestionBean();
		if(question != null && question.getBenchmark() != null)
			benchmarkQuestionBean.setBenchmarkId(question.getBenchmark().getId());
		prepareQuestionModel(question, model, benchmarkQuestionBean);
		setAttributesInModel(model);
		return QUESTION_TEMPLATE_URL;
	}

	/**
	 * method to prepare model attributes for  Question
	 * @param question
	 * @param model
	 * @param benchmarkQuestionBean
	 */
	private void prepareQuestionModel(Question question, Model model, BenchmarkQuestionBean benchmarkQuestionBean) {
		String queType = question.getQuestionType();
		if(queType.equals(QuestionType.NUMBER.toString())){
			JSONNumberQuestionMapper jsonNumberQuestionMapper = new JSONNumberQuestionMapper();
			if(question.getQueTypeText() !=null)
				jsonNumberQuestionMapper = Utility.convertJSONStringToObject(question.getQueTypeText(), JSONNumberQuestionMapper.class);
			model.addAttribute("jsonNumberQuestionMapper", jsonNumberQuestionMapper);
		}else if(queType.equals(QuestionType.MULTCHOICE.toString())){
			if(benchmarkQuestionBean.getBenchmarkId() != null){
				Set<Integer> setOfCheckedBenchmarkItemIds = new HashSet<>();
				List<BenchmarkItem> listOfBenchmarkItems = benchmarkService.findByBenchmark(benchmarkService.findOne(benchmarkQuestionBean.getBenchmarkId()));
				if(question.getQueTypeText() != null){
					JSONBenchmarkMultiChoiceQuestionMapper jsonBenchmarkMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(question.getQueTypeText(), JSONBenchmarkMultiChoiceQuestionMapper.class);
					for (BenchmarkItem benchmarkItem : listOfBenchmarkItems) {
						for (JSONBenchmarkQuestionOptionMapper benchmarkQuestionOptionMapper : jsonBenchmarkMultiChoiceQuestionMapper.getOptions()) {
							if(benchmarkQuestionOptionMapper.getBenchmarkItemId().equals(benchmarkItem.getId())){
								setOfCheckedBenchmarkItemIds.add(benchmarkItem.getId());
								break;
							}
						}
					}
				}
				model.addAttribute("listOfBenchmarkItem", listOfBenchmarkItems);
				model.addAttribute("setOfCheckedBenchmarkItemIds", setOfCheckedBenchmarkItemIds);
			} else{
				JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(question.getQueTypeText(), JSONMultiChoiceQuestionMapper.class);
				model.addAttribute("otherOption", jsonMultiChoiceQuestionMapper.isOtherOption());
			}
			model.addAttribute("benchmarkQuestionBean", benchmarkQuestionBean);
		}	
		if(question.getBenchmark() == null && benchmarkQuestionBean.getBenchmarkId() == null)
			model.addAttribute("decodedArrayOfOption", questionService.getQuestionOptions(question));
		model.addAttribute("questionForm", question);
		
	}

	/**
	 * view {@code question} with a given {@code Question} {@code id}.
	 * @param model
	 * @param idOfQuestion
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_VIEW'})")
	public String viewQuestion(Model model, @PathVariable(value = "id") Integer idOfQuestion){
		Question question = questionService.findOne(idOfQuestion);
		// Check if the user can access this question.
		workspaceSecurityService.authorizeQuestionAccess(question);
		model.addAttribute("oneQQ", questionService.createQQBeanForViewQuestion(question, "[Name of your Asset]", 0));
		return "question/viewQuestion";
	}
	
	/**
	 * Set {@code attributes} in model.
	 * @param model
	 */
	private void setAttributesInModel(Model model) {
		
		List<Category> categoryList = categoryService.findAll();		
		categoryList.removeIf(category -> category.getName().equals(Constants.__SYS_TCO));	
		model.addAttribute("activeWorkspace", customUserDetailsService.getActiveWorkSpaceForUser());
		model.addAttribute("listOfCategory", categoryList);
	}

	
	/**
	 * method to create varient question (same question with different option) 
	 * @param idOfQuetion
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}/createVariant", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY'})")
	public String createVariantQuestion(@PathVariable(value = "id") Integer idOfQuetion, Model model){
		Question question = questionService.findFullyLoaded(idOfQuetion);
		// Check if the user can access5 this question.
		workspaceSecurityService.authorizeQuestionAccess(question);
		
		Question variantQuestion = questionService.createVariantQuestion(question);
		BenchmarkQuestionBean benchmarkQuestionBean = new BenchmarkQuestionBean();
		if(question.getBenchmark() != null)
			benchmarkQuestionBean.setBenchmarkId(question.getBenchmark().getId());
		prepareQuestionModel(variantQuestion, model, benchmarkQuestionBean);
		
		setAttributesInModel(model);
		return QUESTION_TEMPLATE_URL;
	}
}
