package org.birlasoft.thirdeye.controller.aid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.validators.AIDValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for createAID , saveAID and listOfAIDs .
 *
 */
@Controller
@RequestMapping(value="/aid")
public class AIDController {
	
	private AssetTemplateService assetTemplateService;
	private CustomUserDetailsService customUserDetailsService;
	private AIDService aidService; 
	private AIDValidator aidValidator;

	/**
	 * Constructor to initialize services
	 * @param assetTemplateService
	 * @param customUserDetailsService
	 * @param aidService
	 * @param aidValidator
	 */
	@Autowired
	public AIDController(AssetTemplateService assetTemplateService,
			CustomUserDetailsService customUserDetailsService,
			AIDService aidService,
			AIDValidator aidValidator) {
		this.assetTemplateService = assetTemplateService;
		this.customUserDetailsService = customUserDetailsService;
		this.aidService = aidService;
		this.aidValidator = aidValidator;
	}

	/**
	 * Create new {@code AID} within a workspace.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String createAID(Model model) {
		// List those templates which the user has access to in the active workspace
		List<Workspace> listOfWorkspaces = new ArrayList<> ();
		List<AssetTemplate> listOfAssetTemplates = new ArrayList<>();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());

		for (AssetTemplate assetTemplate : assetTemplateService.findByWorkspaceIn(listOfWorkspaces)) {
			if (!assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name()))
				listOfAssetTemplates.add(assetTemplate);
		}
		model.addAttribute("listOfAssetTemplate", listOfAssetTemplates);
		model.addAttribute("aid", new Aid());
		return "aid/createAID";
	}
	
/**
 * Save {@code Aid}.
 * @param aid
 * @param result
 * @param model
 * @param assetTemplateId
 * @return
 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_MODIFY'})")
	public String saveAID(Aid aid, BindingResult result, Model model, @RequestParam(value="assetTemplateId") Integer assetTemplateId) {
		
		AssetTemplate assetTemplate = assetTemplateService.findOne(assetTemplateId);
		aid.setAssetTemplate(assetTemplate);
		aidValidator.validate(aid, result);
		if(result.hasErrors()){
			// List those templates which the user has access to in the active workspace
			List<Workspace> listOfWorkspaces = new ArrayList<> ();
			listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
			model.addAttribute("listOfAssetTemplate", assetTemplateService.findByWorkspaceIn(listOfWorkspaces));
			return "aid/createAID";
		}
		if(aid.getId() == null){
			aidService.save(aidService.createNewAidObject(aid, customUserDetailsService.getCurrentUser(), customUserDetailsService.getActiveWorkSpaceForUser(), assetTemplate));
		}
		return "redirect:/aid/list";
	}
	
	/**
	 * List of {@code Aids} that the user has access to {@code (based on the workspaces they are linked to)}.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_VIEW'})")
	public String listOfAIDs(Model model) {
		
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Aid> listOfAids = new ArrayList<>();
		
		if(!listOfWorkspaces.isEmpty()){
			listOfAids = aidService.findByWorkspaceIn(listOfWorkspaces);
		}
		
		model.addAttribute("listOfAids", listOfAids);
		return "aid/listAID";
	}
}
