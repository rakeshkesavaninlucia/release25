/**
 * @author: ananta.sethi
 * module for applying select on select asset type in Questionnaire and chart of account
 * year selection in chart of account
 */
Box.Application.addModule('module-createQuestionnaireselect', function(context) {

	// private methods here
	var $;

	function calendarYear(){
		var myDate = new Date().getFullYear() +10;
		$( "#datepicker" ).datepicker(
				{
					minViewMode: 2,
					format: 'yyyy',
					startDate: '2000',
					//yearRange: '2000:+0',
					endDate: myDate+'',
					//yearRange: '',
					autoclose: true
				});
	}

	return {

		behaviors : [ 'behavior-select2'],

		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			calendarYear();
		}
	}
});
