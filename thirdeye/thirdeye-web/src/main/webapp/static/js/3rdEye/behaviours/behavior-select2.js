/**
 * This behavior can be used to activate the select2 functionality to
 * the drop boxes / select boxes available on the page. It can be done by adding a class
 * select2 to your <select>. 
 * 
 * Further configurations allow you to activate specific features / customization.
 * 
 * Add CSS Class 'allowOptionAddition' to provide the users the ability to add more options
 * which are not already on the list.
 * 
 * Add CSS class 'autocomplete' and provide data-url attribute for providing an autocomplete 
 * functionality with a server side hit.
 * 
 * 
 * 
 * @author: dhruv.sood  
 */
Box.Application.addBehavior('behavior-select2', function(context) {
	'use strict';

	var $;
	var commonUtils;	
	var $moduleDiv;
	
	
	function setupBasic($selectElement){
		$selectElement.select2({ width: '100%' });
	}
	
	function setupForAutoComplete($selectElement){
		var autocompleteUrl = commonUtils.getAppBaseURL($selectElement.data("url"));
		$selectElement.select2({

					ajax : {
						url : autocompleteUrl,
						dataType : 'json',
						delay : 250,
						data : function(params) {
							return {
								q : params.term, // search term
							};
						},
						processResults : function(data) { // Response
							return {
								results : $.map(data, function(
										value, key) { // Rendering response as options
									return {
										id : key,
										text : value
									};
								})
							};
						},
						cache : false
					},
					minimumInputLength : 1,width: '100%',

				});
	
	}
	
	function setupForModifiable($selectElement){
		var properties={
				tags: true,
				maximumSelectionSize: 1,width: '100%',
		};
		
		$selectElement.select2(properties);
	}
	
	/**
	 * Bootstrap the select2 functionality
	 */
	function setupSelectsForModule(){
		$('select.select2', $moduleDiv).each(function(){
			if ($(this).hasClass("allowOptionAddition")){
				//Apply select2 with dynamic adding of options
				setupForModifiable($(this));
			} else if ($(this).hasClass("autocomplete")){
				//Apply select2 with options from AJAX response
				setupForAutoComplete($(this));
			} else {
				//Apply only select2
				setupBasic($(this));
			}
		})
	}

	return {
		init : function() {
			$ = context.getGlobal('jQuery');
			$moduleDiv = $(context.getElement());
			commonUtils = context.getService('commonUtils');
			
			setupSelectsForModule();
		}
	}

});
