/**
 * This module is for Parameter Value Boost
 * @author dhruv.sood
 */

Box.Application.addModule('module-pvb', function(context) {

	// private methods here
	var $, commonUtils, moduleDiv;	
	
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner(moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	
	function getParamTable(data){
			var url ="parameter/boost/"; 
			url = commonUtils.getAppBaseURL(url+"fetchParameters");
	        var postRequest = $.post(url, data);
	        $('#pvbParameters', moduleDiv).remove()
	        postRequest.done(function(incomingData){  
	        	showLoading(false);	        	
	        	$(moduleDiv).append(incomingData).fadeIn(1300); 
	        	Box.Application.startAll(moduleDiv);
			 })
	}

	
	return {
		messages: ['assetSelected'],
		behaviors : [ 'behavior-select2' ], 
		init: function (){
			
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			moduleDiv = $(context.getElement());
			commonUtils = context.getService('commonUtils');			

		},  	
		onmessage: function(name, data) {
			
			if(name === 'assetSelected'){
        		showLoading(true);
        		//Get the parameter table
        		getParamTable(data);        		
    	}		
        },
	};
});