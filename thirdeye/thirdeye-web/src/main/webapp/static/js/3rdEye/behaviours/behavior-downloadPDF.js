/**

 * @author: tanushree.mittal

 *  Behavior for Download report as PDF

 *  This behavior allows us to download the reports  as PDF 

 */

Box.Application.addBehavior('behavior-downloadPDF', function(context) {
	'use strict';

	var $;
	var $html2canvas,commonUtils;
	var _moduleConfig;
	var $imagename;
	var notifyService;


	function downloadPDFButton()
	{
		$('.box-header').append('<div class="downloadPDF"><i title ="Download PDF" class="fa fa-file-pdf-o clickable"/></div>');
		//on click of download button
		$('.downloadPDF').click( function() {
			$html2canvas($('.box-body'), {
				onrendered: function(canvas) {
					var dataObj={};
					if(_moduleConfig.pngimagename === "null" || _moduleConfig.pngimagename == undefined)
						$imagename = 'Download_PDF';
					else
						$imagename =_moduleConfig.pngimagename.replace(/'/g,"");
					dataObj.fileName = $imagename; 
					var canvasData = canvas.toDataURL("image/png");
					var pdf = new jsPDF("l", "mm", [canvas.width, canvas.height]);
					pdf.addImage(canvasData, 'png', 0, 0,canvas.width,canvas.height);
					  
					pdf.save($imagename+".pdf");
				 },
				background :'#FFFFFF',
			},false);
		})
	}

	return {
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			notifyService = context.getService('service-notify');
			$html2canvas = context.getGlobal('html2canvas');
			_moduleConfig = context.getConfig();
			downloadPDFButton();
		}
	}
})

