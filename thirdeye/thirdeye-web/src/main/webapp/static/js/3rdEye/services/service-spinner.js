/**This service is responsible for returning the object with tailor made sping.js properties
 * 
 * @author: dhruv.sood
 */
Box.Application.addService('service-spinner', function() {
	
	'use strict';
		
	//This function sets the options for the spinner
	function spinnerProperties(){		
		var options = {
				 	lines: 9 // The number of lines to draw
					, length: 12 // The length of each line
					, width: 9 // The line thickness
					, radius: 18 // The radius of the inner circle
					, scale: 0.25 // Scales overall size of the spinner
					, corners: 1 // Corner roundness (0..1)
					, color: '#000000' // #rgb or #rrggbb or array of colors
					, opacity: 0.25 // Opacity of the lines
					, rotate: 0 // The rotation offset
					, direction: 1 // 1: clockwise, -1: counterclockwise
					, speed: 1.5 // Rounds per second
					, trail: 44 // Afterglow percentage
					, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
					, zIndex: 2e9 // The z-index (defaults to 2000000000)
					, className: 'spinner' // The CSS class to assign to the spinner
					, top: '50%' // Top position relative to parent
					, left: '49%' // Left position relative to parent
					, shadow: false // Whether to render a shadow
					, hwaccel: false // Whether to use hardware acceleration
					, position: 'absolute' // Element positioning
				};
		
		return options;
	}
	
	return {
		getSpinner : function($moduleDiv,show,spinner){	
			
			//Placing div on the page for displaying spinner
			$moduleDiv.append('<div class="spinner"></div>');
			
			var spinner_div = $('div.spinner',$moduleDiv).get(0);
			var spinProperties = spinnerProperties();
			
			//Checking whether to show or hide the spinner
			if(show){
				spinner = new Spinner(spinProperties).spin(spinner_div);
				return spinner;
			}else{
				spinner.stop(spinner_div);
			}			
		}
	};
	
});