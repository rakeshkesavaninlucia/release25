/**
 * @author: sunil1.gupta
 */
Box.Application.addModule('module-deleteBenchmark', function(context) {
	'use strict';
	var $,commonUtils;


	return {
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');

			var clickHandlerConfiguration = {};
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("benchmark/delete");
			$("#benchmarkView").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		}
	}
});
