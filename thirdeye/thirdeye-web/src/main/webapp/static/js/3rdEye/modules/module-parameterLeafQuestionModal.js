/**
 * @author samar.gupta
 */

Box.Application.addModule('module-parameterLeafQuestionModal', function(context) {

    // private methods here
	var $, commonUtils,moduleEl;
	var dataObj = {};
	var pageUrl = "parameter/lp/showModal";
	
	function showQuestionSelectorModal(data){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(pageUrl),data)
		.then(function(){
			Box.Application.startAll(moduleEl);
			// Show the modal
			var $modalDiv = $(moduleEl).modal({
				keyboard: true
			})			
		});	
	}
	function hideQuestionSelectorModal(){
		$(moduleEl).modal('hide');
	}
	function saveQuestionsTemporary(){		
			var table = $('#questionListInModal').DataTable();
			var dataQuestion = table.$('.questionClass:checked').map(function() { return this.value; }).get();
			var dataMandatoryQuestion = table.$('.mandatoryQuestionClass:checked').map(function() { return this.value; }).get();
			dataObj.question = dataQuestion;
			dataObj.mandatoryQuestion = dataMandatoryQuestion;
			var postRequest = $.post( commonUtils.getAppBaseURL("parameter/lp/saveTemporary"), dataObj);
			postRequest.done(function(htmlResponse){
				$('#selectedQuestionTableId').remove();
				$( htmlResponse ).insertAfter( "#questionParamFooter" );
				$('.errorDiv').remove();
				hideQuestionSelectorModal();
			})
	}
   
    return {

    	messages: [ 'plqm::showQuestionSelector' ],

        onmessage: function(name, data) {
            if (name === 'plqm::showQuestionSelector') {
            		showQuestionSelectorModal(data);
            }
        },
        
        onclick: function(event, element, elementType) {        
        	if (elementType === 'saveQuestionsTemporary') {
        		saveQuestionsTemporary();
        		event.preventDefault();
        	}
        },
    
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            moduleEl = context.getElement();
        },
        
        destroy: function() {
        	 dataObj = {};
        	 moduleEl= null;
       }
    };
});