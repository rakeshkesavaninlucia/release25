/**
*This module handle all the parameter tiles that are displayed
*on the home page.
*This method also handle's the parameter click on the home page.
*
*@author: dhruv.sood  
*/

Box.Application.addModule('module-homeParameter', function(context) {
	
	 'use strict';

	// private methods here
	var $, $moduleDiv, url, moduleConfig, commonUtils;
	var dataObj = {};
	
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner($moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	
	//Setting the parameters for the request
	function getParameters(){
		dataObj.assetClassName = moduleConfig.assetclass.replace(/'/g, "");
		dataObj.parameterName = moduleConfig.parametername.replace(/'/g, "");
		dataObj.parameterId = moduleConfig.parameterid;	
     }
	
	//This function retrives the parameter values from the controller and populates it
	function getParamValues(){
        url = commonUtils.getAppBaseURL("homeParameterTiles");
        var postRequest = $.post(url, dataObj);
        postRequest.done(function(incomingData){  
        	showLoading(false);
        	$moduleDiv.hide().append(incomingData).fadeIn(1300);	       	
	       	$('[data-toggle="popover"]', $moduleDiv).popover();
		 })
        
     }
	
	//This function handles the request of the click on any of the parameter on the home page.
	function parameterClickThrough(){
		window.location.href = commonUtils.getAppBaseURL("assetParameterChart/"+dataObj.parameterId);		
	}
	
	
	return {
		
		 init: function (){
			 
			// Retrieving the references
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv= $(context.getElement());
			moduleConfig = context.getConfig();  
			
			//Get the parameters for the json data
			getParameters();			
			
			showLoading(true);
			//Now gettig the Param Values
			getParamValues();
		 },
		 		
		 onclick: function(event, element, elementType) {	
			 if(elementType === 'parameterTile'){
			 parameterClickThrough();
			 }
	     }
		 
	};
});
	
