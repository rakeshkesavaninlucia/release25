/**
 * @author samar.gupta
 */
Box.Application.addModule('module-costStructure', function(context) {
 
       var $;
       var commonUtils;
       var pageUrl = "coststructure/create";

       return {
    	   
	        init: function (){
	        	var _this = this;
	              // retrieve a reference to jQuery
	            $ = context.getGlobal('jQuery');
	            commonUtils = context.getService('commonUtils');
	            _this._bindCostStructureCreation();
	        },
	        
	        _bindCostStructureCreation: function(){
				var _this= this;
				$(".createCostStructure,.editCostStructure").click(_this._createCostStructure);
			},
			
			/**
			 * Code to handle the creation / update of an existing Cost Structure
			 * when the anchor is clicked.
			 */
			_createCostStructure: function(){
				
				var params ={};
				
				if ($(this).is("a.editCostStructure") && $(this).data("coststructureid")){
					params["costStructureidId"] = $(this).data("coststructureid");
				}
				
				commonUtils.getHTMLContent(commonUtils.getAppBaseURL(pageUrl),params).then(function(content){
					
					var $newModal = commonUtils.appendModalToPage("createCostStructureModal", content, true);
					
					_bindCostStructureModalRequirements($newModal);
					
					$newModal.modal({backdrop:false,show:true});
				});
				
				// Once you get the modal you append it and show it.
				
				// The form submit is simple ajax form submit. If form errors you come back else you go to the new URL 
				function _bindCostStructureModalRequirements ($newModal){
					// Bind the ajax form 
					$('#createCostStructureForm', $newModal).ajaxForm(function(htmlResponse) {
						
						if (htmlResponse.redirect){
							window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
						} else{ 
							var $replacement = commonUtils.swapModalContent("createCostStructureModal", htmlResponse);
							$replacement.modal({backdrop:false});
							_bindCostStructureModalRequirements($replacement);
						}
					}); 
				}
			}
	    }
});
