/**
 * 
 */

Box.Application.addModule('module-viewBCMLevelAsset', function(context) {

	// private methods here
	var $,commonUtils,jsonService,moduleEl,qualityGateId;	
	
	function loadBCMlevelAssetHealth(data){		
		var postRequest = $.post( commonUtils.getAppBaseURL("bcm/"+data.bcmLevelId+"/bcmAssetWithHealth"), data);
		postRequest.done(function(htmlResponse){			
			 $(".bcmlevelplace", moduleEl).empty();			
			 $(".bcmlevelplace", moduleEl).append(htmlResponse);
			 //showLoading(false);
			 //Box.Application.startAll();
		})
	 }
	
	function showLoading(toShow){
		if (toShow){
			$(".bcmlevelcontainer .overlay").fadeIn("slow");
		} else {
			$(".bcmlevelcontainer .overlay").fadeOut("slow");
		}
	}
	function loadAsset(element){
		
		$(element).attr('id');
		var data = $(element).attr('value');
		location.href = commonUtils.getAppBaseURL("templates/asset/view/"+data);
		
	}
	
	
	return {
		behaviors : [ 'behavior-select2' ], 
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			jsonService = context.getService('jsonService');
			commonUtils = context.getService('commonUtils');	
			 moduleEl = context.getElement();
			this.loadBcmLevelAsset();	
			
		},  
        
		loadBcmLevelAsset : function(){	
			
			//$( ".bcmlevelplace" ).each(function() {
				//$( this ).empty();
				var bcmLevelId = $(".bcmlevelplace",moduleEl).data( "bcmlevelid" );		
				var fcParameterId = $(".bcmlevelplace",moduleEl).data( "fcparameterid" );				
				var qeId = $(".bcmlevelplace",moduleEl).data( "qeid" );
				var dataObj={};
				dataObj.bcmLevelId = bcmLevelId;
				dataObj.fcParameterId = fcParameterId;
				dataObj.qeId = qeId;
				dataObj.qualityGateId = qualityGateId;
				loadBCMlevelAssetHealth(dataObj)	;
		//	});	
		},
		
		onchange: function(event, element, elementType) {
			if (elementType === 'qualityGates') { 
				 qualityGateId = $(element).val();
				 //showLoading(true);
				 this.loadBcmLevelAsset(); 
			}
		},
		
		onclick: function(event, element, elementType){
			if(elementType === 'assetName'){
				loadAsset(element);
			
				
			
				
				
				
			}
		}
	};
});