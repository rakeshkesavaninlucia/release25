/**
 * @author: ananta.sethi
 *  module for view asset template
 *  (functions-edit,delete,save)
 */
Box.Application.addModule('module-benchmarkItemScore',function(context) {
	'use strict';
	var $,commonUtils,moduleEl,notifyService;	

	function initBanchMarkItemScore(){
	
		$("#addRowButton").bind('click',function(){
			
			$(".saveRow").trigger('click');
			if ($(".dirtyrow").length < 1) 
			{
				var benchmarkItemid =  moduleEl.querySelector('[name="id"]').value;
				commonUtils.addRowToTableFromURL("benchmarkItemScoreColumns",commonUtils.getAppBaseURL("/benchmarkItemScore/columns/newRow/"+benchmarkItemid));
			
			}
		});

		// Bind the click on the table
		// Base configuration for the click handler.
		var clickHandlerConfiguration = {};
		clickHandlerConfiguration.saveClass = "saveRow";
		clickHandlerConfiguration.saveURL = commonUtils.getAppBaseURL("benchmarkItemScore/columns/saveRow");
		clickHandlerConfiguration.editClass = "editRow";
		clickHandlerConfiguration.editURL = commonUtils.getAppBaseURL("benchmarkItemScore/columns/editRow/");
		clickHandlerConfiguration.deleteClass = "deleteRow";
		clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("benchmarkItemScore/columns/deleteRow");

		$("#benchmarkItemScoreColumns").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		
	} //eof
	
	
	return {
		behaviors : ['behavior-datepicker'],
		init : function(){		
			$ = context.getGlobal('jQuery');		
			commonUtils = context.getService('commonUtils');		
			moduleEl = context.getElement();
			initBanchMarkItemScore();
		}
		
	};
});