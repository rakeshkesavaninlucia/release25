/**
 * This module is to view the saving reports.
 * @author: sunil1.gupta
 */
Box.Application.addModule('module-viewSavedReport', function(context) {
	 'use strict';
	 
	 var $, _moduleConfig,commonUtils,waveAxis,noOfWaves,notifyService;
	 var spinner = null;
	
	 function viewReport(reportType,reportConfig){
		 switch (reportType) {
		    case "Health Analysis":
		        fetchWrapperData(reportConfig,"repo::HealthAnalysis",commonUtils.getAppBaseURL("graph/scatter/plot"));
		        break;
		    case "Wave Analysis":
		    	fetchWrapperData(getWaveConfigParam(reportConfig),"repo::WaveAnalysis",commonUtils.getAppBaseURL("graph/scatter/plot"));
		        break;
		    case "TCO Analysis":
		    	fetchWrapperData(reportConfig,"repo::TCOAnalysis",commonUtils.getAppBaseURL("graph/tco/plot"));
		        break;
		    case "Portfolio Health":
		    	fetchWrapperData(reportConfig,"repo::PortfolioHealth",commonUtils.getAppBaseURL("portfoliohealth/plot"));
		        break;
		}
	 }
	 
	 function getWaveConfigParam(config){
		 var waveConfigParam =  JSON.parse(config) ;
		 var dataObj = {};
		 dataObj.questionnaireIds = waveConfigParam.questionnaireIds;
		 dataObj.parameterIds = waveConfigParam.parameterIds;
		 waveAxis = waveConfigParam.waveAxis;
		 noOfWaves = waveConfigParam.noOfWaves;
		 return JSON.stringify(dataObj);
	 }
	 
	 function fetchWrapperData(reportConfig,reportType,pageUrl){
		 var search =JSON.parse(reportConfig) ;
		 var url = commonUtils.getAppBaseURL("filter/facets?");
		 if(search.filterMap != undefined){
		   document.cookie = "filterURL=" + url+search.filterMap + "; path=/";
		    pageUrl = pageUrl + "?"+search.filterMap;
		    delete search.filterMap;
	     }
		 $.ajax({
			    type : "POST",
			    dataType : "json",
	            url : pageUrl,
	            data : search,       
	            beforeSend : loadFromCookie,
	            success : function(response) {
	            	brodcastMessageForReport(reportType, response);
	            },
	            error : function() {
	                console.log("error getting response.....");
	            }
	        });
		 
	 }
	 
	 function brodcastMessageForReport(reportType, response){
		 if(reportType === 'repo::WaveAnalysis'){
			 var waveResponse = {}
			 waveResponse.response = response;
			 waveResponse.waveAxis = waveAxis;
			 waveResponse.noOfWaves = noOfWaves
			 context.broadcast(reportType, waveResponse);
		 }else{
			 context.broadcast(reportType, response);
		 }
	 }

	 function saveSavedReportasLandingPage(data){
   		 commonUtils = context.getService('commonUtils');
		 notifyService = context.getService('service-notify');
		 var savedreportUrl=commonUtils.getAppBaseURL("report/updateReportStatus");
		 var postRequest = $.post(savedreportUrl,data);
		 postRequest.done();
		 notifyService.setNotification("Saved", "", "success");
	}//eof

	function showLoading(container, show){
	   var spinService = context.getService('service-spinner');
	   spinner = spinService.getSpinner(container,show,spinner);   	
	}
	 
	function loadFromCookie(){
		var filterURL = commonUtils.readCookie("filterURL");
	
		if(filterURL!=null){
				var urlParams = filterURL.split("?")[1];
				//Fetch the facet data
				context.broadcast('fetchFacetData', filterURL);
				var paramArray = [];
				paramArray = urlParams.split('&');
				var paramObj ={};
				for(var i=0; i<paramArray.length-1;i++){
					paramObj.checked = true;
					paramObj.paramValue = paramArray[i];
					
					//Updating the tags
					context.broadcast('updateTag', paramObj);
             		//Check the filter of Asset Class
					if(paramArray[i].split("=")[0]==='assetClass'){
						checkAssetType(paramArray[i]);
					}
				}
			}
		} 
    return {
    	messages: ['repo:ViewSuccess'],
		init: function(){
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			 _moduleConfig = context.getConfig(); 
			 viewReport(_moduleConfig.reportType.replace(/[']/g, ""),_moduleConfig.reportConfig.replace(/[']/g, ""));
		},
		onmessage: function(name, data) {
			 if(name === 'repo:ViewSuccess') {
				 showLoading($('.scatterPlot'), false);
			 }
		 },
	    destroy: function() {
	    	waveAxis = null;
	    	noOfWaves = null;
		},
		onmessage: function(name, data){
			
			if (name === 'buttonOnSavedReportlandingpage') {
			    saveSavedReportasLandingPage(data);
			    
			}
			
		}  
    
    };
 });