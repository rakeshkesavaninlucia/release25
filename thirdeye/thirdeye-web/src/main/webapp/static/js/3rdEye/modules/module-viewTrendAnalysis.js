/**
 * @author: sunil1.gupta
 */
Box.Application.addModule('module-viewTrendAnalysis', function(context) {
	'use strict';
	// private methods here
	var $,d3, commonUtils, $moduleDiv, title, message,	moduleConfig,filterMapData;
	var spinner = null;

	//Method to handle Spinner image loading
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	 }

	function drawTrendGraph(trendWrapper) {
		$('#chart').empty();
		var tooltip = d3.select("#chart")
		.append("div")
		.style("position", "absolute")
		.style("z-index", "10")
		.style("display", "none")
		.text("a simple tooltip");
		// Set the dimensions of the canvas / graph
		var	margin = {top: 30, right: 20, bottom: 30, left: 50},
			width = 600 - margin.left - margin.right,
			height = 370 - margin.top - margin.bottom;
		// Parse the date / time
		//var	parseDate = d3.time.format("%d-%b-%y").parse;
		var parseDate = d3.time.format("%m/%d/%Y").parse;
		// Set the ranges
		var	x = d3.scale.linear().range([0, width]);
		var	y = d3.scale.linear().range([height, 0]);
		// Define the axes
		var	xAxis = d3.svg.axis().scale(x)
			.orient("bottom").ticks(10);
		var	yAxis = d3.svg.axis().scale(y)
			.orient("left").ticks(10);
		// Define the line
		var	valueline = d3.svg.line()
			.x(function(d) { return x(d.close); })
			.y(function(d) { return y(d.more); });
		    
		// Adds the svg canvas
		var	svg = d3.select("#chart")
		    .append("div")
			.append("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
			.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		// Get the data
		drawGraph(trendWrapper.trendData);
		function drawGraph(data) {
			data.forEach(function(d) {
				d.date = parseDate(d.date);
				//d.date =+ d.date;
				d.close =+ d.close;
				d.more =+ d.more;
			});
			// Scale the range of the data
			x.domain(d3.extent(data, function(d) { return d.close; }));
			y.domain([0, d3.max(data, function(d) { return d.more; })]);
			// Add the valueline path.
			svg.append("path")		
				.attr("class", "line")
				.attr("d", valueline(data));
		// draw the scatterplot
			svg.selectAll("dot")									// provides a suitable grouping for the svg elements that will be added
				.data(data)											// associates the range of data to the group of elements
			.enter().append("circle")								// adds a circle for each data point
				.attr("r", 5)										// with a radius of 3.5 pixels
				.attr("cx", function(d) { return x(d.close); })		// at an appropriate x coordinate 
				.attr("cy", function(d) { return y(d.more); }) 	// and an appropriate y coordinate
		    .on("mousemove", function(d){
		        tooltip.text('Date:  ' + d.date +'  '+d.xParameterName+'  :  '+ d.close+'  '+d.yParameterName+'  :  '+d.more);
		        return tooltip.style("display", "inline-block");
		        })
		    .on("mouseout", function(){return tooltip.style("display", "none");});
			// Add the X Axis
			svg.append("g")			// Add the X Axis
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);
			// Add the Y Axis
			svg.append("g")			// Add the Y Axis
				.attr("class", "y axis")
				.call(yAxis);
		}
	}
	 
	function showTrendGraph(trendWrapper) {
		if(trendWrapper.trendData != null && trendWrapper.trendData.length > 0){
			drawTrendGraph(trendWrapper);
		}else{
			$('#chart').empty();
	  	  $("#chart").append("<p id='nodataavailable' style='font-size:16px;text-align: center;'><b>No Data Available.</b></p>");
        }
	}

	function initPageBindings() {
		var ajaxFormOptions = {
			beforeSubmit : showRequest,
			success : function(trendWrapper) {
				showTrendGraph(trendWrapper);
				showLoading($('.trendAnalysis'), false);
			}
		}
		$("#trendAnalysisControl").ajaxForm(ajaxFormOptions);
	}
	
	function showRequest(formData, jqForm, options) {
		showLoading($('.trendAnalysis'), true);
	}
	function removeCookie(){
		commonUtils.removeCookies();
	}
	
	return {
		behaviors : [ 'behavior-fetchParameterGraph', 'behavior-select2','behavior-datepicker'],
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv = context.getElement();
			d3 = context.getGlobal('d3');
			initPageBindings();
			removeCookie();
		},
		onclick: function(event, element, elementType) {
			if (elementType === "download"){
				saveSvgAsPng(document.getElementById("graphDown"), "trendGraph-" + title + ".png")
			}
		}
	}
});