/**
 * @author sunil1.gupta
 */
Box.Application.addModule('module-listReports',function(context) {
	'use strict';
	
	var $,commonUtils,moduleEl,notifyService,reportid;
	var modalUrl = "report/shareReportModal";

	function initListReports(){
		// Bind the click on the table
		// Base configuration for the click handler.
		var clickHandlerConfiguration = {};
		clickHandlerConfiguration.saveClass = "saveRow";
		clickHandlerConfiguration.saveURL = commonUtils.getAppBaseURL("report/columns/saveRow");
		clickHandlerConfiguration.editClass = "editRow";
		clickHandlerConfiguration.editURL = commonUtils.getAppBaseURL("report/columns/editRow/");
		clickHandlerConfiguration.deleteClass = "deleteRow";
		clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("report/columns/deleteRow");

		$("#reportView").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		
	} //eof
	
     function saveSavedReportasLandingPage(element){
		var dataObj = {};
		if($(element).data("checkedstatus") === -1){
			dataObj.reporturl= "/home/homeLanding";
		}else{
			dataObj.reporturl= $(element).data("reporturl");
		}
		dataObj.functionname  = $(element).data("idenfiervalue");
		dataObj.id = $(element).data("checkedstatus");
		var savedreportUrl=commonUtils.getAppBaseURL("report/updateReportStatus");
		if(confirm("Do you want to set this page as your default Landing page?") === true){
		var postRequest = $.post(savedreportUrl,dataObj);
		postRequest.done();
	    notifyService.setNotification("Saved", "", "success");
	    }
      }//eof
	  
	 
	  function saveSharedReportToUsers(data){
			var dataObj = {};
			dataObj.userids = data.userids;
			dataObj.reportid = data.reportid;
			var url = commonUtils.getAppBaseURL("report/sharedReport/save");
			var postRequest = $.post(url,dataObj);
			postRequest.done();
		    notifyService.setNotification("Report Shared", "", "success");
		    
		 }
	
	  function deleteReport(dataObj2,dataObj3){

		  var url = commonUtils.getAppBaseURL("report/columns/deleteRow");
		  var postRequest = $.post(url,dataObj2);
		  if(confirm("Do you want to delete this report ?") === true){
			  postRequest.done(function(htmlResponse){		
				  notifyService.setNotification("Deleted", "", "success");
			  }) 
			  window.location.reload(true);
		  }
	  }	
	 		
return {
		
	init : function(){ 
		$ = context.getGlobal('jQuery'); 
		commonUtils = context.getService('commonUtils'); 
		moduleEl = context.getElement(); 
		initListReports(); 
		notifyService = context.getService('service-notify'); 
		},
		
		messages: [ 'buttonOnSavedReportlandingpage','sr::selectedUser'],

		onmessage: function(name,data) {
			if (name === 'buttonOnSavedReportlandingpage') {
				saveSavedReportasLandingPage(element);
			}
			if(name === 'sr::selectedUser'){
				saveSharedReportToUsers(data)
			}
		},
		
		onclick: function(event, element, elementType){
			
			if (elementType === 'reportStatusDataType') {
			 saveSavedReportasLandingPage(element);
			}
			
			if (elementType === 'sharedReportOpenModal') {
				var dataObj1={};
				dataObj1.reportid = $(element).data("reportid");
			    context.broadcast('sr::showUserSelector', dataObj1);
        	}
			
			if (elementType === 'savedReportDelete') {
				var dataObj2={};
				var dataObj3={};
				dataObj2.id = $(element).data("reportid");
				dataObj3.statusShared = $(element).data("sharedstatus");
				deleteReport(dataObj2,dataObj3);
        	}
		}
	};
	
});