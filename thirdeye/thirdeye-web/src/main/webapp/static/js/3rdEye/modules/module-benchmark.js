
Box.Application.addModule('module-benchmark', function(context) {
	'use strict';
	var $,commonUtils,moduleEl,notifyService;	
	function initBanchMark(){		
		$(".editableBenchmark").blur(function(event){
			submitForm($(this));
		});

		$('.editableBenchmark').keydown(function(event) {
		    // enter has keyCode = 13, change it if you want to use another button
		    if (event.keyCode == 13) {
		    	submitForm($(this));
		    }
		  });
	}// EOF
	
	function submitForm($editedParam){
		var $form = $editedParam.closest("form.benchMarkForm");
		$form.ajaxForm(function(htmlResponse) {
			 $("#box-body1",moduleEl).empty();
			 $("#box-body1",moduleEl).append(htmlResponse);	
			 if($(htmlResponse).find('.error_msg').size() > 1){	
					notifyService.setNotification("Fields Missing","", "error");								
			 }else{
					notifyService.setNotification("Saved", "", "success");	
			 }
			 Box.Application.startAll(moduleEl);			
		
			 initBanchMark();
		}); 

		$form.submit();

	}
	function initBanchMarkItems(){
	
		$("#addRowButton").bind('click',function(){
			
			$(".saveRow").trigger('click');
			if ($(".dirtyrow").length < 1) 
			{
				var benchmarkid =  moduleEl.querySelector('[name="id"]').value;
				var benchmarkName =  moduleEl.querySelector('[name="name"]').value;
				if(benchmarkid !=""){
					commonUtils.addRowToTableFromURL("benchmarkColumns",commonUtils.getAppBaseURL("benchmarkItem/columns/newRow/"+benchmarkid));	
				}else{
					// need to write some logic to validate benchmark not to be blank at the time of saving benchmarkItem.
					//notifyService.setNotification("Fields Missing","", "error");
					if(benchmarkName =="")
						  setMessage("Benchmark Name can not be empty.");
				}
				
			}
		});

		// Bind the click on the table
		// Base configuration for the click handler.
		var clickHandlerConfiguration = {};
		clickHandlerConfiguration.saveClass = "saveRow";
		clickHandlerConfiguration.saveURL = commonUtils.getAppBaseURL("benchmarkItem/columns/saveRow");
		clickHandlerConfiguration.editClass = "editRow";
		clickHandlerConfiguration.editURL = commonUtils.getAppBaseURL("benchmarkItem/columns/editRow/");
		clickHandlerConfiguration.deleteClass = "deleteRow";
		clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("benchmarkItem/columns/deleteRow");

		$("#benchmarkColumns").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		
	} //eof
	function setMessage(message) {
        var messageEl = moduleEl.querySelector('.error_msg');
        messageEl.innerText = message;
    }
	
	return {
		
		init : function(){		
			$ = context.getGlobal('jQuery');		
			commonUtils = context.getService('commonUtils');
		    notifyService = context.getService('service-notify');
			moduleEl = context.getElement();
			initBanchMark();
			initBanchMarkItems();
		}
		
	};
});