/**
 * This module is for displaying Filter tags
 * @author dhruv.sood
 */

Box.Application.addModule('module-filterTags', function(context) {
	 
	'use strict';
	
	// private methods here
	var $, moduleDiv;	
	
	var dataObj ={};
	
	//Method to display the tags
	function displayTags(data){
		 var splittedData = data.paramValue.split("=")[0];
	     var tag = data.paramValue.split(splittedData+"=").join('');
	     var deleteCross= "<span class='clickable' style='padding-top:12px;padding-bottom:12px;' id='"+(data.paramValue).split(' ').join('_').replace(/[&\/\\#,+()$~%.='":*?@<>{}]/g, "")+"' data-type='"+data.paramValue+"'>x</span>&nbsp";		    		     
	     var newHtmlTag="<div style='float:left;padding:12px 5px 1px;user-select: none;'><span style='padding:1px 3px 1px 3px;background-color:#e4e4e4;border:1px solid #aaa;border-radius:4px'>"+deleteCross+tag+"</span></div>&nbsp;";
	     $("#tags",moduleDiv).append(newHtmlTag);
	}
	
	//Method to delete the tags
	function removeTags(data){
		var newIDValue = (data.paramValue).split(' ').join('_').replace(/[&\/\\#,+()$~%.='":*?@<>{}]/g, "");
		$("#"+newIDValue, moduleDiv).closest('div').remove();
		
		//Check of deleted tag is of asset class facet
		if(data.paramValue.split("=")[0]==='assetClass'){
			context.broadcast('uncheckAssetClass', newIDValue);
		}
	}
	
	return {
		messages: ['updateTag'],
	
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			moduleDiv = $(context.getElement());
		},
		onmessage: function(name, data) {
			if(name==='updateTag'){
				data.checked ? displayTags(data) : removeTags(data);
			}
        },
        onclick: function(event, element, elementType) {
        	if(element!=null){
        		dataObj.checked = false;
        		dataObj.paramValue = elementType;
        		context.broadcast('tagDeleted', dataObj);
        	}
		}
	};
});