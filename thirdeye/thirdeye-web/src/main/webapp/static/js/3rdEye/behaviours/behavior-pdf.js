/**
 * @author: tanushree.mittal
 *  Behavior for Download report as PDF
 *  This behavior allows us to download the reports  as PDF 
 */

Box.Application.addBehavior('behavior-pdf', function(context) {

	'use strict';
	var $,d3;
	var $html2canvas,commonUtils;
	var _moduleConfig;
	var module;
	var $imagename;
	
	
	function downloadPDFButton()
	{
		var module = document;
		$('.box-header').append('<div class="downloadPDF"><i title = "Download PDF" class="fa fa-file-pdf-o clickable"/></div>');
		//on click of download button
		$('.downloadPDF').click( function() {
			var canvas = module.createElement("canvas");
			var ctx = canvas.getContext("2d");	
			var svgElement = module.querySelector('svg');
			canvas.height = svgElement.clientHeight;
			canvas.width = svgElement.clientWidth;
			svgAsDataUri(svgElement,{},function(uri){
			drawInlineSVG(uri,canvas);
			});
		})


		function drawPDF(canvas){
			var ctx = canvas.getContext("2d");	
			var doc = new PDFDocument();
			var stream = doc.pipe(blobStream());
			var data = canvas.toDataURL("image/png");
			$('img').attr('src', data);
			doc.image(data, {width: 400});
			doc.end();

			var saveData = (function () {
				var a = document.createElement("a");
				document.body.appendChild(a);
				a.style = "display: none";
				return function (blob, fileName) {
					var url = window.URL.createObjectURL(blob);
					a.href = url;
					a.download = fileName;
					a.click();
					window.URL.revokeObjectURL(url);
				};
			}());

			stream.on('finish', function() {
				var dataObj={};
				if(_moduleConfig.pngimagename === "null" || _moduleConfig.pngimagename == undefined)
					$imagename = 'DownloadPDF';
				else
					$imagename =_moduleConfig.pngimagename.replace(/'/g,"");
				dataObj.fileName = $imagename;
				var blob = stream.toBlob('application/pdf');
				saveData(blob, $imagename+".pdf");
			});
		}

		function drawInlineSVG(uri, canvas){					 
			var img  = new Image();
			var ctx = canvas.getContext("2d");	
			img.onload = function(){
				ctx.drawImage(this, 0,0);
				drawPDF(canvas);    
			}
			img.src = uri;
		}
	}
	return {
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$html2canvas = context.getGlobal('html2canvas');
			d3 = context.getGlobal('d3');
			_moduleConfig = context.getConfig();
			module = context.getElement();
			downloadPDFButton();
			//$.getScript("https://github.com/devongovett/blob-stream/releases/download/v0.1.2/blob-stream-v0.1.2.js");
		}
	}
})