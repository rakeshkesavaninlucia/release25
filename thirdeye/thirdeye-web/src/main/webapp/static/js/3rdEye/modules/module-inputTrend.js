
Box.Application.addModule('module-inputTrend',function(context) {
	'use strict';

	var $,moduleEl,commonUtils,notifyService;

	var dataObj ={};

	function addCalender(){
		$( ".datepicker" ).datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
		});
	}
	function addRowToTableFromURL(tableId, urlToHit){
		if ($("#"+tableId+" tbody").length === 0){
			$("#"+tableId).append("<tbody></tbody>");
		}
		$.get( urlToHit, function( newRowContent ) {
			$("#"+tableId+" tbody").append(newRowContent);
			addCalender();
		});
	}

	return {
		// Initialize the page
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			moduleEl = context.getElement();
			commonUtils = context.getService('commonUtils');
			notifyService = context.getService('service-notify');
			addCalender();
		},
		onclick:function(event,element,elementType){

			if(elementType==='addRow'){
				addRowToTableFromURL("inputTrend",commonUtils.getAppBaseURL("trend/row"));
			}

			if(elementType === 'saveRow'){
				dataObj.date = $(".date").val();
				dataObj.value = $(".value").val();
				dataObj.paramId =$(".param").val();
				var url = commonUtils.getAppBaseURL("trend/save/row");
				var getRequest = $.get(url,dataObj);
				getRequest.done(function(incomingData){
					notifyService.setNotification("Saved", "", "success");	
				});
			}
		}
	}
});