/**
 * @author: ananta.sethi
 *  (bcm tree-add value chain)
 *   The purpose of "use strict" is to indicate that the code should be executed in "strict mode"
 *   With strict mode, you can not, for example, use undeclared variables.
 */
Box.Application.addModule('module-BcmLevel', function(context) {

	'use strict';
	var $,commonUtils,$anchorRef;

	function handleDelete(bcmId, dataObj){

		var deferred = $.Deferred();
		var postRequest = $.post( commonUtils.getAppBaseURL("bcmlevel/"+bcmId+"/level/deleteNode"), dataObj);
		postRequest.done(function(incomingData){
			deferred.resolve();
		})

		return deferred.promise();
	} // EOF[handleDeleteLevel]

	function handleFormSubmitForBcmLevelCreation(modalId, htmlResponse){

		var modalPromise= commonUtils.populateModalWithContent(modalId,htmlResponse);
		// Try and see whether you have received valid modal data 
		modalPromise.done ( function (data) {
			$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
				handleFormSubmitForBcmLevelCreation(modalId, htmlResponse);
			});
		})

		// This means that the modal is no longer needed
		.fail (function (htmlContent) {
			// Unbind the clicks
			$("#"+modalId).modal('hide');
			$("#bcmLevelTree").append(htmlResponse);
			// Re-initialize the tree - i wish there was a better way.
			reinitializeTree();
		});
	}

	function handleFormSubmitForChildLevelCreation(modalId, htmlResponse, $anchorRef, dataObj){
		var modalPromise= commonUtils.populateModalWithContent(modalId,htmlResponse);
		// Try and see whether you have received valid modal data 
		modalPromise.done ( function (data) {
			$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
				handleFormSubmitForChildLevelCreation(modalId, htmlResponse, $anchorRef, dataObj);
			});
		})

		// This means that the modal is no longer needed
		.fail (function (htmlContent) {
			// Unbind the clicks
			$("#"+modalId).modal('hide');				    	

			if(dataObj.operation == "edit"){
				$anchorRef.replaceWith(htmlResponse);
			}else{
				var $checkElement = $anchorRef.next();

				$anchorRef.addClass("isParent");
				if ($checkElement.is('.treeview-menu')){
					// or to the ul after the anchor
					$checkElement.append(htmlContent);
				} else {
					// A UL would have been sent across the wire
					$anchorRef.parent().append(htmlContent);
				}
			}

			reinitializeTree();
		});
	}

	function reinitializeTree(){
		$("li a", $('.levelTree')).unbind( "click" );
		// Re-initialize the tree - i wish there was a better way.
		$.AdminLTE.tree('.levelTree');
	}

	function bindBlurToBcmName(){					

		$(".editableBcmName").blur(function(event){
			var $editedParam = $(this);
			var $formForParam = $editedParam.closest("form.editableForm");


			$formForParam.ajaxForm(function(htmlResponse) {
//				alert("thanks");
			}); 

			$formForParam.submit();

		})
	}
	return{
		init : function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');

			var _this = this;

			var modalId = "pageModal";

			var $levelTreeRef = $("#bcmLevelTree");


			// See which questionnaire we are working with
			var bcmid = $levelTreeRef.data("bcmid");

			$levelTreeRef.click(function(event){
				// We only route specific click
				var $eventTarget = $(event.target);
				if ($eventTarget.is('.tools>i')){
					// So that the tree view does not expand on us
					event.stopPropagation();

					// So that the anchor does not follow
					event.preventDefault();

					// Navigate to the anchor
					$anchorRef = $eventTarget.parent().next();


					if ($eventTarget.is('.addLevel') || $eventTarget.is('.editNode')){

						var dataObj = {};
						dataObj.parentLevel = $anchorRef.attr("id");
						if($eventTarget.is('.addLevel')){
							dataObj.operation = "add";
						}else if($eventTarget.is('.editNode')){
							dataObj.operation = "edit";
						}

						commonUtils.populateModalWithURL(modalId,commonUtils.getAppBaseURL("bcmlevel/"+bcmid+"/level/fetchModal"),dataObj)
						.then(function(){
							// Bind the ajax form
							$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
								handleFormSubmitForChildLevelCreation(modalId, htmlResponse, $anchorRef, dataObj);
							}); 

							// Show the modal
							var $modalDiv = $("#"+modalId).modal({
								keyboard: true
							})
						});

					}else if($eventTarget.is('.deleteNode')){
						var dataObj = {};
						var parentLevelId = $anchorRef.data("parentlevel");
						if (parentLevelId){
							dataObj.parentLevelId = parentLevelId;
						}

						if ($anchorRef.is(".level")){
							dataObj.levelId = $anchorRef.attr("id");
						}

						handleDelete(bcmid, dataObj).then(updateGUIForNodeDelete);
						
						function updateGUIForNodeDelete(){
							var $liToDelete = $anchorRef.parent();

							if ($("li.treeview",$liToDelete.parent()).size() > 1){
								// Remove the li
								$liToDelete.fadeOut(300, function() { $(this).remove(); });
							} else {
								// Remove the UL all together.
								$liToDelete.parent().fadeOut(300, function() { $(this).remove(); });
							}
						}



					}
				}
			});
			$("#AddL0").click(function(event){
				commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL("bcmlevel/"+bcmid+"/level/fetchModal"))
				.then(function(){
					// Bind the ajax form
					$('form', "#"+modalId).ajaxForm(function(htmlResponse) {
						handleFormSubmitForBcmLevelCreation(modalId, htmlResponse);
					}); 

					// Show the modal
					var $modalDiv = $("#"+modalId).modal({
						keyboard: true
					})
				});
			})

			bindBlurToBcmName();

			$.AdminLTE.tree('.levelTree');
		},// end of init function			


	}
});