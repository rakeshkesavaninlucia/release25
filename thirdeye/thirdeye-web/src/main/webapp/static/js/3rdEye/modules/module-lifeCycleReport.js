/**
 * This module is for lifeCycle report
 * @author ananta.sethi
 */

Box.Application.addModule('module-lifeCycleReport', function(context) {

	// private methods here
	var $, moduleDiv;	
	
	
	
	return {
		behaviors : [ 'behavior-select2' ], 
		init: function (){
			
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			moduleDiv = $(context.getElement());

		},  	
		onchange: function(event, element, elementType) {
			if(elementType === "assetTemplate"){
				var dataObj = {};
				dataObj.assettemplateid = $(element).val();
        		context.broadcast('assetSelected', dataObj);
    	}		
        },
	};
});