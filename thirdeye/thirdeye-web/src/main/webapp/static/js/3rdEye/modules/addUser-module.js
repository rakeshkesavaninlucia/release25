/**
 * @author: dhruv.sood
 */
Box.Application.addModule('addUser-module', function(context) {

	'use strict';
	
	// private methods here
	var $;

	return {
		behaviors : [ 'behavior-select2' ],

		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
		},

		onsubmit : function(event, element, elementType) {
			if (elementType === "userAddForm" && $('select', $(element))[0].selectedIndex <= 0) {
				event.preventDefault();
					return false;				
			}
		}
	};
});
