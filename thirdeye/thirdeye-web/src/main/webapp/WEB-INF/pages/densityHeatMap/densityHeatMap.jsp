<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = 'DensityHeatMap')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.densityHeatMap.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
	<span></span>
	</div>
	<div class="container" th:fragment="contentContainer">
	<div class="row">
		<div class="col-md-12">
		
			<div class="box box-primary">
			<div class="box-header with-border"></div>
			<div class="box-body" data-module="module-densityHeatMap">
			 <script type="text/x-config" th:inline="javascript">{"pngimagename":"Density Heat Map"}</script>
			<div id="graph" style="overflow-x: scroll;width: 100%;">
			<span style="position: absolute;bottom: 253px;font-family: serif;" th:text="#{pages.reports.legend.heatMap.title}"></span>
			<div class= "legend" style="position:  absolute; bottom: 1px;"></div></div>
			  </div>
			</div>
		</div>
	</div>
</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/3rdEye/modules/module-densityHeatMap.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3-tip.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3.min.js}"></script>
	</div>
</body>
</html>