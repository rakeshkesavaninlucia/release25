<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<!-- small box -->
     <div th:fragment="parameterFragment" class="small-box clickable"  th:style="'background-color:'+${paramBean.hexColor}" data-toggle="popover" th:attr="data-content=${paramBean.aggregate}" data-trigger="hover" th:title="${paramBean.name}" data-container="body" data-placement="top">
		<div class="inner  homeParameter">
			<h3 th:text="${paramBean.aggregate}" style="font-size: 2em;"></h3>
			<div th:text="${paramBean.name}" style="font-size: 0.9em; overflow: hidden; text-overflow: ellipsis; white-space: nowrap"></div>
		</div>
		<div class="small-box-footer" data-type="parameterTile" th:attr="data-paramid=${paramBean.id}">
			<div class="small-box-footer">More info<i class="fa fa-arrow-circle-right"></i>
			</div>
		</div>
	</div>	
    
</body>
</html>