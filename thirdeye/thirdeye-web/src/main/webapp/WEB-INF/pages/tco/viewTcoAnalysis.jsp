<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.tco.analysis.nav})">
<head>
<meta charset="utf-8" />
<title>TCO Analysis</title>
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
  Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<style th:fragment="onPageStyles">

.chart-responsive{
	overflow:auto;
}

.axis text {
  font: 10px sans-serif;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: steelblue;
  fill-opacity: .9;
}

.x1.axis path {
  display: none;
}

.x1.axis text {
  text-anchor: end !important;
}

.nv-x.nv-axis text {
  text-anchor: end !important;
  transform: translate(-10px, 0) !important;
  transform: rotate(-60deg) !important;
}

.brush .extent {
      stroke: #fff;
      fill-opacity: .125;
      shape-rendering: crispEdges;
    }

</style>
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.tco.analysis.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.reports.tco.analysis.subtitle}"></span>
	</div>
	<div th:fragment="contentContainer">
				
		<div data-module="module-tcoAnalysisGraphs">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body analysisGraph">
							<form role="form" th:action="@{/graph/tco/plot}" method="post" id="tcoAnalysisGraph">
								<table class="table table-bordered table-condensed table-hover row" style="margin-left: 0px;">
									<tr>
										<td class="col-md-2 horizontal"><label>Select Graph</label></td>
										<td colspan="3"><select class="form-control select2" data-type="graphType" name="graphType">
											<option th:each="oneMode : ${T(org.birlasoft.thirdeye.constant.TcoGraphTypes).values()}" th:value="${oneMode}" th:text="${oneMode.value}"></option>
										</select></td>
									</tr>
									<tr>
										<td class="col-md-2 horizontal"><label>Year</label></td>
										<td class="col-md-4"><select class="form-control select2" name="chartOfAccountYear">
											<option value="-1">--Select--</option>
											<option th:each="oneChartOfAccount : ${SetOfChartOfAccountYear}" th:value="${oneChartOfAccount}" th:text="${oneChartOfAccount}" />
										</select></td>
										<td class="col-md-2 horizontal"><label>Asset Type</label></td>
										<td class="col-md-4"><select class="form-control select2" name="assetTypeId">
											<option value="-1">--Select--</option>
											<option th:each="oneAssetType : ${AssetTypeList}" th:value="${oneAssetType.id}" th:text="${oneAssetType.AssetTypeName}" />
										</select></td>
									</tr>
									<tr id="getFragment">
										<td class="col-md-2 horizontal"><label>Questionnaire</label></td>
										<td class="col-md-4"><select class="form-control ajaxSelector select2" th:attr="data-dependency=qe ,data-url='graph/scatter/fetchRootParameters/'" name="questionnaireId">
											<option value="-1">--Select--</option>
											<option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
										</select></td>
										<td class="col-md-2 horizontal"><label>Parameter</label><span class="asterisk">*</span></td>
										<td class="col-md-4"><select class="form-control ajaxPopulated select2" th:attr="data-dependson=qe" name="parameterId">
											<option value="-1">--Select--</option>
								    		<option th:each="oneParameter : ${listOfParameters}" th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
										</select></td>
									</tr>
								</table>
								<div class="box-footer">
									<div class="pull-right">
										<button type="submit" class="btn btn-primary">Plot</button>
									</div>
									<div id="saveAs" class="pull-right" style="display:none;margin-right: 5px;"><a sec:authorize="@securityService.hasPermission({'SAVE_EDIT_DELETE_REPORT'})"  class="btn btn-primary" data-type = "saveReport" th:text="#{report.modal.title}"></a></div>
									<script type="text/x-config" th:inline="javascript">{ "id": "","reportType":"[[${T(org.birlasoft.thirdeye.constant.ReportType).TCO_ANALYSIS.description}]]"}</script>
									<script type="text/x-config" th:inline="javascript">{"pngimagename":"Tco_Analysis"}</script>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary scatterPlot" >
						<div class="box-header with-border">
							<h3 class="box-title graphTitle"></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-type="download" title = "Download">
									<i class="glyphicon glyphicon-download clickable"></i>
								</button>
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body chart-responsive" >
							<div class="chart">
								<svg></svg>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div data-module="module-report">
		  <script type="text/x-config" th:inline="javascript">{"url":"report/createReport", "modal": "saveReportModal"}</script>
		</div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-tcoAnalysisGraphs.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/report/module-report.js}"></script>
		<script th:src="@{/static/js/ext/thirdEye/saveSvgAsPng.min.js}"></script>
		
		
		
		
	</div>

</body>
</html>