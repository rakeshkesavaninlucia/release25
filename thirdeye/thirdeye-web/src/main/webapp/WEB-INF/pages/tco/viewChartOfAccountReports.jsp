<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.tco.viewtco.title})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
 
 <div th:fragment="pageTitle" >
      <span th:text="#{pages.tco.viewtco.title}"></span>
      
      <div  id="new" data-module ="module-showHelp">
			<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;margin-top: -19px;margin-right: 50px;"></button>
	  </div>
 </div>
 <div th:fragment="pageSubTitle">
     <span th:text="#{pages.tco.viewtco.subtitle}"></span>
 </div>
 
<div class="container" th:fragment="contentContainer">
	 <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body">
				 		<div class="table-responsive" data-module="common-data-table">
							<table class="table table-bordered table-condensed table-hover" id="templateColumnsofview">
								<thead>
									<tr>
										<th><span th:text="#{tco.name.table}"></span></th>
										<th><span th:text="#{questionnaire.description.table}"></span></th>
										<th><span th:text="#{questionnaire.date.table}"></span></th>
										<th><span th:text="#{questionnaire.action.table}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneOfQuestionnaire : ${listOfQuestionnaire}" th:id="${oneOfQuestionnaire.id}">
									<td><span th:text="${oneOfQuestionnaire.name}"></span></td>
									<td><span th:text="${oneOfQuestionnaire.description}"></span></td>
									<td><span th:text="${#dates.format(oneOfQuestionnaire.updatedDate, 'dd-MMM-yyyy')}"></span></td>
									<td>
										&nbsp;<a th:href="@{/tco/reports/{id}/sunburst(id=${oneOfQuestionnaire.id})}" class="fa fa-certificate" th:title="#{icon.title.view.sunburst}"></a>&nbsp;
										<a th:href="@{/tco/reports/{id}/treeMap(id=${oneOfQuestionnaire.id})}" class="glyphicon glyphicon-tree-deciduous" th:title="#{icon.title.view.treemap}"></a>
									</td>
									</tr>					
								</tbody>
							</table>
					 </div>
				</div>		
			</div>
		</div>
	</div>
	 
</div>
</body>
</html>