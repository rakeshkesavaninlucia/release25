<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>Fragments for TCO</title>
</head>
<body>
	<div th:fragment="createNewCostElement">
		<div class="modal fade" id="createNewCostElementModal" tabindex="-1" role="dialog" aria-labelledby="createNewCostElementModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/costElement/save}" method="post" th:object="${costElementForm}"  id="createNewCostElementForm">
								<input type="hidden" th:field="*{id}"/>								
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" th:text="*{id != null }? #{edit.cost.element} : #{create.cost.elements}"></h4>
						      </div>
						      <div class="modal-body">						          
						          <div class="form-group">
						          	<label for="title" class="control-label">Name:</label><span class="asterisk">*</span>
						            <input type="text" class="form-control" th:field="*{title}"/>						            
			            			<span th:if="${#fields.hasErrors('title')}" th:errors="*{title}" style="color: red;"></span>
			            		 </div>				            		 
			            		 <div class="form-group">
			            		   <label for="helpText" class="control-label">Description:</label>						            
						           <input type="text" class="form-control" th:field="*{helpText}"/>
			            		   <span th:if="${#fields.hasErrors('helpText')}" th:errors="*{helpText}" style="color: red;"></span>
						         </div>							      
						     </div>
						      <div class="modal-footer">
						     	  <input type="submit" th:value="*{id != null }? 'Save' : 'Create'" th:title=" *{id != null }? #{icon.title.save} : #{icon.title.create}" class="btn btn-primary"></input>
						     </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>	
	
	<div th:fragment="createCostStructure">
		<div class="modal fade" id="createCostStructureModal" tabindex="-1" role="dialog" aria-labelledby="createCostStructureModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/coststructure/save}" method="post" id="createCostStructureForm" th:object="${costStructureForm}">
							  <input type="hidden" th:field="*{id}" />
							  <input type="hidden" th:field="*{baseParameterId}" />
							  <input type="hidden" name="rootParameterId" th:value="${rootParameterId}"/>
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" th:text="*{id != null }? 'Edit Cost Structure' : 'Create Cost Structure'"></h4>
						      </div>
						      <div class="modal-body">
						      	  <div class="form-group">
						            <label for="uniqueName" class="control-label" th:text="#{tco.unique.name}"></label>
						            <div th:if="${costStructureForm.id eq null}" th:remove="tag">
						            	<span class="asterisk">*</span>
							            <input type="text" class="form-control" th:field="*{uniqueName}" />
				            			<span th:if="${#fields.hasErrors('uniqueName')}" th:errors="*{uniqueName}" class="error_msg">Test</span>
			            			</div>
			            			<div th:if="${costStructureForm.id ne null}" th:remove="tag">
										<span th:text="${costStructureForm.uniqueName}"></span>
										<input type="hidden" th:field="*{uniqueName}" />
									</div>
						          </div>
						          <div class="form-group">
						            <label for="displayName" class="control-label" th:text="#{tco.display.name}"></label>
						            <span class="asterisk">*</span>
						            <input type="text" class="form-control" th:field="*{displayName}" />
			            			<span th:if="${#fields.hasErrors('displayName')}" th:errors="*{displayName}" class="error_msg">Test</span>
						          </div>
						          <div class="form-group">
						              <label for="description" class="control-label" th:text="#{tco.description}"></label>
							          <textarea class="form-control" th:field="*{description}"></textarea>
							      </div>
						      </div>
						      <div class="modal-footer">
						        <input type="submit" th:value="*{id != null }? 'Edit Cost Structure' : 'Create Cost Structure'" class="btn btn-primary"></input>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
	<div th:fragment="listCostElement"> 
		<div class="modal fade" id="viewCostElementModal" tabindex="-1" role="dialog" aria-labelledby="viewCostElementModal">
		<div class="modal-dialog modal-md" role="document">
			    <div class="modal-content">
					<form th:action="@{/coststructure/selectedCostElements/save}" method="post" id="costElementSelector">
						<input type="hidden" name="baseParameterId" th:value="${baseParameterId}"/>
						<input type="hidden" name="rootParameterId" th:value="${rootParameterId}"/>
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Select Cost Elements</h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive" data-module="common-data-table">
								<table class="table table-bordered table-condensed table-hover">
									<thead>
										<tr>
										    <th></th>
											<th><span>Name</span></th>
											<th><span>Updated Date</span></th>
										</tr>
									</thead>
									<tbody th:if="${not #lists.isEmpty(listCostElement)}">
										<tr th:each="oneCostElement : ${listCostElement}" th:id="${oneCostElement.id}">
											<td><input type="checkbox" name="costElement[]" class="costElementClass" th:value="${oneCostElement.id}"/></td>
											<td><span th:text="${oneCostElement.displayName}"></span></td>
											<td><span th:text="${#dates.format(oneCostElement.updatedDate,'dd-MMM-yyyy')}"></span></td>
										</tr>
									</tbody>
								</table>
							</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" value="Done" class="btn btn-primary"></input>
				      </div>
				    </form>
			    </div>
		 </div>
		 </div>
	</div>
	<div th:fragment="exportImportCOAExcelModal">
		<div class="modal fade" id="exportImportCOAExcelModal" tabindex="-1" role="dialog" aria-labelledby="exportImportCOAExcelModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<div th:replace="tco/tcoFragment :: exportImportCOAExcel"></div>
				    </div>
			 </div>
		 </div>
	</div>	
	
	<div th:fragment="exportImportCOAExcel">
		 <div class="box box-primary">
		  <div class="box-header with-border">
             <h3 class="box-title" th:text="'Response to Chart of Accounts Using Excel'"></h3>
				<div th:Switch="${activeButton}" class="box-tools pull-right">
				  <div th:case="Collapse">
				    <button class="btn btn-box-tool" data-widget="collapse">
					    <i class="fa fa-minus"></i>
				    </button>
				</div>
				<div th:case="Close">
					<button  type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
         </div><!-- /.box-header -->
		 <div class="box-body">		 
 			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs" style="width: 100%">
					<li th:class="${activeTab == 'exportTab' ? 'active' : ''} ">
						<a href="#selectExport" data-toggle="tab" aria-expanded="true" th:text="#{export.tab}"> Export </a>
					</li>
					<li th:class="${activeTab == 'importTab' ? 'active' : ''} ">
						<a href="#selectImport" data-toggle="tab" aria-expanded="false" th:text="#{import.tab}"> Import </a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" th:classappend="${activeTab == 'exportTab' ? 'active' : ''} " id="selectExport">
						<h3 th:text="#{export.tab.heading}"></h3>
						<a class="btn btn-primary" th:href="@{/tco/{id}/response/exportCOA(id=${chartOfAccountId})}" th:text="#{export.button}"></a>
						<h4>Please Note:</h4>
						<ol>
						    <li>Only you can upload from this template.</li>
							<li>Please do not make any modifications to the structure of this file.</li>
						</ol>
					</div><!-- /.tab-pane -->
					<div class="tab-pane" th:classappend="${activeTab == 'importTab' ? 'active' : ''} " id="selectImport">
						<h3 th:text="#{import.tab.heading}">Test</h3>
						<form th:action="@{/tco/{id}/import?_csrf={_csrf.token}(id=${chartOfAccountId},_csrf.token=${_csrf.token})}" method="post" id="importQuestionnaireForm" enctype="multipart/form-data">
							<input type="file" name="fileImport" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
							<input type="submit" value="Upload"/>
							<div th:if="${activeTab == 'importTab'}">
								<div th:if="${#lists.isEmpty(listOfErrors)}">
									<div class="alert alert-success alert-dismissable">
										<h4><i class="icon fa fa-check"></i> Alert!</h4>
										<span th:text="#{import.success}"></span>
									</div>
								</div>
								<div th:if="${not #lists.isEmpty(listOfErrors)}">
									<div class="alert alert-danger alert-dismissable">
										<h4><i class="icon fa fa-ban"></i> Alert!</h4>
										<ul th:each="errorCell : ${listOfErrors}">
											<span th:if="${errorCell.row ne null and errorCell.column ne null}">Column: <span th:text="${errorCell.column }"></span> Row: <span th:text="${errorCell.row }"></span></span>
												<ul th:each="error : ${errorCell.errors}">				
													<li><span th:text="${error}"></span></li>
												</ul>											
										</ul>
									</div>
								</div>
							</div>
						</form>
					</div><!-- /.tab-pane -->
				</div><!-- /.tab-content -->
			</div>	
			</div>	
		</div>		
	</div>
</body>
</html>