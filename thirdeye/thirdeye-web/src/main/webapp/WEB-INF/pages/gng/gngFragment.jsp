<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="gngReportPlot(assetWrapper)" class="gngReport">
		<div class="box-body">
			<div class="table-responsive" data-module="common-data-table">
				<table class="table table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th><span th:text="#{gng.report.asset.name}"></span></th>
							<th><span th:text="#{gng.report.asset.parameterValue}"></span></th>
							<th><span th:text="#{gng.report.asset.completeness}"></span></th>
						</tr>
					</thead>
					<tbody>
						<tr th:each="oneAssetValues : ${assetWrapper.assetsValues}">

							<td><span th:text="${oneAssetValues.assetName}"></span></td>

							<td class="col-md-6 horizontal"
								th:style="'text-align:center; background:'+${oneAssetValues.color}+'; width:84px;padding:.5em;'"><span
								th:text="${oneAssetValues.description}"></span></td>
							<td><span th:text="${oneAssetValues.percentage + '%'}"
								th:align="left"></span>
								<div
									class="progress progress-xs progress-striped ">
									<div class="progress-bar progress-bar-primary"
										th:style="'width:'+ ${oneAssetValues.percentage} + '%'"></div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>