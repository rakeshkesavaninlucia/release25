<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.questionnaire.viewquestionresponse.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{question.view}"></span></div>
	<div class="container"  th:fragment="contentContainer">
		<div th:switch="${oneQQ.question.type}" th:fragment="oneQuestionResponseContainer(oneQQ)">
			<div th:case="TEXT"><div  th:replace="question/questionResponseFragments :: TEXT(oneQQ=${oneQQ})"></div></div>
			<div th:case="PARATEXT"><div  th:replace="question/questionResponseFragments :: PARATEXT(oneQQ=${oneQQ})"></div></div>
			<div th:case="MULTCHOICE" ><div th:replace="question/questionResponseFragments :: MULTCHOICE(oneQQ=${oneQQ})"></div></div>
			<div th:case="NUMBER" ><div th:replace="question/questionResponseFragments :: NUMBER(oneQQ=${oneQQ})"></div></div>
			<div th:case="DATE" ><div th:replace="question/questionResponseFragments :: DATE(oneQQ=${oneQQ})"></div></div>
		</div>
	</div>
</body>
</html>