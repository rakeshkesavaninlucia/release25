<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.questionnaire.responsenqrq.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	 <div th:fragment="pageTitle">
	  <a class="pull-left fa fa-chevron-left btn btn-default"
	style="margin-left: 10px;" data-type="backButton" href="javascript:window.history.back();"></a>
	 <span th:text="${questionnaire.name}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="${questionnaire.description}"></span></div>
	
	<div class="container"  th:fragment="contentContainer">
	
		<div class="row">
			<div class="col-md-8 userquestionnaire" data-module='module-questionnaireResponse'>
				<div class="box box-primary">				
	                <div th:each="oneQQ : ${listOfQuestions}"> 
	                	<div  th:replace="questionnaire/viewQuestionResponse :: oneQuestionResponseContainer(oneQQ=${oneQQ})"></div>
	                </div>
		        </div> 
			</div>
			<div class="col-md-8" th:if ="${emptyQQBean}">
				<div>		
				<span style="font-size:16px;"><b>No Data Available</b></span>		
		        </div> 
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script th:src="@{/static/js/3rdEye/modules/module-questionnaireResponse.js}"></script>
	</div>
</body>
</html>