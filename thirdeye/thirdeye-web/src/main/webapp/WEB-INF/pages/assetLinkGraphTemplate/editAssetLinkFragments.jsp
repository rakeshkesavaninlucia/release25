<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="createGraphForm">
		<form th:action="@{/add/save}" method="post" th:object="${graph}">
				<input type="hidden" th:field="*{id}" />
				<div class="form-group">
					<span>Graph Name :<span style="color: red;">*</span></span> <input type="text" th:field="*{graphName}" class="form-control" />
					<span th:if="${#fields.hasErrors('graphName')}" th:errors="*{graphName}" style="color: red;">Test</span> 
					<br/>
					<span>Description :</span>
					<textarea rows="3" class="form-control" th:field="*{description}"></textarea>
					<br/>
					<!-- <div class="checkbox" th:each="assetTemplate : ${listOfAssetTemplate}">
					     <input type="checkbox" class="editableParameter" th:name="assetTemplatesCheckbox" th:value="${assetTemplate.id}" th:text="${assetTemplate.assetType.assetTypeName} + ' :: ' + ${assetTemplate.assetTemplateName}"/>
					 </div> -->
					 <!--  <span th:if="${#fields.hasErrors('assetTemplatesCheckbox')}" style="color: red;">Test</span>-->
					<div class="box-header with-border"  th:each="oneEntry : ${mapOfTemplate}">
		                  <h3 class='box-title'><i class="fa fa-tag"></i><span th:text="${oneEntry.key}"></span></h3>
		                  <!-- <section class="content"> -->
					           <div class="row">
					                <div class="col-lg-3 col-xs-4">
									  <div class="small-box bg-white" th:each="oneTemplate : ${oneEntry.value}" >
							              <input type="checkbox" th:checked="${#maps.containsKey(mapOfCheckedTemplate, oneTemplate.id)}" th:name="assetTemplatesCheckbox" th:value="${oneTemplate.id}"/>
							              <span  th:text="${oneTemplate.assetTemplateName}"></span>
						              </div>
					            	</div>
					            </div>
					      <!-- </section> -->
	              </div>
					<input class="btn btn-primary" type="submit" value="Create Graph"/>
				</div>
		</form>
	</div>
</body>
</html>