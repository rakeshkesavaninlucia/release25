<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.assetgraph.viewgraph.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.reports.assetgraph.viewgraph.add.title}"></span>
	<div  id="new" data-module ="module-showHelp">
			<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;margin-top: -19px;margin-right: 50px;"></button>
	</div>
</div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.reports.assetgraph.viewgraph.add.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
	  <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body">
						 <div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
			<table class="table table-bordered table-condensed table-hover" id="listOfQuestion">
				<thead>
					<tr>
						<th><span th:text="#{graph.name}"></span></th>
						<th><span th:text="#{graph.desc}"></span></th>
						<th><span th:text="#{graph.update.date}"></span></th>
						<th><span th:text="#{graph.action}"></span></th>
					</tr>
				</thead>
				<tbody>
					<tr  th:each="oneGraph : ${graphsInUserWorkspaces}" th:id="${oneGraph.id}">
					<td><a th:href="@{/graph/view/{id}(id=${oneGraph.id})}"><span th:text="${oneGraph.graphName}">Test</span></a></td>
					<td><span th:text="${oneGraph.description}">Test</span></td>
					<td><span th:text="${#dates.format(oneGraph.updatedDate,'dd-MMM-yyyy')}">Test</span></td>
					<td><a th:href="@{/add/edit/{id}(id=${oneGraph.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}"></a>
					<a th:href="@{/graph/view/{id}(id=${oneGraph.id})}" class="fa fa-search" th:title="#{icon.title.view.add}"></a></td>
					</tr>					
				</tbody>
			</table>
		</div>
		</div>
		 <div class="box-footer clearfix">
	                  <div>
				          <a class="btn btn-primary" th:href="@{/add/create}">Create Graph</a>
				        </div>
	                </div>
		</div>
		</div>
		</div>	
	</div>

</body>
</html>