<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="functionalMapData">
		<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
					<form>
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title"><b><span th:text="#{functionalMap.data.for}"></span>&nbsp;<span th:text="${fmName}"></span></b></h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive" data-module="common-data-table">				          	
							<table class="table table-bordered table-condensed table-hover paramListTable">
								<thead>
									<tr>
									    <th><span th:text="#{functionalMap.data.bcmLevel}"></span></th>
										<th><span th:text="#{functionalMap.data.asset}"></span></th>
										<th><span th:text="#{functionalMap.data.response}"></span></th>
										<!--<th><span th:text="#{parameter.theader.displayname}"></span></th> -->
									</tr>
								</thead>
								<tbody >
									<tr th:each="oneRow : ${listOfFMDataBean}" th:id="${oneRow.id}">
										<td><span th:if="${oneRow.bcmLevelBean.bcmLevelName ne null}" th:text="${oneRow.bcmLevelBean.bcmLevelName}">Test</span></td>
										<td><span th:if="${oneRow.assetBean.shortName ne null}" th:text="${oneRow.assetBean.shortName}">Test</span></td>
										<td><span th:if="${oneRow.responseData ne null}" th:text="${oneRow.responseData}">Test</span></td>
									</tr>			
								</tbody>
							</table>
						</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </form>
			    </div>
		 </div>
	</div>
</body>
</html>