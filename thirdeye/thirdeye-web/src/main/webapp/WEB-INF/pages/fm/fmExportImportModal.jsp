<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>Fragments for Functional Maps</title>
</head>
<body>
	<div th:fragment="exportImportFm">
		<div class="modal fade" id="exportImportFmModal" tabindex="-1" role="dialog" aria-labelledby="exportImportFmModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" th:text="#{export.modal.title.fm}">Test</h4>
					</div>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs" style="width: 100%">
							<li th:class="${activeTab == 'exportTab' ? 'active' : ''} ">
								<a href="#selectExport" data-toggle="tab" aria-expanded="true" th:text="#{export.tab}"> Export </a>
							</li>
							<li th:class="${activeTab == 'importTab' ? 'active' : ''} ">
								<a href="#selectImport" data-toggle="tab" aria-expanded="false" th:text="#{import.tab}"> Import </a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" th:classappend="${activeTab == 'exportTab' ? 'active' : ''} " id="selectExport">
								<h3 th:text="#{export.tab.heading}">Test</h3>
								<a class="btn btn-primary" th:href="@{/fm/{id}/exportTemplate(id=${fmId})}" th:text="#{export.button}"></a>
								<h4>Please Note:</h4>
								<ol>
									<li>Only you can upload from this template.</li>
									<li>Please do not make any modifications to the structure of this file.</li>
								</ol>
							</div><!-- /.tab-pane -->
							<div class="tab-pane" th:classappend="${activeTab == 'importTab' ? 'active' : ''} " id="selectImport">
								<h3 th:text="#{import.tab.heading}">Test</h3>
								<form th:action="@{/fm/{id}/import/{qid}?_csrf={_csrf.token}(id=${fmId},qid=${questionnaireId},_csrf.token=${_csrf.token})}" method="post" id="importFmForm" enctype="multipart/form-data">
									<input type="file" name="fileImport" size="20" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
									<input type="submit" value="Upload"/>
									<div th:if="${activeTab == 'importTab'}">
										<div th:if="${#lists.isEmpty(listOfErrors)}">
											<div class="alert alert-success alert-dismissable">
												<h4><i class="icon fa fa-check"></i> Alert!</h4>
												<span th:text="#{import.success}"></span>
											</div>
										</div>
										<div th:if="${not #lists.isEmpty(listOfErrors)}">
											<div th:replace="fm/fmExportImportModal :: importInventoryError"></div>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->
						</div><!-- /.tab-content -->
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="importInventoryError">
		<div class="alert alert-danger alert-dismissable">
			<h4><i class="icon fa fa-ban"></i> Alert!</h4>
			<ul th:each="errorCell : ${listOfErrors}">
				<li><span th:if="${errorCell.row ne null and errorCell.column ne null}">Column: <span th:text="${errorCell.column }"></span> Row: <span th:text="${errorCell.row }"></span></span></li>
				<ul th:each="error : ${errorCell.errors}">				
					<li><span th:text="${error}"></span></li>
				</ul>
				
			</ul>
		</div>
	</div>
</body>
</html>