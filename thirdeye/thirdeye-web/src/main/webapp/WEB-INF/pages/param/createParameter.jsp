<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.parametermanagement.createparameter.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<a class="pull-left fa fa-chevron-left btn btn-default" th:href="@{/parameter/list}" style="margin-left: 10px;"></a>
		<div th:if="${parameterBeanForm.baseParameterId eq null}" th:switch="${parameterBeanForm.id}">
			<span th:case="null" th:text="#{pages.parametermanagement.createparameter.title}"></span>
			<span th:case="*" th:text="#{pages.parametermanagement.updateparameter.title}"></span>
		</div>
		<div th:if="${parameterBeanForm.baseParameterId ne null}" th:switch="${parameterBeanForm.id}">
			<span th:case="null">Create a  Variant Parameter</span>
			<span th:case="*">Update the Variant Parameter</span>
		</div>
	</div>
	<div class="container"  th:fragment="contentContainer">
		<div class="row" data-module="module-createParameter">
			<div class="col-md-12">
				<div class="box box-primary">
					<div th:if="${parameterBeanForm.baseParameterId ne null}"><span th:text="#{parameter.variant.help}"></span></div>
		    		<div class="box-body">
						<form th:action="@{/parameter/save}" method="post" th:object="${parameterBeanForm}">
							<input type="hidden" th:field="*{id}" />
							<input type="hidden" th:field="*{baseParameterId}" />
								<div class="form-group">
									<table class="table table-bordered table-condensed table-hover" id="createParameterTableId">
										<tr>
											<td class="col-md-2 horizontal"><label for="workspaceId" class="control-label" th:text="#{label.workspace}"></label></td>
											<td class="col-md-10">
												<div th:if="${parameterBeanForm.id eq null and parameterBeanForm.baseParameterId eq null}">
													<select th:field="*{workspaceId}" class="form-control select2" id="workspaceId">
														<option th:selected="${parameterBeanForm.workspaceId ne null and (parameterBeanForm.workspaceId eq activeWorkspace.id)}" th:value="${activeWorkspace.id}" th:text="${activeWorkspace.workspaceName}" />
														<option th:selected="${parameterBeanForm.workspaceId eq null or parameterBeanForm.workspaceId eq -1}" value="-1" th:text="#{option.systemlevel}">Independent from workspace</option>
													</select> 
												</div>
												<div th:if="${parameterBeanForm.id ne null or parameterBeanForm.baseParameterId ne null}">
													<span th:if="${parameterBeanForm.workspaceId ne null}" th:text="${activeWorkspace.workspaceName}"></span>
													<span th:if="${parameterBeanForm.workspaceId eq null}" th:text="#{option.systemlevel}"></span>
													<input th:if="${parameterBeanForm.workspaceId ne null}" type="hidden" th:field="*{workspaceId}" id="workspaceId" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="col-md-2 horizontal"><label for="paramType" class="control-label" th:text="#{parameter.label.paramType}"></label><span class="asterisk">*</span></td>
											<td class="col-md-10">
												<div th:if="${parameterBeanForm.baseParameterId eq null and #sets.size(listOfVariants) eq 0 and parameterBeanForm.id eq null}">
													<select th:field="*{paramType}" class="showHideQuestionAndParameter form-control select2" data-type="addButton">
														<option value="select">--Select--</option>
														<div th:each="paramType : ${T(org.birlasoft.thirdeye.constant.ParameterType).values()}" th:remove="tag">
															<option th:if="${paramType ne T(org.birlasoft.thirdeye.constant.ParameterType).TCOA and paramType ne T(org.birlasoft.thirdeye.constant.ParameterType).TCOL}" th:value="${paramType}" th:text="${paramType.value} + ' - ' +${paramType.helpText}"></option>
														</div>
													</select>
													<span th:if="${#fields.hasErrors('paramType')}" th:errors="*{paramType}" class="error_msg">Test</span>
												</div>
												<div th:if="${parameterBeanForm.baseParameterId ne null or #sets.size(listOfVariants) gt 0 or parameterBeanForm.id ne null}">
													<span th:text="${parameterBeanForm.paramType}"></span>
													<input type="hidden" th:field="*{paramType}" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="col-md-2 horizontal"><label for="name" class="control-label" th:text="#{parameter.label.uniqueName}"></label><span class="asterisk">*</span></td>
												<td class="col-md-10">
													<div th:if="${parameterBeanForm.id eq null}">
														<input type="text" th:field="*{name}" class="form-control" maxlength="45"/>
														<span th:if="${#fields.hasErrors('name')}" th:errors="*{name}" class="error_msg">Test</span>
													</div>
													<div th:if="${parameterBeanForm.id ne null}">
														<span th:text="${parameterBeanForm.name}"></span>
														<input type="hidden" th:field="*{name}" />
													</div>
												</td>
										</tr>
										<tr>
											<td class="col-md-2 horizontal"><label for="displayName" class="control-label" th:text="#{parameter.label.displayName}"></label><span class="asterisk">*</span></td>
											<td class="col-md-10">
												<div th:if="${#lists.isEmpty(listOfQuestParam)}">
													<input type="text" th:field="*{displayName}" class="form-control" maxlength="45"/>
													<span th:if="${#fields.hasErrors('displayName')}" th:errors="*{displayName}" class="error_msg">Test</span>
												</div>
												<div th:if="${not #lists.isEmpty(listOfQuestParam) or #paramStatus eq true}">
													<div th:if="${parameterBeanForm.baseParameterId eq null}"><span th:text="${parameterBeanForm.displayName}"></span></div>
													<input type="hidden" th:field="*{displayName}" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="col-md-2 horizontal"><label for="description" class="control-label" th:text="#{parameter.label.description}"></label></td>
											<td class="col-md-10">
												<div th:if="${#lists.isEmpty(listOfQuestParam)}">
													<textarea class="form-control" th:field="*{description}"></textarea>
												</div>
												<div th:if="${not #lists.isEmpty(listOfQuestParam) or paramStatus eq true}">
													<div th:if="${parameterBeanForm.baseParameterId eq null}"><span th:text="${parameterBeanForm.description}"></span></div>
													<input type="hidden" th:field="*{description}" disabled ="disabled" />
												</div>
											</td>
										</tr>
									</table>
									<div th:if="${parameterBeanForm.baseParameterId eq null and #sets.size(listOfVariants) eq 0 and parameterBeanForm.id eq null}" th:switch="${parameterBeanForm.paramType}" class="showButtonDependsOnParamType box-footer" id="questionParamFooter">
										<div th:case="LP">
											<a class="addQuestion btn btn-primary" data-type="buttonClick">Add Question</a>
										</div>
										<div th:case="AP">
											<a class="addParameter btn btn-primary" data-type="buttonClick">Add Parameter</a>
										</div>
										<div th:case="FC">
											<a class="addFunctionalMap btn btn-primary" data-type="buttonClick">Add Functional Map</a>
										</div>
									</div>
									<div class="errorDiv">
										<span th:if="${#fields.hasErrors('error')}" th:errors="*{error}" class="error_msg">Test</span>
									</div>
									<div th:if="${parameterBeanForm.paramType eq 'LP' and selectedQuestions ne null and #sets.size(selectedQuestions) gt 0}"><div th:replace="param/paramFragment :: selectedQuestion"></div></div>
									<div th:if="${parameterBeanForm.paramType eq 'AP' and selectedParameters ne null and #sets.size(selectedParameters) gt 0}"><div th:replace="param/paramFragment :: selectedParameter"></div></div>
									<div th:if="${parameterBeanForm.paramType eq 'FC' and selectedFunctionalMap ne null and selectedFunctionalMap.functionalMapId ne null}"><div th:replace="param/paramFragment :: selectedFunctionalMap"></div></div>
									<input class="btn btn-primary" style="margin-top: 10px" type="submit" value="Done"/>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>		
		<div th:if="${parameterBeanForm.id ne null and parameterBeanForm.baseParameterId eq null and #sets.size(listOfVariants) gt 0}" class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class = "box-header with-border">
							<h3 class="box-title">Variant Details</h3>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-condensed table-hover table-responsive">
									<tr>
										<td><span></span></td>
										<td th:each="oneVariant : ${listOfVariants}"><a th:href="@{/parameter/{id}/edit(id=${oneVariant.id})}" class="text-lowercase" th:text="${oneVariant.uniqueName}"></a></td>
									</tr>
									<tr th:each="onePF : ${listOfChildEntities}">
										<td>
											<a th:if="${onePF.question ne null}" th:href="@{/question/{id}/view(id=${onePF.question.id})}" th:text="${onePF.question.title}"></a>
											<a th:if="${onePF.parameterByChildparameterId ne null}" th:href="@{/parameter/{id}/edit(id=${onePF.parameterByChildparameterId.id})}" th:text="${onePF.parameterByChildparameterId.uniqueName}"></a>
										</td>
										<td th:each="oneVariant : ${listOfVariants}">
											<span th:if="${onePF.question ne null and #maps.containsKey(mapToShow.get(oneVariant.id), onePF.question.id)}" th:text="${mapToShow.get(oneVariant.id).get(onePF.question.id)}"></span>
											<span th:if="${onePF.parameterByChildparameterId ne null and #maps.containsKey(mapToShow.get(oneVariant.id), onePF.parameterByChildparameterId.id)}" th:text="${mapToShow.get(oneVariant.id).get(onePF.parameterByChildparameterId.id)}"></span>
										</td>
									</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" tabindex="-1" data-module="module-parameterFunctionalMapModal" role="dialog" style="display: none;"></div>
		<div class="modal fade" tabindex="-1" data-module="module-parameterLeafQuestionModal" role="dialog" style="display: none;"></div>
		<div class="modal fade" tabindex="-1" data-module="module-parameterAggregateModal" role="dialog" style="display: none;"></div>
		<div th:if="${parameterBeanForm.id ne null}" data-module="module-parameterColoring">
			<div th:attr="data-loadurl='parameter/color/'+${parameterBeanForm.id}"></div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script th:src="@{/static/js/3rdEye/modules/module-createParameter.js}"></script>
       <script th:src="@{/static/js/3rdEye/modules/module-parameterFunctionalMapModal.js}"></script>
       <script th:src="@{/static/js/3rdEye/modules/module-parameterLeafQuestionModal.js}"></script>
       <script th:src="@{/static/js/3rdEye/modules/module-parameterAggregateModal.js}"></script>
       <script th:src="@{/static/js/3rdEye/modules/module-parameterColoring.js}"></script>
        <script th:src="@{/static/js/3rdEye/modules/module-qualityGateCondition.js}"></script>
    </div>
</body>
</html>