<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pvb.edit.title})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	<a class="pull-left fa fa-chevron-left btn btn-default" th:href="@{/parameter/boost/{id}/view(id=${questionnaireId})}" style="margin-left: 10px;"></a>
	<span th:text="#{pvb.edit.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{pvb.edit.subtitle}"></span></div>

	
	
	<div class="container"  th:fragment="contentContainer">
	<div data-module="module-pvb">
		<div class="row">
	       	<div class=" col-lg-8 col-md-8 col-xs-8 col-sm-8">
	        	<div class="box box-primary">
	                <div class="box-body">	
					     <form th:action="@{/parameter/boost/save}" method="post" th:object="${pvbFormBean}">
								<div class="form-group">
									<input type="hidden" th:field="*{qeId}" />
									<input type="hidden" th:field="*{parameterId}" />
									<input type="hidden" th:field="*{assetId}" />
										 <table class="table table-hover">
											<tr>
												<td align="right"><strong><span th:text="'Questionnaire :'"></span></strong></td>
												<td><span th:text="${questionnaireName}"></span></td>											
											</tr>
											<tr>
												<td align="right"><strong><span th:text="'Parameter :'"></span></strong></td>
												<td><span th:text="${pvbFormBean.parameterName}"></span></td>											
											</tr>
											<tr>
												<td align="right"><strong><span th:text="'Asset :'"></span></strong></td>
												<td><span th:text="${assetName}"></span></td>											
											</tr>																					
											<tr>
												<td align="right"><strong><span th:text="'Boost Percentage'"></span></strong></td>
												<td><input type="text" th:value="${pvbFormBean.boostPercentage}" th:field="*{boostPercentage}" ></input>&nbsp;&nbsp;&nbsp;<i><span th:if="${success ne null}" style="color:green;" th:text="${success}"></span></i></td>											
											</tr>
											<tr th:if="${error ne null}"><td></td><td><i><span style="color:red;" th:text="${error}"></span></i></td></tr>																															
										</table>
								</div>					
					<div class="box-footer clearfix" align="center">
				       <input type="submit" class="btn btn-primary" th:title="'Save'" th:value="'Save'"></input>							
					</div>
						</form>	
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>