<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.parametermanagement.viewparameter.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.parametermanagement.viewparameter.title}"></span>
	<div  id="new" data-module ="module-showHelp">
			<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;margin-top: -19px;margin-right: 50px;"></button>
	</div>
</div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.parametermanagement.viewparameter.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
        <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary" data-module="module-parameterList">
	                <div class="box-body">
						<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover">
								<thead>
									<tr>
										<th><span th:text="#{parameter.theader.uniquename}"></span></th>
										<th><span th:text="#{parameter.theader.description}"></span></th>
										<th><span th:text="#{parameter.theader.baseparameter}"></span></th>
										<th><span th:text="#{parameter.theader.displayname}"></span></th>
										<th><span th:text="#{tableheader.actions}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneRow : ${listOfParameter}" th:id="${oneRow.id}">
										<td><span th:text="${oneRow.uniqueName}">Test</span></td>
										<td><span th:text="${oneRow.description}">Test</span></td>
										<td><span th:if="${oneRow.parameter ne null}" th:text="${oneRow.parameter.uniqueName}">Test</span></td>
										<td> 
											<span th:if="${oneRow.workspace eq null}" th:text="${oneRow.displayName}">System Level</span>
											<span th:if="${oneRow.workspace ne null}" th:text="${oneRow.displayName} + ' - *'">Workspace</span>
										</td>
										<td>
											<a th:if="${not #sets.contains(setOfClosedParam,oneRow)}" th:href="@{/parameter/{id}/edit(id=${oneRow.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}"></a>
											<a th:if="${oneRow.parameter eq null and oneRow.type eq T(org.birlasoft.thirdeye.constant.ParameterType).AP.name() or oneRow.type eq T(org.birlasoft.thirdeye.constant.ParameterType).LP.name()}" th:href="@{/parameter/{id}/createVariant(id=${oneRow.id})}" class="fa fa-plus-square" title="Create Variant"></a>
											<a th:if="${oneRow.type eq T(org.birlasoft.thirdeye.constant.ParameterType).AP.name() or oneRow.type eq T(org.birlasoft.thirdeye.constant.ParameterType).LP.name()}" data-type="viewParameterTree" th:attr="data-parameterid=${oneRow.id}" class="fa fa-tree" th:title="#{icon.title.treeview}" style="cursor: pointer;"></a>
											<a th:if="${not #maps.containsKey(mapOfParams, oneRow.id) and not #sets.contains(setOfExtendedBaseParams, oneRow.id)}" data-type="deleteParameter" th:attr="data-parameterid=${oneRow.id}" class="fa fa-trash-o" th:title="#{icon.title.delete}" style="cursor: pointer;"></a>
										</td>
									</tr>			
								</tbody>
							</table>
						</div>
						<div>	
							<span th:text="#{parameter.asterisk.workspace.help}"></span>
						</div>
					</div>
					<div class="box-footer clearfix">
	                  <div>
				          <a sec:authorize="@securityService.hasPermission('PARAMETER_MODIFY')" class="btn btn-primary" th:href="@{/parameter/create}">Create Parameter</a>
				        </div>
	                </div>
	                <!-- Div for parameter tree view -->
	                <div class="modal fade paramTreeModalModule" data-module="module-paramTreeModal"></div>
				</div>
			</div>
		</div>	
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-parameterList.js}"></script>
       <script	th:src="@{/static/js/3rdEye/modules/module-paramTreeModal.js}"></script>
    </div>
	</body>
	</html>