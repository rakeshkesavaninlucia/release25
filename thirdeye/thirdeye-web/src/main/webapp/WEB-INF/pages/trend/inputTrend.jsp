<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = 'trend')">
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.trend.title}"></span>
		</div>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary">
				
					<div class="box-body">
							<form role="form"  data-module = "module-inputTrend">
							<div class="row">
									<div class="col-sm-12">
										<label>Select Parameters</label>
										<select class="form-control select2 param" >
										 <option value="-1">--Select--</option>
										<option th:each = "oneParam : ${listParameterAPLP}" th:value="${oneParam.id}" th:text="${oneParam.displayName}"/>
										</select>
									</div>
									</div>
									<div>
								<table class="table table-bordered table-condensed table-hover row" id = "inputTrend" style="margin-left: 0px;">
								<thead>
									<tr>
										<th><span th:text="SelectDate"></span></th>
										<th><span th:text="Value"></span></th>
										<th><span th:text="Action"></span></th>
										
									</tr>
								</thead>
								<tbody>
								<tr th:fragment ="addDateValue">
								<td> <input type="text" class="form-control datepicker date"></input></td>
									<td ><input type="number" class="form-control value"></input></td>
										<td><a title ="Add Row" class="fa fa-plus-square" data-type ="addRow" ></a></td>
										<td><a title ="Save" class="fa fa-check-square-o" data-type ="saveRow" ></a></td>
										</tr>
										</tbody>
								</table>
								</div>
							</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<div th:fragment="scriptsContainer"  th:remove="tag">
       <script th:src="@{/static/js/3rdEye/modules/module-inputTrend.js}"></script>
       </div>
</body>
</html>
