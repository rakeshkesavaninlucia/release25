<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventoryasset.nav })">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<span th:text="AssetAudittrail"></span>
	</div>

	<div th:fragment="pageSubTitle">
		<span th:text="AssetAuditTrail"></span>
	</div>

	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div data-module="module-viewAssetAuditTrail">
				<div class="col-md-4">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title" th:text="ReportFilteroption"></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div id="box-body1" class="box-body"
							style="width: 100%; height: 500px; overflow-y: scroll;">
								<div class="form-group">
									<table class="table table-bordered table-condensed table-hover">
									<tr>
									<th style="width: 200px; text-decoration: underline;">
												Asset template</th>
									</tr>
										<tr th:each = "oneAssetTemplate : ${ listOfAssetTemplateInActiveWorkspace}" class="userName">
										<td><input type = "checkbox" class = "assetTemplateChecked" name ="assetTemplateChecked" th:value = "${oneAssetTemplate.id}"/> <span th:text = "${oneAssetTemplate.assetTemplateName}"></span></td>
										</tr>
										<tr>
											<th style="text-decoration: underline;">
												Date Range</th>
										</tr>
										<tr>
											<th>From:</th>
										</tr>
										<tr>
										<td><input type="text" class="form-control datepicker fromDate" /></td>
										</tr>
										<tr>
											<th>To:</th>
										</tr>
										<tr>
											<td><input type="text" class="form-control datepicker toDate " /></td>
										</tr>
										<tr>
											<th style="width: 150px; text-decoration: underline;">
												UserName</th>
										</tr>
										<tr th:each = "oneUser : ${ listOfUserInActiveWorkspace}" class="userName">
										<td><input type = "checkbox" class = "userChecked" name ="userChecked" th:value = "${oneUser.firstName} + ' ' + ${oneUser.lastName} "/> <span th:text = "${oneUser.firstName} + ' '+ ${oneUser.lastName}"></span></td>
										</tr>
										<tr>
											<th style="width: 150px; text-decoration: underline;">
												Events</th>
										</tr>
										<tr th:each = "event : ${T(org.birlasoft.thirdeye.constant.AssetEvent).values()}">
										<td><input type = "checkbox" class = "eventChecked" name ="eventChecked"  th:value = "${event.event}"/><span th:text = "${event.event}"></span></td>
										</tr>
									</table> 
									<input class="btn btn-primary" type="submit"
										value="Generate Report" data-type="generateReport"/>
								</div>
							
						</div>


					</div>
				</div>
	<div  th:fragment="assetAuditTrailData" class = "assetauditTrail">
				<div class="col-md-8">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title" th:text=" AssetAuditTrail"></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body1" style="width: 100%; height:500px; overflow-y: scroll; overflow-x: scroll;">
							<div data-module="common-data-table">
								<table class="table table-bordered table-condensed table-hover"
									id="assetTypeView" th:if="${not #lists.isEmpty(listOfAssetAuditTrail)}">
									<thead>
										<tr>
											<th><span th:text="AssetName"></span></th>
											<th><span th:text="UserName"></span></th>
											<th><span th:text="DateTime"></span></th>
											<th><span th:text="EventName"></span></th>
											<th><span th:text="EventDescription"></span></th>
											
										</tr>
									</thead>
									<tbody>
										<tr th:each="oneAssetType : ${listOfAssetAuditTrail}">
											<td><span th:text="${oneAssetType.assetName}">Test</span></td>
											<td><span th:text="${oneAssetType.createdBy}">Test</span></td>
											<td><span th:text="${oneAssetType.createdDate}">Test</span></td>
											<td><span th:text="${oneAssetType.eventName}">Test</span></td>
											<td><span th:text="${oneAssetType.eventDescription}">Test</span></td>
										</tr>	
									</tbody>
								</table>
							<div style="margin-top: 150px;margin-left: 200px;" th:fragment="assetFragment" th:if="${#lists.isEmpty(listOfAssetAuditTrail)}"> 
				<span><b style ="font-size:17px;"> NoDataAvailable</b></span>
				</div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
			
				
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
		<script
			th:src="@{/static/js/3rdEye/modules/module-viewAssetAuditTrail.js}"></script>
	</div>
</body>
</html>