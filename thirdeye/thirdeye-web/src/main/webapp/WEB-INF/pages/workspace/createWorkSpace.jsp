<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.workspacemanagement.createworkspace.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	<a class="fa fa-chevron-left btn btn-default" style="margin-left: 10px;" href="/thirdeye-web/workspace/list"></a>
	<span th:text="#{pages.workspacemanagement.createworkspace.title}"></span>
	
	<div  id="new" data-module ="module-showHelp">
		<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;margin-top: -19px;margin-right: 50px;"></button>
	</div>
	</div>
	<div class="container"  th:fragment="contentContainer">
		<div th:replace="workspace/editWorkspaceFragments :: createWorkSpaceForm"></div>		
	</div>
</body>
</html>