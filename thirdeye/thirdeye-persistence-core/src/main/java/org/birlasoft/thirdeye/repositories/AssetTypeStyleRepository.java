package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.AssetTypeStyle;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssetTypeStyleRepository extends JpaRepository<AssetTypeStyle, Serializable> {

	public AssetTypeStyle findByWorkspaceAndAssetType(Workspace workspace, AssetType assetType);
	public AssetTypeStyle findByWorkspaceIsNullAndAssetType(AssetType assetType);

}
