package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for Report.
 * @author sunil1.gupta
 */
public interface ReportRepository extends JpaRepository<Report, Serializable> {
	
    
	/**
	 * @param activeWorkSpaceForUser
	 * @param currentUser
	 * @return
	 */
	List<Report> findByWorkspaceAndUserByCreatedBy(Workspace activeWorkSpaceForUser, User currentUser);
	
	
	/**
	 * @param workspaces
	 * @return
	 */
	List<Report> findByWorkspaceIn(Set<Workspace> workspaces);

	/**
	 * @param activeWorkSpaceForUser
	 * @param b
	 * @return
	 */
	Report findByWorkspaceAndStatus(Workspace activeWorkSpaceForUser, boolean b);
	
	/** 
	 * @param reportName
	 * @return
	 */
	Report findByreportName(String reportName);
	
	
	/**
	 * @param status
	 */
	@Modifying
	@Query("Update Report re SET re.status=false WHERE re.status =?1")
	public void updateStatus(boolean status);
	
	public List<Report> findById(Integer reportId);
	
	@Override
	public List<Report> findAll();
	
	
	public List<Report> findByUserByCreatedBy(User creatorId);
	
	@Query("select re.userByCreatedBy from Report re WHERE re.id=?1")
	public User findReportOwner(Integer reportId);
	
	@Modifying
	@Query("Update Report re SET re.deletedStatus=true WHERE re.id =?1")
	public void setDeleteStatus(Integer reportId);
	
}
