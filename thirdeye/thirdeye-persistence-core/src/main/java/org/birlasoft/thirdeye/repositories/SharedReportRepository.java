package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import org.birlasoft.thirdeye.entity.SharedReport;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SharedReportRepository extends JpaRepository<SharedReport, Serializable>{

	List<SharedReport> findByUserId(Integer userId);
	
	List<SharedReport> findById(Integer sharedReportId);
	
	public SharedReport findByWorkspaceId(Integer workspaceid);
	
	/**
	 * @param reportId
	 * @param workspaceId
	 * @return
	 */
	public List<SharedReport> findByReportIdAndWorkspaceId(Integer reportId,Integer workspaceId);


}


