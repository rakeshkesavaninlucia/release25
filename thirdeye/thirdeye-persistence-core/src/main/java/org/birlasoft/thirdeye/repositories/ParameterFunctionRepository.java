package org.birlasoft.thirdeye.repositories;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for parameter function.
 */
public interface ParameterFunctionRepository extends JpaRepository<ParameterFunction, Integer> {
	/**
     * Find list of parameter function by parameter parent id. 
     * @param parameter
     * @return {@code List<ParameterFunction>}
     */
	public List<ParameterFunction> findByParameterByParentParameterId(Parameter parameter);
	
	 /**
     * Find by mandatory question.
     * @param isMandatoryQuestion
     * @return {@code List<ParameterFunction>}
     */
	public List<ParameterFunction> findByMandatoryQuestion(boolean isMandatoryQuestion);
	
	/**
	 * This method will give Invalid derived query error
	 * this is just the spring data validation error and not the compilation error
	 * @param parameterId
	 * @return
	 */
	@Procedure(name="getParameterTree", outputParameterName="rv")
	public String getParameterTree(@Param("inParam1") Integer parameterId);

	/**
	 * Get parameter function by parent parameter
	 * @param p
	 * @return
	 */
	public List<ParameterFunction> findByParameterByParentParameterIdAndParameterByChildparameterIdIsNull(Parameter p);
	/**
     * Find list of chind parameter from parameter function by list of parameter parent id. 
     * @param parameter
     * @return {@code List<ParameterFunction>}
     */
	@Query("SELECT pf.parameterByChildparameterId FROM ParameterFunction pf where pf.parameterByParentParameterId in (:params) ")
	public Set<Parameter> findByParameterByParentParameterIdIn(@Param("params") Set<Parameter> parameters);
	/**
	 * Fetch parameter function which has parameter as a child parameter.
	 * @param param
	 * @return {@code Set<ParameterFunction>}
	 */
	public Set<ParameterFunction> findByParameterByChildparameterId(Parameter param);
}
