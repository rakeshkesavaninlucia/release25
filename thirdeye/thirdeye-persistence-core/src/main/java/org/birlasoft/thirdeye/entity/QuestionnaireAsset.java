package org.birlasoft.thirdeye.entity;

// Generated Jul 24, 2015 2:05:49 PM by Hibernate Tools 4.3.1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * QuestionnaireAsset generated by hbm2java
 */
@Entity
@Table(name = "questionnaire_asset", uniqueConstraints = @UniqueConstraint(columnNames = {
		"questionnaireId", "assetId" }))
@NamedEntityGraphs(value = {
		@NamedEntityGraph(name = "QuestionnaireAsset.asset", attributeNodes = 				
										@NamedAttributeNode(value = "asset", subgraph = "assetTemplate"),
										subgraphs = @NamedSubgraph(name = "assetTemplate", attributeNodes = 
										{@NamedAttributeNode("assetTemplate"), @NamedAttributeNode("assetDatas")})
					), 
		})
public class QuestionnaireAsset implements java.io.Serializable {

	private static final long serialVersionUID = -3502594968990282828L;
	private Integer id;
	private Asset asset;
	private Questionnaire questionnaire;
	private Set<QuestionnaireQuestion> questionnaireQuestions = new HashSet<QuestionnaireQuestion>(
			0);

	public QuestionnaireAsset() {
	}

	public QuestionnaireAsset(Asset asset, Questionnaire questionnaire) {
		this.asset = asset;
		this.questionnaire = questionnaire;
	}

	public QuestionnaireAsset(Asset asset, Questionnaire questionnaire,
			Set<QuestionnaireQuestion> questionnaireQuestions) {
		this.asset = asset;
		this.questionnaire = questionnaire;
		this.questionnaireQuestions = questionnaireQuestions;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "assetId", nullable = false)
	public Asset getAsset() {
		return this.asset;
	}

	public void setAsset(Asset asset) {
		this.asset = asset;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "questionnaireId", nullable = false)
	public Questionnaire getQuestionnaire() {
		return this.questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questionnaireAsset")
	public Set<QuestionnaireQuestion> getQuestionnaireQuestions() {
		return this.questionnaireQuestions;
	}

	public void setQuestionnaireQuestions(
			Set<QuestionnaireQuestion> questionnaireQuestions) {
		this.questionnaireQuestions = questionnaireQuestions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionnaireAsset other = (QuestionnaireAsset) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
	
}
