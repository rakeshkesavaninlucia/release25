package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.ExportLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for export log .
 */
public interface ExportLogRepository extends JpaRepository<ExportLog, Serializable> {

	ExportLog findByHash(String hash);	

}
