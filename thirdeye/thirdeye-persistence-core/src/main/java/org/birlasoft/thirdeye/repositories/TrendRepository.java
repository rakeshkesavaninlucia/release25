package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.birlasoft.thirdeye.entity.InputTrend;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for inputTrend.
 * @author sunil1.gupta
 *
 */
public interface TrendRepository extends JpaRepository<InputTrend, Serializable> {
	
	@Query("select t from InputTrend t where t.parameter = (:parameter) AND t.workspace = (:workspace) AND t.startDate >= (:from) AND t.startDate <=(:to)")
	public List<InputTrend> findByParameterAndWorkspaceAndStartDateBetween(@Param("parameter")Parameter parameter,@Param("workspace")Workspace workspace,@Param("from")Date from,@Param("to") Date to);
}
