package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository interface for Benchmark Item score.
 * @author samar.gupta
 */
public interface BenchmarkItemScoreRepository extends JpaRepository<BenchmarkItemScore, Serializable> {
	
	/**
	 * Find item score by benchmark item and current date.
	 * @param benchmarkItem
	 * @param currentDate
	 * @return {@code List<BenchmarkItemScore>}
	 */
	@Query("SELECT bis FROM BenchmarkItemScore bis WHERE bis.benchmarkItem = ?1 AND ?2 BETWEEN bis.startYear AND bis.endYear")
	public List<BenchmarkItemScore> findByBenchmarkItemAndCurrentDate(BenchmarkItem benchmarkItem, Date currentDate);
	/**
	 * Find list of benchmarkItemScore by benchmarkItemId
	 * @param benchmarkItemId
	 * @return {@code List<BenchmarkItemScore>}
	 */
	public Set<BenchmarkItemScore> findByBenchmarkItemId(Integer benchmarkItemId);
	
	/**
	 * Find item score by benchmark item and current date.
	 * @param benchmarkItem
	 * @param currentDate
	 * @return {@code BenchmarkItemScore}
	 */
	@Query("SELECT bis FROM BenchmarkItemScore bis WHERE bis.benchmarkItem = ?1 AND ?2 BETWEEN bis.startYear AND bis.endYear")
	public BenchmarkItemScore findBISByBenchmarkItemAndCurrentDate(BenchmarkItem benchmarkItem, Date currentDate);

	
}
