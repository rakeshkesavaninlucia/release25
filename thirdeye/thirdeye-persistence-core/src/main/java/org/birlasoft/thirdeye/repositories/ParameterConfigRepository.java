package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for ParameterConfig.
 * @author samar.gupta
 */
public interface ParameterConfigRepository extends JpaRepository<ParameterConfig, Serializable> {
	/**
	 * Get {@code List} of {@link ParameterConfig} by {@link Parameter}
	 * @param parameter
	 * @return
	 */
	public List<ParameterConfig> findByParameter(Parameter parameter);
	/**
	 * Get {@code List} of {@link ParameterConfig} by {@code set} of {@link Parameter}
	 * @param paramSet
	 * @return
	 */
	public List<ParameterConfig> findByParameterIn(Set<Parameter> paramSet);	
}
