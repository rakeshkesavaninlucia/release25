package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.StyleMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StyleMasterRepository extends JpaRepository<StyleMaster, Serializable> {
	
	public StyleMaster findTopByActiveStatus(Boolean activeStatus);
	
	public StyleMaster findByBcmLevelIdAndActiveStatus(Integer bcmLevelId, Boolean status);
	
	

}
