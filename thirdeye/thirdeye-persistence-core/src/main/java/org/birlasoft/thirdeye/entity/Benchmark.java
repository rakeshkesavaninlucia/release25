package org.birlasoft.thirdeye.entity;

// Generated Jan 9, 2017 5:54:23 PM by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Benchmark generated by hbm2java
 */
@Entity
@Table(name = "benchmark", uniqueConstraints = @UniqueConstraint(columnNames = {
		"name", "workspaceId" }))
public class Benchmark implements java.io.Serializable {

	private static final long serialVersionUID = 9079799876969095145L;
	private Integer id;
	private User userByCreatedBy;
	private User userByUpdatedBy;
	private Workspace workspace;
	private String name;
	private Date createdDate;
	private Date updatedDate;
	private Set<Question> questions = new HashSet<Question>(0);
	private Set<BenchmarkItem> benchmarkItems = new HashSet<BenchmarkItem>(0);

	public Benchmark() {
	}

	public Benchmark(User userByCreatedBy, User userByUpdatedBy, String name,
			Date createdDate, Date updatedDate) {
		this.userByCreatedBy = userByCreatedBy;
		this.userByUpdatedBy = userByUpdatedBy;
		this.name = name;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public Benchmark(User userByCreatedBy, User userByUpdatedBy,
			Workspace workspace, String name, Date createdDate,
			Date updatedDate, Set<Question> questions,
			Set<BenchmarkItem> benchmarkItems) {
		this.userByCreatedBy = userByCreatedBy;
		this.userByUpdatedBy = userByUpdatedBy;
		this.workspace = workspace;
		this.name = name;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.questions = questions;
		this.benchmarkItems = benchmarkItems;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdBy", nullable = false)
	public User getUserByCreatedBy() {
		return this.userByCreatedBy;
	}

	public void setUserByCreatedBy(User userByCreatedBy) {
		this.userByCreatedBy = userByCreatedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updatedBy", nullable = false)
	public User getUserByUpdatedBy() {
		return this.userByUpdatedBy;
	}

	public void setUserByUpdatedBy(User userByUpdatedBy) {
		this.userByUpdatedBy = userByUpdatedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workspaceId")
	public Workspace getWorkspace() {
		return this.workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdDate", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updatedDate", nullable = false, length = 19)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "benchmark")
	public Set<Question> getQuestions() {
		return this.questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "benchmark")
	public Set<BenchmarkItem> getBenchmarkItems() {
		return this.benchmarkItems;
	}

	public void setBenchmarkItems(Set<BenchmarkItem> benchmarkItems) {
		this.benchmarkItems = benchmarkItems;
	}

}
