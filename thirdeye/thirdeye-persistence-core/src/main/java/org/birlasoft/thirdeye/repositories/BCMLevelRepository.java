package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BCMLevelRepository extends JpaRepository<BcmLevel, Serializable> {
	
	public List<BcmLevel> findByBcm(Bcm bcm); 
	public List<BcmLevel> findByBcmLevel(BcmLevel parentBcmLevel);
	public BcmLevel findTopByBcmAndLevelNumberOrderBySequenceNumberDesc(Bcm bcm, Integer levelNumber);
	public BcmLevel findTopByBcmLevelOrderBySequenceNumberDesc(BcmLevel bcmLevel);
	public List<BcmLevel> findByBcmAndLevelNumberIn(Bcm bcm, Set<Integer> levelNumber);
	public List<BcmLevel> findByBcmAndLevelNumberIn(List<Bcm> bcm,Integer levelNumber);
	public List<BcmLevel> findByBcmLevelIdIn(Integer[] parentLevelId);
}
