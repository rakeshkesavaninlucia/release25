package org.birlasoft.thirdeye.constant.aid;

public enum AIDBlockType {

	BLOCK_1("Put 6 pieces of info arranged neatly in a table"),
	BLOCK_2("Put 1 big paragraph"),
	BLOCK_3("Put upto 4 key values"),
	BLOCK_4("Put upto 2 key values with more space for the values");
	
	private String blockDescription;

	AIDBlockType(String blockDescription){
		this.blockDescription = blockDescription;
	}

	public String getBlockDescription() {
		return blockDescription;
	}
	
	public String getViewFragmentName(){
		return this.name() + "_view";
	}
	
	public String getEditFragmentName(){
		return this.name() + "_edit";
	}
	
	public String getViewName(){
		return "aid/blocks";
	}
	
	public String getCompleteFragmentForView(String modelAttributeName){
		return getViewName() + "/" + this.name() + " :: " + getViewFragmentName() + "(blockForDisplay=${"+modelAttributeName+"})";
	}
	
	public String getCompleteFragmentForEdit(String modelAttributeName){
		return getViewName() + "/" + this.name() + " :: " + getEditFragmentName() + "(blockForDisplay=${"+modelAttributeName+"})";
	}
}
