package org.birlasoft.thirdeye.constant;

public enum ParameterSelectorType {
	
	QUESTIONNAIRE_PARAMETER_MODE ("questionnaireParameter");
	
	private String value;
	
	private ParameterSelectorType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}

