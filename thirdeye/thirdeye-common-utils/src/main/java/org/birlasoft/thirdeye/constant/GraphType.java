package org.birlasoft.thirdeye.constant;

/**
 * Enum for graph type (Parent Child or Network).
 */
public enum GraphType {
	PC("Hierarchical"), NW("Network");
	
	private final String value;       

    GraphType(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}
}
