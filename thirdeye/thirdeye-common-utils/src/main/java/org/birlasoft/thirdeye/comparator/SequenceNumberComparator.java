package org.birlasoft.thirdeye.comparator;

import java.util.Comparator;

/**
 * Comparator for sequence number in asset template column to compare objects as sequence number.
 * @author samar.gupta
 *
 */
public class SequenceNumberComparator implements Comparator<Sequenced> {
	
	@Override
	public int compare(Sequenced assetTemplateColumn1, Sequenced assetTemplateColumn2){
		int col1SeqNo = assetTemplateColumn1.getSequenceNumber();
		int col2SeqNo = assetTemplateColumn2.getSequenceNumber();
 
		if (col1SeqNo > col2SeqNo) {
			return 1;
		} else if (col1SeqNo < col2SeqNo) {
			return -1;
		} else {
			return 0;
		}
	}
}
