/**
 * 
 */
package org.birlasoft.thirdeye.constant;

/**
 * @author dhruv.sood
 * ENUM for user roles
 */
public enum RoleType {
	ROLE_APP_ADMIN("ROLE_APP_ADMIN"), 
	ROLE_APP_USER("ROLE_APP_USER"), 
	ROLE_CONSULTANT("ROLE_CONSULTANT"),
	ROLE_CUSTOMER("ROLE_CUSTOMER");
	
	private final String role;       

    private RoleType(String role) {
        this.role = role;
    }

	public String getRole() {
		return role;
	}

}
