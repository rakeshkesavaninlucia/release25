package org.birlasoft.thirdeye.constant;

/**
 * Enum to describe Questionnaire status
 */
public enum QuestionnaireStatusType {

	EDITABLE ("Editable"),
	PUBLISHED ("Published"),
	REVIEWED ("Reviewed"),
	CLOSED ("Closed");
	
	private String status;
	
	QuestionnaireStatusType(String status){
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
}
