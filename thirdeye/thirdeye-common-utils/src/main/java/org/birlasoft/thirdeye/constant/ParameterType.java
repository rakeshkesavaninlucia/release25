package org.birlasoft.thirdeye.constant;


/**
 * Parameter Type Enum for parameter.
 * @author samar.gupta
 *
 */
public enum ParameterType {
	AP("Aggregate Parameter", "Parameter of Parameters"), LP("Leaf Parameter", "Parameter of Questions"), FC("Functional Coverage", "Parameter of Functional Map"), TCOA("TCO-Aggregate", "Total Cost Ownership-Aggregate"), TCOL("TCO-Leaf", "Total Cost Ownership-Leaf");
	
	private final String value;     
	private final String helpText;

    ParameterType(String value, String helpText) {
        this.value = value;
        this.helpText = helpText;
    }

	public String getValue() {
		return value;
	}

	public String getHelpText() {
		return helpText;
	}
}
